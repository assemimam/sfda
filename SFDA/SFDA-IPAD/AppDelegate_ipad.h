//
//  AppDelegate.h
//  sfda-ipad
//
//  Created by Assem Imam on 5/20/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


@interface AppDelegate_ipad : AppDelegate
-(void)setCurrentViewController:(UIViewController*)controller;
-(void)ShowLoading:(BOOL)show;
@end
