//
//  AppDelegate.m
//  sfda-ipad
//
//  Created by Assem Imam on 5/20/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "AppDelegate_ipad.h"
#import "Reachability.h"
#import "Language.h"
#import "ipadMasterViewController.h"
@interface AppDelegate_ipad()
{
    
}
@property(nonatomic,retain)ipadMasterViewController*CurrentViewCntroller;
@end

@implementation AppDelegate_ipad
@synthesize CurrentViewCntroller;
-(void)setCurrentViewController:(UIViewController*)controller{
    @try {
        self.CurrentViewCntroller =(ipadMasterViewController*) controller;
    }
    @catch (NSException *exception) {
        
    }
}
- (void) runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat;
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: 2.0 * M_PI  ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = CGFLOAT_MAX;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

-(void)ShowLoading2:(NSString*)show
{
    
    vLoading.frame = CGRectMake(CurrentViewCntroller.ContainerView.frame.size.width/2 - vLoading.frame.size.width/2, CurrentViewCntroller.ContainerView.frame.size.height/2 - vLoading.frame.size.height/2, vLoading.frame.size.width, vLoading.frame.size.height);
    vLoading.tag= LOADING_VIEW_TAG;
    
    
    [self runSpinAnimationOnView:vLoading duration:0.5 rotations:360 repeat:1000000000000000000];
    
    if ([CurrentViewCntroller.ContainerView viewWithTag:LOADING_VIEW_TAG]) {
        if ([show boolValue]) {
            [vLoading setHidden:NO];
            
            [CurrentViewCntroller.ContainerView bringSubviewToFront:vLoading];
        }
        else
        {
            
            [vLoading setHidden:YES];
            
            [CurrentViewCntroller.ContainerView sendSubviewToBack:vLoading];
        }
    }
    else{
        
        //[CurrentViewCntroller.ContainerView addSubview:vLoading];
        UIWindow* mainWindow = [[UIApplication sharedApplication] keyWindow];
        [mainWindow addSubview:vLoading];
        if ([show boolValue])
        {
            [vLoading setHidden:NO];
            
            UIWindow* mainWindow = [[UIApplication sharedApplication] keyWindow];
            [mainWindow bringSubviewToFront:vLoading];
            
           // [CurrentViewCntroller.ContainerView bringSubviewToFront:vLoading];
          
        }
        else
        {
            [vLoading setHidden:YES];
            
            
            UIWindow* mainWindow = [[UIApplication sharedApplication] keyWindow];
            [mainWindow sendSubviewToBack:vLoading];
            
           // [CurrentViewCntroller.ContainerView sendSubviewToBack:vLoading];
        }
    }
    

}

-(NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    return UIInterfaceOrientationMaskAll;
}
-(void)ShowLoading:(BOOL)show
{
    [self performSelectorOnMainThread:@selector(ShowLoading2:) withObject:[NSString stringWithFormat:@"%i",show] waitUntilDone:YES];
}
@end
