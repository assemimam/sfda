//
//  BarcodeScannerViewController.m
//  SFDA
//
//  Created by Assem Imam on 5/7/14.
//  Copyright (c) 2014 NUITEX. All rights reserved.
//

#import "ipadBarcodeScannerViewController.h"

@interface ipadBarcodeScannerViewController ()

@end

@implementation ipadBarcodeScannerViewController
@synthesize delegate,readerView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    [[UIApplication sharedApplication]setStatusBarHidden:YES];
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewDidUnload
{
    [self cleanup];
    [super viewDidUnload];
}
- (void) viewDidAppear: (BOOL) animated
{
    

    // run the reader when the view is visible
    [self.readerView start];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
   [readerView start];
    
    
      readerView.readerDelegate = self;
    
    [self rotateZbarView:readerView];
    
    
   [readerView.scanner setSymbology: ZBAR_I25
                       config:ZBAR_QRCODE|ZBAR_CODE128| ZBAR_CODE93| ZBAR_CODE39| ZBAR_I25|ZBAR_DATABAR| ZBAR_DATABAR_EXP|ZBAR_EAN13| ZBAR_EAN8|ZBAR_EAN2| ZBAR_EAN5| ZBAR_COMPOSITE|ZBAR_UPCA| ZBAR_UPCE|ZBAR_ISBN13| ZBAR_ISBN10 to: 0];
}
- (void) cleanup
{
 
    readerView.readerDelegate = nil;
    readerView = nil;
    readerView = nil;

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) viewWillDisappear: (BOOL) animated
{
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [self.readerView stop];
}
- (void) readerView: (ZBarReaderView*) view
     didReadSymbols: (ZBarSymbolSet*) syms
          fromImage: (UIImage*) img
{
    @try {
        for(ZBarSymbol *sym in syms) {
            
            if (self.delegate) {
                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                [self.delegate didFinishPickingBarcode:sym.data ForBarcodeImage:img];
              
            }
            
            break;
        }
    }
    @catch (NSException *exception) {
        
    }
}
- (IBAction)CancelAction:(id)sender {
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [self dismissModalViewControllerAnimated:YES];
}

-(void)rotateZbarView:(UIView *)view
{
    view.transform = CGAffineTransformMakeRotation(M_PI_2);
}



@end
