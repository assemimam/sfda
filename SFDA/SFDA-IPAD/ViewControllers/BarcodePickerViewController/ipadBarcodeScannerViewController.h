//
//  BarcodeScannerViewController.h
//  SFDA
//
//  Created by Assem Imam on 5/7/14.
//  Copyright (c) 2014 NUITEX. All rights reserved.
//
@protocol BarCodeScannerDelegate <NSObject>
@optional
-(void)didFinishPickingBarcode:(id)barcode ForBarcodeImage:(UIImage*)barcodeImage;
@end
#import <UIKit/UIKit.h>
#import "ZBarReaderView.h"
@interface ipadBarcodeScannerViewController : UIViewController<ZBarReaderViewDelegate>
{
  
    __weak IBOutlet UIButton *CancelButton;
   
}
@property (weak, nonatomic) IBOutlet ZBarReaderView *readerView;

@property(assign) id<BarCodeScannerDelegate> delegate;
- (IBAction)CancelAction:(id)sender;
@end
