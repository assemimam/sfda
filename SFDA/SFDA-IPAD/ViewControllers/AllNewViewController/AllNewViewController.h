//
//  AllNewViewController.h
//  sfda-ipad
//
//  Created by Assem Imam on 6/4/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ipadMasterViewController.h"


@interface AllNewViewController :ipadMasterViewController
{
    __weak IBOutlet UIView *HeaderView;
    __weak IBOutlet UISegmentedControl *SegmentControl;
}
@property(assign)id<AllNewsTyesDelegate> delegate;
- (IBAction)SegmentAction:(UISegmentedControl *)sender;
- (IBAction)MenuButtonAction2:(UIButton *)sender ;
@end
