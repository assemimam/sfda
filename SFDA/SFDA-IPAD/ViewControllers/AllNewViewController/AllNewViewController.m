//
//  AllNewViewController.m
//  sfda-ipad
//
//  Created by Assem Imam on 6/4/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "AllNewViewController.h"
#import "NewsView.h"
#import "ipadNewsViewController.h"

#define STYLE_SEGMENT for (int i=0; i<[SegmentControl.subviews count]; i++)\
{\
if ([[SegmentControl.subviews objectAtIndex:i]isSelected] )\
{\
UIColor *tintcolor=[UIColor colorWithRed:92/255.0f  green:98/255.0f blue:106/255.0f alpha:1];\
[[SegmentControl.subviews objectAtIndex:i] setTintColor:tintcolor];\
}\
else\
{\
[[SegmentControl.subviews objectAtIndex:i] setTintColor:nil];\
}\
}\

@interface AllNewViewController ()<NewsDelegate>
{
    NewsView*v_news;
    
}
@end

@implementation AllNewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSDictionary *highlightedAttributes = [NSDictionary
                                           dictionaryWithObject:[UIFont fontWithName:@"GESSTwoBold-Bold" size:13] forKey:UITextAttributeFont];
    [SegmentControl setTitleTextAttributes:highlightedAttributes forState:UIControlStateNormal];
    
    STYLE_SEGMENT
    
    v_news =(NewsView*)[[[NSBundle mainBundle]loadNibNamed:@"NewsView" owner:nil options:nil]objectAtIndex:0];
    v_news.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
    v_news.delegate=nil;
    self.delegate= v_news;
    v_news.OpenType=OPEN_TYPE_REGULAR;
    v_news.delegate=self;
    v_news.Sector=ALL;
    [self.ContainerView addSubview: v_news];
    [self.ContainerView setHidden:YES];
    [v_news LoadNews];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)SegmentAction:(UISegmentedControl *)sender {
    @try {
        STYLE_SEGMENT
        
        switch (sender.selectedSegmentIndex) {
            case 0:
            {
                if (self.delegate) {
                    [self.delegate didSelectNewsType:SFDA_NEWS];
                }
            }
                break;
            case 1:
            {
                if (self.delegate) {
                    [self.delegate didSelectNewsType:ALL_WRNING_NEWS];
                }
            }
                break;
            case 2:
            {
                if (self.delegate) {
                    [self.delegate didSelectNewsType:ALL_NEWS];
                }
            }
                break;
        }
    }
    @catch (NSException *exception) {
    }
}



@end
