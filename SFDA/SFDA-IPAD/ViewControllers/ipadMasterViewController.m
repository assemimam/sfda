//
//  ipadMasterViewController.m
//  sfda-ipad
//
//  Created by Assem Imam on 5/20/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "ipadMasterViewController.h"
#import "SFDA_MainViewController.h"
#import "SectorsViewController.h"
#import "ElectronicServicesViewController.h"
#import "AboutApplicationViewController.h"
#import "ipadNewsViewController.h"
#import "AllNewViewController.h"
#import "NewsSearchResultViewController.h"
#import "FavouriteNewsViewController.h"
#import "ReportView.h"
#define SFDA_OPTION 0
#define NEWS_OPTION 1
#define FOOD_OPTION 2
#define DRUGS_OPTION 3
#define MEDICAL_OPTION 4
#define E_SERVICES_OPTION 5
#define ABOUT_APP_OPTION 6
#define FAVOURITE_OPTION 7

@interface ipadMasterViewController ()<NewsScliderDelegate>
{
    id AllNewsList;
    ReportView *reportView;
 
}
@end

@implementation ipadMasterViewController
@synthesize ContainerView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [MenuTableView reloadData];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
 
 
    
    AllNewsList = [[NSMutableArray alloc]init];
    self.navigationController.navigationBarHidden =YES;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
     [((AppDelegate_ipad*)[[UIApplication sharedApplication]delegate]) setCurrentViewController:self];
    UISwipeGestureRecognizer *swipLeftRecognizer =[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipActionLeft)];
    swipLeftRecognizer.direction=UISwipeGestureRecognizerDirectionLeft;
    [ContainerView addGestureRecognizer:swipLeftRecognizer];
   
    UISwipeGestureRecognizer *swipRightRecognizer =[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipActionRight)];
    swipRightRecognizer.direction=UISwipeGestureRecognizerDirectionRight;
    [ContainerView addGestureRecognizer:swipRightRecognizer];
  
    [CommonMethods  FormatView:MenuView WithShadowRadius:1 CornerRadius:0 ShadowOpacity:0.8 BorderWidth:0 BorderColor:[UIColor blackColor] ShadowColor:[UIColor blackColor] andXPosition:3];
    TitleLabel.font= [UIFont fontWithName:@"GESSTwoBold-Bold" size:17];
    UpdateTitleLabel.font= [UIFont fontWithName:@"GESSTwoBold-Bold" size:13];
    UpdateDateLabel.font= [UIFont fontWithName:@"GESSTwoLight-Light" size:14];
    
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"show_menu"]==nil) {
        [self performSelector:@selector(MenuButtonAction:) withObject:[[UIButton alloc]init] afterDelay:1.0];
        [self performSelector:@selector(MenuButtonAction:) withObject:[[UIButton alloc]init] afterDelay:2.5];
        [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"show_menu"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)swipActionRight
{
    @try {
        SideMenView *sideMenuView ;
        
        if ([self.view viewWithTag:SIDE_MENU_VIEW_TAG]) {
            sideMenuView=(SideMenView*)[self.view viewWithTag:SIDE_MENU_VIEW_TAG];
            [UIView animateWithDuration:0.3 animations:^{
                [sideMenuView setFrame:CGRectMake(self.view.frame.size.width, MenuView.frame.origin.y, MenuView.frame.size.width, MenuView.frame.size.height)];
            } completion:^(BOOL finished) {
                if (finished) {
                     [MenuButton setImage:[UIImage imageNamed:@"menuic.png"] forState:UIControlStateNormal];
                    [sideMenuView removeFromSuperview];
                }
            }];
        }
        else{
        }
         [CommonMethods  FormatView:sideMenuView WithShadowRadius:1 CornerRadius:0 ShadowOpacity:0.4 BorderWidth:0 BorderColor:[UIColor blackColor] ShadowColor:[UIColor blackColor] andXPosition:-2];
        sideMenuView.delegate=self;
    }
    @catch (NSException *exception) {
        
    }
}

-(void)swipActionLeft
{
    @try {
        SideMenView *sideMenuView ;
         [MenuButton setImage:[UIImage imageNamed:@"menuicselceted.png"] forState:UIControlStateNormal];
        if ([self.view viewWithTag:SIDE_MENU_VIEW_TAG]) {
            
        }
        else{
            sideMenuView=(SideMenView*)[[[NSBundle mainBundle]loadNibNamed:@"SideMenView" owner:nil options:nil]objectAtIndex:0];
            sideMenuView.tag= SIDE_MENU_VIEW_TAG;
            [self.view addSubview:sideMenuView];
            [sideMenuView setFrame:CGRectMake(self.view.frame.size.width, MenuView.frame.origin.y, MenuView.frame.size.width, MenuView.frame.size.height)];
            [self.view bringSubviewToFront:sideMenuView];
            [UIView animateWithDuration:0.3 animations:^{
                [sideMenuView setFrame:CGRectMake(MenuView.frame.origin.x+1, MenuView.frame.origin.y, MenuView.frame.size.width, MenuView.frame.size.height)];
            }];
        }
        [CommonMethods  FormatView:sideMenuView WithShadowRadius:2 CornerRadius:0 ShadowOpacity:0.4 BorderWidth:0 BorderColor:[UIColor blackColor] ShadowColor:[UIColor blackColor] andXPosition:-2];
        sideMenuView.delegate=self;
    }
    @catch (NSException *exception) {
        
    }
    
    
}
- (IBAction)SearchAction:(UIButton *)sender {
    @try {
        [SearchTextField resignFirstResponder];
        if ([SearchTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]].length > 0) {
            [self swipActionRight];
            NewsSearchResultViewController *vc_news_search =[[NewsSearchResultViewController alloc]initWithNibName:@"NewsSearchResultViewController" bundle:nil];
            vc_news_search.newsTitle=SearchTextField.text;
            [self.navigationController pushViewController:vc_news_search animated:YES];
        }
    }
    @catch (NSException *exception) {
        
    }
   
}
#pragma -mark news delegate methods
-(void)didFinishLoadingNews:(id)newsList{
    @try {
       HIDE_LOADINGINDICATOR
        [self.ContainerView setHidden:NO];
        AllNewsList = newsList;
        if ([[NSUserDefaults standardUserDefaults]objectForKey:NEWS_SFDA_CASH_KEY]) {
            NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
            [formatter setDateFormat:@" EEEE MMMM yyyy  hh:mm a"];
            [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ar_SA"]];
            
            UpdateDateLabel.text = [formatter stringFromDate:[[NSUserDefaults standardUserDefaults]objectForKey:NEWS_SFDA_CASH_KEY]];
        }
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)didSelectNewsItem:(id)item atIndex:(int)index{
    @try {
        [self swipActionRight];
        ipadNewsViewController *vcnews =[[ipadNewsViewController alloc]initWithNibName:@"ipadNewsViewController" bundle:nil];
        vcnews.AllNewsList= AllNewsList;
        vcnews.SelectedNewsIndex=index;
        vcnews.delegate=self;
        [self.navigationController pushViewController:vcnews animated:NO];
   }
    @catch (NSException *exception) {
        
    }
    
}
-(void)didSelectNewsHeader:(id)item
{
    @try {
        ipadNewsViewController *vcnews =[[ipadNewsViewController alloc]initWithNibName:@"ipadNewsViewController" bundle:nil];
        vcnews.AllNewsList= AllNewsList;
        vcnews.SelectedNewsIndex=0;
        vcnews.delegate=self;
        [self.navigationController pushViewController:vcnews animated:NO];
    }
    @catch (NSException *exception) {
        
    }
    
}

#pragma -mark sidemenu delegate
-(void)didSelectMenuItem:(id)ite Atindex:(int)index{
    @try {
        switch (index) {
            case SFDA_OPTION:
            {
                SFDA_MainViewController*vc_sfda =[[SFDA_MainViewController alloc]init];
                self.navigationController.viewControllers = [NSArray arrayWithObject:vc_sfda];
                [self.navigationController popToRootViewControllerAnimated:NO];

            }
                break;
            case NEWS_OPTION:
            {
                AllNewViewController*vc_allNews =[[AllNewViewController alloc]init];
                self.navigationController.viewControllers = [NSArray arrayWithObject:vc_allNews];
                [self.navigationController popToRootViewControllerAnimated:NO];
            }
                break;

            case FOOD_OPTION:
            {
                SectorsViewController*vc_sectors =[[SectorsViewController alloc]init];
                vc_sectors.Sector= FOODS_SECTOR;
                CurrentSectorType=FOODS_SECTOR;
                self.navigationController.viewControllers = [NSArray arrayWithObject:vc_sectors];
                [self.navigationController popToRootViewControllerAnimated:NO];
            }
                break;
            case DRUGS_OPTION:
            {
                SectorsViewController*vc_sectors =[[SectorsViewController alloc]init];
                vc_sectors.Sector= DRUGS_SECTOR;
                 CurrentSectorType=DRUGS_SECTOR;
                self.navigationController.viewControllers = [NSArray arrayWithObject:vc_sectors];
                [self.navigationController popToRootViewControllerAnimated:NO];

            }
                break;
            case MEDICAL_OPTION:
            {
                SectorsViewController*vc_sectors =[[SectorsViewController alloc]init];
                vc_sectors.Sector= MEDICAL_SECTOR;
                 CurrentSectorType=MEDICAL_SECTOR;
                self.navigationController.viewControllers = [NSArray arrayWithObject:vc_sectors];
                [self.navigationController popToRootViewControllerAnimated:NO];
              
            }
                break;
            case E_SERVICES_OPTION:
            {
                ElectronicServicesViewController*vc_electronicServices =[[ElectronicServicesViewController alloc]init];
               
                self.navigationController.viewControllers = [NSArray arrayWithObject:vc_electronicServices];
                [self.navigationController popToRootViewControllerAnimated:NO];
            }
                break;
            case ABOUT_APP_OPTION:
            {
                AboutApplicationViewController*vc_abotApp =[[AboutApplicationViewController alloc]init];
                vc_abotApp.modalPresentationStyle=UIModalPresentationFormSheet;
                [self presentModalViewController:vc_abotApp animated:YES];
            }
                break;
            case FAVOURITE_OPTION:
            {
                FavouriteNewsViewController*vc_favouriteNews =[[FavouriteNewsViewController alloc]init];
                self.navigationController.viewControllers = [NSArray arrayWithObject:vc_favouriteNews];
                [self.navigationController popToRootViewControllerAnimated:NO];
            }
                break;

        }
        
        SideMenView *sideMenuView=(SideMenView*) [self.view viewWithTag:SIDE_MENU_VIEW_TAG];
        [CommonMethods  FormatView:sideMenuView WithShadowRadius:1 CornerRadius:0 ShadowOpacity:0.4 BorderWidth:0 BorderColor:[UIColor blackColor] ShadowColor:[UIColor blackColor] andXPosition:-2];
        [UIView animateWithDuration:0.3 animations:^{
            [sideMenuView setFrame:CGRectMake(self.view.frame.size.width, MenuView.frame.origin.y, MenuView.frame.size.width, MenuView.frame.size.height)];
        } completion:^(BOOL finished) {
            if (finished) {
                [sideMenuView removeFromSuperview];
            }
        }];
    }
    @catch (NSException *exception) {
        
    }
    
}
- (IBAction)MenuButtonAction:(UIButton *)sender {
    @try {
        SideMenView *sideMenuView ;
      
        if ([self.view viewWithTag:SIDE_MENU_VIEW_TAG]) {
            sideMenuView=(SideMenView*)[self.view viewWithTag:SIDE_MENU_VIEW_TAG];
            [UIView animateWithDuration:0.3 animations:^{
                [sideMenuView setFrame:CGRectMake(self.view.frame.size.width, MenuView.frame.origin.y, MenuView.frame.size.width, MenuView.frame.size.height)];
            } completion:^(BOOL finished) {
                if (finished) {
                    [sender setImage:[UIImage imageNamed:@"menuic.png"] forState:UIControlStateNormal];
                    [sideMenuView removeFromSuperview];
                }
            }];
        }
        else{
            [sender setImage:[UIImage imageNamed:@"menuicselceted"] forState:UIControlStateNormal];
            sideMenuView=(SideMenView*)[[[NSBundle mainBundle]loadNibNamed:@"SideMenView" owner:nil options:nil]objectAtIndex:0];
            sideMenuView.tag= SIDE_MENU_VIEW_TAG;
             [sideMenuView setFrame:CGRectMake(self.view.frame.size.width, MenuView.frame.origin.y, MenuView.frame.size.width, MenuView.frame.size.height)];
            [self.view addSubview:sideMenuView];
           
            [self.view bringSubviewToFront:sideMenuView];
            [UIView animateWithDuration:0.3 animations:^{
                [sideMenuView setFrame:CGRectMake(MenuView.frame.origin.x+1, MenuView.frame.origin.y, MenuView.frame.size.width, MenuView.frame.size.height)];
            }];
        }
        
       [CommonMethods  FormatView:sideMenuView WithShadowRadius:1 CornerRadius:0 ShadowOpacity:0.4 BorderWidth:0 BorderColor:[UIColor blackColor] ShadowColor:[UIColor blackColor] andXPosition:-2];
        sideMenuView.delegate=self;
    }
    @catch (NSException *exception) {
        
    }
    
}
#pragma -mark news sclicing delegate
-(void)didFinishScliding{
    @try {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [MenuTableView reloadData];
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark uitextfield delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    @try {
        [textField resignFirstResponder];
        if ([SearchTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]].length > 0) {
            [self swipActionRight];
            NewsSearchResultViewController *vc_news_search =[[NewsSearchResultViewController alloc]initWithNibName:@"NewsSearchResultViewController" bundle:nil];
            vc_news_search.newsTitle=SearchTextField.text;
            [self.navigationController pushViewController:vc_news_search animated:YES];
        }
    }
    @catch (NSException *exception) {
        
    }
   
    return YES;
}


 


@end
