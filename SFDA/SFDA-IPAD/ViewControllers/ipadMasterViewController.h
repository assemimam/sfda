//
//  ipadMasterViewController.h
//  sfda-ipad
//
//  Created by Assem Imam on 5/20/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LibrariesConstants.h"
#import "SideMenView.h"
#import  "NewsView.h"
#import "PDFViewController.h"
#import "Cashing.h"

#define LOADING_VIEW_TAG 2000000

@interface ipadMasterViewController : UIViewController<SideMenuDelegate,NewsDelegate,ReaderViewControllerDelegate,UITextFieldDelegate>
{
    __weak IBOutlet UITableView *MenuTableView;
    __weak IBOutlet UIView *MenuView;
    __weak IBOutlet UILabel *TitleLabel;
    __weak IBOutlet UIView *UpdateView;
    __weak IBOutlet UILabel *UpdateDateLabel;
    __weak IBOutlet UILabel *UpdateTitleLabel;
    __weak IBOutlet UITextField *SearchTextField;
    __weak IBOutlet UIView *vLoading;
    __weak IBOutlet UILabel *lblLoading;
    __weak IBOutlet UIActivityIndicatorView *aiLoading;
        __weak IBOutlet UIButton *MenuButton;
}
@property(nonatomic,weak) IBOutlet UIView *ContainerView;
- (IBAction)MenuButtonAction:(UIButton *)sender;
- (IBAction)SearchAction:(UIButton *)sender;
- (IBAction)ClearCashedData:(UIButton *)sender;
@end
