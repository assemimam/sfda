//
//  SFDA_MainViewController.h
//  sfda-ipad
//
//  Created by Assem Imam on 5/20/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ipadMasterViewController.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "ReaderViewController.h"

#pragma mark Constants
#define DEMO_VIEW_CONTROLLER_PUSH FALSE

@interface SFDA_MainViewController : ipadMasterViewController<UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate>
{
    
}



@end
