//
//  SFDA_MainViewController.m
//  sfda-ipad
//
//  Created by Assem Imam on 5/20/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "SFDA_MainViewController.h"
#import "MainMenuCell.h"
#import "MembersView.h"
#import "ContactUsView.h"
#import "ImportantLinksView.h"
#import "AboutView.h"
#import "CustomWebView.h"
#import "AwarnessCenterView.h"
#import "EmploymentView.h"
#import "Database.h"
#import "ReportView.h"

#define SFDA_NEWS 0
#define ABOUT_SFDA 1
#define EMPLOYMENT 2
#define MEMBERS_LIST 3
#define REPORT 4
#define AWARNESS_CENTER 5
#define IMPORTANT_LINKS 6
#define CONTACT_US 7

@interface SFDA_MainViewController ()<ContactUsDelegate,ImportantLinksDelegate,AwarnessDelegate , ReportViewDelegate , UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    UIImagePickerController *imagePicker;
    AwarnessCenterView*v_awarness;
    NSMutableArray*MainMenuOptions;
    NewsView*v_news;
    ReportView *v_reports;
    BOOL NEWS_FINISH_LOADING;
    AwarnessCell *CurrentAwarnessCell;
    int CurrentAwarnessCellIndex;
    
}
@end

@implementation SFDA_MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}
-(void)viewDidDisappear:(BOOL)animated
{
    v_awarness.delegate=nil;
}
-(void)viewWillAppear:(BOOL)animated
{
    v_awarness.delegate=self;
    v_news.delegate=self;
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    imagePicker= [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    
    
    CurrentAwarnessCellIndex=-1;
    NEWS_FINISH_LOADING=NO;
    MainMenuOptions = [[NSMutableArray alloc]init];
    id MenuItems = [[Language getObject]getArrayWithKey:@"MainMenuOptions"];
    for (id item in MenuItems) {
        NSMutableDictionary*menuItem=[[NSMutableDictionary alloc]init];
        [menuItem setObject:[item objectForKey:@"image"] forKey:@"image"];
        [menuItem setObject:[item objectForKey:@"option"] forKey:@"option"];
        [menuItem setObject:@"0" forKey:@"selected"];
        [MainMenuOptions addObject:menuItem];
    }
    [[MainMenuOptions objectAtIndex:0]setObject:@"1" forKey:@"selected"];
    
    v_news =(NewsView*)[[[NSBundle mainBundle]loadNibNamed:@"NewsView" owner:nil options:nil]objectAtIndex:0];
    v_news.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
    v_news.delegate=nil;
    v_news.delegate=self;
    v_news.OpenType=OPEN_TYPE_REGULAR;
    v_news.Sector=SFDA_SECTOR;
    v_news.NewsHeaderView.backgroundColor=MenuView.backgroundColor;
    [v_news LoadNews];
    [self.ContainerView addSubview: v_news];
    [v_news setHidden:YES];
    if ([[NSUserDefaults standardUserDefaults]objectForKey:NEWS_SFDA_CASH_KEY]) {
        NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"EEEE MMMM yyyy  hh:mm a"];
        [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ar_SA"]];
        
        UpdateDateLabel.text = [formatter stringFromDate:[[NSUserDefaults standardUserDefaults]objectForKey:NEWS_SFDA_CASH_KEY]];
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MainMenuCell*cell;
    @try {
        cell=(MainMenuCell*)[tableView dequeueReusableCellWithIdentifier:@"cleMenu"];
        if (cell==nil) {
            cell=(MainMenuCell*)[[[NSBundle mainBundle]loadNibNamed:@"MainMenuCell" owner:nil options:nil]objectAtIndex:0];
        }
        NSString*itemImageName =[[MainMenuOptions objectAtIndex:indexPath.row]objectForKey:@"image"];
        NSString*itemName=[[MainMenuOptions objectAtIndex:indexPath.row]objectForKey:@"option"];
        cell.MenuItemIconImageView.image = [UIImage imageNamed:itemImageName];
        cell.MenuItemLabel.text=itemName;
        cell.MenuItemLabel.font= [UIFont fontWithName:@"GESSTwoBold-Bold" size:17];
        UIView *SelectionView = [[UIView alloc]initWithFrame:cell.frame];
        SelectionView.backgroundColor = [UIColor colorWithRed:69/255.0f  green:67/255.0f blue:79/255.0f alpha:1];
        if ([[[MainMenuOptions objectAtIndex:indexPath.row]objectForKey:@"selected"]intValue]==1) {
            cell.contentView.backgroundColor = [UIColor colorWithRed:69/255.0f  green:67/255.0f blue:79/255.0f alpha:1];
            [cell.ArrowIndicatorImageView setHidden:NO];
        }
        else{
            cell.contentView.backgroundColor = [UIColor colorWithRed:92/255.0f  green:98/255.0f blue:106/255.0f alpha:1];
            [cell.ArrowIndicatorImageView setHidden:YES];
        }
        
        cell.selectedBackgroundView = SelectionView;
    }
    @catch (NSException *exception) {
        
    }
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [MainMenuOptions count];
}
#pragma -mark uitableview delegate methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        SHOW_LOADINGINDICATOR
        [self.ContainerView setUserInteractionEnabled:YES];
        switch (indexPath.row) {
            case SFDA_NEWS:
            {
                
                if (NEWS_FINISH_LOADING) {
                    for (UIView*view in [self.ContainerView subviews]) {
                        [view removeFromSuperview];
                    }
                    [self.ContainerView setAlpha:0];
                    [UIView animateWithDuration:0.3 animations:^{
                        [self.ContainerView setAlpha:1];
                    }];
                    
                    v_news.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
                    v_news.delegate=self;
                    v_news.OpenType=OPEN_TYPE_REGULAR;
                    [v_news setHidden:YES];
                    [self.ContainerView addSubview: v_news];
                    [v_news LoadNews];
                    NEWS_FINISH_LOADING=NO;
                }
                
            }
                break;
                
            case ABOUT_SFDA:
            {
                NEWS_FINISH_LOADING=YES;
                for (UIView*view in [self.ContainerView subviews]) {
                    [view removeFromSuperview];
                }
                [self.ContainerView setAlpha:0];
                [UIView animateWithDuration:0.3 animations:^{
                    [self.ContainerView setAlpha:1];
                }];
                
                AboutView*v_about =(AboutView*)[[[NSBundle mainBundle]loadNibNamed:@"AboutView" owner:nil options:nil]objectAtIndex:0];
                v_about.Sector=SFDA_SECTOR;
                v_about.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
                [self.ContainerView addSubview: v_about];
            }
                break;
            case EMPLOYMENT:
            {
                HIDE_LOADINGINDICATOR
                NEWS_FINISH_LOADING=YES;
                for (UIView*view in [self.ContainerView subviews]) {
                    [view removeFromSuperview];
                }
                [self.ContainerView setAlpha:0];
                [UIView animateWithDuration:0.3 animations:^{
                    [self.ContainerView setAlpha:1];
                }];
                
                EmploymentView*v_employment =(EmploymentView*)[[[NSBundle mainBundle]loadNibNamed:@"EmploymentView" owner:nil options:nil]objectAtIndex:0];
                
                v_employment.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
                [self.ContainerView addSubview: v_employment];
                
                
            }
                break;
            case MEMBERS_LIST:
            {
                NEWS_FINISH_LOADING=YES;
                for (UIView*view in [self.ContainerView subviews]) {
                    [view removeFromSuperview];
                }
                [self.ContainerView setAlpha:0];
                [UIView animateWithDuration:0.3 animations:^{
                    [self.ContainerView setAlpha:1];
                }];
                
                MembersView*v_members =(MembersView*)[[[NSBundle mainBundle]loadNibNamed:@"MembersView" owner:nil options:nil]objectAtIndex:0];
                v_members.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
                [self.ContainerView addSubview: v_members];
                
            }
                break;
            case REPORT:
            {
                HIDE_LOADINGINDICATOR
                for (UIView*view in [self.ContainerView subviews]) {
                    [view removeFromSuperview];
                }
                [self.ContainerView setAlpha:0];
                [UIView animateWithDuration:0.3 animations:^{
                    [self.ContainerView setAlpha:1];
                }];
                
                v_reports =(ReportView*)[[[NSBundle mainBundle]loadNibNamed:@"ReportView" owner:nil options:nil]objectAtIndex:0];
                v_reports.delegate = self;
                v_reports.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
                [self.ContainerView addSubview: v_reports];
                
            }
                break;
            case AWARNESS_CENTER:
            {
                NEWS_FINISH_LOADING=YES;
                for (UIView*view in [self.ContainerView subviews]) {
                    [view removeFromSuperview];
                }
                [self.ContainerView setAlpha:0];
                [UIView animateWithDuration:0.5 animations:^{
                    [self.ContainerView setAlpha:1];
                }];
                
                v_awarness =(AwarnessCenterView*)[[[NSBundle mainBundle]loadNibNamed:@"AwarnessCenterView" owner:nil options:nil]objectAtIndex:0];
                v_awarness.delegate=nil;
                v_awarness.delegate=self;
                v_awarness.Sector=SFDA_SECTOR;
                [self.ContainerView addSubview: v_awarness];
            }
                break;
            case IMPORTANT_LINKS:
            {
                NEWS_FINISH_LOADING=YES;
                for (UIView*view in [self.ContainerView subviews]) {
                    [view removeFromSuperview];
                }
                [self.ContainerView setAlpha:0];
                [UIView animateWithDuration:0.3 animations:^{
                    [self.ContainerView setAlpha:1];
                }];
                
                
                ImportantLinksView*v_importantLinks =(ImportantLinksView*)[[[NSBundle mainBundle]loadNibNamed:@"ImportantLinksView" owner:nil options:nil]objectAtIndex:0];
                v_importantLinks.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
                v_importantLinks.delegate=self;
                [self.ContainerView addSubview: v_importantLinks];
                
            }
                break;
            case CONTACT_US:
            {
                NEWS_FINISH_LOADING=YES;
                for (UIView*view in [self.ContainerView subviews]) {
                    [view removeFromSuperview];
                }
                [self.ContainerView setAlpha:0];
                [UIView animateWithDuration:0.3 animations:^{
                    [self.ContainerView setAlpha:1];
                }];
                
                ContactUsView*v_contactUs =(ContactUsView*)[[[NSBundle mainBundle]loadNibNamed:@"ContactUsView" owner:nil options:nil]objectAtIndex:0];
                v_contactUs.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
                v_contactUs.delegate=self;
                [self.ContainerView addSubview: v_contactUs];
            }
                break;
        }
        for (id item in MainMenuOptions) {
            [item setObject:@"0" forKey:@"selected"];
        }
        [[MainMenuOptions objectAtIndex:indexPath.row] setObject:@"1" forKey:@"selected"];
        [tableView reloadData];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    @catch (NSException *exception) {
        
    }
    
}
#pragma -mark contactus delegate methods
-(void)didClickAtContact:(id)contact{
    @try {
        NSString *name = [contact objectForKey:@"name"];
        NSString *value = [contact objectForKey:@"value"];
        if([name isEqualToString:@"رقم الهاتف"] || [name isEqualToString:@"رقم الهاتف 1"]){
            
            //            if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",value]]])
            //            {
            //                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",value]]];
            //            }
            //            else{
            //                 [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"NoCallerMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            //  }
        }else if([name isEqualToString:@"البريد"]  || [name isEqualToString:@"مراسلة الرئيس التنفيذي"]){
            if ([MFMailComposeViewController canSendMail]) {
                
                
                MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                controller.mailComposeDelegate = self;
                [controller setSubject:@""];
                [controller setToRecipients:[NSArray arrayWithObjects:value,nil]];
                [controller setMessageBody:@"" isHTML:NO];
                
                if (controller) [self presentModalViewController:controller animated:YES];
            }
            else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"ادخل بريدك الالكترونى"
                                                               delegate:nil
                                                      cancelButtonTitle:@"موافق"
                                                      otherButtonTitles: nil];
                [alert show];
                
            }
            
        }else if([name isEqualToString:@"لموقع الإلكترونى"]){
            if(![name hasPrefix:@"http://"]){
                value = [@"http://" stringByAppendingString:value];
            }
            NSURL *url = [NSURL URLWithString:value];
            [[UIApplication sharedApplication] openURL:url];
            
            
        }else if([name isEqualToString:@"تحويلة 1"]){
            //            NSString *firstPhoneNumber =[[sdfaOptionsList objectAtIndex:0]valueForKey:@"ValueAr" ];
            //            if([[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",firstPhoneNumber]]])
            //            {
            //
            //            }
            //            else{
            //            }
        }
        else{
            if(![value hasPrefix:@"http://"] && ![value hasPrefix:@"https://"] ){
                value = [@"http://" stringByAppendingString:value];
            }
            
            //NSLog(@"%@",value);
            NSURL *url = [NSURL URLWithString:value];
            [[UIApplication sharedApplication] openURL:url];
            
        }
        
        
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark imortant links delegate
-(void)didSelectLink:(id)link{
    @try {
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:link]];
    }
    @catch (NSException *exception) {
        
    }
}
#pragma  mark MFMailCompose delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        //NSLog(@"It's away!");
    }
    [self dismissModalViewControllerAnimated:YES];
}

#pragma -mark awarness delegate

-(void)ShowPDF:(id)article
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *Documentpath = [paths objectAtIndex:0];
    NSString *TargetPath = [Documentpath stringByAppendingPathComponent:@"AwarnessPDF"];
    TargetPath = [Documentpath stringByAppendingPathComponent:[article objectForKey:@"sectionName"]];
    TargetPath= [TargetPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.pdf",[article objectForKey:@"$id"]]];
    NSFileManager *Fmanager = [NSFileManager defaultManager];
    if ([Fmanager fileExistsAtPath:TargetPath isDirectory:nil]) {
        
        if (CurrentAwarnessCellIndex%2==0) {
            [CurrentAwarnessCell.DownloadPDFImageView1 setHidden:YES];
            [CurrentAwarnessCell.ViewPDFImageView1 setHidden:NO];
            [CurrentAwarnessCell.AcivityIndicator1 stopAnimating];
        }
        else{
            [CurrentAwarnessCell.DownloadPDFImageView2 setHidden:YES];
            [CurrentAwarnessCell.ViewPDFImageView2 setHidden:NO];
            [CurrentAwarnessCell.ActivityIndicator2 stopAnimating];
        }
        
        [self performSelectorOnMainThread:@selector(HandleShowPDF:) withObject:TargetPath waitUntilDone:NO];
        return;
    }
    
    dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    //this will start the image loading in bg
    dispatch_async(concurrentQueue, ^{
        
        NSString *url = [[[article objectForKey:@"PDFUrl"] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding ]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *pdfUrl = [NSURL URLWithString:url];
        NSURLRequest* imgRequest = [NSURLRequest requestWithURL:pdfUrl];
        NSData *pdfData = [[NSData alloc]initWithData:[NSURLConnection sendSynchronousRequest:imgRequest returningResponse:nil error:nil]];
        imgRequest=nil;
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *Documentpath = [paths objectAtIndex:0];
        NSString *TargetPath = [Documentpath stringByAppendingPathComponent:@"AwarnessPDF"];
        TargetPath = [Documentpath stringByAppendingPathComponent:[article objectForKey:@"sectionName"]];
        NSFileManager *Fmanager = [NSFileManager defaultManager];
        NSString *fileName= [TargetPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.pdf",[article objectForKey:@"$id"]] ];
        
        if ([Fmanager fileExistsAtPath:TargetPath isDirectory:nil]) {
            
        }
        else
        {
            BOOL ret =[Fmanager createDirectoryAtPath:TargetPath withIntermediateDirectories:YES attributes:nil error:nil];
            if (ret) {
                
            };
        }
        
        
        if (pdfData!=nil &&pdfData.length > 0 ) {
            [pdfData writeToFile:fileName atomically:YES];
            
            if (CurrentAwarnessCellIndex%2==0) {
                [CurrentAwarnessCell.DownloadPDFImageView1 setHidden:YES];
                [CurrentAwarnessCell.ViewPDFImageView1 setHidden:NO];
                [CurrentAwarnessCell.AcivityIndicator1 stopAnimating];
            }
            else{
                [CurrentAwarnessCell.DownloadPDFImageView2 setHidden:YES];
                [CurrentAwarnessCell.ViewPDFImageView2 setHidden:NO];
                [CurrentAwarnessCell.ActivityIndicator2 stopAnimating];
            }
            
            
            NSMutableArray *cashedData = [[NSMutableArray alloc]init];
            
            DB_Field *articleID = [[DB_Field alloc]init];
            articleID.FieldDataType=FIELD_DATA_TYPE_NUMBER;
            articleID.IS_PRIMARY_KEY=YES;
            articleID.FieldName=@"id";
            articleID.FieldValue=[article objectForKey:@"$id"];
            
            DB_Field *sectionID = [[DB_Field alloc]init];
            sectionID.FieldDataType=FIELD_DATA_TYPE_NUMBER;
            sectionID.IS_PRIMARY_KEY=YES;
            sectionID.FieldName=@"SectionId";
            sectionID.FieldValue=[article objectForKey:@"SectionId"];
            
            
            DB_Field *PDFPath= [[DB_Field alloc]init];
            PDFPath.FieldDataType=FIELD_DATA_TYPE_TEXT;
            PDFPath.FieldName=@"PDFPath";
            PDFPath.FieldValue=fileName;
            
            DB_Field *Loaded = [[DB_Field alloc]init];
            Loaded.FieldDataType=FIELD_DATA_TYPE_NUMBER;
            Loaded.FieldName=@"Loaded";
            Loaded.FieldValue=@"1";
            
            
            DB_Recored *record = [[DB_Recored alloc]init];
            record.Fields=[NSMutableArray arrayWithObjects:articleID,sectionID,PDFPath,Loaded, nil];
            [cashedData addObject:record];
            
            
            [[Cashing getObject] setDatabase:@"SDFAipad"];
            BOOL Success=  [[Cashing getObject]CashData:cashedData inTable:@"AwarenessCenter"];
            
            
            [self performSelectorOnMainThread:@selector(HandleShowPDF:) withObject:fileName waitUntilDone:NO];
            
        }
        else{
            if (CurrentAwarnessCellIndex%2==0) {
                [CurrentAwarnessCell.DownloadPDFImageView1 setHidden:NO];
                [CurrentAwarnessCell.ViewPDFImageView1 setHidden:YES];
                [CurrentAwarnessCell.AcivityIndicator1 stopAnimating];
            }
            else{
                [CurrentAwarnessCell.DownloadPDFImageView2 setHidden:NO];
                [CurrentAwarnessCell.ViewPDFImageView2 setHidden:YES];
                [CurrentAwarnessCell.ActivityIndicator2 stopAnimating];
            }
            
            
            [self.ContainerView setUserInteractionEnabled:YES];
        }
        
    });
    
    
    
    
}

-(void)HandleShowPDF:(id)path
{
    if (CurrentAwarnessCellIndex%2==0) {
        [CurrentAwarnessCell.DownloadPDFImageView1 setHidden:YES];
        [CurrentAwarnessCell.ViewPDFImageView1 setHidden:NO];
        [CurrentAwarnessCell.AcivityIndicator1 stopAnimating];
    }
    else{
        [CurrentAwarnessCell.DownloadPDFImageView2 setHidden:YES];
        [CurrentAwarnessCell.ViewPDFImageView2 setHidden:NO];
        [CurrentAwarnessCell.ActivityIndicator2 stopAnimating];
    }
    [self.ContainerView setUserInteractionEnabled:YES];
    if ([path stringByReplacingOccurrencesOfString:@" " withString:@""].length>0) {
        ReaderDocument *document = [ReaderDocument withDocumentFilePath:path password:nil];
        ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
        readerViewController.delegate = self;
        
        readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:readerViewController animated:YES completion:NULL];
    }
    
}

-(void)didSelectArticle:(id)article AtCell:(UIView *)cell AtIndex:(int)index{
    @try {
        [self.ContainerView setUserInteractionEnabled:NO];
        CurrentAwarnessCell=(AwarnessCell*)cell;
        CurrentAwarnessCellIndex=index;
        if ([[article objectForKey:@"Loaded"]intValue]==0) {
            if (!internetConnection) {
                [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
                [self.ContainerView setUserInteractionEnabled:YES];
                HIDE_LOADINGINDICATOR
                return;
            }
            
            if (index%2==0) {
                [((AwarnessCell*)cell).DownloadPDFImageView1 setHidden:YES];
                [((AwarnessCell*)cell).AcivityIndicator1 startAnimating];
            }
            else{
                [((AwarnessCell*)cell).DownloadPDFImageView2 setHidden:YES];
                [((AwarnessCell*)cell).ActivityIndicator2 startAnimating];
            }
            
            
            
            if ([[article objectForKey:@"PDFUrl"] stringByReplacingOccurrencesOfString:@" " withString:@""].length>0) {
                [self ShowPDF:article];
            }
        }
        else
        {
            [self performSelectorOnMainThread:@selector(HandleShowPDF:) withObject:[article objectForKey:@"PDFPath"] waitUntilDone:NO];
            
        }
        
    }
    @catch (NSException *exception) {
        
    }
}



#pragma mark ReaderViewControllerDelegate methods

- (void)dismissReaderViewController:(ReaderViewController *)viewController
{
    HIDE_LOADINGINDICATOR
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}
#pragma -mark news delegate
-(void)didFinishLoadingNews:(id)newsList
{
    NEWS_FINISH_LOADING=YES;
    [super didFinishLoadingNews:newsList];
    
}

- (IBAction)ClearCashedData:(UIButton *)sender {
    @try {
        if (!internetConnection)
        {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"]
                                      delegate:nil
                             cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"]
                             otherButtonTitles:nil, nil]show];
            
            [self.ContainerView setUserInteractionEnabled:YES];
            
            HIDE_LOADINGINDICATOR
            
        }
        
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:NEWS_SFDA_CASH_KEY];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:LINKS_CASH_KEY];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:CONTACT_US_CASH_KEY];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:ABOUT_SFDA_CASH_KEY];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:AWARNESS_SFDA_CASH_KEY];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:MEMBERS_CASH_KEY];
        
        CLEARE_CASHED_DATA;
        
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        if (NEWS_FINISH_LOADING) {
            SFDA_MainViewController*vc_sectors =[[SFDA_MainViewController alloc]init];
            self.navigationController.viewControllers = [NSArray arrayWithObject:vc_sectors];
            [self.navigationController popToRootViewControllerAnimated:NO];
            
            NEWS_FINISH_LOADING=NO;
        }
        
        
    }
    @catch (NSException *exception) {
        
    }
    
}

-(void)presentView:(UIImagePickerController *)view_c atIndex:(NSInteger)index
{
    
    imagePicker.modalPresentationStyle = UIModalPresentationFormSheet;
    
    if (index == 0)
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self.navigationController presentViewController:imagePicker animated:YES completion:nil];
        }];
        
    }
    else if (index == 1)
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self.navigationController presentViewController:imagePicker animated:YES completion:nil];
        }];
    }
}


#pragma mark - UIImagePicker Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    @try
    {
        
        UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
        pickerImage= chosenImage;
        
         NSNotification*   imagePickerNotification =[NSNotification
                                  notificationWithName:IMAGE_PICKER_DID_FINISH_PICKING_IMAGE
                                  object:nil];
        
         [[NSNotificationCenter defaultCenter]  removeObserver:imagePickerNotification];
         [[NSNotificationCenter defaultCenter]  postNotification:imagePickerNotification];
        

        [picker dismissViewControllerAnimated:YES completion:NULL];
    }
    @catch (NSException *exception)
    {
        
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return  UIInterfaceOrientationLandscapeLeft| UIInterfaceOrientationLandscapeRight;
}


-(BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscape;
}
@end
