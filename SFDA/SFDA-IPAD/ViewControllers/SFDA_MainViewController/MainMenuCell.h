//
//  MainMenuCell.h
//  sfda-ipad
//
//  Created by Assem Imam on 5/20/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainMenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *MenuItemIconImageView;
@property (weak, nonatomic) IBOutlet UILabel *MenuItemLabel;
@property (weak, nonatomic) IBOutlet UIImageView *ArrowIndicatorImageView;
@property (weak, nonatomic) IBOutlet UILabel *SeperatorLabel;

@end
