//
//  NewsSearchResultViewController.h
//  sfda-ipad
//
//  Created by Assem Imam on 6/5/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//



@interface NewsSearchResultViewController : UIViewController<UITableViewDataSource>
{
    
    __weak IBOutlet UILabel *NoResultLabel;
    __weak IBOutlet UILabel *TitleLabel;
    __weak IBOutlet UITableView *NewsTableView;
}
@property(nonatomic,retain)NSString*newsTitle;
- (IBAction)CloseAction:(UIButton *)sender;
@end
