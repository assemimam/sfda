//
//  NewsSearchResultViewController.m
//  sfda-ipad
//
//  Created by Assem Imam on 6/5/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "NewsSearchResultViewController.h"
#import "NewsCell.h"
#import "ImageCache.h"
#import "Cashing.h"
#import "ipadNewsViewController.h"

@interface NewsSearchResultViewController ()<NewsScliderDelegate>
{
    id NewsList;
}
@end

@implementation NewsSearchResultViewController
@synthesize newsTitle;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    TitleLabel.font= [UIFont fontWithName:@"GESSTwoBold-Bold" size:17];
    NoResultLabel.font= [UIFont fontWithName:@"GESSTwoBold-Bold" size:30];
    
    DB_Field *fldNewsId = [[DB_Field alloc]init];
    fldNewsId.FieldName=@"id";
    fldNewsId.FieldAlias=@"$id";
    
    DB_Field *fldSerial = [[DB_Field alloc]init];
    fldSerial.FieldName=@"serial";
    
    DB_Field *fldTitleAr = [[DB_Field alloc]init];
    fldTitleAr.FieldName=@"title_ar";
    fldTitleAr.FieldAlias=@"TitleAr";
    
    
    
    DB_Field *fldContentAr = [[DB_Field alloc]init];
    fldContentAr.FieldName=@"content_ar";
    fldContentAr.FieldAlias=@"DescriptionAr";
    
    
    
    DB_Field *fldCategoryAr = [[DB_Field alloc]init];
    fldCategoryAr.FieldName=@"cat_ar";
    fldCategoryAr.FieldAlias=@"CategoryAr";
    
    
    
    DB_Field *fldPublishDate = [[DB_Field alloc]init];
    fldPublishDate.FieldName=@"PubDate";
    fldPublishDate.FieldAlias=@"PubDate";
    
    DB_Field *fldNewsUrl = [[DB_Field alloc]init];
    fldNewsUrl.FieldName=@"link";
    fldNewsUrl.FieldAlias=@"Url";
    
    DB_Field *fldSector = [[DB_Field alloc]init];
    fldSector.FieldName=@"sector";
    fldSector.FieldAlias=@"sector";
    DB_Field *fldImagePath= [[DB_Field alloc]init];
    fldImagePath.FieldName=@"image_path";
    
    
    DB_Field *fldFavourite = [[DB_Field alloc]init];
    fldFavourite.FieldName=@"is_favourite";
    
    [[Cashing getObject]setDatabase:@"SDFAipad"];
    
    id result =[[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects: fldTitleAr,fldContentAr,fldCategoryAr,fldPublishDate,fldFavourite,fldImagePath, nil] Tables:[NSArray arrayWithObject:@"news"] Where:[NSString stringWithFormat:@"title_ar like'%%%@%%' group by id",self.newsTitle] FromIndex:nil ToIndex:nil];
    NewsList =[[NSArray alloc]initWithArray:[result mutableCopy]];
    
    [NewsTableView reloadData];
    
    if ([result count]==0){
        [NoResultLabel setHidden:NO];
        [NewsTableView setHidden:YES];
    }
    else{
        [NoResultLabel setHidden:YES];
        [NewsTableView setHidden:NO];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)CloseAction:(UIButton *)sender {
    [self.navigationController popToRootViewControllerAnimated:NO];
}
#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewsCell*cell;
    int index=((indexPath.row)*2)+1;
    
    
    NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM/dd/yyyy hh:mm:ss a"];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    NSDateFormatter *stringFormatter =[[NSDateFormatter alloc]init];
    [stringFormatter setDateFormat:@"yyyy/MM/dd"];
    [stringFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    @try {
        //cell= (NewsCell*)[tableView dequeueReusableCellWithIdentifier:@"cleNews"];
        if (cell==nil) {
            cell=(NewsCell*)[[[NSBundle mainBundle] loadNibNamed:@"NewsCell" owner:nil options:nil]objectAtIndex:0];
        }
        cell.NewsTitleLabel.font = [UIFont fontWithName:@"GESSTwoBold-Bold" size:13];
        cell.NewsTitleLabel2.font = [UIFont fontWithName:@"GESSTwoBold-Bold" size:13];
        cell.NewsDateLabel.font = [UIFont fontWithName:@"GESSTwoLight-Light" size:13];
        cell.NewsDateLabel2.font = [UIFont fontWithName:@"GESSTwoLight-Light" size:13];
        
        NSDate *firstDate =[formatter dateFromString:[[NewsList objectAtIndex:index -1]objectForKey:@"PubDate"]];
        
        if ( index<[NewsList count]) {
            
            NSDate *seconedDate =[formatter dateFromString:[[NewsList objectAtIndex:index]objectForKey:@"PubDate"]];
            
            cell.NewsMarkerView.backgroundColor=[UIColor colorWithRed:92/255.0f  green:98/255.0f blue:106/255.0f alpha:1];
            cell.NewsmarkerView2.backgroundColor=[UIColor colorWithRed:92/255.0f  green:98/255.0f blue:106/255.0f alpha:1];
            
            if ([[NewsList objectAtIndex:index -1]objectForKey:@"ImageUrl"])
            {
                [cell.NewsActivityIndicator startAnimating];
                NSURL*firstNewsImageUrl =[NSURL URLWithString: [[NewsList objectAtIndex:index -1]objectForKey:@"ImageUrl"]];
                if (firstNewsImageUrl) {
                    [[ImageCache sharedInstance]downloadImageAtURL:firstNewsImageUrl completionHandler:^(UIImage *image) {
                        [cell.NewsActivityIndicator stopAnimating];
                        if (image) {
                            cell.NewsImageView.image = image;
                        }
                        else{
                            cell.NewsImageView.image = [UIImage imageNamed:@"maindefaultimgrows.png"];
                        }
                    }];
                }
            }
            else{//from cashe
                @autoreleasepool {
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                         NSUserDomainMask, YES);
                    NSString *documentsDirectory = [paths objectAtIndex:0];
                    NSString *imagePath=[[NewsList objectAtIndex:index -1]objectForKey:@"image_path"];
                    documentsDirectory =[documentsDirectory stringByAppendingFormat:@"/%@",imagePath];
                    
                    [[ImageCache sharedInstance] downloadImageAtURL:[NSURL fileURLWithPath:documentsDirectory]completionHandler:^(UIImage *image) {
                        if (image) {
                            cell.NewsImageView.image = image;
                        }
                        else{
                            cell.NewsImageView.image = [UIImage imageNamed:@"maindefaultimgrows.png"];
                        }
                    }];
                }
                
            }
            
            if ([[NewsList objectAtIndex:index ]objectForKey:@"ImageUrl"]) {
                [cell.NewsActivityIndicator2 startAnimating];
                NSURL*seconedNewsImageUrl =[NSURL URLWithString: [[NewsList objectAtIndex:index ]objectForKey:@"ImageUrl"]];
                if (seconedNewsImageUrl) {
                    [[ImageCache sharedInstance]downloadImageAtURL:seconedNewsImageUrl completionHandler:^(UIImage *image) {
                        [cell.NewsActivityIndicator2 stopAnimating];
                        if (image) {
                            cell.NewsImageView2.image = image;
                        }
                        else{
                            cell.NewsImageView2.image = [UIImage imageNamed:@"maindefaultimgrows.png"];
                        }
                    }];
                }
                
                
            }
            else{//from cashe
                @autoreleasepool {
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                         NSUserDomainMask, YES);
                    NSString *documentsDirectory = [paths objectAtIndex:0];
                    NSString *imagePath=[[NewsList objectAtIndex:index]objectForKey:@"image_path"];
                    documentsDirectory =[documentsDirectory stringByAppendingFormat:@"/%@",imagePath];
                    
                    [[ImageCache sharedInstance] downloadImageAtURL:[NSURL fileURLWithPath:documentsDirectory]completionHandler:^(UIImage *image) {
                        if (image) {
                            cell.NewsImageView2.image = image;
                        }
                        else{
                            cell.NewsImageView2.image = [UIImage imageNamed:@"maindefaultimgrows.png"];
                        }
                    }];
                    

                    
                }
                
            }
            
            if([[[NewsList objectAtIndex:index -1]objectForKey:@"CategoryAr"]  rangeOfString:@"تنبيه"].location!= NSNotFound  ){
                cell.NewsMarkerView.backgroundColor=[UIColor colorWithRed:215/255.0f  green:45/255.0f blue:40/255.0f alpha:1];
            }
            if([[[NewsList objectAtIndex:index ]objectForKey:@"CategoryAr"] rangeOfString:@"تنبيه"].location!= NSNotFound  ){
                cell.NewsmarkerView2.backgroundColor=[UIColor colorWithRed:215/255.0f  green:45/255.0f blue:40/255.0f alpha:1];
            }
            
            
            cell.NewsTitleLabel.text = [[NewsList objectAtIndex:index -1]objectForKey:@"TitleAr"];
            cell.NewsTitleLabel2.text = [[NewsList objectAtIndex:index]objectForKey:@"TitleAr"];
            cell.NewsDateLabel.text = [stringFormatter stringFromDate:firstDate];
            cell.NewsDateLabel2.text = [stringFormatter stringFromDate:seconedDate];
            cell.NewsBackButton.tag=index -1;
            cell.NewsBacButton2.tag=index ;
            [cell.NewsBackButton addTarget:self action:@selector(didSelectNewsCellAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.NewsBacButton2 addTarget:self action:@selector(didSelectNewsCellAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        else {
            [cell.NewsView2 setHidden:YES];
            
            
            
            if([[[NewsList objectAtIndex:index -1] objectForKey:@"CategoryAr"] rangeOfString:@"تنبيه"].location!= NSNotFound  ){
                cell.NewsMarkerView.backgroundColor=[UIColor colorWithRed:215/255.0f  green:45/255.0f blue:40/255.0f alpha:1];
            }
            
            cell.NewsTitleLabel.text = [[NewsList objectAtIndex:index-1 ]objectForKey:@"TitleAr"];
            cell.NewsDateLabel.text = [stringFormatter stringFromDate:firstDate];
            cell.NewsBackButton.tag=index-1 ;
            [cell.NewsBackButton addTarget:self action:@selector(didSelectNewsCellAction:) forControlEvents:UIControlEventTouchUpInside];
            
            if ([[NewsList objectAtIndex:index -1]objectForKey:@"ImageUrl"]) {
                [cell.NewsActivityIndicator startAnimating];
                NSURL*firstNewsImageUrl =[NSURL URLWithString: [[NewsList objectAtIndex:index -1]objectForKey:@"ImageUrl"]];
                if (firstNewsImageUrl) {
                    [[ImageCache sharedInstance]downloadImageAtURL:firstNewsImageUrl completionHandler:^(UIImage *image) {
                        [cell.NewsActivityIndicator stopAnimating];
                        if (image) {
                            cell.NewsImageView.image = image;
                        }
                        else{
                            cell.NewsImageView.image = [UIImage imageNamed:@"maindefaultimgrows.png"];
                        }
                    }];
                }
                
                
            }
            else{//from cashe
                @autoreleasepool {
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                         NSUserDomainMask, YES);
                    NSString *documentsDirectory = [paths objectAtIndex:0];
                    NSString *imagePath=[[NewsList objectAtIndex:index -1]objectForKey:@"image_path"];
                    documentsDirectory =[documentsDirectory stringByAppendingFormat:@"/%@",imagePath];
                    
                    [[ImageCache sharedInstance] downloadImageAtURL:[NSURL fileURLWithPath:documentsDirectory]completionHandler:^(UIImage *image) {
                        if (image) {
                            cell.NewsImageView.image = image;
                        }
                        else{
                            cell.NewsImageView.image = [UIImage imageNamed:@"maindefaultimgrows.png"];
                        }
                    }];
                    
                    
                }
            }
            
        }
        
    }
    @catch (NSException *exception) {
        
    }
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int noOfRows=0;
    @try {
        if (NewsList && [NewsList count]>0) {
            if ([NewsList count]>2) {
                noOfRows = floor(([NewsList count]-1)/2)+1;
                
            }
            else{
                noOfRows = 1;
            }
            
        }
    }
    @catch (NSException *exception) {
        
    }
    
    return noOfRows;
}
-(void)didSelectNewsCellAction:(UIButton*)sender
{
    @try {
        ipadNewsViewController *vcnews =[[ipadNewsViewController alloc]initWithNibName:@"ipadNewsViewController" bundle:nil];
        vcnews.AllNewsList= NewsList;
        vcnews.SelectedNewsIndex=sender.tag;
        vcnews.delegate=self;
        [self.navigationController pushViewController:vcnews animated:YES];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(NSString*)getImagePath:(NSString*)news_title
{
    @try {
        DB_Field *fldImagePath= [[DB_Field alloc]init];
        fldImagePath.FieldName=@"image_path";
        [[Cashing getObject]setDatabase:@"SDFAipad"];
        
        id result =[[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObject:fldImagePath] Tables:[NSArray arrayWithObject:@"news"] Where:[NSString stringWithFormat:@"title_ar='%@'",news_title] FromIndex:nil ToIndex:nil];
        return  [[result objectAtIndex:0] objectForKey:@"image_path"];
    }
    @catch (NSException *exception) {
        
    }
    
}
#pragma -mark news sclicing delegate
-(void)didFinishScliding{
    @try {
        [self.navigationController popViewControllerAnimated:NO];
        
    }
    @catch (NSException *exception) {
        
    }
}
@end
