//
//  PopoverController.m
//  sfda-ipad
//
//  Created by Assem Imam on 6/25/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "PopoverController.h"
#import "Language.h"
#import "ShareCell.h"

@interface PopoverController ()
{
    id ShareList;
}
@end

@implementation PopoverController
@synthesize delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    ShareList = [[Language getObject]getArrayWithKey:@"ShareOptions"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma -mark tableview datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        ShareCell *cell = (ShareCell*)[tableView dequeueReusableCellWithIdentifier:@"cleShae"];
        if (cell==nil) {
            cell=(ShareCell*)[[[NSBundle mainBundle]loadNibNamed:@"ShareCell" owner:nil options:nil]objectAtIndex:0];
        }
        cell.ShareTitleLabel.font=[UIFont fontWithName:@"GESSTwoLight-Light" size:13];
        cell.ShareImageView.image = [UIImage imageNamed:[[ShareList objectAtIndex:indexPath.row]objectForKey:@"icon"]];
        cell.ShareTitleLabel.text =[[ShareList objectAtIndex:indexPath.row]objectForKey:@"title"];
        return cell;
    }
    @catch (NSException *exception) {
        
    }
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ShareList count];
}
#pragma -mark tableview delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        if (self.delegate) {
            [self.delegate didSelectShareItemAtIndex:indexPath.row];
        }
    }
    @catch (NSException *exception) {
        
    }
   
}
@end
