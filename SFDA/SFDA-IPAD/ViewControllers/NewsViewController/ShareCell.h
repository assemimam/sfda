//
//  ShareCell.h
//  sfda-ipad
//
//  Created by Assem Imam on 6/25/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ShareImageView;
@property (weak, nonatomic) IBOutlet UILabel *ShareTitleLabel;

@end
