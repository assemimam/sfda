//
//  PopoverController.h
//  sfda-ipad
//
//  Created by Assem Imam on 6/25/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//
@protocol ShareDelegate <NSObject>

@optional
-(void)didSelectShareItemAtIndex:(int)index;

@end
#import <UIKit/UIKit.h>

@interface PopoverController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property(assign)id<ShareDelegate>delegate;
@end
