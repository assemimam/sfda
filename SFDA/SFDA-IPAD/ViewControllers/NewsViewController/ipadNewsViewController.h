

//
//  NewsViewController.h
//  sfda-ipad
//
//  Created by Assem Imam on 6/2/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//
@protocol NewsScliderDelegate <NSObject>
@optional
-(void)didFinishScliding;

@end
#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "PopoverController.h"

@interface ipadNewsViewController : UIViewController<UIScrollViewDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,ShareDelegate>
{
    __weak IBOutlet UIButton *FavouriteButton;
    __weak IBOutlet UIButton *NextButton;
    __weak IBOutlet UIScrollView *ContainerScrollView;
    __weak IBOutlet UIButton *IncreaseButton;
    __weak IBOutlet UIButton *DecreaseButton;
    __weak IBOutlet UIButton *PreviousButton;
    __weak IBOutlet UIView *FooterView;
}
@property(assign)id<NewsScliderDelegate>delegate;
- (IBAction)ShareAction:(UIButton *)sender;
@property(nonatomic)int SelectedNewsIndex;
@property(nonatomic,retain)id AllNewsList;
@property (weak, nonatomic) IBOutlet UILabel *FontSizeLabel;
- (IBAction)IncreaseButtonAction:(UIButton *)sender;
- (IBAction)DecreaseButtonAction:(UIButton *)sender;
- (IBAction)CloseAction:(UIButton *)sender;
- (IBAction)PreviousButtonAction:(UIButton *)sender;
- (IBAction)FavouriteAction:(UIButton *)sender;
- (IBAction)NextButtonAction:(UIButton *)sender;
@end
