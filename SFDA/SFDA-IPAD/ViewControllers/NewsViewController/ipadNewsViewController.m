//
//  NewsViewController.m
//  sfda-ipad
//
//  Created by Assem Imam on 6/2/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "ipadNewsViewController.h"
#import "CommonMethods.h"
#import "NewsItemView.h"
#import "ImageCache.h"
#import "LibrariesConstants.h"
#import "Cashing.h"
#import "Global.h"

@interface ipadNewsViewController ()
{
    int fontSize;
    id favourites ;
    UIPopoverController *popover;
}
@end

@implementation ipadNewsViewController
@synthesize AllNewsList,SelectedNewsIndex,delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(NSString*)getImagePath:(NSString*)news_title
{
    @try {
        DB_Field *fldImagePath= [[DB_Field alloc]init];
        fldImagePath.FieldName=@"image_path";
        [[Cashing getObject]setDatabase:@"SDFAipad"];
        
        id result =[[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObject:fldImagePath] Tables:[NSArray arrayWithObject:@"news"] Where:[NSString stringWithFormat:@"title_ar='%@'",news_title] FromIndex:nil ToIndex:nil];
        return  [[result objectAtIndex:0] objectForKey:@"image_path"];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)DrowAllNews
{
    @try {
        @autoreleasepool {
            for (UIView *view in ContainerScrollView.subviews) {
                [view removeFromSuperview];
            }
            int X=0;
            int Height=0;
            int index=1;
            NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"MM/dd/yyyy hh:mm:ss a"];
            [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
            
            NSDateFormatter *stringFormatter =[[NSDateFormatter alloc]init];
            [stringFormatter setDateFormat:@"yyyy/MM/dd"];
            [stringFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
            
            for (id item in self.AllNewsList) {
                NewsItemView *addView = (NewsItemView*)[[[NSBundle mainBundle]loadNibNamed:@"NewsItemView" owner:nil options:nil]objectAtIndex:0];
                Height= addView.frame.size.height;
                addView.tag=index;
                addView.NewsTitleLabel.text= [item objectForKey:@"TitleAr"];
                addView.NewsDateLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:20];
                addView.NewsTitleLabel.font = [UIFont fontWithName:@"GESSTwoBold-Bold" size:20];
                addView.NewsDescriptionLabel.font = [UIFont fontWithName:@"GESSTwoLight-Light" size:fontSize];
                
                NSMutableAttributedString* newsDescription = [[NSMutableAttributedString  alloc]initWithString:[[item objectForKey:@"DescriptionAr"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
               
                NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
                [style setLineSpacing:15];
                [style setAlignment:NSTextAlignmentRight];
                
                
                [newsDescription addAttribute:NSParagraphStyleAttributeName
                                           value:style
                                           range:NSMakeRange(0, [[item objectForKey:@"DescriptionAr"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length)];
                [addView.NewsDescriptionLabel setAdjustsFontSizeToFitWidth:YES];
                if(IS_DEVICE_RUNNING_IOS_7_AND_ABOVE){
                   addView.NewsDescriptionLabel.attributedText= newsDescription;
                }
                else{
                     addView.NewsDescriptionLabel.text= [[item objectForKey:@"DescriptionAr"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                [addView.NewsDescriptionLabel sizeToFit];
                
                [addView.ContainerScrollView setContentSize:CGSizeMake(addView.ContainerScrollView.frame.size.width, addView.NewsDescriptionLabel.frame.origin.y + addView.NewsDescriptionLabel.frame.size.height + 10)];
                addView.NewsDateLabel.text= [stringFormatter stringFromDate:[formatter dateFromString:[item objectForKey:@"PubDate"]]];
                [addView setFrame:CGRectMake(X, 0, addView.frame.size.width, addView.frame.size.height)];
                X+=addView.frame.size.width;
                
          //from cash
                    @autoreleasepool {
                        NSString *path =[item objectForKey:@"image_path"];// [self getImagePath:[item objectForKey:@"TitleAr"]];
                        [[ImageCache sharedInstance] downloadImageAtURL:[NSURL fileURLWithPath:path ]completionHandler:^(UIImage *image) {
                            if (image) {
                                addView.NewsImageView.image = image;
                            }
                            else{
                                addView.NewsImageView.image = [UIImage imageNamed:@"maindefaultimgrows.png"];
                            }
                        }];
                        
                    }
                
                [ContainerScrollView addSubview:addView];
                addView =nil;
                index++;
            }
            [ContainerScrollView setContentSize:CGSizeMake(X, Height)];
            [ContainerScrollView setContentOffset:CGPointMake(self.SelectedNewsIndex *ContainerScrollView.frame.size.width, 0)];
            if (SelectedNewsIndex==0) {
                 [PreviousButton setEnabled:NO];
            }
            [self performSelectorOnMainThread:@selector(HideLoading) withObject:nil waitUntilDone:NO];
        }
    }
    @catch (NSException *exception) {
        
    }
}
-(void)HideLoading
{
    HIDE_LOADING
    id news_item = [AllNewsList objectAtIndex:SelectedNewsIndex];
    [FavouriteButton setTag:[[news_item objectForKey:@"is_favourite"]intValue]];
    if ( [self CheckFavouriteNews:[news_item objectForKey:@"TitleAr"]]==0){
        [FavouriteButton setImage:[UIImage imageNamed:@"favoriteic2.png"] forState:UIControlStateNormal];
        [FavouriteButton setImage:[UIImage imageNamed:@"favoriteic2.png"] forState:UIControlStateHighlighted];
    }
    else{
        [FavouriteButton setImage:[UIImage imageNamed:@"favoriteicslected.png"] forState:UIControlStateNormal];
        [FavouriteButton setImage:[UIImage imageNamed:@"favoriteicslected.png"] forState:UIControlStateHighlighted];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    @try {
       
        if ([[NSUserDefaults standardUserDefaults]objectForKey:@"favourites"]) {
            favourites=[[[NSUserDefaults standardUserDefaults]objectForKey:@"favourites"]mutableCopy];
        }
        else{
            favourites=[[NSMutableArray alloc]init ];
        }
        
        fontSize=17;
        self.FontSizeLabel.font =[UIFont fontWithName:@"GESSTwoLight-Light" size:15];
        [CommonMethods  FormatView:FooterView WithShadowRadius:2 CornerRadius:0 ShadowOpacity:0.8 BorderWidth:0 BorderColor:[UIColor blackColor] ShadowColor:[UIColor blackColor] andXPosition:3];
        
        
        [NSThread detachNewThreadSelector:@selector(DrowAllNews) toTarget:self withObject:nil];

    }
    @catch (NSException *exception) {
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)IncreaseButtonAction:(UIButton *)sender {
    @try {
       
        NewsItemView*currentView=(NewsItemView*)[ContainerScrollView viewWithTag:self.SelectedNewsIndex+1];
        if (currentView.NewsDescriptionLabel.font.pointSize>=30) {
            fontSize=30;
            return;
        }
        fontSize++;
        currentView.NewsDescriptionLabel.font = [UIFont fontWithName:@"GESSTwoLight-Light" size:fontSize];
        [currentView.NewsDescriptionLabel sizeToFit];
        [currentView.ContainerScrollView setContentSize:CGSizeMake(currentView.ContainerScrollView.frame.size.width, currentView.NewsDescriptionLabel.frame.origin.y + currentView.NewsDescriptionLabel.frame.size.height + 10)];
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)DecreaseButtonAction:(UIButton *)sender {
    @try {
       
        NewsItemView*currentView=(NewsItemView*)[ContainerScrollView viewWithTag:self.SelectedNewsIndex+1];
        if (currentView.NewsDescriptionLabel.font.pointSize<=17) {
            fontSize=17;
            return;
        }
        fontSize--;
        currentView.NewsDescriptionLabel.font = [UIFont fontWithName:@"GESSTwoLight-Light" size:fontSize];
        [currentView.NewsDescriptionLabel sizeToFit];
        [currentView.ContainerScrollView setContentSize:CGSizeMake(currentView.ContainerScrollView.frame.size.width, currentView.NewsDescriptionLabel.frame.origin.y + currentView.NewsDescriptionLabel.frame.size.height + 10)];
    }
    @catch (NSException *exception) {
        
    }
    
   
}

- (IBAction)CloseAction:(UIButton *)sender {
    @try {
        if (self.delegate) {
            
            [self.delegate didFinishScliding];
        }
    }
    @catch (NSException *exception) {
        
    }
   
}

- (IBAction)PreviousButtonAction:(UIButton *)sender {
    @try {
        fontSize=17;
        self.SelectedNewsIndex--;
        if (SelectedNewsIndex<0) {
             self.SelectedNewsIndex=0;
            return;
        }
        [NextButton setEnabled:YES];
        [ContainerScrollView setContentOffset:CGPointMake(self.SelectedNewsIndex*ContainerScrollView.frame.size.width, 0) animated:YES];
       
        id news_item = [AllNewsList objectAtIndex:SelectedNewsIndex];
        [FavouriteButton setTag:[[news_item objectForKey:@"is_favourite"]intValue]];
         if ( [self CheckFavouriteNews:[news_item objectForKey:@"TitleAr"]]==0){
            [FavouriteButton setImage:[UIImage imageNamed:@"favoriteic2.png"] forState:UIControlStateNormal];
            [FavouriteButton setImage:[UIImage imageNamed:@"favoriteic2.png"] forState:UIControlStateHighlighted];
        }
        else{
            [FavouriteButton setImage:[UIImage imageNamed:@"favoriteicslected.png"] forState:UIControlStateNormal];
            [FavouriteButton setImage:[UIImage imageNamed:@"favoriteicslected.png"] forState:UIControlStateHighlighted];
        }
        
        if (SelectedNewsIndex==0) {
            [sender setEnabled:NO];
        }
    }
    @catch (NSException *exception) {
        
    }

}

- (IBAction)FavouriteAction:(UIButton *)sender {
    @try {
        id news_item = [AllNewsList objectAtIndex:SelectedNewsIndex];
        if (sender.tag==1){
            [FavouriteButton setImage:[UIImage imageNamed:@"favoriteic2.png"] forState:UIControlStateNormal];
            [FavouriteButton setImage:[UIImage imageNamed:@"favoriteic2.png"] forState:UIControlStateHighlighted];
            sender.tag=0;
        }
        else{
            [FavouriteButton setImage:[UIImage imageNamed:@"favoriteicslected.png"] forState:UIControlStateNormal];
            [FavouriteButton setImage:[UIImage imageNamed:@"favoriteicslected.png"] forState:UIControlStateHighlighted];
            sender.tag=1;
        }
        
        DB_Recored *addedRecored = [[DB_Recored alloc]init];
        
        DB_Field *fldFavourite = [[DB_Field alloc]init];
        fldFavourite.FieldDataType = FIELD_DATA_TYPE_NUMBER;
        fldFavourite.FieldName=@"is_favourite";
        fldFavourite.FieldValue =[NSString stringWithFormat:@"%i",sender.tag];
        
        DB_Field *fldTitleAr = [[DB_Field alloc]init];
        fldTitleAr.FieldName=@"title_ar";
        fldTitleAr.FieldDataType=FIELD_DATA_TYPE_TEXT;
        fldTitleAr.IS_PRIMARY_KEY=YES;
        fldTitleAr.FieldValue =[news_item objectForKey:@"TitleAr"];
        
        
        DB_Field *fldNewsID = [[DB_Field alloc]init];
        fldNewsID.FieldName=@"id";
        fldNewsID.FieldDataType=FIELD_DATA_TYPE_NUMBER;
        fldNewsID.IS_PRIMARY_KEY=YES;
        fldNewsID.FieldValue =[news_item objectForKey:@"$id"];
        
        addedRecored.Fields = [NSMutableArray arrayWithObjects:fldFavourite,fldTitleAr,fldNewsID, nil];
        [[Cashing getObject]setDatabase:@"SDFAipad"];
        [[Cashing getObject] CashData:[NSArray arrayWithObject:addedRecored] inTable:@"news"];
       
        
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)NextButtonAction:(UIButton *)sender {
    @try {
        fontSize=17;
        self.SelectedNewsIndex++;
        if (SelectedNewsIndex>=[AllNewsList count]) {
            SelectedNewsIndex=[AllNewsList count]-1;
           
            return;
        }
        [PreviousButton setEnabled:YES];
        [ContainerScrollView setContentOffset:CGPointMake(self.SelectedNewsIndex*ContainerScrollView.frame.size.width, 0) animated:YES];
        
        id news_item = [AllNewsList objectAtIndex:SelectedNewsIndex];
        [FavouriteButton setTag:[[news_item objectForKey:@"is_favourite"]intValue]];
        if ( [self CheckFavouriteNews:[news_item objectForKey:@"TitleAr"]]==0){
            [FavouriteButton setImage:[UIImage imageNamed:@"favoriteic2.png"] forState:UIControlStateNormal];
            [FavouriteButton setImage:[UIImage imageNamed:@"favoriteic2.png"] forState:UIControlStateHighlighted];
        }
        else{
            [FavouriteButton setImage:[UIImage imageNamed:@"favoriteicslected.png"] forState:UIControlStateNormal];
            [FavouriteButton setImage:[UIImage imageNamed:@"favoriteicslected.png"] forState:UIControlStateHighlighted];
        }
        
        if (SelectedNewsIndex>=[AllNewsList count]-1) {
            [sender setEnabled:NO];
           
        }
        
        
    }
    @catch (NSException *exception) {
        
    }

}
- (IBAction)ShareAction:(UIButton *)sender {
    @try {
        
        PopoverController *vc_popover = [[PopoverController alloc] initWithNibName:@"PopoverController" bundle:nil];
        vc_popover.delegate=self;
        popover = [[UIPopoverController alloc] initWithContentViewController:vc_popover];
        [popover setPopoverContentSize:CGSizeMake(200, 175)];
        popover.backgroundColor = [UIColor colorWithRed:247/255.0f green:247/255.0f blue:247/255.0f alpha:1];
        [popover presentPopoverFromRect:sender.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionRight animated:NO];

    }
    @catch (NSException *exception) {
        
    }
    
}
-(int)CheckFavouriteNews:(NSString*)news_title
{
    @try {
        int IS_FAVOURITE=0;
        
        DB_Field *fldFavourite = [[DB_Field alloc]init];
        fldFavourite.FieldName=@"is_favourite";
      
        [[Cashing getObject]setDatabase:@"SDFAipad"];
        
        id result =[[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObject:fldFavourite] Tables:[NSArray arrayWithObject:@"news"] Where:[NSString stringWithFormat:@"title_ar='%@'",news_title]FromIndex:nil ToIndex:nil];
        for (id item in result) {
            if ([[item  objectForKey:@"is_favourite"]intValue]==1) {
                IS_FAVOURITE =[[item  objectForKey:@"is_favourite"]intValue];
                break;
                
            }
            
        }
        return  IS_FAVOURITE;

    }
    @catch (NSException *exception) {
        
    }
   
}
#pragma -mark uiscrollview delegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    @try {
        [NextButton setEnabled:YES];
        [PreviousButton setEnabled:YES];
        
        CGFloat width = scrollView.frame.size.width;
        NSInteger page = (scrollView.contentOffset.x + (0.5f * width)) / width;
        SelectedNewsIndex=page;
        NewsItemView*currentView=(NewsItemView*)[ContainerScrollView viewWithTag:self.SelectedNewsIndex+1];
        fontSize =currentView.NewsDescriptionLabel.font.pointSize;
        id news_item = [AllNewsList objectAtIndex:SelectedNewsIndex];
           [FavouriteButton setTag:[[news_item objectForKey:@"is_favourite"]intValue]];
          if ( [self CheckFavouriteNews:[news_item objectForKey:@"TitleAr"]]==0){
            [FavouriteButton setImage:[UIImage imageNamed:@"favoriteic2.png"] forState:UIControlStateNormal];
             [FavouriteButton setImage:[UIImage imageNamed:@"favoriteic2.png"] forState:UIControlStateHighlighted];
        }
        else{
            [FavouriteButton setImage:[UIImage imageNamed:@"favoriteicslected.png"] forState:UIControlStateNormal];
            [FavouriteButton setImage:[UIImage imageNamed:@"favoriteicslected.png"] forState:UIControlStateHighlighted];
        }
        if (SelectedNewsIndex>=[AllNewsList count]-1) {
            [NextButton setEnabled:NO];
            
        }
        if (SelectedNewsIndex==0) {
            [PreviousButton setEnabled:NO];
        }

    }
    @catch (NSException *exception) {
        
    }
   
}
#pragma -mark share delegate
-(void)didSelectShareItemAtIndex:(int)index{
    @try {
        [popover dismissPopoverAnimated:YES];
        switch (index) {
            case 0:
            {
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[@"http://www.facebook.com/sharer.php?u=" stringByAppendingString:[[AllNewsList objectAtIndex:SelectedNewsIndex ]  objectForKey:@"Url"]]]];
                
            }
                break;
            case 1:
            {
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[@"http://www.twitter.com/share?url=" stringByAppendingString:[[AllNewsList objectAtIndex:SelectedNewsIndex ]  objectForKey:@"Url"]]]];
            }
                break;
            case 2:
            {
                [self sendNativeSMS];
            }
                break;
            case 3:
            {
                if ([MFMailComposeViewController canSendMail]){
                MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                controller.mailComposeDelegate = self;
                [controller setSubject:@""];
                [controller setMessageBody:[[AllNewsList objectAtIndex:SelectedNewsIndex ] objectForKey:@"Url"] isHTML:NO];
                
                if (controller) [self presentModalViewController:controller animated:YES];
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"ادخل بريدك الالكترونى"
                                                                   delegate:nil
                                                          cancelButtonTitle:@"موافق"
                                                          otherButtonTitles: nil];
                    [alert show];

                }
            }
                break;
        }
        
    }
    @catch (NSException *exception) {
        
    }

   
}

#pragma  mark MFMailCompose delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        //NSLog(@"It's away!");
    }
    [self dismissModalViewControllerAnimated:YES];

}

#pragma  mark MFMessage delegate
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    
    [self dismissModalViewControllerAnimated:YES];
  
}
-(void)sendNativeSMS{
    
    @try {
        Class messageClass = (NSClassFromString(@"MFMessageComposeViewController"));
        if (messageClass != nil) {
           
            if ([messageClass canSendText]) {
                MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
                picker.messageComposeDelegate = self;
                picker.recipients = [NSArray arrayWithObject:@""];
                picker.body = [[AllNewsList objectAtIndex:SelectedNewsIndex ] objectForKey:@"Url"];
                
                [self presentModalViewController:picker animated:YES];
                
                
            }else{
                UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:nil message:@"غير مسموح بارسال رسائل قصيره" delegate:self cancelButtonTitle:@"الغاء" otherButtonTitles:nil, nil];
                [Alert show];
            }
        }else{
            UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:nil message:@"غير مسموح بارسال رسائل قصيره" delegate:self cancelButtonTitle:@"الغاء" otherButtonTitles:nil, nil];
            [Alert show];
        }

    }
    @catch (NSException *exception) {
        
    }
   
    }

@end
