//
//  ElectronicServicesViewController.m
//  sfda-ipad
//
//  Created by Assem Imam on 5/27/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "ElectronicServicesViewController.h"
#import "MainMenuCell.h"
#import "ElectronicServicesCell.h"
#import "Cashing.h"
#import  "Database.h"

#define SFDA_SERVICES 0
#define FOOD_SERVICES 1
#define DRUGS_SERVICES 2
#define MEDICAL_SERVICES 3

@interface ElectronicServicesViewController ()
{
    NSMutableArray*MainMenuOptions;
    NSMutableArray *sfdaOptionsList;
    NSMutableArray *foodOptionsList;
    NSMutableArray *drugOptionsList;
    NSMutableArray *medicalOptionsList;
    int CurrentServicesCategory;
    int index;
}
@end

@implementation ElectronicServicesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([[NSUserDefaults standardUserDefaults]objectForKey:E_SERVICES_CASH_KEY]) {
        NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"EEEE MMMM yyyy  hh:mm a"];
        [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ar_SA"]];
        
        UpdateDateLabel.text = [formatter stringFromDate:[[NSUserDefaults standardUserDefaults]objectForKey:E_SERVICES_CASH_KEY]];
    }

    index=0;
    MainMenuOptions = [[NSMutableArray alloc]init];
    id MenuItems = [[Language getObject]getArrayWithKey:@"EServiceSectionList"];
    for (id item in MenuItems) {
        NSMutableDictionary*menuItem=[[NSMutableDictionary alloc]init];
        [menuItem setObject:[item objectForKey:@"image"] forKey:@"image"];
        [menuItem setObject:[item objectForKey:@"option"] forKey:@"option"];
        [menuItem setObject:@"0" forKey:@"selected"];
        [MainMenuOptions addObject:menuItem];
    }
    [[MainMenuOptions objectAtIndex:0]setObject:@"1" forKey:@"selected"];
    
    sfdaOptionsList = [[NSMutableArray alloc]init];
    foodOptionsList = [[NSMutableArray alloc]init];
    drugOptionsList = [[NSMutableArray alloc]init];
    medicalOptionsList = [[NSMutableArray alloc]init];
    CurrentServicesCategory = SFDA_SERVICES;
    SHOW_LOADINGINDICATOR;
    [NSThread detachNewThreadSelector:@selector(GetServices) toTarget:self withObject:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)HandleGetServicesResult:(id)result
{
    @try {
        HIDE_LOADINGINDICATOR;
        [ServicesTableView reloadData];
        CurrentServicesCategory=SFDA_SERVICES;
        [NSThread detachNewThreadSelector:@selector(CashServicesData:) toTarget:self withObject:result];

    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)HandleGetCasheServices
{
    @try {
        HIDE_LOADINGINDICATOR;
        index=0;
        [ServicesTableView reloadData];
        CurrentServicesCategory=SFDA_SERVICES;
    }
    @catch (NSException *exception) {
        
    }
    
}
-(id)GetCasheServices
{
    @try {
        DB_Field *fldTitle_ar = [[DB_Field alloc]init];
        fldTitle_ar.FieldName=@"title_ar";
        fldTitle_ar.FieldAlias= @"TitleAr";
        
        DB_Field *fldSectionId = [[DB_Field alloc]init];
        fldSectionId.FieldName=@"SectionId";
        fldSectionId.FieldAlias= @"SectionId";
        
        
        DB_Field *fldLink= [[DB_Field alloc]init];
        fldLink.FieldName=@"link";
        fldLink.FieldAlias= @"Link";
        [[Cashing getObject]setDatabase:@"SDFAipad"];
        id result =[[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldTitle_ar,fldSectionId,fldLink, nil] Tables:[NSArray arrayWithObject:@"EService"] Where:nil  FromIndex:nil ToIndex:nil];
        
        return result;

    }
    @catch (NSException *exception) {
        
    }
   
}
-(void)CashServicesData:(id)result
{
    @try {
          NSMutableArray *recordList = [[NSMutableArray alloc]init];
        for (id service in result) {
            DB_Recored *serviceRecored =[[DB_Recored alloc]init];
            DB_Field *fldTitle_ar = [[DB_Field alloc]init];
            fldTitle_ar.FieldDataType= FIELD_DATA_TYPE_TEXT;
            fldTitle_ar.FieldName=@"title_ar";
            fldTitle_ar.IS_PRIMARY_KEY=YES;
            fldTitle_ar.FieldValue = [service objectForKey:@"TitleAr"];
            
            DB_Field *fldSectionId = [[DB_Field alloc]init];
            fldSectionId.FieldDataType= FIELD_DATA_TYPE_NUMBER;
            fldSectionId.FieldName=@"SectionId";
            fldSectionId.IS_PRIMARY_KEY=YES;
            fldSectionId.FieldValue = [service objectForKey:@"SectionId"];
            
            
            DB_Field *fldLink= [[DB_Field alloc]init];
            fldLink.FieldDataType= FIELD_DATA_TYPE_TEXT;
            fldLink.FieldName=@"link";
            fldLink.IS_PRIMARY_KEY=YES;
            fldLink.FieldValue = [service objectForKey:@"Link"];
            
            serviceRecored.Fields = [[NSMutableArray alloc]initWithObjects:fldLink,fldSectionId,fldTitle_ar, nil];
            [recordList addObject:serviceRecored];
        }
        [[Cashing getObject]setDatabase:@"SDFAipad"];
        [[Database getObject]setDatabase:@"SDFAipad"];
        [[Database getObject]DeleteFromTable:@"EService" Where:nil];
        
        BOOL Success = [[Cashing getObject] CashData:recordList inTable:@"EService"];
        if (Success) {
            [[NSUserDefaults standardUserDefaults]setValue:[NSDate date] forKey:E_SERVICES_CASH_KEY];
            [[NSUserDefaults standardUserDefaults]synchronize];
           
                NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
                [formatter setDateFormat:@" EEEE MMMM yyyy  hh:mm a"];
                [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ar_SA"]];
                
                UpdateDateLabel.text = [formatter stringFromDate:[[NSUserDefaults standardUserDefaults]objectForKey:E_SERVICES_CASH_KEY]];
            
        }

    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)GetServices
{
    @try {
        if ([[NSUserDefaults standardUserDefaults]objectForKey:E_SERVICES_CASH_KEY]) {
            int  munites = ceil([[NSDate date] timeIntervalSinceDate:[[NSUserDefaults standardUserDefaults]objectForKey:E_SERVICES_CASH_KEY]]/60);
            if (munites>180) {
                id result = [Webservice GetElectronicServices];
                if ([result count]>0) {
                    for (int i=0; i<[result count]; i++) {
                        
                        NSInteger sectionId = [[[result objectAtIndex:i] objectForKey:@"SectionId"] intValue];
                        
                        if(sectionId == 1){
                            [sfdaOptionsList addObject:[result objectAtIndex:i]];
                        }else if(sectionId == 2){
                            [foodOptionsList addObject:[result objectAtIndex:i]];
                        }else if(sectionId == 3){
                            [drugOptionsList addObject:[result objectAtIndex:i]];
                        }else if(sectionId == 4){
                            [medicalOptionsList addObject:[result objectAtIndex:i]];
                        }
                        
                    }
                   [self performSelectorOnMainThread:@selector(HandleGetServicesResult:) withObject:result waitUntilDone:NO];

                }
                else{
                    id result = [self GetCasheServices];
                    if ([result count]>0) {
                        for (int i=0; i<[result count]; i++) {
                            
                            NSInteger sectionId = [[[result objectAtIndex:i] objectForKey:@"SectionId"] intValue];
                            
                            if(sectionId == 1){
                                [sfdaOptionsList addObject:[result objectAtIndex:i]];
                            }else if(sectionId == 2){
                                [foodOptionsList addObject:[result objectAtIndex:i]];
                            }else if(sectionId == 3){
                                [drugOptionsList addObject:[result objectAtIndex:i]];
                            }else if(sectionId == 4){
                                [medicalOptionsList addObject:[result objectAtIndex:i]];
                            }
                            
                        }
                        [self performSelectorOnMainThread:@selector(HandleGetCasheServices) withObject:nil waitUntilDone:NO];
                        
                    }

                }
            }
            else{
               
                id result = [self GetCasheServices];
                if ([result count]>0) {
                    for (int i=0; i<[result count]; i++) {
                        
                        NSInteger sectionId = [[[result objectAtIndex:i] objectForKey:@"SectionId"] intValue];
                        
                        if(sectionId == 1){
                            [sfdaOptionsList addObject:[result objectAtIndex:i]];
                        }else if(sectionId == 2){
                            [foodOptionsList addObject:[result objectAtIndex:i]];
                        }else if(sectionId == 3){
                            [drugOptionsList addObject:[result objectAtIndex:i]];
                        }else if(sectionId == 4){
                            [medicalOptionsList addObject:[result objectAtIndex:i]];
                        }
                        
                    }
                    [self performSelectorOnMainThread:@selector(HandleGetCasheServices) withObject:nil waitUntilDone:NO];
                    
                }
            }
        }
        else{
            id result = [Webservice GetElectronicServices];
            if ([result count]>0) {
                for (int i=0; i<[result count]; i++) {
                    
                    NSInteger sectionId = [[[result objectAtIndex:i] objectForKey:@"SectionId"] intValue];
                    
                    if(sectionId == 1){
                        [sfdaOptionsList addObject:[result objectAtIndex:i]];
                    }else if(sectionId == 2){
                        [foodOptionsList addObject:[result objectAtIndex:i]];
                    }else if(sectionId == 3){
                        [drugOptionsList addObject:[result objectAtIndex:i]];
                    }else if(sectionId == 4){
                        [medicalOptionsList addObject:[result objectAtIndex:i]];
                    }
                    
                }
                
                [self performSelectorOnMainThread:@selector(HandleGetServicesResult:) withObject:result waitUntilDone:NO];

                
            }
            else{
                id result = [self GetCasheServices];
                if ([result count]>0) {
                    for (int i=0; i<[result count]; i++) {
                        
                        NSInteger sectionId = [[[result objectAtIndex:i] objectForKey:@"SectionId"] intValue];
                        
                        if(sectionId == 1){
                            [sfdaOptionsList addObject:[result objectAtIndex:i]];
                        }else if(sectionId == 2){
                            [foodOptionsList addObject:[result objectAtIndex:i]];
                        }else if(sectionId == 3){
                            [drugOptionsList addObject:[result objectAtIndex:i]];
                        }else if(sectionId == 4){
                            [medicalOptionsList addObject:[result objectAtIndex:i]];
                        }
                        
                    }
                    [self performSelectorOnMainThread:@selector(HandleGetCasheServices) withObject:nil waitUntilDone:NO];
                    
                }
                
            }
        }
        
        
        }
    @catch (NSException *exception) {
        
    }
}
-(void)TapRegonizerAction:(UITapGestureRecognizer*)recognizer
{
    @try {
        
        if (!internetConnection) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            HIDE_LOADINGINDICATOR
            return;
        }
        
        NSString*url=@"";
        switch (CurrentServicesCategory) {
            case SFDA_SERVICES:
            {
                url = [[sfdaOptionsList objectAtIndex:recognizer.view.tag]objectForKey:@"Link"];
            }
                break;
            case FOOD_SERVICES:
            {
                url = [[foodOptionsList objectAtIndex:recognizer.view.tag]objectForKey:@"Link"];
            }
                break;
            case DRUGS_SERVICES:
            {
                url = [[drugOptionsList objectAtIndex:recognizer.view.tag]objectForKey:@"Link"];
            }
                break;
            case MEDICAL_SERVICES:
            {
                url = [[medicalOptionsList objectAtIndex:recognizer.view.tag]objectForKey:@"Link"];
            }
                break;
        }
         [[UIApplication sharedApplication]openURL:[NSURL URLWithString:url]];

    }
    @catch (NSException *exception) {
        
    }
    
}
#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id cell;
    @try {
        if (tableView==ServicesTableView) {
            cell=(ElectronicServicesCell*)[tableView dequeueReusableCellWithIdentifier:@"cleMenu"];
            if (cell==nil) {
                cell=(ElectronicServicesCell*)[[[NSBundle mainBundle]loadNibNamed:@"ElectronicServicesCell" owner:nil options:nil]objectAtIndex:0];
            }
             ((ElectronicServicesCell*)cell).ServiceNameLabel1.font= [UIFont fontWithName:@"GESSTwoBold-Bold" size:14];
             ((ElectronicServicesCell*)cell).ServiceNameLabel2.font= [UIFont fontWithName:@"GESSTwoBold-Bold" size:14];
            
            switch (CurrentServicesCategory) {
                case SFDA_SERVICES:
                {
                    if (indexPath.row<=[sfdaOptionsList count]-2) {
                        ((ElectronicServicesCell*)cell).ServiceNameLabel1.text= [[sfdaOptionsList objectAtIndex:indexPath.row]objectForKey:@"TitleAr"];
                        ((ElectronicServicesCell*)cell).ServiceNameLabel2.text= [[sfdaOptionsList objectAtIndex:indexPath.row +1]objectForKey:@"TitleAr"];
                    }
                }
                    break;
                    
                case FOOD_SERVICES:
                {
                    if (indexPath.row<=[foodOptionsList count]-2) {
                        ((ElectronicServicesCell*)cell).ServiceNameLabel1.text= [[foodOptionsList objectAtIndex:indexPath.row ]objectForKey:@"TitleAr"];
                        ((ElectronicServicesCell*)cell).ServiceNameLabel2.text= [[foodOptionsList objectAtIndex:indexPath.row +1]objectForKey:@"TitleAr"];
                    }
                }
                    break;
                case DRUGS_SERVICES:
                {
                    if (indexPath.row<=[drugOptionsList count]-2) {
                        ((ElectronicServicesCell*)cell).ServiceNameLabel1.text= [[drugOptionsList objectAtIndex:indexPath.row ]objectForKey:@"TitleAr"];
                        ((ElectronicServicesCell*)cell).ServiceNameLabel2.text= [[drugOptionsList objectAtIndex:indexPath.row +1]objectForKey:@"TitleAr"];
                    }
                }
                    break;
                case MEDICAL_SERVICES:
                {
                    if (indexPath.row<=[medicalOptionsList count]-2) {
                        ((ElectronicServicesCell*)cell).ServiceNameLabel1.text= [[medicalOptionsList objectAtIndex:indexPath.row ]objectForKey:@"TitleAr"];
                        ((ElectronicServicesCell*)cell).ServiceNameLabel2.text= [[medicalOptionsList objectAtIndex:indexPath.row +1]objectForKey:@"TitleAr"];
                    }
                }
                    break;
            }
            UITapGestureRecognizer *tapRecognizer =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(TapRegonizerAction:)];
            tapRecognizer.numberOfTapsRequired = 1;
            UITapGestureRecognizer *tapRecognizer2 =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(TapRegonizerAction:)];
            tapRecognizer2.numberOfTapsRequired = 1;
            ((ElectronicServicesCell*)cell).ServiceView1.tag=indexPath.row;
            ((ElectronicServicesCell*)cell).ServiceView2.tag=indexPath.row+1;
            [((ElectronicServicesCell*)cell).ServiceView1 addGestureRecognizer:tapRecognizer];
            [((ElectronicServicesCell*)cell).ServiceView2 addGestureRecognizer:tapRecognizer2];
            index+=2;
            
        }
        else{
            cell=(MainMenuCell*)[tableView dequeueReusableCellWithIdentifier:@"cleMenu"];
            if (cell==nil) {
                cell=(MainMenuCell*)[[[NSBundle mainBundle]loadNibNamed:@"MainMenuCell" owner:nil options:nil]objectAtIndex:0];
            }
            NSString*itemImageName =[[MainMenuOptions objectAtIndex:indexPath.row]objectForKey:@"image"];
            NSString*itemName=[[MainMenuOptions objectAtIndex:indexPath.row]objectForKey:@"option"];
            ((MainMenuCell*)cell).MenuItemIconImageView.image = [UIImage imageNamed:itemImageName];
            ((MainMenuCell*)cell).MenuItemLabel.text=itemName;
            ((MainMenuCell*)cell).MenuItemLabel.font= [UIFont fontWithName:@"GESSTwoBold-Bold" size:14];
            UIView *SelectionView = [[UIView alloc]initWithFrame:((MainMenuCell*)cell).frame];
            SelectionView.backgroundColor = [UIColor colorWithRed:111/255.0f  green:68/255.0f blue:66/255.0f alpha:1];
            if ([[[MainMenuOptions objectAtIndex:indexPath.row]objectForKey:@"selected"]intValue]==1) {
                ((MainMenuCell*)cell).contentView.backgroundColor = [UIColor colorWithRed:111/255.0f  green:68/255.0f blue:66/255.0f alpha:1];
                [((MainMenuCell*)cell).ArrowIndicatorImageView setHidden:NO];
            }
            else{
                ((MainMenuCell*)cell).contentView.backgroundColor = [UIColor colorWithRed:135/255.0f  green:78/255.0f blue:74/255.0f alpha:1];
                [((MainMenuCell*)cell).ArrowIndicatorImageView setHidden:YES];
            }
            
            ((MainMenuCell*)cell).selectedBackgroundView = SelectionView;
            ((MainMenuCell*)cell).SeperatorLabel.backgroundColor = MenuView.backgroundColor;
        }
    }
    @catch (NSException *exception) {
        
    }
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int RowsCount=0;
    if (tableView==ServicesTableView) {
        switch (CurrentServicesCategory) {
            case SFDA_SERVICES:
            {
                RowsCount = floor(([sfdaOptionsList count]/2));
                
            }
                break;
                
            case FOOD_SERVICES:
            {
                 RowsCount = floor(([foodOptionsList count]/2));
            }
                break;
            case DRUGS_SERVICES:
            {
                 RowsCount = floor(([drugOptionsList count]/2));
            }
                break;
            case MEDICAL_SERVICES:
            {
                 RowsCount = floor(([medicalOptionsList count]/2));
            }
                break;
        }

    }
    else{
     RowsCount=[MainMenuOptions count];
    }
    return RowsCount;
}
#pragma -mark uitableview delegate methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
       
        [self.ContainerView setUserInteractionEnabled:YES];
        if (tableView==ServicesTableView) {
            
        }
        else {
            for (id item in MainMenuOptions) {
                [item setObject:@"0" forKey:@"selected"];
            }
            [[MainMenuOptions objectAtIndex:indexPath.row] setObject:@"1" forKey:@"selected"];
            index=0;
            CurrentServicesCategory= indexPath.row;
            [ServicesTableView reloadData];
            [tableView reloadData];
      }
    }
    @catch (NSException *exception) {
        
    }
    
}
- (IBAction)ClearCashedData:(UIButton *)sender {
    @try {
        if (!internetConnection) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            [self.ContainerView setUserInteractionEnabled:YES];
            HIDE_LOADINGINDICATOR
            
        }

         [[NSUserDefaults standardUserDefaults] setValue:nil forKey:E_SERVICES_CASH_KEY];
        [[NSUserDefaults standardUserDefaults]synchronize];
         CLEARE_CASHED_DATA;
        ElectronicServicesViewController*vc_eServices =[[ElectronicServicesViewController alloc]init];
        self.navigationController.viewControllers = [NSArray arrayWithObject:vc_eServices];
        [self.navigationController popToRootViewControllerAnimated:NO];
        
    }
    @catch (NSException *exception) {
        
    }
    
}
@end
