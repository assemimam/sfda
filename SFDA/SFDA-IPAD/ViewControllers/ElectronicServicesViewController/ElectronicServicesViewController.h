//
//  ElectronicServicesViewController.h
//  sfda-ipad
//
//  Created by Assem Imam on 5/27/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "ipadMasterViewController.h"

@interface ElectronicServicesViewController : ipadMasterViewController<UITableViewDataSource,UITableViewDelegate>
{
    

    __weak IBOutlet UITableView *ServicesTableView;
}
@end
