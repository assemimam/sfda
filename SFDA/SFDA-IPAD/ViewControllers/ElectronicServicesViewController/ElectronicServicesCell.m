//
//  ElectronicServicesCell.m
//  sfda-ipad
//
//  Created by Assem Imam on 5/27/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "ElectronicServicesCell.h"

@implementation ElectronicServicesCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
