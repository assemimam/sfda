//
//  ElectronicServicesCell.h
//  sfda-ipad
//
//  Created by Assem Imam on 5/27/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ElectronicServicesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *ServiceNameLabel1;
@property (weak, nonatomic) IBOutlet UILabel *ServiceNameLabel2;
@property (weak, nonatomic) IBOutlet UIView *ServiceView1;
@property (weak, nonatomic) IBOutlet UIView *ServiceView2;

@end
