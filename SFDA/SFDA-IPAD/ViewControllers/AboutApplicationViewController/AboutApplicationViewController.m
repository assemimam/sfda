//
//  AboutApplicationViewController.m
//  sfda-ipad
//
//  Created by Assem Imam on 5/28/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "AboutApplicationViewController.h"
#import "CommonMethods.h"

@interface AboutApplicationViewController ()

@end

@implementation AboutApplicationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    NSMutableAttributedString* subTitleAttrString = [[NSMutableAttributedString  alloc]initWithString:AboutTextView.text];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setLineSpacing:10];
    [style setAlignment:NSTextAlignmentRight];
    [subTitleAttrString addAttribute:NSParagraphStyleAttributeName
                               value:style
                               range:NSMakeRange(0, AboutTextView.text.length)];
    AboutTextView.attributedText = subTitleAttrString;
    AboutTextView.font=[UIFont fontWithName:@"GESSTwoLight-Light" size:17];
    VersionNumberLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:14];
    Supportlabel.font=[UIFont fontWithName:@"GESSTwoLight-Light" size:14];
    EmailButton.titleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:14];
    [CommonMethods  FormatView:self.view WithShadowRadius:5 CornerRadius:5 ShadowOpacity:0.8 BorderWidth:1 BorderColor:[UIColor whiteColor] ShadowColor:[UIColor whiteColor] andXPosition:3];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)EmailAction:(UIButton *)sender {
    @try {
        if ([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
            controller.mailComposeDelegate = self;
            [controller setSubject:@""];
            UIImage *myImage = [UIImage imageNamed:@"logo.png"];
            NSData *imageData = UIImagePNGRepresentation(myImage);
            [controller addAttachmentData:imageData mimeType:@"image/png" fileName:@"logo"];
            [controller setToRecipients:[NSArray arrayWithObjects:@"smart-app@sfda.gov.sa",nil]];
            [controller setMessageBody:@"" isHTML:NO];
            if (controller) [self presentModalViewController:controller animated:YES];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"ادخل بريدك الالكترونى"
                                                           delegate:nil
                                                  cancelButtonTitle:@"موافق"
                                                  otherButtonTitles: nil];
            [alert show];

            
        }

    }
    @catch (NSException *exception) {
        
    }
   
}

- (IBAction)CloseAction:(UIButton *)sender {
    [self dismissModalViewControllerAnimated:YES];
}

#pragma -mark mailcomposer delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    @try {
        UIAlertView * alert;
        switch (result)
        {
            case MFMailComposeResultCancelled:
                
                //NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued");
                break;
            case MFMailComposeResultSaved:
                //NSLog(@"Mail saved: you saved the email message in the Drafts folder");
                break;
            case MFMailComposeResultSent:
            {
                alert = [[UIAlertView alloc]initWithTitle:@"الدعم الفني" message:@"لقد تم ارسال الايمل بنجاح " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }
                break;
            case MFMailComposeResultFailed:
            {
                alert = [[UIAlertView alloc]initWithTitle:@"الدعم الفني" message:@"لم يتم ارسال الايميل حاول مره اخري " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }
                break;
            default:
                //NSLog(@"Mail not sent");
                break;
        }
        
        [self dismissModalViewControllerAnimated:YES];
    }
    @catch (NSException *exception) {
        
    }
    
    
}



@end
