//
//  AboutApplicationViewController.h
//  sfda-ipad
//
//  Created by Assem Imam on 5/28/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
@interface AboutApplicationViewController : UIViewController<MFMailComposeViewControllerDelegate>
{
    
    __weak IBOutlet UIButton *EmailButton;
    __weak IBOutlet UIBarButtonItem *CloseButton;
    __weak IBOutlet UILabel *Supportlabel;
    __weak IBOutlet UILabel *VersionNumberLabel;
    __weak IBOutlet UITextView *AboutTextView;
}
- (IBAction)EmailAction:(UIButton *)sender;
- (IBAction)CloseAction:(UIButton *)sender;
@end
