//
//  PDFViewController.h
//  sfda-ipad
//
//  Created by Assem Imam on 5/29/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReaderViewController.h"
@interface PDFViewController : UIViewController<ReaderViewControllerDelegate>

@end
