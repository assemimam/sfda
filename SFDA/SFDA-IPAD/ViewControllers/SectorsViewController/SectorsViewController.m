//
//  SectorsViewController.m
//  sfda-ipad
//
//  Created by Assem Imam on 5/22/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "SectorsViewController.h"
#import "MainMenuCell.h"
#import "AboutView.h"
#import "CustomWebView.h"
#import "AwarnessCenterView.h"
#import "FAQView.h"
#import "FoodSearchView.h"
#import "ipadBarcodeScannerViewController.h"
#import "DrugsSearchView.h"
#import "MedicalSearchView.h"
#import "DrugsSearchListView.h"
#import "DrugDetailsView.h"
#import "MedicalDetailsView.h"
#import "MedicalSearchListView.h"
#import "BarcodeResultView.h"
#import "FoodDetailsView.h"
#import "FoodSearchListView.h"
#import <AVFoundation/AVFoundation.h>

#define SECTOR_NEWS 0
#define ABOUT_SECTOR 1
#define SECTOR_SEARCH 2
#define FOOD_AWARNESS_CENTER 3
#define MEDICAL_AWARNESS_CENTER 3
#define DRUGS_REPORT_SIDE_EFFECT 3
#define FOOD_INFORMATION_CENTER 4
#define MEDICAL_INFORMATION_CENTER 4
#define DRUGS_AWARNESS_CENTER 4
#define SECTOR_FAQ 5
#define DRUGS_FAQ 6
#define DRUGS_INFORMATION_CENTER 5
#define DRUGS_SEARCH_TAG 1000
#define DRUGS_SEARCH_LIST_TAG 2000
#define DRUGS_SEARCH_DETAILS_TAG 3000
#define MEDICAL_SEARCH_TAG 4000
#define MEDICAL_SEARCH_LIST_TAG 5000
#define MEDICAL_SEARCH_DETAILS_TAG 6000
#define BARCODE_RESUT_TAG 7000
#define FOOD_SEARCH_TAG 8000
#define FOOD_SEARCH_LIST_TAG 9000
#define FOOD_SEARCH_DETAILS_TAG 10000

@interface SectorsViewController ()<AwarnessDelegate,FoodSearchDelegate,BarCodeScannerDelegate,MedicalSearchDelegate,DrugsSearchDelegate,DrugsSearchListDelegate,DrugDetailsDelegate,MedicalDetailsDelegate,MedicalSearchListDelegate,FoodSearchListDelegate,FoodDetailsDelegate>
{

    NSMutableArray*MainMenuOptions;
    NewsView*v_news;
    AwarnessCenterView*v_awarness;
    BOOL HIDE_DRUGS_SEARCH_VIEW;
    BOOL HIDE_MEDICAL_SEARCH_VIEW;
    BOOL NEWS_FINISH_LOADING;
    NSString *SectorSearchString;
    AwarnessCell *CurrentAwarnessCell;
    int CurrentAwarnessCellIndex;
    BOOL isGranted;
}

@end

@implementation SectorsViewController
@synthesize Sector;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidDisappear:(BOOL)animated
{
    v_awarness.delegate=nil;
      v_news.delegate=nil;
}
-(void)viewWillAppear:(BOOL)animated
{
    v_awarness.delegate=self;
    v_news.delegate=self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    SectorSearchString=@"";
    HIDE_DRUGS_SEARCH_VIEW=YES;
    HIDE_MEDICAL_SEARCH_VIEW=YES;
    CurrentAwarnessCellIndex=-1;
    
    NEWS_FINISH_LOADING=NO;
    MainMenuOptions = [[NSMutableArray alloc]init];
    id MenuItems ;
    NSString *sectorCashedKey=nil;
    switch (self.Sector) {
        case FOODS_SECTOR:
        {
            TitleLabel.text=@"قطاع الغذاء";
            sectorCashedKey=NEWS_FOOD_CASH_KEY;
            MenuItems = [[Language getObject]getArrayWithKey:@"FoodSectorMenuOptions"];
            MenuView.backgroundColor = [UIColor colorWithRed:82/255.0f green:160/255.0f blue:102/255.0f alpha:1];
            UpdateView.backgroundColor=[UIColor colorWithRed:71/255.0f  green:134/255.0f blue:87/255.0f alpha:1];
        }
            break;
        case DRUGS_SECTOR:
        {
            TitleLabel.text=@"قطاع الدواء";
            sectorCashedKey=NEWS_DRUGS_CASH_KEY;
            MenuItems = [[Language getObject]getArrayWithKey:@"DrugsSectorMenuOptions"];
            MenuView.backgroundColor = [UIColor colorWithRed:65/255.0f green:136/255.0f blue:175/255.0f alpha:1];
            UpdateView.backgroundColor=[UIColor colorWithRed:59/255.0f  green:116/255.0f blue:147/255.0f alpha:1];
        }
            break;
        case MEDICAL_SECTOR:
        {
            TitleLabel.text=@"قطاع الاجهزة الطبية";
            sectorCashedKey=NEWS_MEDICAL_CASH_KEY;
            MenuItems = [[Language getObject]getArrayWithKey:@"MedicalServicesSectorMenuOptions"];
            MenuView.backgroundColor = [UIColor colorWithRed:216/255.0f green:102/255.0f blue:86/255.0f alpha:1];
            UpdateView.backgroundColor=[UIColor colorWithRed:178/255.0f  green:87/255.0f blue:74/255.0f alpha:1];
        }
            break;
        case SFDA_SECTOR:
        {
            
        }
            break;
    }
    
    if ([[NSUserDefaults standardUserDefaults]valueForKey:sectorCashedKey]) {
        NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"EEEE MMMM yyyy  hh:mm a"];
        [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ar_SA"]];
        
        UpdateDateLabel.text = [formatter stringFromDate:[[NSUserDefaults standardUserDefaults]valueForKey:sectorCashedKey]];
    }
    
    for (id item in MenuItems) {
        NSMutableDictionary*menuItem=[[NSMutableDictionary alloc]init];
        [menuItem setObject:[item objectForKey:@"image"] forKey:@"image"];
        [menuItem setObject:[item objectForKey:@"option"] forKey:@"option"];
        [menuItem setObject:@"0" forKey:@"selected"];
        [MainMenuOptions addObject:menuItem];
    }
   
    [[MainMenuOptions objectAtIndex:0]setObject:@"1" forKey:@"selected"];
    v_news =(NewsView*)[[[NSBundle mainBundle]loadNibNamed:@"NewsView" owner:nil options:nil]objectAtIndex:0];
    v_news.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
    v_news.delegate=nil;
    v_news.OpenType=OPEN_TYPE_REGULAR;
    v_news.delegate=self;
    v_news.Sector=Sector;
    v_news.NewsHeaderView.backgroundColor=MenuView.backgroundColor;
    [v_news LoadNews];
    [self.ContainerView addSubview: v_news];
    [v_news setHidden:YES];
    

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MainMenuCell*cell;
    @try {
        cell=(MainMenuCell*)[tableView dequeueReusableCellWithIdentifier:@"cleMenu"];
        if (cell==nil) {
            cell=(MainMenuCell*)[[[NSBundle mainBundle]loadNibNamed:@"MainMenuCell" owner:nil options:nil]objectAtIndex:0];
        }
        NSString*itemImageName =[[MainMenuOptions objectAtIndex:indexPath.row]objectForKey:@"image"];
        NSString*itemName=[[MainMenuOptions objectAtIndex:indexPath.row]objectForKey:@"option"];
        cell.MenuItemIconImageView.image = [UIImage imageNamed:itemImageName];
        cell.MenuItemLabel.text=itemName;
        cell.MenuItemLabel.font= [UIFont fontWithName:@"GESSTwoBold-Bold" size:14];
        UIView *SelectionView = [[UIView alloc]initWithFrame:cell.frame];
        switch (self.Sector) {
            case FOODS_SECTOR:
            {
                SelectionView.backgroundColor = [UIColor colorWithRed:60/255.0f  green:107/255.0f blue:72/255.0f alpha:1];
                if ([[[MainMenuOptions objectAtIndex:indexPath.row]objectForKey:@"selected"]intValue]==1) {
                     cell.contentView.backgroundColor = [UIColor colorWithRed:60/255.0f  green:107/255.0f blue:72/255.0f alpha:1];
                    [cell.ArrowIndicatorImageView setHidden:NO];
                }
                else{
                    cell.contentView.backgroundColor = [UIColor colorWithRed:71/255.0f  green:134/255.0f blue:87/255.0f alpha:1];
                    [cell.ArrowIndicatorImageView setHidden:YES];
                }
            
            }
                break;
            case DRUGS_SECTOR:
            {
                SelectionView.backgroundColor = [UIColor colorWithRed:53/255.0f  green:96/255.0f blue:119/255.0f alpha:1];
                if ([[[MainMenuOptions objectAtIndex:indexPath.row]objectForKey:@"selected"]intValue]==1) {
                     cell.contentView.backgroundColor = [UIColor colorWithRed:53/255.0f  green:96/255.0f blue:119/255.0f alpha:1];
                    [cell.ArrowIndicatorImageView setHidden:NO];
                }
                else{
                     cell.contentView.backgroundColor = [UIColor colorWithRed:59/255.0f  green:116/255.0f blue:147/255.0f alpha:1];
                    [cell.ArrowIndicatorImageView setHidden:YES];
                }
               
            }
                break;
            case MEDICAL_SECTOR:
            {
                SelectionView.backgroundColor = [UIColor colorWithRed:141/255.0f  green:72/255.0f blue:63/255.0f alpha:1];
                if ([[[MainMenuOptions objectAtIndex:indexPath.row]objectForKey:@"selected"]intValue]==1) {
                     cell.contentView.backgroundColor = [UIColor colorWithRed:141/255.0f  green:72/255.0f blue:63/255.0f alpha:1];
                    [cell.ArrowIndicatorImageView setHidden:NO];
                }
                else{
                     cell.contentView.backgroundColor = [UIColor colorWithRed:178/255.0f  green:87/255.0f blue:74/255.0f alpha:1];
                    [cell.ArrowIndicatorImageView setHidden:YES];
                }
               
            }
                break;
            case SFDA_SECTOR:
            {
                
            }
                break;
        }
    
        cell.selectedBackgroundView = SelectionView;
        cell.SeperatorLabel.backgroundColor = MenuView.backgroundColor;
    }
    @catch (NSException *exception) {
        
    }
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [MainMenuOptions count];
}
#pragma -mark uitableview delegate methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
         [self.ContainerView setUserInteractionEnabled:YES];
        switch (self.Sector) {
            case FOODS_SECTOR:
            {
                switch (indexPath.row) {
                    case SECTOR_NEWS:
                    {
                        for (UIView*view in [self.ContainerView subviews]) {
                            [view removeFromSuperview];
                        }
                        [self.ContainerView setAlpha:0];
                        [UIView animateWithDuration:0.5 animations:^{
                            [self.ContainerView setAlpha:1];
                        }];
                        
                        v_news.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
                        v_news.OpenType=OPEN_TYPE_REGULAR;
                        v_news.delegate=nil;
                        v_news.delegate=self;
                        v_news.Sector=Sector;
                        [v_news LoadNews];
                        [self.ContainerView addSubview: v_news];
                    }
                        break;
                        
                    case ABOUT_SECTOR:
                    {
                        for (UIView*view in [self.ContainerView subviews]) {
                            [view removeFromSuperview];
                        }
                        [self.ContainerView setAlpha:0];
                        [UIView animateWithDuration:0.5 animations:^{
                            [self.ContainerView setAlpha:1];
                        }];
                        
                        AboutView*v_about =(AboutView*)[[[NSBundle mainBundle]loadNibNamed:@"AboutView" owner:nil options:nil]objectAtIndex:0];
                        v_about.Sector=Sector;
                        v_about.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
                        [self.ContainerView addSubview: v_about];
                        
                    }
                        break;
                    case SECTOR_SEARCH:
                    {
                        for (UIView*view in [self.ContainerView subviews]) {
                            [view removeFromSuperview];
                        }
                        [self.ContainerView setAlpha:0];
                        [UIView animateWithDuration:0.5 animations:^{
                            [self.ContainerView setAlpha:1];
                        }];

                            FoodSearchView*v_search =(FoodSearchView*)[[[NSBundle mainBundle]loadNibNamed:@"FoodSearchView" owner:nil options:nil]objectAtIndex:0];
                            v_search.delegate=self;
                            v_search.tag=FOOD_SEARCH_TAG;
                            v_search.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
                            [self.ContainerView addSubview: v_search];
                        
                    }
                        break;
                    case FOOD_AWARNESS_CENTER:
                    {
                        for (UIView*view in [self.ContainerView subviews]) {
                            [view removeFromSuperview];
                        }
                        [self.ContainerView setAlpha:0];
                        [UIView animateWithDuration:0.5 animations:^{
                            [self.ContainerView setAlpha:1];
                        }];
                        
                        v_awarness =(AwarnessCenterView*)[[[NSBundle mainBundle]loadNibNamed:@"AwarnessCenterView" owner:nil options:nil]objectAtIndex:0];
                        v_awarness.delegate=nil;
                        v_awarness.delegate=self;
                        v_awarness.Sector=Sector;
                        [self.ContainerView addSubview: v_awarness];
                        
                    }
                        break;
                    case FOOD_INFORMATION_CENTER:
                    {
                        for (UIView*view in [self.ContainerView subviews]) {
                            [view removeFromSuperview];
                        }
                        [self.ContainerView setAlpha:0];
                        [UIView animateWithDuration:0.5 animations:^{
                            [self.ContainerView setAlpha:1];
                        }];
                        

                        CustomWebView*v_web =(CustomWebView*)[[[NSBundle mainBundle]loadNibNamed:@"CustomWebView" owner:nil options:nil]objectAtIndex:0];
                        v_web.Url=@"http://sfda.gov.sa/ar/mobile/food/Pages/default.aspx";
                        v_web.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
                        [self.ContainerView addSubview: v_web];

                    }
                        break;
                    case SECTOR_FAQ:
                    {
                        for (UIView*view in [self.ContainerView subviews]) {
                            [view removeFromSuperview];
                        }
                        [self.ContainerView setAlpha:0];
                        [UIView animateWithDuration:0.5 animations:^{
                            [self.ContainerView setAlpha:1];
                        }];
                        FAQView*v_FAQ =(FAQView*)[[[NSBundle mainBundle]loadNibNamed:@"FAQView" owner:nil options:nil]objectAtIndex:0];
                        v_FAQ.Sector=Sector;
                        v_FAQ.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
                        [self.ContainerView addSubview: v_FAQ];
                        
                    }
                        break;
                        
                }

            }
                break;
                
            case DRUGS_SECTOR:
            {
                switch (indexPath.row) {
                    case SECTOR_NEWS:
                    {
                        HIDE_DRUGS_SEARCH_VIEW=YES;
                        for (UIView*view in [self.ContainerView subviews]) {
                            [view removeFromSuperview];
                        }
                        [self.ContainerView setAlpha:0];
                        [UIView animateWithDuration:0.5 animations:^{
                            [self.ContainerView setAlpha:1];
                        }];
                        
                        v_news.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
                        v_news.delegate=nil;
                        v_news.OpenType=OPEN_TYPE_REGULAR;
                        v_news.delegate=self;
                        v_news.Sector=Sector;
                        [v_news LoadNews];
                        [self.ContainerView addSubview: v_news];
                    }
                        break;
                        
                    case ABOUT_SECTOR:
                    {
                        HIDE_DRUGS_SEARCH_VIEW=YES;
                        for (UIView*view in [self.ContainerView subviews]) {
                            [view removeFromSuperview];
                        }
                        [self.ContainerView setAlpha:0];
                        [UIView animateWithDuration:0.5 animations:^{
                            [self.ContainerView setAlpha:1];
                        }];
                        
                        AboutView*v_about =(AboutView*)[[[NSBundle mainBundle]loadNibNamed:@"AboutView" owner:nil options:nil]objectAtIndex:0];
                        v_about.Sector=Sector;
                        v_about.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
                        [self.ContainerView addSubview: v_about];
                        
                    }
                        break;
                    case SECTOR_SEARCH:
                    {
                        if ( !HIDE_DRUGS_SEARCH_VIEW) {
                            return;
                        }
                        for (UIView*view in [self.ContainerView subviews]) {
                            if ((view.tag==DRUGS_SEARCH_TAG )| (view.tag==DRUGS_SEARCH_LIST_TAG )| (view.tag==DRUGS_SEARCH_DETAILS_TAG )) {
                                
                            }
                            else{
                                [view removeFromSuperview];
                            }
                            
                        }
                        [self.ContainerView setAlpha:0];
                        [UIView animateWithDuration:0.5 animations:^{
                            [self.ContainerView setAlpha:1];
                        }];
                        DrugsSearchView*v_search;

                        v_search.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
                        if ([self.ContainerView viewWithTag:DRUGS_SEARCH_TAG]) {
                            v_search =(DrugsSearchView*)[self.ContainerView viewWithTag:DRUGS_SEARCH_TAG];
                            v_search.delegate=self;
                            v_search.tag=DRUGS_SEARCH_TAG;
                        }
                        else{
                          v_search=(DrugsSearchView*)[[[NSBundle mainBundle]loadNibNamed:@"DrugsSearchView" owner:nil options:nil]objectAtIndex:0];
                            v_search.delegate=self;
                            v_search.tag=DRUGS_SEARCH_TAG;
                            [self.ContainerView addSubview: v_search];
                        }
                        [v_search setHidden:NO];
                        [self.ContainerView bringSubviewToFront:v_search];
                        HIDE_DRUGS_SEARCH_VIEW =NO;
                        
                        
                    }
                        break;
                    case DRUGS_AWARNESS_CENTER:
                    {
                        HIDE_DRUGS_SEARCH_VIEW=YES;
                        for (UIView*view in [self.ContainerView subviews]) {
                            [view removeFromSuperview];
                        }
                        [self.ContainerView setAlpha:0];
                        [UIView animateWithDuration:0.5 animations:^{
                            [self.ContainerView setAlpha:1];
                        }];
                        
                        v_awarness =(AwarnessCenterView*)[[[NSBundle mainBundle]loadNibNamed:@"AwarnessCenterView" owner:nil options:nil]objectAtIndex:0];
                        v_awarness.delegate=nil;
                        v_awarness.delegate=self;
                        v_awarness.Sector=Sector;
                        [self.ContainerView addSubview: v_awarness];
                    }
                        break;
                    case DRUGS_REPORT_SIDE_EFFECT:
                    {
                        HIDE_DRUGS_SEARCH_VIEW=YES;
                        for (UIView*view in [self.ContainerView subviews]) {
                            [view removeFromSuperview];
                        }
                        [self.ContainerView setAlpha:0];
                        [UIView animateWithDuration:0.5 animations:^{
                            [self.ContainerView setAlpha:1];
                        }];
                        
                        CustomWebView*v_web =(CustomWebView*)[[[NSBundle mainBundle]loadNibNamed:@"CustomWebView" owner:nil options:nil]objectAtIndex:0];
                        v_web.Url=@"https://ade.sfda.gov.sa/";
                        v_web.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
                        [self.ContainerView addSubview: v_web];

            
                    }
                        break;
                    case DRUGS_INFORMATION_CENTER:
                    {
                            HIDE_DRUGS_SEARCH_VIEW=YES;
                        for (UIView*view in [self.ContainerView subviews]) {
                            [view removeFromSuperview];
                        }
                        [self.ContainerView setAlpha:0];
                        [UIView animateWithDuration:0.5 animations:^{
                            [self.ContainerView setAlpha:1];
                        }];
                        
                        CustomWebView*v_web =(CustomWebView*)[[[NSBundle mainBundle]loadNibNamed:@"CustomWebView" owner:nil options:nil]objectAtIndex:0];
                        v_web.Url=@"http://sfda.gov.sa/ar/mobile/drug/Pages/default.aspx";
                        v_web.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
                        [self.ContainerView addSubview: v_web];

                    }
                        break;
                    case DRUGS_FAQ:
                    {
                        for (UIView*view in [self.ContainerView subviews]) {
                            [view removeFromSuperview];
                        }
                        [self.ContainerView setAlpha:0];
                        [UIView animateWithDuration:0.5 animations:^{
                            [self.ContainerView setAlpha:1];
                        }];
                        FAQView*v_FAQ =(FAQView*)[[[NSBundle mainBundle]loadNibNamed:@"FAQView" owner:nil options:nil]objectAtIndex:0];
                        v_FAQ.Sector=Sector;
                        v_FAQ.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
                        [self.ContainerView addSubview: v_FAQ];
                        
                    }
                        break;
                }

            }
                break;
            case MEDICAL_SECTOR:
            {
                switch (indexPath.row) {
                    case SECTOR_NEWS:
                    {
                        HIDE_MEDICAL_SEARCH_VIEW=YES;
                        for (UIView*view in [self.ContainerView subviews]) {
                            [view removeFromSuperview];
                        }
                        [self.ContainerView setAlpha:0];
                        [UIView animateWithDuration:0.5 animations:^{
                            [self.ContainerView setAlpha:1];
                        }];
                        
                        v_news.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
                        v_news.delegate=nil;
                        v_news.delegate=self;
                        v_news.OpenType=OPEN_TYPE_REGULAR;
                        v_news.Sector=Sector;
                        [v_news LoadNews];
                        [self.ContainerView addSubview: v_news];
                    }
                        break;
                        
                    case ABOUT_SECTOR:
                    {
                        HIDE_MEDICAL_SEARCH_VIEW=YES;
                        for (UIView*view in [self.ContainerView subviews]) {
                            [view removeFromSuperview];
                        }
                        [self.ContainerView setAlpha:0];
                        [UIView animateWithDuration:0.5 animations:^{
                            [self.ContainerView setAlpha:1];
                        }];
                        
                        AboutView*v_about =(AboutView*)[[[NSBundle mainBundle]loadNibNamed:@"AboutView" owner:nil options:nil]objectAtIndex:0];
                        v_about.Sector=Sector;
                        v_about.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
                        [self.ContainerView addSubview: v_about];
                        
                    }
                        break;
                    case SECTOR_SEARCH:
                    {
                        
                        if ( !HIDE_MEDICAL_SEARCH_VIEW) {
                            return;
                        }
                        for (UIView*view in [self.ContainerView subviews]) {
                            if ((view.tag==MEDICAL_SEARCH_TAG )| (view.tag==MEDICAL_SEARCH_LIST_TAG )| (view.tag==MEDICAL_SEARCH_DETAILS_TAG )) {
                                
                            }
                            else{
                                [view removeFromSuperview];
                            }
                            
                        }
                        [self.ContainerView setAlpha:0];
                        [UIView animateWithDuration:0.5 animations:^{
                            [self.ContainerView setAlpha:1];
                        }];
                        MedicalSearchView*v_search;
                        
                        v_search.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
                        if ([self.ContainerView viewWithTag:MEDICAL_SEARCH_TAG]) {
                            v_search =(MedicalSearchView*)[self.ContainerView viewWithTag:MEDICAL_SEARCH_TAG];
                            v_search.delegate=self;
                            v_search.tag=DRUGS_SEARCH_TAG;
                        }
                        else{
                            v_search=(MedicalSearchView*)[[[NSBundle mainBundle]loadNibNamed:@"MedicalSearchView" owner:nil options:nil]objectAtIndex:0];
                            v_search.delegate=self;
                            v_search.tag=MEDICAL_SEARCH_TAG;
                            [self.ContainerView addSubview: v_search];
                        }
                        [v_search setHidden:NO];
                        [self.ContainerView bringSubviewToFront:v_search];
                        HIDE_MEDICAL_SEARCH_VIEW =NO;
       
                    }
                        break;
                    case MEDICAL_AWARNESS_CENTER:
                    {
                        HIDE_MEDICAL_SEARCH_VIEW=YES;
                        for (UIView*view in [self.ContainerView subviews]) {
                            [view removeFromSuperview];
                        }
                        [self.ContainerView setAlpha:0];
                        [UIView animateWithDuration:0.5 animations:^{
                            [self.ContainerView setAlpha:1];
                        }];
                        
                        v_awarness =(AwarnessCenterView*)[[[NSBundle mainBundle]loadNibNamed:@"AwarnessCenterView" owner:nil options:nil]objectAtIndex:0];
                        v_awarness.delegate=nil;
                        v_awarness.delegate=self;
                        v_awarness.Sector=Sector;
                        [self.ContainerView addSubview: v_awarness];
                    }
                        break;
                    case MEDICAL_INFORMATION_CENTER:
                    {
                        HIDE_MEDICAL_SEARCH_VIEW=YES;
                        for (UIView*view in [self.ContainerView subviews]) {
                            [view removeFromSuperview];
                        }
                        [self.ContainerView setAlpha:0];
                        [UIView animateWithDuration:0.5 animations:^{
                            [self.ContainerView setAlpha:1];
                        }];
                        
                        CustomWebView*v_web =(CustomWebView*)[[[NSBundle mainBundle]loadNibNamed:@"CustomWebView" owner:nil options:nil]objectAtIndex:0];
                        v_web.Url=@"http://sfda.gov.sa/ar/mobile/medicaldevices/Pages/default.aspx";
                        v_web.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
                        [self.ContainerView addSubview: v_web];
            
                    }
                        break;
                    case SECTOR_FAQ:
                    {
                        HIDE_MEDICAL_SEARCH_VIEW=YES;
                        for (UIView*view in [self.ContainerView subviews]) {
                            [view removeFromSuperview];
                        }
                        [self.ContainerView setAlpha:0];
                        [UIView animateWithDuration:0.5 animations:^{
                            [self.ContainerView setAlpha:1];
                        }];
                        FAQView*v_FAQ =(FAQView*)[[[NSBundle mainBundle]loadNibNamed:@"FAQView" owner:nil options:nil]objectAtIndex:0];
                        v_FAQ.Sector=Sector;
                        v_FAQ.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
                        [self.ContainerView addSubview: v_FAQ];

                    }
                        break;
                }

            }
                break;
        }
        for (id item in MainMenuOptions) {
            [item setObject:@"0" forKey:@"selected"];
        }
        [[MainMenuOptions objectAtIndex:indexPath.row] setObject:@"1" forKey:@"selected"];
        [tableView reloadData];
    }
    @catch (NSException *exception) {
        
    }
    
}
#pragma -mark awarness delegate
-(void)didSelectArticle:(id)article AtCell:(UIView *)cell AtIndex:(int)index{
    @try {
        [self.ContainerView setUserInteractionEnabled:NO];
        CurrentAwarnessCell=(AwarnessCell*)cell;
        CurrentAwarnessCellIndex=index;
        if ([[article objectForKey:@"Loaded"]intValue]==0) {
            if (!internetConnection) {
                [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
                [self.ContainerView setUserInteractionEnabled:YES];
                HIDE_LOADINGINDICATOR
                return;
            }

            if (index%2==0) {
                [((AwarnessCell*)cell).DownloadPDFImageView1 setHidden:YES];
                [((AwarnessCell*)cell).AcivityIndicator1 startAnimating];
            }
            else{
                [((AwarnessCell*)cell).DownloadPDFImageView2 setHidden:YES];
                [((AwarnessCell*)cell).ActivityIndicator2 startAnimating];
            }
            if ([[article objectForKey:@"PDFUrl"] stringByReplacingOccurrencesOfString:@" " withString:@""].length>0) {
                [NSThread detachNewThreadSelector:@selector(ShowPDF:) toTarget:self withObject:article];
            }
        }
        else
        {
            [self performSelectorOnMainThread:@selector(HandleShowPDF:) withObject:[article objectForKey:@"PDFPath"] waitUntilDone:NO];
           
        }
       
    }
    @catch (NSException *exception) {
        
    }
}

-(void)ShowPDF:(id)article
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *Documentpath = [paths objectAtIndex:0];
    NSString *TargetPath = [Documentpath stringByAppendingPathComponent:@"AwarnessPDF"];
    TargetPath = [Documentpath stringByAppendingPathComponent:[article objectForKey:@"sectionName"]];
    TargetPath= [TargetPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.pdf",[article objectForKey:@"$id"]]];
    NSFileManager *Fmanager = [NSFileManager defaultManager];
    if ([Fmanager fileExistsAtPath:TargetPath isDirectory:nil]) {
        
        if (CurrentAwarnessCellIndex%2==0) {
            [CurrentAwarnessCell.DownloadPDFImageView1 setHidden:YES];
            [CurrentAwarnessCell.ViewPDFImageView1 setHidden:NO];
            [CurrentAwarnessCell.AcivityIndicator1 stopAnimating];
        }
        else{
            [CurrentAwarnessCell.DownloadPDFImageView2 setHidden:YES];
            [CurrentAwarnessCell.ViewPDFImageView2 setHidden:NO];
            [CurrentAwarnessCell.ActivityIndicator2 stopAnimating];
        }
        
        [self performSelectorOnMainThread:@selector(HandleShowPDF:) withObject:TargetPath waitUntilDone:NO];
        return;
    }
    
    dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    //this will start the image loading in bg
    dispatch_async(concurrentQueue, ^{
        
        NSString *url = [[[article objectForKey:@"PDFUrl"] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding ]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *pdfUrl = [NSURL URLWithString:url];
        NSURLRequest* imgRequest = [NSURLRequest requestWithURL:pdfUrl];
        NSData *pdfData = [[NSData alloc]initWithData:[NSURLConnection sendSynchronousRequest:imgRequest returningResponse:nil error:nil]];
        imgRequest=nil;
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *Documentpath = [paths objectAtIndex:0];
        NSString *TargetPath = [Documentpath stringByAppendingPathComponent:@"AwarnessPDF"];
        TargetPath = [Documentpath stringByAppendingPathComponent:[article objectForKey:@"sectionName"]];
        NSFileManager *Fmanager = [NSFileManager defaultManager];
        NSString *fileName= [TargetPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.pdf",[article objectForKey:@"$id"]] ];
        
        if ([Fmanager fileExistsAtPath:TargetPath isDirectory:nil]) {
            
        }
        else
        {
            BOOL ret =[Fmanager createDirectoryAtPath:TargetPath withIntermediateDirectories:YES attributes:nil error:nil];
            if (ret) {
                
            };
        }
        
        if (CurrentAwarnessCellIndex%2==0) {
            [CurrentAwarnessCell.DownloadPDFImageView1 setHidden:YES];
            [CurrentAwarnessCell.ViewPDFImageView1 setHidden:NO];
            [CurrentAwarnessCell.AcivityIndicator1 stopAnimating];
        }
        else{
            [CurrentAwarnessCell.DownloadPDFImageView2 setHidden:YES];
            [CurrentAwarnessCell.ViewPDFImageView2 setHidden:NO];
            [CurrentAwarnessCell.ActivityIndicator2 stopAnimating];
        }
        
        
        if (pdfData!=nil &&pdfData.length > 0 ) {
            [pdfData writeToFile:fileName atomically:YES];
            
            
            NSMutableArray *cashedData = [[NSMutableArray alloc]init];
            
            DB_Field *articleID = [[DB_Field alloc]init];
            articleID.FieldDataType=FIELD_DATA_TYPE_NUMBER;
            articleID.IS_PRIMARY_KEY=YES;
            articleID.FieldName=@"id";
            articleID.FieldValue=[article objectForKey:@"$id"];
            
            DB_Field *sectionID = [[DB_Field alloc]init];
            sectionID.FieldDataType=FIELD_DATA_TYPE_NUMBER;
            sectionID.IS_PRIMARY_KEY=YES;
            sectionID.FieldName=@"SectionId";
            sectionID.FieldValue=[article objectForKey:@"SectionId"];
            
            
            DB_Field *PDFPath= [[DB_Field alloc]init];
            PDFPath.FieldDataType=FIELD_DATA_TYPE_TEXT;
            PDFPath.FieldName=@"PDFPath";
            PDFPath.FieldValue=fileName;
            
            DB_Field *Loaded = [[DB_Field alloc]init];
            Loaded.FieldDataType=FIELD_DATA_TYPE_NUMBER;
            Loaded.FieldName=@"Loaded";
            Loaded.FieldValue=@"1";
            
            
            DB_Recored *record = [[DB_Recored alloc]init];
            record.Fields=[NSMutableArray arrayWithObjects:articleID,sectionID,PDFPath,Loaded, nil];
            [cashedData addObject:record];
            
            
            [[Cashing getObject] setDatabase:@"SDFAipad"];
            BOOL Success=  [[Cashing getObject]CashData:cashedData inTable:@"AwarenessCenter"];
            
            
            [self performSelectorOnMainThread:@selector(HandleShowPDF:) withObject:fileName waitUntilDone:NO];
            
        }
        
    });
    
    
    
    
}

-(void)HandleShowPDF:(id)path
{
    if (CurrentAwarnessCellIndex%2==0) {
        [CurrentAwarnessCell.DownloadPDFImageView1 setHidden:YES];
        [CurrentAwarnessCell.ViewPDFImageView1 setHidden:NO];
        [CurrentAwarnessCell.AcivityIndicator1 stopAnimating];
    }
    else{
        [CurrentAwarnessCell.DownloadPDFImageView2 setHidden:YES];
        [CurrentAwarnessCell.ViewPDFImageView2 setHidden:NO];
        [CurrentAwarnessCell.ActivityIndicator2 stopAnimating];
    }
    [self.ContainerView setUserInteractionEnabled:YES];
    
    if ([path stringByReplacingOccurrencesOfString:@" " withString:@""].length>0) {
        ReaderDocument *document = [ReaderDocument withDocumentFilePath:path password:nil];
        ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
        readerViewController.delegate = self;
        
        readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:readerViewController animated:YES completion:NULL];
    }
    
}


-(void)HandleSearchDrugListResult:(NSArray*)result
{
    @try {
        HIDE_LOADINGINDICATOR;
        if (result==nil) {
               [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"msgNoData"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            return;
        }
        else if ([result count]==0) {
              [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"msgNoData"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            return;
        }
        NSMutableArray *DrugsList = [[NSMutableArray alloc]init];
        for (id item in result) {
            NSMutableDictionary *dic2 = [[NSMutableDictionary alloc]init];
            [dic2 setValue:[item objectForKey:@"TradeName"] forKey:@"Value"];
            [dic2 setValue:@"الاسم التجارى" forKey:@"Description"];
            [dic2 setValue:[item objectForKey:@"RegistrationNo"] forKey:@"SearchKey"];
            
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            [dic setValue:[item objectForKey:@"GenericName"] forKey:@"Value"];
            [dic setValue:@"الاسم العلمي" forKey:@"Description"];
            [dic setValue:[item objectForKey:@"RegistrationNo"] forKey:@"SearchKey"];
            
            NSMutableDictionary *dic3 = [[NSMutableDictionary alloc]init];
            [dic3 setValue:[item objectForKey:@"RegistrationNo"] forKey:@"Value"];
            [dic3 setValue:@"رقم التسجيل" forKey:@"Description"];
            [dic3 setValue:[item objectForKey:@"RegistrationNo"] forKey:@"SearchKey"];
            
            [DrugsList addObject:dic];
            [DrugsList addObject:dic2];
            [DrugsList addObject:dic3];
            
            
            
        }

        [self.view sendSubviewToBack:[self.ContainerView viewWithTag:DRUGS_SEARCH_TAG]];
        [[self.ContainerView viewWithTag:DRUGS_SEARCH_TAG] setHidden:YES];
        
        DrugsSearchListView*v_searchList;
        
       
        if ([self.ContainerView viewWithTag:DRUGS_SEARCH_LIST_TAG]) {
            v_searchList = (DrugsSearchListView*) [self.ContainerView viewWithTag:DRUGS_SEARCH_LIST_TAG];
             v_searchList.tag=DRUGS_SEARCH_LIST_TAG;
        }
        else
        {
        v_searchList=(DrugsSearchListView*)[[[NSBundle mainBundle]loadNibNamed:@"DrugsSearchListView" owner:nil options:nil]objectAtIndex:0];
             v_searchList.tag=DRUGS_SEARCH_LIST_TAG;
            v_searchList.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
            v_searchList.delegate = self;
            [self.ContainerView addSubview: v_searchList];
        }
        [v_searchList setHidden:NO];
        v_searchList.frame =CGRectMake((self.ContainerView.frame.size.width), 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
        [UIView animateWithDuration:0.3 animations:^{
            v_searchList.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
        }];
        v_searchList.SubTitleLabel.text=[NSString stringWithFormat:@"نتائج البحث عن %@",SectorSearchString];
         v_searchList.DrugsList= DrugsList;
        [v_searchList ReLoadData];
        [self.ContainerView bringSubviewToFront:v_searchList];
       
        
    }
    @catch (NSException *exception) {
        
    }
   
}
-(void)SearchDrugList:(NSString*)searchString
{
    @try {
        id result = [Webservice GetDrugsByName:searchString];
        [self performSelectorOnMainThread:@selector(HandleSearchDrugListResult:) withObject:result waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
   
}

-(void)HandleSearchMedicalListResult:(NSArray*)result
{
    @try {
        HIDE_LOADINGINDICATOR;
        if (result==nil) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"msgNoData"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            return;
        }
        else if ([result count]==0) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"msgNoData"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            return;
        }
        NSMutableArray *MedicalList = [[NSMutableArray alloc]init];
        for (id item in result) {
            NSMutableDictionary *dic2 = [[NSMutableDictionary alloc]init];
            [dic2 setObject:[item objectForKey:@"Product_id"] forKey:@"Value"];
            [dic2 setObject:@"رقم قيد الجهاز" forKey:@"Description"];
            [dic2 setObject:[item objectForKey:@"Product_id"] forKey:@"SearchKey"];
            
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            [dic setObject:[item objectForKey:@"Brand_Name"] forKey:@"Value"];
            [dic setObject:@"الاسم التجارى" forKey:@"Description"];
            [dic setObject:[item objectForKey:@"Product_id"] forKey:@"SearchKey"];
            
            NSMutableDictionary *dic3 = [[NSMutableDictionary alloc]init];
            [dic3 setObject:[item objectForKey:@"Legal_Manufacturer"] forKey:@"Value"];
            [dic3 setObject:@"الشركة المصنعة" forKey:@"Description"];
            [dic3 setObject:[item objectForKey:@"Product_id"] forKey:@"SearchKey"];
            
            NSMutableDictionary *dic4 = [[NSMutableDictionary alloc]init];
            [dic4 setObject:[item objectForKey:@"Model_Number"] forKey:@"Value"];
            [dic4 setObject:@"رقم الموديل" forKey:@"Description"];
            [dic4 setObject:[item objectForKey:@"Product_id"] forKey:@"SearchKey"];

            
            [MedicalList addObject:dic];
            [MedicalList addObject:dic2];
            [MedicalList addObject:dic3];
            [MedicalList addObject:dic4];
            
            
        }
        
        [self.view sendSubviewToBack:[self.ContainerView viewWithTag:MEDICAL_SEARCH_TAG]];
        [[self.ContainerView viewWithTag:MEDICAL_SEARCH_TAG] setHidden:YES];
        
        MedicalSearchListView*v_searchList;

        if ([self.ContainerView viewWithTag:MEDICAL_SEARCH_LIST_TAG]) {
            v_searchList = (MedicalSearchListView*) [self.ContainerView viewWithTag:MEDICAL_SEARCH_LIST_TAG];
            v_searchList.tag=MEDICAL_SEARCH_LIST_TAG;
        }
        else
        {
            v_searchList=(MedicalSearchListView*)[[[NSBundle mainBundle]loadNibNamed:@"MedicalSearchListView" owner:nil options:nil]objectAtIndex:0];
            v_searchList.tag=MEDICAL_SEARCH_LIST_TAG;
            
            v_searchList.delegate = self;
            [self.ContainerView addSubview: v_searchList];
        }
        [v_searchList setHidden:NO];
        v_searchList.frame =CGRectMake((self.ContainerView.frame.size.width), 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
        [UIView animateWithDuration:0.3 animations:^{
            v_searchList.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
        }];
        v_searchList.SubTitleLabel.text=[NSString stringWithFormat:@"نتائج البحث عن %@",SectorSearchString];
        v_searchList.MedicalList= MedicalList;
        [v_searchList ReLoadData];
        [self.ContainerView bringSubviewToFront:v_searchList];
        
        
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)SearchMedicalList:(NSString*)searchString
{
    @try {
        id result = [Webservice GetMedicalEquipmentByName:searchString];
        [self performSelectorOnMainThread:@selector(HandleSearchMedicalListResult:) withObject:result waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark Food Details Delegate
-(void)didClickFoodDetailsBackButton
{
    @try {
        [UIView animateWithDuration:0.3 animations:^{
            [self.ContainerView viewWithTag:FOOD_SEARCH_DETAILS_TAG].frame =CGRectMake(self.ContainerView.frame.size.width, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
        } completion:^(BOOL finished) {
            if (finished) {
                if ([self.ContainerView viewWithTag:FOOD_SEARCH_DETAILS_TAG]) {
                    [[self.ContainerView viewWithTag:FOOD_SEARCH_DETAILS_TAG] removeFromSuperview];
                    
                }
                if ([self.ContainerView viewWithTag:FOOD_SEARCH_LIST_TAG]) {
                    [[self.ContainerView viewWithTag:FOOD_SEARCH_LIST_TAG] setHidden:NO];
                    [self.ContainerView bringSubviewToFront: [self.ContainerView viewWithTag:FOOD_SEARCH_LIST_TAG]];
                }
            }
        }];
    }
    @catch (NSException *exception) {
        
    }
   
}
#pragma -mark medical Details Delegate
-(void)didClickMedicalDetailsBackButton
{
    @try {
        [UIView animateWithDuration:0.3 animations:^{
            [self.ContainerView viewWithTag:MEDICAL_SEARCH_DETAILS_TAG].frame =CGRectMake(self.ContainerView.frame.size.width, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
        } completion:^(BOOL finished) {
            if (finished) {
                if ([self.ContainerView viewWithTag:MEDICAL_SEARCH_DETAILS_TAG]) {
                    [[self.ContainerView viewWithTag:MEDICAL_SEARCH_DETAILS_TAG] removeFromSuperview];
                    
                }
                if ([self.ContainerView viewWithTag:MEDICAL_SEARCH_LIST_TAG]) {
                    [[self.ContainerView viewWithTag:MEDICAL_SEARCH_LIST_TAG] setHidden:NO];
                    [self.ContainerView bringSubviewToFront: [self.ContainerView viewWithTag:MEDICAL_SEARCH_LIST_TAG]];
                }
            }
        }];
        
       
    }
    @catch (NSException *exception) {
        
    }
    
}

#pragma -mark Drug Details Delegate
-(void)didClickDrugDetailsBackButton
{
    @try {
        [UIView animateWithDuration:0.3 animations:^{
            [self.ContainerView viewWithTag:DRUGS_SEARCH_DETAILS_TAG].frame =CGRectMake(self.ContainerView.frame.size.width, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
        } completion:^(BOOL finished) {
            if ([self.ContainerView viewWithTag:DRUGS_SEARCH_DETAILS_TAG]) {
                [[self.ContainerView viewWithTag:DRUGS_SEARCH_DETAILS_TAG] removeFromSuperview];
                
            }
            if ([self.ContainerView viewWithTag:DRUGS_SEARCH_LIST_TAG]) {
                [[self.ContainerView viewWithTag:DRUGS_SEARCH_LIST_TAG] setHidden:NO];
                [self.ContainerView bringSubviewToFront: [self.ContainerView viewWithTag:DRUGS_SEARCH_LIST_TAG]];
            }
        }];
        
    }
    @catch (NSException *exception) {
        
    }
    
}
#pragma -mark MedicalList Delegate
-(void)didClickMedicalListBackButton
{
    @try {
        [UIView animateWithDuration:0.3 animations:^{
            [self.ContainerView viewWithTag:MEDICAL_SEARCH_LIST_TAG].frame =CGRectMake(self.ContainerView.frame.size.width, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
        } completion:^(BOOL finished) {
            if (finished) {
                if ([self.ContainerView viewWithTag:MEDICAL_SEARCH_LIST_TAG]) {
                    [[self.ContainerView viewWithTag:MEDICAL_SEARCH_LIST_TAG] setHidden:YES];
                    [self.ContainerView sendSubviewToBack: [self.ContainerView viewWithTag:MEDICAL_SEARCH_LIST_TAG]];
                }
                if ([self.ContainerView viewWithTag:MEDICAL_SEARCH_TAG]) {
                     [[self.ContainerView viewWithTag:MEDICAL_SEARCH_TAG] setAlpha:0];
                    [[self.ContainerView viewWithTag:MEDICAL_SEARCH_TAG] setHidden:NO];
                    [UIView animateWithDuration:0.1 animations:^{
                        [[self.ContainerView viewWithTag:MEDICAL_SEARCH_TAG] setAlpha:1];
                    }];
                    [self.ContainerView bringSubviewToFront: [self.ContainerView viewWithTag:MEDICAL_SEARCH_TAG]];
                }
            }
        }];
        
    }
    @catch (NSException *exception) {
        
    }
}
-(void)didSelecteMedicalEquipment:(id)item{
    @try {
        if (!internetConnection) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            
            return;
        }

        SHOW_LOADINGINDICATOR;
        //NSLog(@"selected medical %@",item);
        MedicalDetailsView*v_searchDetails;
        [self.view sendSubviewToBack:[self.ContainerView viewWithTag:MEDICAL_SEARCH_LIST_TAG]];
        [[self.ContainerView viewWithTag:MEDICAL_SEARCH_LIST_TAG] setHidden:YES];
        
        v_searchDetails=(MedicalDetailsView*)[[[NSBundle mainBundle]loadNibNamed:@"MedicalDetailsView" owner:nil options:nil]objectAtIndex:0];
        
        v_searchDetails.frame =CGRectMake(self.ContainerView.frame.size.width, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
        [UIView animateWithDuration:0.3 animations:^{
            v_searchDetails.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
        }];
        v_searchDetails.delegate= self;
        v_searchDetails.tag= MEDICAL_SEARCH_DETAILS_TAG;
        [v_searchDetails setHidden:NO];
        v_searchDetails.delegate= self;
        v_searchDetails.ProductNumber=item;
        v_searchDetails.SubTitleLabel.text=[NSString stringWithFormat:@"نتائج البحث عن %@",SectorSearchString];
        [self.ContainerView addSubview: v_searchDetails];
        [self.ContainerView bringSubviewToFront:v_searchDetails];
        
        
    }
    @catch (NSException *exception) {
        
    }
    
}

#pragma -mark FoodList Delegate
-(void)didClickFoodListBackButton
{
    @try {
        [UIView animateWithDuration:0.3 animations:^{
            [self.ContainerView viewWithTag:FOOD_SEARCH_LIST_TAG].frame =CGRectMake(self.ContainerView.frame.size.width, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
        } completion:^(BOOL finished) {
            if (finished) {
                if ([self.ContainerView viewWithTag:FOOD_SEARCH_LIST_TAG]) {
                    [[self.ContainerView viewWithTag:FOOD_SEARCH_LIST_TAG] setHidden:YES];
                    [self.ContainerView sendSubviewToBack: [self.ContainerView viewWithTag:FOOD_SEARCH_LIST_TAG]];
                }
                if ([self.ContainerView viewWithTag:FOOD_SEARCH_TAG]) {
                    [[self.ContainerView viewWithTag:FOOD_SEARCH_TAG] setAlpha:0];
                    [[self.ContainerView viewWithTag:FOOD_SEARCH_TAG] setHidden:NO];
                    [UIView animateWithDuration:0.1 animations:^{
                        [[self.ContainerView viewWithTag:FOOD_SEARCH_TAG] setAlpha:1];
                    }];
                    [self.ContainerView bringSubviewToFront: [self.ContainerView viewWithTag:FOOD_SEARCH_TAG]];
                }
            }
        }];

    }
    @catch (NSException *exception) {
        
    }
   
}
-(void)didSelecteFood:(id)item
{
@try {
    if (!internetConnection) {
        [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
        
        return;
    }
    
       //NSLog(@"selected Drug %@",item);
    FoodDetailsView*v_searchDetails;
    
    [self.view sendSubviewToBack:[self.ContainerView viewWithTag:FOOD_SEARCH_LIST_TAG]];
    [[self.ContainerView viewWithTag:FOOD_SEARCH_LIST_TAG] setHidden:YES];
    
    v_searchDetails=(FoodDetailsView*)[[[NSBundle mainBundle]loadNibNamed:@"FoodDetailsView" owner:nil options:nil]objectAtIndex:0];
    v_searchDetails.frame =CGRectMake(self.ContainerView.frame.size.width, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
    [UIView animateWithDuration:0.3 animations:^{
        v_searchDetails.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
    }];
    v_searchDetails.delegate= self;
    v_searchDetails.tag= FOOD_SEARCH_DETAILS_TAG;
    [v_searchDetails setHidden:NO];
    v_searchDetails.delegate= self;
    v_searchDetails.Product=item;
    
    [self.ContainerView addSubview: v_searchDetails];
    [self.ContainerView bringSubviewToFront:v_searchDetails];
    
}
@catch (NSException *exception) {
    
}

}
#pragma -mark DrugsList Delegate
-(void)didClickDrugListBackButton
{
    @try {
        
        [UIView animateWithDuration:0.3 animations:^{
            [self.ContainerView viewWithTag:DRUGS_SEARCH_LIST_TAG].frame =CGRectMake(self.ContainerView.frame.size.width, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
        } completion:^(BOOL finished) {
            if (finished) {
                if ([self.ContainerView viewWithTag:DRUGS_SEARCH_LIST_TAG]) {
                    [[self.ContainerView viewWithTag:DRUGS_SEARCH_LIST_TAG] setHidden:YES];
                    [self.ContainerView sendSubviewToBack: [self.ContainerView viewWithTag:DRUGS_SEARCH_LIST_TAG]];
                }
                if ([self.ContainerView viewWithTag:DRUGS_SEARCH_TAG]) {
                     [[self.ContainerView viewWithTag:DRUGS_SEARCH_TAG] setAlpha:0];
                    [[self.ContainerView viewWithTag:DRUGS_SEARCH_TAG] setHidden:NO];
                    [UIView animateWithDuration:0.1 animations:^{
                        [[self.ContainerView viewWithTag:DRUGS_SEARCH_TAG] setAlpha:1];
                    }];
                    [self.ContainerView bringSubviewToFront: [self.ContainerView viewWithTag:DRUGS_SEARCH_TAG]];
                }
            }
        }];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)didSelecteDrug:(id)item{
    @try {
        if (!internetConnection) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            
            return;
        }

        SHOW_LOADINGINDICATOR;
        //NSLog(@"selected Drug %@",item);
        DrugDetailsView*v_searchDetails;
        
        [self.view sendSubviewToBack:[self.ContainerView viewWithTag:DRUGS_SEARCH_LIST_TAG]];
        [[self.ContainerView viewWithTag:DRUGS_SEARCH_LIST_TAG] setHidden:YES];
    
        v_searchDetails=(DrugDetailsView*)[[[NSBundle mainBundle]loadNibNamed:@"DrugDetailsView" owner:nil options:nil]objectAtIndex:0];
        v_searchDetails.frame =CGRectMake(self.ContainerView.frame.size.width, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
        [UIView animateWithDuration:0.3 animations:^{
            v_searchDetails.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
        }];
        v_searchDetails.delegate= self;
        v_searchDetails.tag= DRUGS_SEARCH_DETAILS_TAG;
        [v_searchDetails setHidden:NO];
        v_searchDetails.delegate= self;
        v_searchDetails.RegistrationNumber=item;
        v_searchDetails.SubTitleLabel.text=[NSString stringWithFormat:@"نتائج البحث عن %@",SectorSearchString];
        [self.ContainerView addSubview: v_searchDetails];
        [self.ContainerView bringSubviewToFront:v_searchDetails];
        
    }
    @catch (NSException *exception) {
        
    }
    
}
#pragma -mark DrugsSearch delegate
-(void)didClickDrugsSearchButton:(id)searcheString
{
    @try {
        SectorSearchString=searcheString;
        if (!internetConnection) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            
            return;
        }

        if ([searcheString length]>0) {
            SHOW_LOADINGINDICATOR
            [NSThread detachNewThreadSelector:@selector(SearchDrugList:) toTarget:self withObject:searcheString];
        }
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark MedicalSearch delegate
-(void)didClickMedicalSearchButton:(id)searcheString
{
    @try {
        SectorSearchString=searcheString;
        if ([searcheString length]>0) {
            SHOW_LOADINGINDICATOR
            [NSThread detachNewThreadSelector:@selector(SearchMedicalList:) toTarget:self withObject:searcheString];
        }
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark FoodSearch delegate
-(void)didClickCameraButton
{
    @try {
        isGranted =NO;
        [[UIApplication sharedApplication]setStatusBarHidden:YES];
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        if(authStatus == AVAuthorizationStatusAuthorized) {
            isGranted=YES;
        } else if(authStatus == AVAuthorizationStatusDenied){
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                if(!granted){
                      isGranted=NO;
                }
                else{
                      isGranted=YES;
                }
            }];
            
        } else if(authStatus == AVAuthorizationStatusRestricted){
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                if(!granted){
                    isGranted=NO;
                }
                else{
                    isGranted=YES;
                }
            }];
        } else if(authStatus == AVAuthorizationStatusNotDetermined){
            // not determined?!
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                if(!granted){
                    isGranted=NO;
                }
                else{
                    isGranted=YES;
                }
            }];
        } else {
           isGranted=NO;
        }
        
        if (isGranted) {
            [ZBarReaderView class];
            ipadBarcodeScannerViewController *vc_scanner = [[ipadBarcodeScannerViewController alloc]init];
            vc_scanner.delegate=self;
            [self presentViewController:vc_scanner animated:YES completion:^{
                
            }];
        }
        else{
             [[[UIAlertView alloc]initWithTitle:nil message:@"برجاء تمكين التطبيق من استخدام الكاميرا" delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
        }
    }
    @catch (NSException *exception) {
        
    }
}
-(void)didClickFoodSearchButton:(UIButton *)sender
{
    @try {

         SHOW_LOADINGINDICATOR
        [sender setUserInteractionEnabled:NO];
        [NSThread detachNewThreadSelector:@selector(SearchFoodProducts:) toTarget:self withObject:sender];
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark news delegate
-(void)didFinishLoadingNews:(id)newsList{
    NEWS_FINISH_LOADING=YES;
    [super didFinishLoadingNews:newsList];
    
}
#pragma -mark Barcode ios7+ delegate
-(void)didFinishPickingBarcode:(id)barcode
{
    @try {
        [self dismissModalViewControllerAnimated:YES];
        [self.view sendSubviewToBack:[self.ContainerView viewWithTag:FOOD_SEARCH_TAG]];
        [[self.ContainerView viewWithTag:FOOD_SEARCH_TAG] setHidden:YES];
        
        if ([self.ContainerView viewWithTag:BARCODE_RESUT_TAG]) {
            [[self.ContainerView viewWithTag:BARCODE_RESUT_TAG]removeFromSuperview];
        }
        
        BarcodeResultView *v_barcodeResult =(BarcodeResultView*)[[[NSBundle mainBundle]loadNibNamed:@"BarcodeResultView" owner:nil options:nil]objectAtIndex:0];
        v_barcodeResult.tag=BARCODE_RESUT_TAG;
        v_barcodeResult.BarcodeImageView.image = nil;
        v_barcodeResult.BarcodeLabel.text = barcode;
        v_barcodeResult.SearchButton.accessibilityLabel = barcode;
        [v_barcodeResult.SearchButton addTarget:self action:@selector(BarcodeResultSearchAction:) forControlEvents:UIControlEventTouchUpInside];
        [v_barcodeResult.BackButton addTarget:self action:@selector(BarcodeResultBackAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.ContainerView addSubview:v_barcodeResult];
        v_barcodeResult.frame =CGRectMake((self.ContainerView.frame.size.width), 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
        [UIView animateWithDuration:0.3 animations:^{
            v_barcodeResult.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
        }];
        
    }
    @catch (NSException *exception) {
        
    }

}
#pragma -mark Barcode delegate
-(void)didFinishPickingBarcode:(id)barcode ForBarcodeImage:(UIImage *)barcodeImage
{
    @try {
        [self dismissModalViewControllerAnimated:YES];
        [self.view sendSubviewToBack:[self.ContainerView viewWithTag:FOOD_SEARCH_TAG]];
        [[self.ContainerView viewWithTag:FOOD_SEARCH_TAG] setHidden:YES];
        
        if ([self.ContainerView viewWithTag:BARCODE_RESUT_TAG]) {
            [[self.ContainerView viewWithTag:BARCODE_RESUT_TAG]removeFromSuperview];
        }
        
        BarcodeResultView *v_barcodeResult =(BarcodeResultView*)[[[NSBundle mainBundle]loadNibNamed:@"BarcodeResultView" owner:nil options:nil]objectAtIndex:0];
        v_barcodeResult.tag=BARCODE_RESUT_TAG;
        v_barcodeResult.BarcodeImageView.image = barcodeImage;
        v_barcodeResult.BarcodeLabel.text = barcode;
        v_barcodeResult.SearchButton.accessibilityLabel = barcode;
        [v_barcodeResult.SearchButton addTarget:self action:@selector(BarcodeResultSearchAction:) forControlEvents:UIControlEventTouchUpInside];
        [v_barcodeResult.BackButton addTarget:self action:@selector(BarcodeResultBackAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.ContainerView addSubview:v_barcodeResult];
        v_barcodeResult.frame =CGRectMake((self.ContainerView.frame.size.width), 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
        [UIView animateWithDuration:0.3 animations:^{
            v_barcodeResult.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
        }];
        
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)BarcodeResultBackAction:(UIButton*)sender
{
    @try {
        if ([self.ContainerView viewWithTag:BARCODE_RESUT_TAG]) {
            [[self.ContainerView viewWithTag:BARCODE_RESUT_TAG]removeFromSuperview];
        }
         [[self.ContainerView viewWithTag:FOOD_SEARCH_TAG] setAlpha:0];
         [[self.ContainerView viewWithTag:FOOD_SEARCH_TAG] setHidden:NO];
         [self.ContainerView bringSubviewToFront:[self.ContainerView viewWithTag:FOOD_SEARCH_TAG]];
        [UIView animateWithDuration:0.3 animations:^{
            [[self.ContainerView viewWithTag:FOOD_SEARCH_TAG] setAlpha:1];
        }];
        
    }
    @catch (NSException *exception) {
        
    }
}
-(void)BarcodeResultSearchAction:(UIButton*)sender
{
    @try {
        
        if (!internetConnection) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            
            return;
        }
        
        SHOW_LOADINGINDICATOR
        [sender setUserInteractionEnabled:NO];
        [NSThread detachNewThreadSelector:@selector(SearchFoodProducts:) toTarget:self withObject:sender];
        
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)HandleSearchFoodProductsResult:(id)data
{
    @try {
        id result = [data objectAtIndex:0];
        
        UIButton *sender = (UIButton*)[data objectAtIndex:1];
        HIDE_LOADINGINDICATOR
        [sender setUserInteractionEnabled:YES];
        if ([[[result class]description] isEqualToString:@"__NSCFConstantString"]) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"msgNoData"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            return;
        }
        
         [[self.ContainerView viewWithTag:BARCODE_RESUT_TAG] removeFromSuperview];
        [self.view sendSubviewToBack:[self.ContainerView viewWithTag:FOOD_SEARCH_TAG]];
        [[self.ContainerView viewWithTag:FOOD_SEARCH_TAG] setHidden:YES];
        
        FoodSearchListView*v_searchList;
        
        
        if ([self.ContainerView viewWithTag:FOOD_SEARCH_LIST_TAG]) {
            v_searchList = (FoodSearchListView*) [self.ContainerView viewWithTag:FOOD_SEARCH_LIST_TAG];
            v_searchList.tag=FOOD_SEARCH_LIST_TAG;
        }
        else
        {
            v_searchList=(FoodSearchListView*)[[[NSBundle mainBundle]loadNibNamed:@"FoodSearchListView" owner:nil options:nil]objectAtIndex:0];
            v_searchList.tag=FOOD_SEARCH_LIST_TAG;
            v_searchList.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
            v_searchList.delegate = self;
            [self.ContainerView addSubview: v_searchList];
        }
        [v_searchList setHidden:NO];
        v_searchList.frame =CGRectMake((self.ContainerView.frame.size.width), 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
        [UIView animateWithDuration:0.3 animations:^{
            v_searchList.frame =CGRectMake(0, 0, self.ContainerView.frame.size.width, self.ContainerView.frame.size.height);
        }];
        
        v_searchList.FoodList= result;
        [v_searchList ReLoadData];
        [self.ContainerView bringSubviewToFront:v_searchList];

    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)SearchFoodProducts:(UIButton*)sender
{
    @try {
        id result = [Webservice GetFoodProductsByBarcode:sender.accessibilityLabel];
        if (result==nil) {
            result=@"";
        }
        NSArray *parms = [NSArray arrayWithObjects:result,sender, nil];
        [self performSelectorOnMainThread:@selector(HandleSearchFoodProductsResult:) withObject:parms waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
    
}
- (IBAction)ClearCashedData:(UIButton *)sender {
    @try {
        if (!internetConnection) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            [self.ContainerView setUserInteractionEnabled:YES];
            HIDE_LOADINGINDICATOR
            
        }

        NSString *newsSectorCashedKey=nil;
        NSString *faqSectorCashedKey=nil;
        NSString *awarnessSectorCashedKey=nil;
        switch (self.Sector) {
            case FOODS_SECTOR:
            {
                newsSectorCashedKey=NEWS_FOOD_CASH_KEY;
                faqSectorCashedKey =FAQ_FOOD_CASH_KEY;
                awarnessSectorCashedKey = AWARNESS_FOOD_CASH_KEY;
            }
                break;
            case DRUGS_SECTOR:
            {
                newsSectorCashedKey=NEWS_DRUGS_CASH_KEY;
                faqSectorCashedKey =FAQ_DRUG_CASH_KEY;
                awarnessSectorCashedKey = AWARNESS_DRUGS_CASH_KEY;
            }
                break;
            case MEDICAL_SECTOR:
            {
                newsSectorCashedKey=NEWS_MEDICAL_CASH_KEY;
                faqSectorCashedKey =FAQ_MEDICAL_CASH_KEY;
                awarnessSectorCashedKey = AWARNESS_MEDICAL_CASH_KEY;
            }
                break;
            case SFDA_SECTOR:
            {
     
            }
                break;
        }
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:newsSectorCashedKey];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:faqSectorCashedKey];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:awarnessSectorCashedKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        CLEARE_CASHED_DATA;
  
        if (NEWS_FINISH_LOADING) {
        SectorsViewController*vc_sectors =[[SectorsViewController alloc]init];
        vc_sectors.Sector=CurrentSectorType;
        self.navigationController.viewControllers = [NSArray arrayWithObject:vc_sectors];
        [self.navigationController popToRootViewControllerAnimated:NO];
                    NEWS_FINISH_LOADING=NO;
        }

      
    }
    @catch (NSException *exception) {
        
    }
    
}
#pragma  mark MFMailCompose delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        //NSLog(@"It's away!");
    }
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark ReaderViewControllerDelegate methods

- (void)dismissReaderViewController:(ReaderViewController *)viewController
{
    HIDE_LOADINGINDICATOR
	[self dismissViewControllerAnimated:YES completion:NULL];
    
}
@end
