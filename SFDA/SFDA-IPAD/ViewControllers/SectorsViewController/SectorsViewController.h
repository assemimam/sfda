//
//  SectorsViewController.h
//  sfda-ipad
//
//  Created by Assem Imam on 5/22/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ipadMasterViewController.h"
#import "ReaderViewController.h"
#import <MessageUI/MFMailComposeViewController.h>
@interface SectorsViewController :ipadMasterViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,MFMailComposeViewControllerDelegate>{
    
}
@property(nonatomic)SECTOR_TYPE Sector;
@end
