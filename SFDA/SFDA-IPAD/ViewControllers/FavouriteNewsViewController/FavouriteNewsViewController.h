//
//  FavouriteNewsViewController.h
//  sfda-ipad
//
//  Created by assem on 6/17/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "ipadMasterViewController.h"
#import "SideMenView.h"
@interface FavouriteNewsViewController : ipadMasterViewController<UITableViewDataSource,SideMenuDelegate>
{
    __weak IBOutlet UILabel *NoResultLabel;
    __weak IBOutlet UITableView *NewsTableView;
    __weak IBOutlet UIView *HeaderView;
}
- (IBAction)MenuButtonAction:(UIButton *)sender ;
@end
