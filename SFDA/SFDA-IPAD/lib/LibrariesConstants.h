//
//  LibrariesConstants.h
//  MoRe
//
//  Created by Ahmed Aly on 10/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//


#import "Language.h"
#import "AppDelegate_ipad.h"
#import "WebService.h"
#import "CommonMethods.h"
#import "ImageCache.h"
#import "SideMenView.h"
#import "Database.h"
#import "ArabicConverter.h"
#import "Cashing.h"
#import "ImageCache.h"

typedef enum
{
    ALL_NEWS,
    ALL_WRNING_NEWS,
    SFDA_NEWS
}NewsType;

@protocol AllNewsTyesDelegate <NSObject>
@optional
-(void)didSelectNewsType:(NewsType)type;
@end

typedef enum
{
    FOODS_SECTOR=0,
    DRUGS_SECTOR=1,
    MEDICAL_SECTOR=2,
    SFDA_SECTOR=3,
    ALL =4
    
}
SECTOR_TYPE;
#define CLEARE_CASHED_DATA [[Database getObject]setDatabase:@"SDFAipad"];\
[[Database getObject]DeleteFromTable:@"About" Where:nil];\
[[Database getObject]DeleteFromTable:@"AwarenessCenter" Where:nil];\
[[Database getObject]DeleteFromTable:@"ContactUs" Where:nil];\
[[Database getObject]DeleteFromTable:@"EService" Where:nil];\
[[Database getObject]DeleteFromTable:@"ImportantLinks" Where:nil];\
[[Database getObject]DeleteFromTable:@"news" Where:nil];\
[[Database getObject]DeleteFromTable:@"question" Where:nil];\
[[Database getObject]DeleteFromTable:@"members" Where:nil];\
NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);\
NSString *Documentpath = [paths objectAtIndex:0];\
NSString *TargetPath = [Documentpath stringByAppendingPathComponent:@"AwarnessPDF"];\
NSString *TargetPath2 = [Documentpath stringByAppendingPathComponent:@"Images"];\
NSString *TargetPath3 = [Documentpath stringByAppendingPathComponent:@"HtmlFiles"];\
NSFileManager *Fmanager = [NSFileManager defaultManager];\
[Fmanager removeItemAtPath:TargetPath error:nil];\
[Fmanager removeItemAtPath:TargetPath2 error:nil];\
[Fmanager removeItemAtPath:TargetPath3 error:nil];\



#define MEMBERS_CASH_KEY @"members"
#define NEWS_ALL_CASH_KEY @"news_all"
#define NEWS_SFDA_CASH_KEY @"news_sfda"
#define NEWS_DRUGS_CASH_KEY @"news_drugs"
#define NEWS_FOOD_CASH_KEY @"news_food"
#define NEWS_MEDICAL_CASH_KEY @"news_medical"

#define AWARNESS_ALL_CASH_KEY @"awarness_all"
#define AWARNESS_SFDA_CASH_KEY @"awarness_sfda"
#define AWARNESS_DRUGS_CASH_KEY @"awarness_drugs"
#define AWARNESS_FOOD_CASH_KEY @"awarness_food"
#define AWARNESS_MEDICAL_CASH_KEY @"awarness_medical"

#define ABOUT_SFDA_CASH_KEY @"about_sfda"
#define ABOUT_DRUGS_CASH_KEY @"about_drugs"
#define ABOUT_FOOD_CASH_KEY @"about_food"
#define ABOUT_MEDICAL_CASH_KEY @"about_medical"


#define ABOUT_SFDA_CASH_KEY @"about_sfda"
#define ABOUT_DRUGS_CASH_KEY @"about_drugs"
#define ABOUT_FOOD_CASH_KEY @"about_food"
#define ABOUT_MEDICAL_CASH_KEY @"about_medical"

#define LINKS_CASH_KEY @"links"
#define CONTACT_US_CASH_KEY @"contact_us"
#define E_SERVICES_CASH_KEY @"e_services"
#define FAQ_FOOD_CASH_KEY @"faq_food"
#define FAQ_DRUG_CASH_KEY @"faq_drug"
#define FAQ_MEDICAL_CASH_KEY @"faq_medical"

#define SIDE_MENU_VIEW_TAG 1000000
#define SFDA_TAG  0
#define NEWS_TAG 1
#define FOODS_SECTOR_TAG 2
#define DRUGS_SECTOR_TAG 3
#define MEDICAL_SECTOR_TAG 4
#define E_SERVICES_TAG 5
#define SETTINGS_TAG 6
#define ABOUT_APP_TAG 7
#define MENU_BUTTON_TAG 100
#define BACK_BUTTON_TAG 200
#define REFRESH_BUTTON_TAG 300


#define  ABOUT_SECTION_SFDA_TAG   @"ABOUT_SECTION_SFDA_TAG"
#define  ABOUT_SECTION_FOOD_TAG   @"ABOUT_SECTION_FOOD_TAG"
#define  ABOUT_SECTION_DRUG_TAG   @"ABOUT_SECTION_DRUG_TAG"
#define  ABOUT_SECTION_MEDI_TAG   @"ABOUT_SECTION_MEDI_TAG"

#define  QUESTION_SECTION_FOOD_TAG   @"QUESTION_SECTION_FOOD_TAG"
#define  QUESTION_SECTION_DRUG_TAG   @"QUESTION_SECTION_DRUG_TAG"
#define  QUESTION_SECTION_MEDI_TAG   @"QUESTION_SECTION_MEDI_TAG"

#define  AWARENESS_SECTION_SFDA_TAG   @"AWARENESS_SECTION_SFDA_TAG"
#define  AWARENESS_SECTION_FOOD_TAG   @"AWARENESS_SECTION_FOOD_TAG"
#define  AWARENESS_SECTION_DRUG_TAG   @"AWARENESS_SECTION_DRUG_TAG"
#define  AWARENESS_SECTION_MEDI_TAG   @"AWARENESS_SECTION_MEDI_TAG"

#define  CONTACT_US_SECTION_TAG  @"CONTACT_US_SECTION_TAG"

#define  IMPORTANT_LINKS_SECTION_TAG   @"IMPORTANT_LINKS_SECTION_TAG"

#define  E_SERVICE_SECTION_TAG   @"E_SERVICE_SECTION_TAG"

// iOS 7 Support
#define IS_DEVICE_RUNNING_IOS_7_AND_ABOVE ([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] != NSOrderedAscending)


#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)





// NetworkService
#define NETWORK_TIMEOUT 5

// UserProfile
#define NAVIGATION_BACKGROUND_TAG 777

// CacheManager
#define PLIST_KEY @"plistKey"
#define NEWS_LIST_DICTIONARY_PLIST @"newsListDictionaryPlist"
#define NEWS_ORDER_ARRAY_PLIST @"newsOrderArray"
#define NEWS_CATEGORY_DICTIONARY_PLIST @"newsCategoryDictionary"

#define CHECK_INTERNET  [ (AppDelegate_ipad*)[[UIApplication sharedApplication]delegate]validNetworkConnection]\

#define SET_BACKGROUND [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];\

#define SHOW_LOADINGINDICATOR [ (AppDelegate_ipad*)[[UIApplication sharedApplication]delegate] ShowLoading:YES];\

#define HIDE_LOADINGINDICATOR [ (AppDelegate_ipad*)[[UIApplication sharedApplication]delegate] ShowLoading:NO];\

#define SHOW_ALERT(alertText, okButton) {\
UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertText message:nil delegate:self cancelButtonTitle:okButton otherButtonTitles:nil];\
[alert show];\
}

