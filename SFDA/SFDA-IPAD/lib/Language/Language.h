//
//  Language.h
//  More
//
//  Created by Hani on 9/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#ifndef _H_Language
#define _H_Language

#import <Foundation/Foundation.h>
#import "LibrariesHeader.h"
#import "UserDefaults.h"

@interface Language : NSObject {
    availableLanguages currentLanguage;
	languageDirection currentLanguageDirection;
	NSString *langAbbreviation;
	
	NSDictionary *imagesDictionary;
	NSDictionary *stringsDictionary;
}

@property (nonatomic, readonly) NSString *langAbbreviation;

// Initialization
+ (Language *)getObject;

// Adjust Language
- (void)setLanguage:(availableLanguages)lang;
- (availableLanguages)getLanguage;
- (languageDirection)getLanguageDirection;

// Get Data
- (NSString *)getStringWithKey:(NSString *)stringKey;
- (NSString *)getImageWithKey:(NSString *)imageKey;
- (NSArray *)getArrayWithKey:(NSString *)arrayKey;

// Alert
- (void)alertNoInternet;
- (void)alertNoSMS;
- (void)alertNoMail;
- (void)showAlertWithKey:(NSString *)alertKey;

@end

#endif