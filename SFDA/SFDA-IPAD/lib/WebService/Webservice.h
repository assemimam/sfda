//
//  Webservice.h
//  KFCA
//
//  Created by Assem Imam on 9/4/13.
//  Copyright (c) 2013 assem. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PRODUCTION_VERSION

#ifdef PRODUCTION_VERSION
#define SERVER_IP @"http://sfdaportal.azurewebsites.net/api/"
#define FOOD_SEARCH_URL @"http://sfdaportal.azurewebsites.net/api/Products"
#define MEMBER_BOARD_URL @"http://sfdaportal.azurewebsites.net/Api/boardmembers"
#else
#define SERVER_IP @"http://sfdaportalbeta.azurewebsites.net/api/"
#define FOOD_SEARCH_URL @"http://sfdaportalbeta.azurewebsites.net/api/Products"
#define MEMBER_BOARD_URL @"http://sfdaportalbeta.azurewebsites.net/Api/boardmembers"
#endif



@interface Webservice : NSObject
{
    
}
+(id)GetNotificationURL;
+(id)GetMembersBoard;
+(id)GetFoodProductsByBarcode:(NSString*)barcode;
+(id)GetMedicalEquipmentDetailsByProductNumber:(NSString*)product_no;
+(id)GetDrugDetailsByRegistrationNumber:(NSString*)registration_no;
+(id)GetDrugsByName:(NSString*)name;
+(id)GetMedicalEquipmentByName:(NSString*)name;
+(id)GetQuestionsBySector:(NSString*)sector;
+(id)GetAllAwarnessCenterArticles;
+(id)GetAwarnessCenterArticlesBySector:(NSString*)sector;
+(id)GetElectronicServices;
+(id)GetAboutInformationBySector:(NSString*)sector;
+(id)GetImportantLinksList;
+(id)GetContactUsData;
+(id)GetNewsDetailsByNewsID:(NSString*)news_id;
+(id)GetNewsListBySector:(NSString*)sector;
+(id)sendEmailWithName:(NSString *)name
             mobileNum:(NSString *)mobile
                  city:(NSString *)city
                 email:(NSString *)email
               message:(NSString *)message
           requestType:(NSString *)requestType
           productType:(NSString *)productType
                 image:(NSString *)image
           coordinates:(NSString *)coordinates;
@end
