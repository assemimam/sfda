//
//  WebService.m
//  MoRe
//
//  Created by Ahmed Aly on 10/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PostWebService.h"
#import "Reachability.h"
#import "LibrariesHeader.h"


PostWebService *WebServiceObject = nil;

@interface PostWebService ()
@property (nonatomic, retain) Reachability *reach;
- (void)networkConnectionStatusChanged:(NSNotification *)notification;
- (void)setConnectionForStatus:(NetworkStatus)status;
- (void)checkInternetWithDataWithThread;

@end


@implementation PostWebService
@synthesize connected, reach;

- (id)init{
	self = [super init];
	if (self) {
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(networkConnectionStatusChanged:) name:kReachabilityChangedNotification object:nil];
		self.reach = [Reachability reachabilityForInternetConnection];
		[self.reach startNotifier];
		[self setConnectionForStatus:[self.reach currentReachabilityStatus]];
	}
	return self;
}
-(void)releaseObject{
    if(WebServiceObject != nil ){
        [WebServiceObject release];
        WebServiceObject = nil;
        //     oAuthObject = nil;
    }
}
+ (PostWebService *)getObject{
	if (WebServiceObject == nil) {
		WebServiceObject = [[PostWebService alloc] init];
        //   //////NSLog(@"%i",(oAuthObject == nil));
	}
	
	return WebServiceObject;
}
#pragma -
#pragma Network connectivity check

- (void)networkConnectionStatusChanged:(NSNotification *)notification{
	if ([[notification object] isKindOfClass:[Reachability class]]) {
		Reachability *newReach = (Reachability *)[notification object];
		NetworkStatus status = [newReach currentReachabilityStatus];
		[self setConnectionForStatus:status];
	}
}

- (void)setConnectionForStatus:(NetworkStatus)status{
	switch (status) {
		case NotReachable:
			self.connected = NO;
			break;
		case ReachableViaWiFi:
			self.connected = YES;
			break;
		case ReachableViaWWAN:
			self.connected = YES;
			break;
		default:
			self.connected = NO;
			break;
	}
	if (self.connected) {
        //	[NSThread detachNewThreadSelector:@selector(checkInternetWithDataWithThread) toTarget:self withObject:nil];
	}
}

#pragma -
#pragma Internet Connection

- (void)checkInternetWithDataWithThread{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	self.connected = [self checkInternetWithData];
	[pool release];
}

- (BOOL)checkInternetWithData{
	BOOL returnBool = YES;
	
	NSURLRequest *request = [PostWebService requestWithParameters:nil andBaseURL:@"http://www.google.com"];
	NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:NULL error:NULL];
	if (data == nil) {
		returnBool = NO;
	}
	
	self.connected = returnBool;
	return returnBool;
}


- (BOOL)checkInternet{
	BOOL returnBool = YES;
	
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.google.com"]];
	NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:NULL error:NULL];
	if (data == nil) {
		returnBool = NO;
	}
	
	self.connected = returnBool;
	return returnBool;
}

+ (NSURLRequest *)requestWithParameters:(NSDictionary *)urlParameters andBaseURL:(NSString *)baseURL{
	NSURL *urlObject = nil;
	
	NSString *urlString = [NSString stringWithString:baseURL];
	
	for (id param in urlParameters) {
		if ([param isKindOfClass:[NSString class]] && [[urlParameters objectForKey:param] isKindOfClass:[NSString class]]) {
			urlString = [urlString stringByAppendingFormat:@"%@=%@&",param,[urlParameters objectForKey:param]];
		} else if ([param isKindOfClass:[NSString class]] && [[urlParameters objectForKey:param] isKindOfClass:[NSArray class]]) {
			NSArray *arrayParams = [urlParameters objectForKey:param];
			for (id subParam in arrayParams) {
				if ([subParam isKindOfClass:[NSString class]]) {
					urlString = [urlString stringByAppendingFormat:@"%@=%@&",param,subParam];
				}
			}
		}
	}
	if ([[urlString substringFromIndex:([urlString length] - 1)] isEqualToString:@"&"]) {
		urlString = [urlString substringToIndex:([urlString length] - 1)];
	}
	
	urlObject = [NSURL URLWithString:urlString];
	NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:urlObject cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:NETWORK_TIMEOUT];
	return urlRequest;
}

+ (NSMutableURLRequest *)postRequestWithUploadParameters:(NSMutableDictionary *)urlParameters andBaseURL:(NSString *)baseURL{
	
    
    
	NSURL *urlObject = [NSURL URLWithString:baseURL];
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlObject cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:NETWORK_TIMEOUT];
    
    [request setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
	
    NSString *boundary = [[NSString alloc ]initWithString:@"---------------------------14737809831466499882746641449"];
    NSString *contentType =[NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary] ;
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
	NSArray *paramsKeys=[[NSArray alloc] initWithArray:[urlParameters allKeys]];
    
	for (int i=0; i<[paramsKeys count]; i++) 
	{
        id value = [urlParameters objectForKey:[paramsKeys objectAtIndex:i]] ;
        
        if ([value isKindOfClass:[NSString class]]) 
		{      
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",[paramsKeys objectAtIndex:i]] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithString:value] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[[NSString alloc ]initWithString:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            
		}	
        else if ([value isKindOfClass:[NSNumber class]]) 
		{      
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",[paramsKeys objectAtIndex:i]] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%i",[value intValue]] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[[NSString alloc ]initWithString:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            
		}	
		[body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
	}
    
    
    [paramsKeys release];
	[request setHTTPBody:body];
    
    return request;
}
+ (NSMutableURLRequest *)postRequestForServicesWithUploadParameters:(NSMutableDictionary *)urlParameters andBaseURL:(NSString *)baseURL{
    
    
    NSString * params =@"";
    
        //[[WebService getObject]checkTokenIsLive];
    
	NSURL *urlObject = [NSURL URLWithString:[baseURL stringByAppendingString:params]];
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlObject cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:NETWORK_TIMEOUT];
    
    [request setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
	
    NSString *boundary = [[NSString alloc ]initWithString:@"---------------------------14737809831466499882746641449"];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
	NSArray *paramsKeys=[[NSArray alloc] initWithArray:[urlParameters allKeys]];
    
	for (int i=0; i<[paramsKeys count]; i++) 
	{
        id value = [urlParameters objectForKey:[paramsKeys objectAtIndex:i]] ;
        
        if ([value isKindOfClass:[NSData class]]) 
		{
			[body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
			[body appendData:[[NSString stringWithFormat:@"Content-Disposition: attachment; name=\"%@\"; filename=\"aaaa.png\"\r\n",[paramsKeys objectAtIndex:i]] dataUsingEncoding:NSUTF8StringEncoding]];
			[body appendData:[[[NSString alloc]initWithString:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
			[body appendData:[NSData dataWithData:[urlParameters objectForKey:[paramsKeys objectAtIndex:i]]]];
			[body appendData:[[[NSString alloc ]initWithString:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
		}
        
        else if ([value isKindOfClass:[NSString class]]) 
		{      
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",[paramsKeys objectAtIndex:i]] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithString:value] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[[NSString alloc ]initWithString:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            
		}	
        else if ([value isKindOfClass:[NSNumber class]]) 
		{      
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",[paramsKeys objectAtIndex:i]] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%i",[value intValue]] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[[NSString alloc ]initWithString:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            
		}	
		[body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
	}
    
    
    [paramsKeys release];
	[request setHTTPBody:body];
    
    return request;
}


+(NSString *)encodedOAuthParameterForString:(NSString *)str {
    
    CFStringRef originalString = (CFStringRef) str;
    
    CFStringRef leaveUnescaped = CFSTR("ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                       "abcdefghijklmnopqrstuvwxyz"
                                       "-._~");
    CFStringRef forceEscaped =  CFSTR("%!$&'()*+,/:;=?@");
    
    CFStringRef escapedStr = NULL;
    if (str) {
        escapedStr = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                             originalString,
                                                             leaveUnescaped,
                                                             forceEscaped,
                                                             kCFStringEncodingUTF8);
        [(id)CFMakeCollectable(escapedStr) autorelease];
    }
    
    return (NSString *)escapedStr;
}

#
@end
