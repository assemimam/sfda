//
//  WebService.h
//  MoRe
//
//  Created by Ahmed Aly on 10/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h" 

@interface PostWebService : NSObject {
    BOOL connected;
	Reachability *reach;

}
@property (nonatomic) BOOL connected;

+ (PostWebService *)getObject;
- (BOOL)checkInternetWithData;
- (BOOL)checkInternet;

+ (NSURLRequest *)requestWithParameters:(NSDictionary *)urlParameters andBaseURL:(NSString *)baseURL;
+ (NSMutableURLRequest *)postRequestWithUploadParameters:(NSMutableDictionary *)urlParameters andBaseURL:(NSString *)baseURL;
+ (NSMutableURLRequest *)postRequestForServicesWithUploadParameters:(NSMutableDictionary *)urlParameters andBaseURL:(NSString *)baseURL;
+ (NSString *)encodedOAuthParameterForString:(NSString *)str;
-(void)releaseObject;

@end
