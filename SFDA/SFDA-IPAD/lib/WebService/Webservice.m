//
//  Webservice.m
//  KFCA
//
//  Created by Assem Imam on 9/4/13.
//  Copyright (c) 2013 assem. All rights reserved.
//

#import "Webservice.h"
#import "PostWebService.h"


@implementation Webservice
+(id)sendEmailWithName:(NSString *)name
             mobileNum:(NSString *)mobile
                  city:(NSString *)city
                 email:(NSString *)email
               message:(NSString *)message
           requestType:(NSString *)requestType
           productType:(NSString *)productType
                 image:(NSString *)image
           coordinates:(NSString *)coordinates
{
    id result;
    @try {
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@SendEmailBase64", SERVER_IP]];
        
        NSArray * keys = [NSArray arrayWithObjects:@"ImageBytes",@"Name",@"Phone",@"RequestType",@"ProductType",@"City",@"Email", @"Message",@"Coordinates",nil];
        
        NSArray * values = [NSArray arrayWithObjects:image, name, mobile, requestType, productType, city, email, message, coordinates, nil];
        
        NSDictionary * params = [NSDictionary dictionaryWithObjects:values forKeys:keys];
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONReadingMutableContainers error:nil];
        NSString *jsonRequest = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        NSLog(@"jsonRequest is %@", jsonRequest);
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:160.0];
        
        NSData *requestData = [jsonRequest dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:[NSString stringWithFormat:@"%d", [jsonData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
        
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        if([data length] == 0) return nil;
        
        NSString *incomingResultData =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        result= incomingResultData;
    }
    @catch (NSException *exception)
    {
        
    }
    return result;
}
+(id)GetNotificationURL
{
    @try {
        id result;
        @try {
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@NotificationURLs?platformname=ios",SERVER_IP]];
            NSMutableURLRequest *request = [NSURLRequest requestWithURL:url
                                                            cachePolicy:NSURLCacheStorageAllowed
                                                        timeoutInterval:120];
            NSURLResponse * response = nil;
            NSError * error = nil;
            NSData * data = [NSURLConnection sendSynchronousRequest:request
                                                  returningResponse:&response
                                                              error:&error];
            if (error == nil)
            {
                result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            }
            else{
                result=nil;
            }
        }
        @catch (NSException *exception) {
            
        }
        return result;
        
    }
    @catch (NSException *exception) {
        
    }
    
}
+(id)GetMembersBoard
{
    
    id result;
    @try {
        NSURL *url = [NSURL URLWithString:MEMBER_BOARD_URL];
        NSMutableURLRequest *request = [NSURLRequest requestWithURL:url
                                                        cachePolicy:NSURLCacheStorageAllowed
                                                    timeoutInterval:120];
        NSURLResponse * response = nil;
        NSError * error = nil;
        NSData * data = [NSURLConnection sendSynchronousRequest:request
                                              returningResponse:&response
                                                          error:&error];
        if (error == nil)
        {
            result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil]
            ;
        }
        else{
            result=nil;
        }
    }
    @catch (NSException *exception) {
        
    }
    
    return result;
    
}
+(id)GetFoodProductsByBarcode:(NSString*)barcode
{
    @try {
        id result;
        @try {
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?barcode=%@", FOOD_SEARCH_URL,barcode]];
            NSMutableURLRequest *request = [NSURLRequest requestWithURL:url
                                                            cachePolicy:NSURLCacheStorageAllowed
                                                        timeoutInterval:120];
            NSURLResponse * response = nil;
            NSError * error = nil;
            NSData * data = [NSURLConnection sendSynchronousRequest:request
                                                  returningResponse:&response
                                                              error:&error];
            if (error == nil)
            {
                result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] ;
            }
            else{
                result=nil;
            }
        }
        @catch (NSException *exception) {
            
        }
        return result;
    }
    @catch (NSException *exception) {
        
    }
    
}
+(id)GetMedicalEquipmentDetailsByProductNumber:(NSString*)product_no
{
    @try {
        id result;
        @try {
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@MedicalProductDetails?productId=%@", SERVER_IP,product_no]];
            NSMutableURLRequest *request = [NSURLRequest requestWithURL:url
                                                            cachePolicy:NSURLCacheStorageAllowed
                                                        timeoutInterval:120];
            NSURLResponse * response = nil;
            NSError * error = nil;
            NSData * data = [NSURLConnection sendSynchronousRequest:request
                                                  returningResponse:&response
                                                              error:&error];
            if (error == nil)
            {
                result= [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil]objectAtIndex:0] ;
            }
            else{
                result=nil;
            }
        }
        @catch (NSException *exception) {
            
        }
        return result;
        
    }
    @catch (NSException *exception) {
        
    }
    
    
}
+(id)GetDrugDetailsByRegistrationNumber:(NSString*)registration_no
{
    @try {
        id result;
        @try {
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@DrugDetails?registrationNo=%@", SERVER_IP,registration_no]];
            NSMutableURLRequest *request = [NSURLRequest requestWithURL:url
                                                            cachePolicy:NSURLCacheStorageAllowed
                                                        timeoutInterval:120];
            NSURLResponse * response = nil;
            NSError * error = nil;
            NSData * data = [NSURLConnection sendSynchronousRequest:request
                                                  returningResponse:&response
                                                              error:&error];
            if (error == nil)
            {
                result= [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil]objectAtIndex:0] ;
            }
            else{
                result=nil;
            }
        }
        @catch (NSException *exception) {
            
        }
        return result;
        
    }
    @catch (NSException *exception) {
        
    }
    
}
+(id)GetDrugsByName:(NSString*)name
{
    @try {
        id result;
        @try {
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@DrugList?query=%@", SERVER_IP,name]];
            NSMutableURLRequest *request = [NSURLRequest requestWithURL:url
                                                            cachePolicy:NSURLCacheStorageAllowed
                                                        timeoutInterval:120];
            NSURLResponse * response = nil;
            NSError * error = nil;
            NSData * data = [NSURLConnection sendSynchronousRequest:request
                                                  returningResponse:&response
                                                              error:&error];
            if (error == nil)
            {
                result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] ;
            }
            else{
                result=nil;
            }
        }
        @catch (NSException *exception) {
            
        }
        return result;
        
    }
    @catch (NSException *exception) {
        
    }
}
+(id)GetMedicalEquipmentByName:(NSString*)name
{
    id result;
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@MedicalProductsList?query=%@", SERVER_IP,name]];
        NSMutableURLRequest *request = [NSURLRequest requestWithURL:url
                                                        cachePolicy:NSURLCacheStorageAllowed
                                                    timeoutInterval:120];
        NSURLResponse * response = nil;
        NSError * error = nil;
        NSData * data = [NSURLConnection sendSynchronousRequest:request
                                              returningResponse:&response
                                                          error:&error];
        if (error == nil)
        {
            result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] ;
        }
        else{
            result=nil;
        }
    }
    @catch (NSException *exception) {
        
    }
    return result;
}
+(id)GetQuestionsBySector:(NSString*)sector
{
    id result;
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@FAQs?sectionName=%@", SERVER_IP,sector]];
        NSMutableURLRequest *request = [NSURLRequest requestWithURL:url
                                                        cachePolicy:NSURLCacheStorageAllowed
                                                    timeoutInterval:120];
        NSURLResponse * response = nil;
        NSError * error = nil;
        NSData * data = [NSURLConnection sendSynchronousRequest:request
                                              returningResponse:&response
                                                          error:&error];
        if (error == nil)
        {
            result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] ;
        }
        else{
            result=nil;
        }
    }
    @catch (NSException *exception) {
        
    }
    return result;
    
    
}
+(id)GetAllAwarnessCenterArticles
{
    id result;
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@AwarenessCenter", SERVER_IP]];
        NSMutableURLRequest *request = [NSURLRequest requestWithURL:url
                                                        cachePolicy:NSURLCacheStorageAllowed
                                                    timeoutInterval:120];
        NSURLResponse * response = nil;
        NSError * error = nil;
        NSData * data = [NSURLConnection sendSynchronousRequest:request
                                              returningResponse:&response
                                                          error:&error];
        if (error == nil)
        {
            result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] ;
        }
        else{
            result=nil;
        }
    }
    @catch (NSException *exception) {
        
    }
    return result;
    
    
    
}
+(id)GetAwarnessCenterArticlesBySector:(NSString*)sector
{
    id result;
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@AwarenessCenter?sectionName=%@", SERVER_IP,sector]];
        NSMutableURLRequest *request = [NSURLRequest requestWithURL:url
                                                        cachePolicy:NSURLCacheStorageAllowed
                                                    timeoutInterval:120];
        NSURLResponse * response = nil;
        NSError * error = nil;
        NSData * data = [NSURLConnection sendSynchronousRequest:request
                                              returningResponse:&response
                                                          error:&error];
        if (error == nil)
        {
            result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] ;
        }
        else{
            result=nil;
        }
    }
    @catch (NSException *exception) {
        
    }
    return result;
    
    
}
+(id)GetElectronicServices{
    id result;
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@PublicService", SERVER_IP]];
        NSMutableURLRequest *request = [NSURLRequest requestWithURL:url
                                                        cachePolicy:NSURLCacheStorageAllowed
                                                    timeoutInterval:120];
        NSURLResponse * response = nil;
        NSError * error = nil;
        NSData * data = [NSURLConnection sendSynchronousRequest:request
                                              returningResponse:&response
                                                          error:&error];
        if (error == nil)
        {
            result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] ;
        }
        else{
            result=nil;
        }
    }
    @catch (NSException *exception) {
        
    }
    return result;
}
+(id)GetAboutInformationBySector:(NSString*)sector
{
    id result;
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@AdditionalInfo?sectionName=%@", SERVER_IP,sector]];
        NSMutableURLRequest *request = [NSURLRequest requestWithURL:url
                                                        cachePolicy:NSURLCacheStorageAllowed
                                                    timeoutInterval:120];
        NSURLResponse * response = nil;
        NSError * error = nil;
        NSData * data = [NSURLConnection sendSynchronousRequest:request
                                              returningResponse:&response
                                                          error:&error];
        if (error == nil)
        {
            result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] ;
        }
        else{
            result=nil;
        }
    }
    @catch (NSException *exception) {
        
    }
    return result;
    
}
+(id)GetImportantLinksList
{
    id result;
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@ImportantLink", SERVER_IP]];
        NSMutableURLRequest *request = [NSURLRequest requestWithURL:url
                                                        cachePolicy:NSURLCacheStorageAllowed
                                                    timeoutInterval:120];
        NSURLResponse * response = nil;
        NSError * error = nil;
        NSData * data = [NSURLConnection sendSynchronousRequest:request
                                              returningResponse:&response
                                                          error:&error];
        if (error == nil)
        {
            result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] ;
        }
        else{
            result=nil;
        }
    }
    @catch (NSException *exception) {
        
    }
    return result;
    
}
+(id)GetContactUsData
{
    id result;
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@ContactUs", SERVER_IP]];
        NSMutableURLRequest *request = [NSURLRequest requestWithURL:url
                                                        cachePolicy:NSURLCacheStorageAllowed
                                                    timeoutInterval:120];
        NSURLResponse * response = nil;
        NSError * error = nil;
        NSData * data = [NSURLConnection sendSynchronousRequest:request
                                              returningResponse:&response
                                                          error:&error];
        if (error == nil)
        {
            result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] ;
        }
        else{
            result=nil;
        }
    }
    @catch (NSException *exception) {
        
    }
    return result;
    
}
+(id)GetNewsDetailsByNewsID:(NSString*)news_id
{
    @try {
        
    }
    @catch (NSException *exception) {
        
    }
    
}
+(id)GetNewsListBySector:(NSString*)sector
{
    id result;
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@News?section=%@", SERVER_IP,sector]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url
                                                 cachePolicy:NSURLRequestUseProtocolCachePolicy
                                             timeoutInterval:300];
        NSURLResponse * response = nil;
        NSError * error = nil;
        NSData * data = [NSURLConnection sendSynchronousRequest:request
                                              returningResponse:&response
                                                          error:&error];
        if (error == nil)
        {
            result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] ;
        }
        else{
            result=nil;
        }
    }
    @catch (NSException *exception) {
        
    }
    return result;
}
@end
