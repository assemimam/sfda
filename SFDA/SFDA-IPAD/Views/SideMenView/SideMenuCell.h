//
//  SideMenuCell.h
//  sfda-ipad
//
//  Created by Assem Imam on 5/22/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *MenuItemIconImageView;
@property (weak, nonatomic) IBOutlet UILabel *MenuItemTitleLabel;

@end
