//
//  SideMenView.h
//  sfda-ipad
//
//  Created by Assem Imam on 5/22/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//
@protocol SideMenuDelegate <NSObject>

-(void)didSelectMenuItem:(id)ite Atindex:(int)index;

@end
#import <UIKit/UIKit.h>

@interface SideMenView : UIView<UITableViewDataSource,UITableViewDelegate>
@property(assign)id<SideMenuDelegate>delegate;
@end
