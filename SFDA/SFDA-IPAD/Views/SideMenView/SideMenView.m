//
//  SideMenView.m
//  sfda-ipad
//
//  Created by Assem Imam on 5/22/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "SideMenView.h"
#import "SideMenuCell.h"
#import "LibrariesConstants.h"

@interface SideMenView ()
{
    NSArray *MainMenuOptions;
}
@end
@implementation SideMenView
@synthesize delegate;
-(void)awakeFromNib{
     MainMenuOptions =[[Language getObject]getArrayWithKey:@"PopUpMenuOptions"];
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        MainMenuOptions =[[Language getObject]getArrayWithKey:@"PopUpMenuOptions"];
    }
    return self;
}
-(void)drawRect:(CGRect)rect
{
     MainMenuOptions =[[Language getObject]getArrayWithKey:@"PopUpMenuOptions"];
}
#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SideMenuCell*cell;
    @try {
        cell=(SideMenuCell*)[tableView dequeueReusableCellWithIdentifier:@"cleSideMenu"];
        if (cell==nil) {
            cell=(SideMenuCell*)[[[NSBundle mainBundle]loadNibNamed:@"SideMenuCell" owner:nil options:nil]objectAtIndex:0];
        }
        NSString*itemImageName =[[MainMenuOptions objectAtIndex:indexPath.row]objectForKey:@"image"];
        NSString*itemName=[[MainMenuOptions objectAtIndex:indexPath.row]objectForKey:@"option"];
        cell.MenuItemIconImageView.image = [UIImage imageNamed:itemImageName];
        cell.MenuItemTitleLabel.text=itemName;
        cell.MenuItemTitleLabel.font= [UIFont fontWithName:@"GESSTwoBold-Bold" size:17];
        UIView *SelectionView = [[UIView alloc]initWithFrame:cell.frame];
        cell.backgroundColor=[UIColor clearColor];
        SelectionView.backgroundColor = [UIColor colorWithRed:143/255.0f  green:143/255.0f blue:143/255.0f alpha:0.8];
        cell.selectedBackgroundView = SelectionView;
        
    }
    @catch (NSException *exception) {
        
    }
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [MainMenuOptions count];
}

#pragma -mark uitableview delegate methods
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return roundf(690/[MainMenuOptions count]);
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        if (self.delegate) {
            [self.delegate didSelectMenuItem:[MainMenuOptions objectAtIndex:indexPath.row] Atindex:indexPath.row];
        }
    }
    @catch (NSException *exception) {
        
    }
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
