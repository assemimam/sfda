//
//  BarcodeResultView.h
//  sfda-ipad
//
//  Created by Assem Imam on 6/18/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BarcodeResultView : UIView
{
    __weak IBOutlet UILabel *TitleLabel;
}
@property (weak, nonatomic) IBOutlet UILabel *BarcodeLabel;
@property (weak, nonatomic) IBOutlet UIButton *BackButton;
@property (weak, nonatomic) IBOutlet UIButton *SearchButton;
@property (weak, nonatomic) IBOutlet UIImageView *BarcodeImageView;
@end
