//
//  BarcodeResultView.m
//  sfda-ipad
//
//  Created by Assem Imam on 6/18/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "BarcodeResultView.h"
#import "CommonMethods.h"

@implementation BarcodeResultView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)drawRect:(CGRect)rect
{
    TitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:30];
    self.BackButton.titleLabel.font = [UIFont fontWithName:@"GESSTwoBold-Bold" size:13];
    self.SearchButton.titleLabel.font = [UIFont fontWithName:@"GESSTwoBold-Bold" size:13];
    [CommonMethods FormatView: self.BackButton];
}


@end
