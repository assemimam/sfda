//
//  DrugDetailsView.m
//  sfda-ipad
//
//  Created by assem on 6/15/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "DrugDetailsView.h"
#import "ListCell.h"
#import "LibrariesConstants.h"

@interface DrugDetailsView()
{
    id DrugDetailsList;
}
@end
@implementation DrugDetailsView
@synthesize RegistrationNumber,delegate;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    SHOW_LOADINGINDICATOR
    
    TitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:30];
    self.SubTitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:17];
    BackButton.titleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
    [NSThread detachNewThreadSelector:@selector(GetDrugDetails) toTarget:self withObject:nil];
    
}
-(void)HandleGetDrugDetailsResult
{
    @try {
        HIDE_LOADINGINDICATOR;
        [DetailsTableView reloadData];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)GetDrugDetails
{
    @try {
        id result = [Webservice GetDrugDetailsByRegistrationNumber:RegistrationNumber];
        
        NSMutableDictionary *dicGenericName = [[NSMutableDictionary alloc]init];
        [dicGenericName setValue:[result objectForKey:@"GenericName"] forKey:@"Value"];
        [dicGenericName setValue:@"الاسم العلمى" forKey:@"Description"];
        
        NSMutableDictionary *dicTradeName = [[NSMutableDictionary alloc]init];
        [dicTradeName setValue:[result objectForKey:@"TradeName"] forKey:@"Value"];
        [dicTradeName setValue:@"الاسم التجارى" forKey:@"Description"];
        
        NSMutableDictionary *dicPublicPrice = [[NSMutableDictionary alloc]init];
        [dicPublicPrice setValue:[result objectForKey:@"PublicPrice"] forKey:@"Value"];
        [dicPublicPrice setValue:@"سعر البيع للجمهور" forKey:@"Description"];
        
        NSMutableDictionary *dicRegistrationNo = [[NSMutableDictionary alloc]init];
        [dicRegistrationNo setValue:[result objectForKey:@"RegistrationNo"] forKey:@"Value"];
        [dicRegistrationNo setValue:@"رقم التسجيل" forKey:@"Description"];
        
        NSMutableDictionary *dicProductControl = [[NSMutableDictionary alloc]init];
        [dicProductControl setValue:[result objectForKey:@"ProductControl"] forKey:@"Value"];
        [dicProductControl setValue:@"المراقبة" forKey:@"Description"];
        
        NSMutableDictionary *dicVolume = [[NSMutableDictionary alloc]init];
        [dicVolume setValue:[result objectForKey:@"Volume"] forKey:@"Value"];
        [dicVolume setValue:@"الحجم" forKey:@"Description"];
        
        NSMutableDictionary *dicStrengthValue = [[NSMutableDictionary alloc]init];
        [dicStrengthValue setValue:[result objectForKey:@"StrengthValue"] forKey:@"Value"];
        [dicStrengthValue setValue:@"التركيز" forKey:@"Description"];
        
        NSMutableDictionary *dicPackageType = [[NSMutableDictionary alloc]init];
        [dicPackageType setValue:[result objectForKey:@"PackageType"] forKey:@"Value"];
        [dicPackageType setValue:@"نوع العبوة" forKey:@"Description"];
        
        NSMutableDictionary *dicPackageSize = [[NSMutableDictionary alloc]init];
        [dicPackageSize setValue:[result objectForKey:@"PackageSize"] forKey:@"Value"];
        [dicPackageSize setValue:@"الحجم" forKey:@"Description"];
        
        NSMutableDictionary *dicShelfLife = [[NSMutableDictionary alloc]init];
        [dicShelfLife setValue:[result objectForKey:@"ShelfLife"] forKey:@"Value"];
        [dicShelfLife setValue:@"مدة الصلاحية بالشهر" forKey:@"Description"];
        
        NSMutableDictionary *dicStorageConditions = [[NSMutableDictionary alloc]init];
        [dicStorageConditions setValue:[result objectForKey:@"StorageConditions"] forKey:@"Value"];
        [dicStorageConditions setValue:@"ظروف التخزين" forKey:@"Description"];
        
        NSMutableDictionary *dicDosageForm = [[NSMutableDictionary alloc]init];
        [dicDosageForm setValue:[result objectForKey:@"DosageForm"] forKey:@"Value"];
        [dicDosageForm setValue:@"الشكل الصيدلانى" forKey:@"Description"];
        
        NSMutableDictionary *dicRouteOfAdministration = [[NSMutableDictionary alloc]init];
        [dicRouteOfAdministration setValue:[result objectForKey:@"RouteOfAdministration"] forKey:@"Value"];
        [dicRouteOfAdministration setValue:@"طريقة الاستعمال" forKey:@"Description"];
        
        NSMutableDictionary *dicLegalStatus = [[NSMutableDictionary alloc]init];
        [dicLegalStatus setValue:[result objectForKey:@"DosageForm"] forKey:@"Value"];
        [dicLegalStatus setValue:@"طريقة الوصف" forKey:@"Description"];
        
        NSMutableDictionary *dicManufacturerName = [[NSMutableDictionary alloc]init];
        [dicManufacturerName setValue:[result objectForKey:@"ManufacturerName"] forKey:@"Value"];
        [dicManufacturerName setValue:@"اسم المصنع" forKey:@"Description"];
        
        NSMutableArray *ResultArray = [[NSMutableArray alloc]init];
        [ResultArray addObject:dicGenericName];
        [ResultArray addObject:dicTradeName];
        [ResultArray addObject:dicPublicPrice];
        [ResultArray addObject:dicRegistrationNo];
        [ResultArray addObject:dicProductControl];
        [ResultArray addObject:dicVolume];
        [ResultArray addObject:dicStrengthValue];
        [ResultArray addObject:dicPackageType];
        [ResultArray addObject:dicPackageSize];
        [ResultArray addObject:dicShelfLife];
        [ResultArray addObject:dicStorageConditions];
        [ResultArray addObject:dicDosageForm];
        [ResultArray addObject:dicRouteOfAdministration];
        [ResultArray addObject:dicLegalStatus];
        [ResultArray addObject:dicManufacturerName];
        
        DrugDetailsList = [[NSArray alloc]initWithArray:ResultArray];
        [self performSelectorOnMainThread:@selector(HandleGetDrugDetailsResult) withObject:nil waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
}

#pragma -mark  uitableview datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    @try {
        return  [DrugDetailsList count];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ListCell* cell;
    @try {
        
        cell =(ListCell*)[tableView dequeueReusableCellWithIdentifier:@"cle"];
        if (cell == nil) {
            cell =(ListCell*)[[[NSBundle mainBundle]loadNibNamed:@"ListCell" owner:nil options:nil]objectAtIndex:0];
        }
        cell.ItemTitleLabel.textColor=[UIColor colorWithRed:65/255.0f green:136/255.0f blue:175/255.0f alpha:1];;
        cell.ItemTitleLabel.font= [UIFont fontWithName:@"GESSTwoBold-Bold" size:14];
        cell.ItemValueLabel.font= [UIFont fontWithName:@"GESSTwoBold-Bold" size:12];
        
        cell.ItemTitleLabel.text = [[DrugDetailsList objectAtIndex:indexPath.row]objectForKey:@"Description"];
        cell.ItemValueLabel.text = [[DrugDetailsList objectAtIndex:indexPath.row]
                                    objectForKey:@"Value"];
        
       
        [cell.SeperatorLabel setHidden:NO];
        cell.SeperatorLabel.backgroundColor =[UIColor colorWithRed:65/255.0f green:136/255.0f blue:175/255.0f alpha:1];
        
        
        
        
    }
    @catch (NSException *exception) {
        
    }
    
    return  cell;
    
}
#pragma -mark tableview delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        
        NSString *txtValue,*txtDescription;
        txtValue = [NSNull class ]!=[[[DrugDetailsList objectAtIndex:indexPath.row]objectForKey:@"Value"]class]?[[DrugDetailsList  objectAtIndex:indexPath.row]objectForKey:@"Value"]:@" ";
        txtDescription =[[DrugDetailsList objectAtIndex:indexPath.row]objectForKey:@"Description"];
        CGSize constraint = CGSizeMake(tableView.frame.size.width, 200000000000.0f);
        CGSize valueSize;
        if ([[[DrugDetailsList  objectAtIndex:indexPath.row]objectForKey:@"Description"] isEqualToString:@"Separator"]) {
            valueSize = CGSizeMake(tableView.frame.size.width, 3);
        }
        else{
            valueSize =([[DrugDetailsList  objectAtIndex:indexPath.row]objectForKey:@"Value"]!=[NSNull null])? [txtValue sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping]:CGSizeMake(tableView.frame.size.width, 3);
        }
        
        CGFloat height = MAX(valueSize.height, 40);
        
        return height;
        
    }
    @catch (NSException *exception) {
        
    }
    
}


- (IBAction)BackButtonAction:(UIButton *)sender {
    if (self.delegate) {
        [self.delegate didClickDrugDetailsBackButton];
    }
}
@end
