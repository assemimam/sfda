//
//  DrugDetailsView.h
//  sfda-ipad
//
//  Created by assem on 6/15/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//
@protocol DrugDetailsDelegate <NSObject>

@optional
-(void)didClickDrugDetailsBackButton;
@end
#import <UIKit/UIKit.h>

@interface DrugDetailsView : UIView<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UILabel *TitleLabel;
    
    __weak IBOutlet UIButton *BackButton;
    __weak IBOutlet UITableView *DetailsTableView;
}
- (IBAction)BackButtonAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *SubTitleLabel;

@property(assign)id<DrugDetailsDelegate> delegate;
@property(nonatomic,retain)NSString*RegistrationNumber;
@end
