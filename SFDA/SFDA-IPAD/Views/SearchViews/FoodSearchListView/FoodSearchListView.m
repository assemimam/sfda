//
//  FoodSearchListView.m
//  sfda-ipad
//
//  Created by Assem Imam on 6/18/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "FoodSearchListView.h"
#import "FoodCell.h"

@implementation FoodSearchListView
@synthesize delegate,FoodList;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)awakeFromNib
{
    [FoodListTableView reloadData];
    TitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:30];
    BackButton.titleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
    self.SearchTitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:20];
}

- (IBAction)BackButtonAction:(UIButton *)sender {
    if (self.delegate) {
        [self.delegate didClickFoodListBackButton];
    }
}

-(void)ReLoadData
{
    [FoodListTableView reloadData];
}

#pragma -mark  uitableview datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    @try {
        return  [self.FoodList count];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    FoodCell* cell;
    @try {
        
        cell =(FoodCell*)[tableView dequeueReusableCellWithIdentifier:@"cle"];
        if (cell == nil) {
            cell =(FoodCell*)[[[NSBundle mainBundle]loadNibNamed:@"FoodCell" owner:nil options:nil]objectAtIndex:0];
        }
    
        cell.ProductNameLabel.font= [UIFont fontWithName:@"GESSTwoBold-Bold" size:20];
        cell.ComanyNameLabel.font= [UIFont fontWithName:@"GESSTwoLight-Light" size:15];
      
        cell.ProductNameLabel.text = [[self.FoodList objectAtIndex:indexPath.row]objectForKey:@"ItemDescription"];
        cell.ComanyNameLabel.text = [[self.FoodList objectAtIndex:indexPath.row]
                                    objectForKey:@"EstablishmentInfo.ArabicEstablishmentName"];

    }
    @catch (NSException *exception) {
        
    }
    
    return  cell;
    
}
#pragma -mark tableview delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        
        NSString *txtProductName,*txtCompanyName;
        txtProductName= [NSNull class ]!=[[[self.FoodList objectAtIndex:indexPath.row]objectForKey:@"ItemDescription"]class]?[[self.FoodList  objectAtIndex:indexPath.row]objectForKey:@"ItemDescription"]:@" ";
        txtCompanyName =[[self.FoodList objectAtIndex:indexPath.row]objectForKey:@"EstablishmentInfo.ArabicEstablishmentName"];
        
        CGSize constraint = CGSizeMake(tableView.frame.size.width, 200000000000.0f);
        CGSize ProductNameSize,CompanyNameSize;
        ProductNameSize =([[self.FoodList objectAtIndex:indexPath.row]objectForKey:@"ItemDescription"]!=[NSNull null])? [txtProductName sizeWithFont:[UIFont fontWithName:@"GESSTwoBold-Bold" size:30] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping]:CGSizeMake(tableView.frame.size.width, 3);
        CompanyNameSize =([[self.FoodList objectAtIndex:indexPath.row]objectForKey:@"EstablishmentInfo.ArabicEstablishmentName"]!=[NSNull null])? [txtCompanyName sizeWithFont:[UIFont fontWithName:@"GESSTwoLight-Light" size:30] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping]:CGSizeMake(tableView.frame.size.width, 3);

        
        CGFloat height = MAX(ProductNameSize.height+ CompanyNameSize.height, 50);
        
        return height;
        
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        @try {
            if (self.delegate) {
                [self.delegate didSelecteFood:[FoodList objectAtIndex:indexPath.row]];
            }
        }
        @catch (NSException *exception) {
            
        }

    }
    @catch (NSException *exception) {
        
    }
   
}


@end
