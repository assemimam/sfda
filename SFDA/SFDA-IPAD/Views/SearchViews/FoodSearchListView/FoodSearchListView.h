//
//  FoodSearchListView.h
//  sfda-ipad
//
//  Created by Assem Imam on 6/18/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//
@protocol FoodSearchListDelegate <NSObject>
@optional
-(void)didSelecteFood:(id)item;
-(void)didClickFoodListBackButton;
@end

#import <UIKit/UIKit.h>

@interface FoodSearchListView : UIView<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UIButton *BackButton;
    __weak IBOutlet UILabel *TitleLabel;
    __weak IBOutlet UITableView *FoodListTableView;
}
@property (weak, nonatomic) IBOutlet UILabel *SearchTitleLabel;
- (IBAction)BackButtonAction:(UIButton *)sender;
-(void)ReLoadData;
@property(assign)id<FoodSearchListDelegate> delegate;
@property(nonatomic,retain)NSArray*FoodList;
@end
