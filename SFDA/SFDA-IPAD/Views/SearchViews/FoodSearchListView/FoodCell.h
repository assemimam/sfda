//
//  FoodCell.h
//  sfda-ipad
//
//  Created by Assem Imam on 6/18/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *ProductNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *ComanyNameLabel;

@end
