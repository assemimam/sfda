//
//  FoodSearchView.h
//  sfda-ipad
//
//  Created by Assem Imam on 6/12/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//
@protocol FoodSearchDelegate <NSObject>

@optional
-(void)didClickCameraButton;
-(void)didClickFoodSearchButton:(UIButton*)sender;
@end
#import <UIKit/UIKit.h>

@interface FoodSearchView : UIView<UITextFieldDelegate>
{
    
    __weak IBOutlet UIButton *SearchButton;
    __weak IBOutlet UILabel *TitleLabel;
    __weak IBOutlet UILabel *BarcodeTextLabel;
    __weak IBOutlet UITextField *BarcodeTextField;
}
@property(assign)id<FoodSearchDelegate> delegate;

@end
