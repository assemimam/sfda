//
//  FoodSearchView.m
//  sfda-ipad
//
//  Created by Assem Imam on 6/12/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "FoodSearchView.h"
#import "Language.h"

@interface FoodSearchView()
- (IBAction)SearchButtonAction:(UIButton *)sender;
- (IBAction)BarcdeButtonAction:(UIButton *)sender;
@end
@implementation FoodSearchView
@synthesize delegate;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)awakeFromNib
{
    @try {
        
        TitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:30];
        BarcodeTextLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
       
    }
    @catch (NSException *exception) {
        
    }
   
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)SearchButtonAction:(UIButton *)sender {
    @try {
        [BarcodeTextField resignFirstResponder];
        if (!internetConnection) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            return;
            
        }
        
        if (self.delegate) {
            if ([BarcodeTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length==0) {
                
                 [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"EnterBarcodeMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
                return;
            }
            sender.accessibilityLabel = BarcodeTextField.text;
            [self.delegate didClickFoodSearchButton:sender];
        }
    }
    @catch (NSException *exception) {
        
    }
    
}
- (IBAction)BarcdeButtonAction:(UIButton *)sender {
    @try {
        if (self.delegate) {
            [self.delegate didClickCameraButton];
        }
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark uitextfield delegate
#define ACCEPTABLE_CHARACTERS @"0123456789"
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
    
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    if ([string isEqualToString:filtered]) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 20) ? NO : YES;
    }
    else{
       return NO;
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    @try {
        [textField resignFirstResponder];
        if (!internetConnection) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            return YES;
            
        }
        
        if (self.delegate) {
            if ([BarcodeTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length==0) {
                
                [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"EnterBarcodeMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
                return YES;
            }
          
           SearchButton.accessibilityLabel = BarcodeTextField.text;
            [self.delegate didClickFoodSearchButton:SearchButton];
        }

    }
    @catch (NSException *exception) {
        
    }
}
@end
