//
//  MedicalDetailsView.m
//  sfda-ipad
//
//  Created by assem on 6/16/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "MedicalDetailsView.h"
#import "ListCell.h"
#import "LibrariesConstants.h"
@interface MedicalDetailsView ()
{
    id MedicalDetailsList;
}
@end
@implementation MedicalDetailsView
@synthesize delegate,ProductNumber;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    SHOW_LOADINGINDICATOR
    TitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:30];
    self.SubTitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:17];
    BackButton.titleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
    [NSThread detachNewThreadSelector:@selector(GetMedicalDetails) toTarget:self withObject:nil];
    
}
-(void)HandleGetMedicalDetailsResult
{
    @try {
        HIDE_LOADINGINDICATOR;
        [DetailsTableView reloadData];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)GetMedicalDetails
{
    @try {
        id result = [Webservice GetMedicalEquipmentDetailsByProductNumber:ProductNumber];
        
        NSMutableDictionary *dicGenericName = [[NSMutableDictionary alloc]init];
        [dicGenericName setValue:[result objectForKey:@"Listing_Number"] forKey:@"Value"];
        [dicGenericName setValue:@"رقم قيد الجهاز" forKey:@"Description"];
        
        NSMutableDictionary *dicTradeName = [[NSMutableDictionary alloc]init];
        [dicTradeName setValue:[result objectForKey:@"Brand_Name"] forKey:@"Value"];
        [dicTradeName setValue:@"الاسم التجارى" forKey:@"Description"];
        
        NSMutableDictionary *dicPublicPrice = [[NSMutableDictionary alloc]init];
        [dicPublicPrice setValue:[result objectForKey:@"Product_Description"] forKey:@"Value"];
        [dicPublicPrice setValue:@"وصف المنتج" forKey:@"Description"];
        
        NSMutableDictionary *dicRegistrationNo = [[NSMutableDictionary alloc]init];
        [dicRegistrationNo setValue:[result objectForKey:@"Authorization_Number"] forKey:@"Value"];
        [dicRegistrationNo setValue:@"رقم اذن التسويق" forKey:@"Description"];
        
        NSMutableDictionary *dicProductControl = [[NSMutableDictionary alloc]init];
        [dicProductControl setValue:[result objectForKey:@"Authorization_Validity"] forKey:@"Value"];
        [dicProductControl setValue:@"صلاحية اذن التسويق" forKey:@"Description"];
        
        NSMutableDictionary *dicVolume = [[NSMutableDictionary alloc]init];
        [dicVolume setValue:[result objectForKey:@"AR_Name"] forKey:@"Value"];
        [dicVolume setValue:@"الممثل القانونى" forKey:@"Description"];
        
        NSMutableDictionary *dicStrengthValue = [[NSMutableDictionary alloc]init];
        [dicStrengthValue setValue:[result objectForKey:@"PHONE"] forKey:@"Value"];
        [dicStrengthValue setValue:@"التليفون" forKey:@"Description"];
        
        NSMutableDictionary *dicPackageType = [[NSMutableDictionary alloc]init];
        [dicPackageType setValue:[result objectForKey:@"FAX"] forKey:@"Value"];
        [dicPackageType setValue:@"الفاكس" forKey:@"Description"];
        
        
        NSMutableArray *ResultArray = [[NSMutableArray alloc]init];
        [ResultArray addObject:dicGenericName];
        [ResultArray addObject:dicTradeName];
        [ResultArray addObject:dicPublicPrice];
        [ResultArray addObject:dicRegistrationNo];
        [ResultArray addObject:dicProductControl];
        [ResultArray addObject:dicVolume];
        [ResultArray addObject:dicStrengthValue];
        [ResultArray addObject:dicPackageType];

        
        MedicalDetailsList = [[NSArray alloc]initWithArray:ResultArray];
        [self performSelectorOnMainThread:@selector(HandleGetMedicalDetailsResult) withObject:nil waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
}

#pragma -mark  uitableview datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    @try {
        return  [MedicalDetailsList count];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ListCell* cell;
    @try {
        
        cell =(ListCell*)[tableView dequeueReusableCellWithIdentifier:@"cle"];
        if (cell == nil) {
            cell =(ListCell*)[[[NSBundle mainBundle]loadNibNamed:@"ListCell" owner:nil options:nil]objectAtIndex:0];
        }
        cell.ItemTitleLabel.textColor=[UIColor colorWithRed:216/255.0f green:102/255.0f blue:86/255.0f alpha:1];
        cell.ItemTitleLabel.font= [UIFont fontWithName:@"GESSTwoBold-Bold" size:14];
        cell.ItemValueLabel.font= [UIFont fontWithName:@"GESSTwoBold-Bold" size:12];
        cell.ItemTitleLabel.text = [[MedicalDetailsList objectAtIndex:indexPath.row]objectForKey:@"Description"];
        cell.ItemValueLabel.text = [[MedicalDetailsList objectAtIndex:indexPath.row]
                                    objectForKey:@"Value"];
        
        
        [cell.SeperatorLabel setHidden:NO];
        cell.SeperatorLabel.backgroundColor =[UIColor colorWithRed:216/255.0f green:102/255.0f blue:86/255.0f alpha:1];
        
        
        
        
    }
    @catch (NSException *exception) {
        
    }
    
    return  cell;
    
}
#pragma -mark tableview delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        
        NSString *txtValue,*txtDescription;
        txtValue = [NSNull class ]!=[[[MedicalDetailsList objectAtIndex:indexPath.row]objectForKey:@"Value"]class]?[[MedicalDetailsList  objectAtIndex:indexPath.row]objectForKey:@"Value"]:@" ";
        txtDescription =[[MedicalDetailsList objectAtIndex:indexPath.row]objectForKey:@"Description"];
        CGSize constraint = CGSizeMake(tableView.frame.size.width, 200000000000.0f);
        CGSize valueSize;
        if ([[[MedicalDetailsList  objectAtIndex:indexPath.row]objectForKey:@"Description"] isEqualToString:@"Separator"]) {
            valueSize = CGSizeMake(tableView.frame.size.width, 3);
        }
        else{
            valueSize =([[MedicalDetailsList  objectAtIndex:indexPath.row]objectForKey:@"Value"]!=[NSNull null])? [txtValue sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping]:CGSizeMake(tableView.frame.size.width, 3);
        }
        
        CGFloat height = MAX(valueSize.height, 30);
        
        return height+10;
        
    }
    @catch (NSException *exception) {
        
    }
    
}


- (IBAction)BackButtonAction:(UIButton *)sender {
    if (self.delegate) {
        [self.delegate didClickMedicalDetailsBackButton];
    }
}

@end
