//
//  MedicalDetailsView.h
//  sfda-ipad
//
//  Created by assem on 6/16/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//
@protocol MedicalDetailsDelegate <NSObject>

@optional
-(void)didClickMedicalDetailsBackButton;
@end
#import <UIKit/UIKit.h>

@interface MedicalDetailsView : UIView<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UILabel *TitleLabel;
    __weak IBOutlet UIButton *BackButton;
    __weak IBOutlet UITableView *DetailsTableView;
}
- (IBAction)BackButtonAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *SubTitleLabel;

@property(assign)id<MedicalDetailsDelegate> delegate;
@property(nonatomic,retain)NSString*ProductNumber;


@end
