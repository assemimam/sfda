//
//  MedicalSearchView.h
//  sfda-ipad
//
//  Created by assem on 6/15/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//
@protocol MedicalSearchDelegate <NSObject>

@optional
-(void)didClickMedicalSearchButton:(id)searcheString;
@end
#import <UIKit/UIKit.h>

@interface MedicalSearchView : UIView<UITextFieldDelegate>
{
    __weak IBOutlet UILabel *TitleLabel;
    __weak IBOutlet UITextField *SearchTextField;
}
- (IBAction)SearchButtonAction:(UIButton *)sender;
@property(assign)id<MedicalSearchDelegate>delegate;
@end
