//
//  MedicalSearchView.m
//  sfda-ipad
//
//  Created by assem on 6/15/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "MedicalSearchView.h"

@implementation MedicalSearchView
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)awakeFromNib{
    @try {
        TitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:30];

    }
    @catch (NSException *exception) {
        
    }
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)SearchButtonAction:(UIButton *)sender {
    @try {
        
        [SearchTextField resignFirstResponder];
        if (!internetConnection) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            return ;
            
        }
        if ([SearchTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]].length==0) {
            [[[UIAlertView alloc]initWithTitle:nil message:@"برجاء ادخال اسم الجهاز" delegate:nil cancelButtonTitle:@"موافق" otherButtonTitles:nil, nil]show];
            return;
        }
        if (self.delegate) {
            [self.delegate didClickMedicalSearchButton:SearchTextField.text];
        }
    }
    @catch (NSException *exception) {
        
    }
   
}
#pragma -mark uitextfield delegate
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
//    
//    NSUInteger newLength = [textField.text length] + [string length] - range.length;
//    return (newLength > 20) ? NO : YES;
//    
//}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    @try {
        [textField resignFirstResponder];
        if (!internetConnection) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            return YES;
            
        }
        if (self.delegate) {
            [self.delegate didClickMedicalSearchButton:SearchTextField.text];
        }
    }
    @catch (NSException *exception) {
        
    }
}
@end
