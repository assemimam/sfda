//
//  FoodDetailsView.h
//  sfda-ipad
//
//  Created by Assem Imam on 6/18/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//
@protocol FoodDetailsDelegate <NSObject>

@optional
-(void)didClickFoodDetailsBackButton;
@end
#import <UIKit/UIKit.h>

@interface FoodDetailsView : UIView<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UILabel *HintLabel;
    __weak IBOutlet UILabel *TitleLabel;
    __weak IBOutlet UIButton *BackButton;
    __weak IBOutlet UITableView *DetailsTableView;
}
- (IBAction)BackButtonAction:(UIButton *)sender;
@property(assign)id<FoodDetailsDelegate> delegate;
@property(nonatomic,retain)id Product;
@end
