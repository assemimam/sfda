//
//  FoodDetailsView.m
//  sfda-ipad
//
//  Created by Assem Imam on 6/18/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "FoodDetailsView.h"
#import "ListCell.h"
#import "Language.h"

@interface FoodDetailsView()
{
    id FoodDetailsList;
}
@end
@implementation FoodDetailsView
@synthesize Product,delegate;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)drawRect:(CGRect)rect
{
    HintLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:20];
    TitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:30];
    BackButton.titleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];

    
    NSMutableDictionary *dicState = [[NSMutableDictionary alloc]init];
    [dicState setValue:[[Language getObject]getStringWithKey:@"FoodProduct"]forKey:@"Value"];
    [dicState setValue:[[Language getObject]getStringWithKey:@"ProductState"] forKey:@"Description"];
    
    NSMutableDictionary *dicTradeName = [[NSMutableDictionary alloc]init];
    [dicTradeName setValue:[NSNull null]!=[Product objectForKey:@"TradeName"]&&[Product objectForKey:@"TradeName"]?[Product objectForKey:@"TradeName"]:@"لا يوجد" forKey:@"Value"];
    [dicTradeName setValue:[[Language getObject]getStringWithKey:@"TradeName"] forKey:@"Description"];
    
    NSMutableDictionary *dicOrginCountry = [[NSMutableDictionary alloc]init];
    [dicOrginCountry setValue:[NSNull null]!=[Product objectForKey:@"OrginCountry"]&&[Product objectForKey:@"OrginCountry"]?[Product objectForKey:@"OrginCountry"]:@"لا يوجد" forKey:@"Value"];
    [dicOrginCountry setValue:[[Language getObject]getStringWithKey:@"OrginCountry"] forKey:@"Description"];
    
    NSMutableDictionary *dicPackagingTypes = [[NSMutableDictionary alloc]init];
    [dicPackagingTypes setValue:[NSNull null]!=[Product objectForKey:@"PackagingTypes"]&& [Product objectForKey:@"PackagingTypes"]?[Product objectForKey:@"PackagingTypes"]:@"لا يوجد" forKey:@"Value"];
    [dicPackagingTypes setValue:[[Language getObject]getStringWithKey:@"PackagingTypes"]forKey:@"Description"];
    
    NSMutableDictionary *dicShelfLife = [[NSMutableDictionary alloc]init];
    [dicShelfLife setValue:[NSNull null]!=[Product objectForKey:@"ShelfLife"]&&[Product objectForKey:@"ShelfLife"]?[Product objectForKey:@"ShelfLife"]:@"لا يوجد"forKey:@"Value"];
    [dicShelfLife setValue:[[Language getObject]getStringWithKey:@"ShelfLife"]forKey:@"Description"];
    
    NSMutableDictionary *dicStorageTemperatures = [[NSMutableDictionary alloc]init];
    [dicStorageTemperatures setValue:[NSNull null]!=[Product objectForKey:@"StorageTemperatures"]&&[Product objectForKey:@"StorageTemperatures"]?[Product objectForKey:@"StorageTemperatures"]:@"لا يوجد" forKey:@"Value"];
    [dicStorageTemperatures setValue:[[Language getObject]getStringWithKey:@"StorageTemperatures"] forKey:@"Description"];
    
    NSMutableDictionary *dicUsageMethods = [[NSMutableDictionary alloc]init];
    [dicUsageMethods setValue:[NSNull null]!=[Product objectForKey:@"UsageMethods"]&&[Product objectForKey:@"UsageMethods"]?[Product objectForKey:@"UsageMethods"]:@"لا يوجد"forKey:@"Value"];
    [dicUsageMethods setValue:[[Language getObject]getStringWithKey:@"UsageMethods"]forKey:@"Description"];
    
    NSMutableDictionary *dicAgeGroups= [[NSMutableDictionary alloc]init];
    [dicAgeGroups setValue:[NSNull null]!=[Product objectForKey:@"AgeGroups"] && [Product objectForKey:@"AgeGroups"] ?[Product objectForKey:@"AgeGroups"] :@"لا يوجد" forKey:@"Value"];
    [dicAgeGroups setValue:[[Language getObject]getStringWithKey:@"AgeGroups"] forKey:@"Description"];
    
    NSMutableDictionary *dicItemWeight = [[NSMutableDictionary alloc]init];
    [dicItemWeight setValue:[NSNull null]!=[Product objectForKey:@"ItemWeight"] && [Product objectForKey:@"ItemWeight"]?[NSString stringWithFormat:@"%0.1f",[[Product objectForKey:@"ItemWeight"]floatValue]]:@"لا يوجد" forKey:@"Value"];
    [dicItemWeight setValue:[[Language getObject]getStringWithKey:@"ItemWeight"]forKey:@"Description"];
    
    NSMutableDictionary *dicItemWeightUnit = [[NSMutableDictionary alloc]init];
    [dicItemWeightUnit setValue:[NSNull null]!=[Product objectForKey:@"ItemWeightUnit"] && [Product objectForKey:@"ItemWeightUnit"]?[Product objectForKey:@"ItemWeightUnit"]:@"لا يوجد"forKey:@"Value"];
    [dicItemWeightUnit setValue:[[Language getObject]getStringWithKey:@"ItemWeightUnit"]forKey:@"Description"];
    
    NSMutableDictionary *dicWarnings= [[NSMutableDictionary alloc]init];
    [dicWarnings setValue:[NSNull null]!=[Product objectForKey:@"Warnings"] && [Product objectForKey:@"Warnings"]?[Product objectForKey:@"Warnings"]:@"لا يوجد"forKey:@"Value"];
    [dicWarnings setValue:[[Language getObject]getStringWithKey:@"Warnings"] forKey:@"Description"];
    
    
    NSMutableArray *ResultArray = [[NSMutableArray alloc]init];
    [ResultArray addObject:dicState];
    [ResultArray addObject:dicTradeName];
    [ResultArray addObject:dicOrginCountry];
    [ResultArray addObject:dicPackagingTypes];
    [ResultArray addObject:dicShelfLife];
    [ResultArray addObject:dicStorageTemperatures];
    [ResultArray addObject:dicUsageMethods];
    [ResultArray addObject:dicAgeGroups];
    [ResultArray addObject:dicItemWeight];
    [ResultArray addObject:dicItemWeightUnit];
    [ResultArray addObject:dicWarnings];
    
    FoodDetailsList = [[NSArray alloc]initWithArray:ResultArray];
    [DetailsTableView reloadData];
    
}

#pragma -mark  uitableview datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    @try {
        return  [FoodDetailsList count];
    }
    @catch (NSException *exception) {
        
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ListCell* cell;
    @try {
    
        cell =(ListCell*)[tableView dequeueReusableCellWithIdentifier:@"cle"];
        if (cell == nil) {
            cell =(ListCell*)[[[NSBundle mainBundle]loadNibNamed:@"ListCell" owner:nil options:nil]objectAtIndex:0];
        }
        cell.ItemTitleLabel.textColor=[UIColor colorWithRed:82/255.0f green:160/255.0f blue:102/255.0f alpha:1];
        cell.ItemTitleLabel.font= [UIFont fontWithName:@"GESSTwoBold-Bold" size:14];
        cell.ItemValueLabel.font= [UIFont fontWithName:@"GESSTwoBold-Bold" size:12];
        cell.ItemTitleLabel.text = [[FoodDetailsList objectAtIndex:indexPath.row]objectForKey:@"Description"];
        cell.ItemValueLabel.text = [[FoodDetailsList objectAtIndex:indexPath.row]objectForKey:@"Value"];
        [cell.SeperatorLabel setHidden:NO];
        cell.SeperatorLabel.backgroundColor =[UIColor colorWithRed:82/255.0f green:160/255.0f blue:102/255.0f alpha:1];
        
    }
    @catch (NSException *exception) {
        
    }
    
    return  cell;
}
#pragma -mark tableview delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        
        NSString *txtValue,*txtDescription;
        txtValue = [NSNull class ]!=[[[FoodDetailsList objectAtIndex:indexPath.row]objectForKey:@"Value"]class]?[[FoodDetailsList  objectAtIndex:indexPath.row]objectForKey:@"Value"]:@" ";
        txtDescription =[[FoodDetailsList objectAtIndex:indexPath.row]objectForKey:@"Description"];
        CGSize constraint = CGSizeMake(tableView.frame.size.width, 200000000000.0f);
        CGSize valueSize;
        if ([[[FoodDetailsList  objectAtIndex:indexPath.row]objectForKey:@"Description"] isEqualToString:@"Separator"]) {
            valueSize = CGSizeMake(tableView.frame.size.width, 3);
        }
        else{
            valueSize =([[FoodDetailsList  objectAtIndex:indexPath.row]objectForKey:@"Value"]!=[NSNull null])? [txtValue sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping]:CGSizeMake(tableView.frame.size.width, 3);
        }
        
        CGFloat height = MAX(valueSize.height+10, 50);
        
        return height;
        
    }
    @catch (NSException *exception) {
        
    }
    
}


- (IBAction)BackButtonAction:(UIButton *)sender {
    if (self.delegate) {
        [self.delegate didClickFoodDetailsBackButton];
    }
}
@end
