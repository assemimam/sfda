//
//  DrugsSearchView.m
//  sfda-ipad
//
//  Created by assem on 6/15/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "DrugsSearchView.h"


@implementation DrugsSearchView
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)awakeFromNib{
    @try {
        TitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:30];
        
    }
    @catch (NSException *exception) {
        
    }
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)SearchButtonAction:(UIButton *)sender {
    @try {

        [SearchTextField resignFirstResponder];
        if ([SearchTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]].length==0) {
            [[[UIAlertView alloc]initWithTitle:nil message:@"برجاء ادخال اسم الدواء" delegate:nil cancelButtonTitle:@"موافق" otherButtonTitles:nil, nil]show];
            return;
        }

        if (self.delegate) {
            [self.delegate didClickDrugsSearchButton:SearchTextField.text];
        }
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark uitextfield delegate
//#define ACCEPTABLE_CHARACTERS @"0123456789"
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
//    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
//    
//    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
//    if ([string isEqualToString:filtered]) {
//        NSUInteger newLength = [textField.text length] + [string length] - range.length;
//        return (newLength > 20) ? NO : YES;
//    }
//    else{
//        return NO;
//    }
//}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    @try {
        [textField resignFirstResponder];
        if (self.delegate) {
            [self.delegate didClickDrugsSearchButton:textField.text];
        }
    }
    @catch (NSException *exception) {
        
    }
}
@end
