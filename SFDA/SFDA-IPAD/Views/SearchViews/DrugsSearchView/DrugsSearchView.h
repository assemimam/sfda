//
//  DrugsSearchView.h
//  sfda-ipad
//
//  Created by assem on 6/15/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//
@protocol DrugsSearchDelegate <NSObject>

@optional
-(void)didClickDrugsSearchButton:(id)searcheString;
@end
#import <UIKit/UIKit.h>

@interface DrugsSearchView : UIView<UITextFieldDelegate>
{
    __weak IBOutlet UITextField *SearchTextField;
    __weak IBOutlet UILabel *TitleLabel;
}
@property(assign)id<DrugsSearchDelegate> delegate;
- (IBAction)SearchButtonAction:(UIButton *)sender;
@end
