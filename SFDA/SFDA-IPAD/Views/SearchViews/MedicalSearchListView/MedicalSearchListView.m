//
//  MedicalSearchListView.m
//  sfda-ipad
//
//  Created by assem on 6/16/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "MedicalSearchListView.h"
#import "ListCell.h"

@implementation MedicalSearchListView
@synthesize MedicalList,delegate;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)drawRect:(CGRect)rect
{
    
    TitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:30];
    self.SubTitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:17];
    BackButton.titleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
}

- (IBAction)BackButtonAction:(UIButton *)sender {
    if (self.delegate) {
        [self.delegate didClickMedicalListBackButton];
    }
}

-(void)ReLoadData
{
    [MedicalListTableView reloadData];
}
-(void)BackGroundButtonAction:(UIButton*)sender
{
    @try {
        if (self.delegate) {
            [self.delegate didSelecteMedicalEquipment:sender.accessibilityLabel];
        }
    }
    @catch (NSException *exception) {
        
    }
    
}
#pragma -mark  uitableview datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    @try {
        return  [self.MedicalList count];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ListCell* cell;
    @try {
        
        cell =(ListCell*)[tableView dequeueReusableCellWithIdentifier:@"cle"];
        if (cell == nil) {
            cell =(ListCell*)[[[NSBundle mainBundle]loadNibNamed:@"ListCell" owner:nil options:nil]objectAtIndex:0];
        }
        cell.ItemTitleLabel.textColor=[UIColor colorWithRed:216/255.0f green:102/255.0f blue:86/255.0f alpha:1];
        cell.ItemTitleLabel.font= [UIFont fontWithName:@"GESSTwoBold-Bold" size:14];
        cell.ItemValueLabel.font= [UIFont fontWithName:@"GESSTwoBold-Bold" size:12];
        cell.BackGroundButton.accessibilityLabel =[NSString stringWithFormat:@"%@",[[self.MedicalList objectAtIndex:indexPath.row]objectForKey:@"SearchKey"]];
        [cell.BackGroundButton addTarget:self action:@selector(BackGroundButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.ItemTitleLabel.text = [[self.MedicalList objectAtIndex:indexPath.row]objectForKey:@"Description"];
        cell.ItemValueLabel.text =[NSString stringWithFormat:@"%@", [[self.MedicalList objectAtIndex:indexPath.row]
                                    objectForKey:@"Value"]];
        
        if ((indexPath.row+1)%4==0) {
            [cell.SeperatorLabel setHidden:NO];
            cell.SeperatorLabel.backgroundColor =[UIColor colorWithRed:216/255.0f green:102/255.0f blue:86/255.0f alpha:1];
        }
        
        
        
    }
    @catch (NSException *exception) {
        
    }
    
    return  cell;
    
}
#pragma -mark tableview delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        
        NSString *txtValue,*txtDescription;
        txtValue = [NSNull class ]!=[[[self.MedicalList objectAtIndex:indexPath.row]objectForKey:@"Value"]class]?[[self.MedicalList  objectAtIndex:indexPath.row]objectForKey:@"Value"]:@" ";
        txtDescription =[[self.MedicalList objectAtIndex:indexPath.row]objectForKey:@"Description"];
        CGSize constraint = CGSizeMake(tableView.frame.size.width, 200000000000.0f);
        CGSize valueSize;
       
            valueSize =([[self.MedicalList  objectAtIndex:indexPath.row]objectForKey:@"Value"]!=[NSNull null])? [txtValue sizeWithFont:[UIFont fontWithName:@"GESSTwoBold-Bold" size:12] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping]:CGSizeMake(tableView.frame.size.width, 3);
        
        
        CGFloat height = MAX(valueSize.height, 30);
        
        return height+10;
        
    }
    @catch (NSException *exception) {
        
    }
    
}


@end
