//
//  MedicalSearchListView.h
//  sfda-ipad
//
//  Created by assem on 6/16/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//
@protocol MedicalSearchListDelegate <NSObject>

@optional
-(void)didSelecteMedicalEquipment:(id)item;
-(void)didClickMedicalListBackButton;
@end
#import <UIKit/UIKit.h>

@interface MedicalSearchListView : UIView<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UIButton *BackButton;
    __weak IBOutlet UILabel *TitleLabel;
    __weak IBOutlet UITableView *MedicalListTableView;
}
@property (weak, nonatomic) IBOutlet UILabel *SubTitleLabel;
- (IBAction)BackButtonAction:(UIButton *)sender;
-(void)ReLoadData;
@property(assign)id<MedicalSearchListDelegate> delegate;
@property(nonatomic,retain)NSArray*MedicalList;
@end
