//
//  ListCell.h
//  sfda-ipad
//
//  Created by assem on 6/15/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *SeperatorLabel;
@property (weak, nonatomic) IBOutlet UIButton *BackGroundButton;

@property (weak, nonatomic) IBOutlet UILabel *ItemTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *ItemValueLabel;
@end
