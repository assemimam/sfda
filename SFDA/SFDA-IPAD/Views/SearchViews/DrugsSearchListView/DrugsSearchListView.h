//
//  DrugsSearchListView.h
//  sfda-ipad
//
//  Created by assem on 6/15/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//
@protocol DrugsSearchListDelegate <NSObject>

@optional
-(void)didSelecteDrug:(id)item;
-(void)didClickDrugListBackButton;
@end
#import <UIKit/UIKit.h>

@interface DrugsSearchListView : UIView<UITableViewDataSource,UITableViewDelegate>
{
    
    __weak IBOutlet UIButton *BackButton;
    __weak IBOutlet UILabel *TitleLabel;
    __weak IBOutlet UITableView *DrugsListTableView;
}
@property (weak, nonatomic) IBOutlet UILabel *SubTitleLabel;
- (IBAction)BackButtonAction:(UIButton *)sender;
-(void)ReLoadData;
@property(assign)id<DrugsSearchListDelegate> delegate;
@property(nonatomic,retain)NSArray*DrugsList;
@end
