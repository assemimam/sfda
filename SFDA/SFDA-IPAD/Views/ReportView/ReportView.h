//
//  ReportView.h
//  SFDA_ALL
//
//  Created by iOS Developer on 2/23/1436 AH.
//  Copyright (c) 1436 assem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>


@class ReportView;

@protocol ReportViewDelegate <NSObject>

-(void)presentView:(UIImagePickerController *)view_c atIndex:(NSInteger)index;

@end

@interface ReportView : UIView<MKMapViewDelegate , CLLocationManagerDelegate>
{
    __weak IBOutlet UIScrollView  *ScrollView;
    __weak IBOutlet UIView        *TitleView;
    __weak IBOutlet UIView        *LocationView;
    __weak IBOutlet UILabel       *RequestTypeLabal;
    __weak IBOutlet UILabel       *FoodTypeLabel;
    __weak IBOutlet UITextField   *NameTextField;
    __weak IBOutlet UITextField   *MobileNumberTextField;
    __weak IBOutlet UITextField   *EmailTextField;
    __weak IBOutlet UITextField   *CityTextField;
    __weak IBOutlet UITextView    *DetailTextView;

    __weak IBOutlet MKMapView     *MKmapView;
    __weak IBOutlet UIButton      *SendMailButton , *locationBtn;
    
    
}
@property (nonatomic,retain) IBOutlet UIImageView   *ProductImageView;
@property (nonatomic ,strong) UIImage *ProductImage;
@property (nonatomic ,weak) id<ReportViewDelegate>delegate;

@end
