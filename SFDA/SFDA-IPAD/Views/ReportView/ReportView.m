//
//  ReportView.m
//  SFDA_ALL
//
//  Created by iOS Developer on 2/23/1436 AH.
//  Copyright (c) 1436 assem. All rights reserved.
//

#import "ReportView.h"
#import "Language.h"
#import "AppDelegate.h"
#import "NavigationBar.h"
#import "Global.h"
#import "CommonMethods.h"

@interface ReportView ()<UIActionSheetDelegate , UITextFieldDelegate , UITextViewDelegate  , UIAlertViewDelegate  >
{
    
    UIActionSheet *requestActionSheet , *prodctActionSheet ,*productImageActionSheet;
    NSData *productImageData;
    CLLocationManager *locationManager;
    double latitude , longitude;;
    UIAlertView *locationAlertView;
    BOOL scrollEnabled;
}
@property (nonatomic ,strong) NSMutableArray *validationMessages;

-(void)keyboardWillShowScrollView:(NSNotification *)notification;
-(void)keyboardWillHideScrollView:(NSNotification *)notification;
-(IBAction)selectRequestTypeButtonTouched:(UIButton *)sender;
-(IBAction)selectFoodTypeButtonTouched:(UIButton *)sender;
-(IBAction)addProductImageButtonTouched:(UIButton *)sender;
-(IBAction)setLocationButtonTouched:(UIButton *)sender;
-(IBAction)sendMailButtonTouched:(UIButton *)sender;
-(IBAction)dissmisskeyboard:(id)sender;
@end

@implementation ReportView
-(void)imagePickerDidFinishPickingImage
{
    @try {
        self.ProductImageView.image = pickerImage;
        
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)awakeFromNib
{
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(imagePickerDidFinishPickingImage)
     name:IMAGE_PICKER_DID_FINISH_PICKING_IMAGE
     object:nil];
    
    
    
    DetailTextView.text = @"التفاصيل";
    DetailTextView.textColor = [UIColor lightGrayColor];
    DetailTextView.delegate = self;
    
    [ScrollView setScrollEnabled:YES];
    [ScrollView setContentSize:CGSizeMake(self.frame.size.width, self.frame.size.height +5)];
    
    
    MKmapView.delegate = self;
    MKmapView.showsUserLocation = YES;
    MKmapView.showsPointsOfInterest = YES;
    
    if ([CLLocationManager locationServicesEnabled])
    {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        
        //        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        //        {
        //            [locationManager requestWhenInUseAuthorization];
        //            [locationManager requestAlwaysAuthorization];
        //        }
    }
    
    CLLocation *regionLocation  = [[CLLocation alloc] initWithLatitude:24.6408333 longitude:46.7727778];
    [MKmapView setRegion:[self initializeMapRegionWithLocation:regionLocation] animated:YES];
    
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShowScrollView:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHideScrollView:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
}

#pragma mark - helping methods
-(void)resignTexts
{
    @try {
        [DetailTextView resignFirstResponder];
        [NameTextField resignFirstResponder];
        [MobileNumberTextField resignFirstResponder];
        [CityTextField resignFirstResponder];
        [EmailTextField resignFirstResponder];
    }
    @catch (NSException *exception) {
        
    }
    
}

- (NSString *)encodeToBase64String:(UIImage *)image
{
    @try {
        NSData *imgData =  UIImageJPEGRepresentation(image, 0.1);
        NSString *str  = [imgData base64Encoding];
        return str;
    }
    @catch (NSException *exception) {
        
    }
    
}

#pragma mark validation methods


-(NSMutableArray *)validationMessages
{
    @try {
        if (!_validationMessages)
        {
            _validationMessages = [NSMutableArray array];
            
        }
        
        return _validationMessages;
        
    }
    @catch (NSException *exception) {
        
    }
}


-(void)showValidationMessages
{
    @try {
        NSString *message = [NSString new];
        for (int i = 0; i < self.validationMessages.count; i++)
        {
            if (self.validationMessages.count == 7)
            {
                message = @"";
            }
            else{
                message = [message stringByAppendingFormat:@"%@\n", self.validationMessages[i]];
            }
            
        }
        
        [[[UIAlertView alloc] initWithTitle:@""
                                    message:message
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil] show];
        
    }
    @catch (NSException *exception)
    {
        
    }
    
}


-(BOOL)ValidateData
{
    @try
    {
        [self.validationMessages removeAllObjects];
        
        if (![CommonMethods isValidString:NameTextField.text])
        {
            [self.validationMessages addObject:[[Language getObject] getStringWithKey:@"EnterName"]];
        }
        if (![CommonMethods isValidMobileNumber:MobileNumberTextField.text])
        {
            [self.validationMessages addObject:[[Language getObject] getStringWithKey:@"EnterMobile"]];
        }
        if (![CommonMethods isValidString:CityTextField.text])
        {
            [self.validationMessages addObject:[[Language getObject] getStringWithKey:@"EnterCity"]];
        }
        if (![CommonMethods ValidateMail:EmailTextField.text])
        {
            [self.validationMessages addObject:[[Language getObject] getStringWithKey:@"EnterEmail"]];
        }
        if (![CommonMethods isValidString:DetailTextView.text])
        {
            [self.validationMessages addObject:[[Language getObject] getStringWithKey:@"EnterDetail"]];
            NSLog(@"%@", RequestTypeLabal.text);
        }
        if (![CommonMethods isValidString:RequestTypeLabal.text])
        {
            [self.validationMessages addObject:[[Language getObject] getStringWithKey:@"EnterRequest"]];
        }
        if (![CommonMethods isValidString:FoodTypeLabel.text])
        {
            [self.validationMessages addObject:[[Language getObject] getStringWithKey:@"EnterProduct"]];
        }
        
        return (self.validationMessages.count == 0);
        
    }
    @catch (NSException *exception)
    {
        
    }
    
}

#pragma mark - Button actions

-(IBAction)selectRequestTypeButtonTouched:(UIButton *)sender
{
    
    @try {
        
        requestActionSheet = [[UIActionSheet alloc] initWithTitle:[[Language getObject] getStringWithKey:@"RequestType"]
                                                         delegate:self
                                                cancelButtonTitle:nil
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:nil];
        
        for (NSDictionary *name in [[Language getObject] getArrayWithKey:@"RequestList"])
        {
            [requestActionSheet addButtonWithTitle:name[@"name"]];
        }
        requestActionSheet.cancelButtonIndex = [requestActionSheet addButtonWithTitle:@"إلغاء"];
        [requestActionSheet showInView:self];
        
    }
    @catch (NSException *exception) {
        
    }
    
}

-(IBAction)selectFoodTypeButtonTouched:(UIButton *)sender
{
    @try {
        prodctActionSheet = [[UIActionSheet alloc] initWithTitle:[[Language getObject] getStringWithKey:@"ProductType"]
                                                        delegate:self
                                               cancelButtonTitle:nil
                                          destructiveButtonTitle:nil
                                               otherButtonTitles:nil];
        
        for (NSDictionary *name in [[Language getObject] getArrayWithKey:@"ProductList"])
        {
            [prodctActionSheet addButtonWithTitle:name[@"name"]];
        }
        prodctActionSheet.cancelButtonIndex = [prodctActionSheet addButtonWithTitle:@"إلغاء"];
        [prodctActionSheet showInView:self];
        
    }
    @catch (NSException *exception)
    {
        
    }
}


-(IBAction)addProductImageButtonTouched:(UIButton *)sender
{
    @try {
        productImageActionSheet = [[UIActionSheet alloc] initWithTitle:[[Language getObject] getStringWithKey:@"ProductImage"]
                                                              delegate:self
                                                     cancelButtonTitle:nil
                                                destructiveButtonTitle:nil
                                                     otherButtonTitles:nil];
        
        for (NSDictionary *name in [[Language getObject] getArrayWithKey:@"ProductImageList"])
        {
            [productImageActionSheet addButtonWithTitle:name[@"image"]];
        }
        productImageActionSheet.cancelButtonIndex = [productImageActionSheet addButtonWithTitle:@"إلغاء"];
        [productImageActionSheet showInView:self];
        
    }
    @catch (NSException *exception)
    {
        
    }
}


-(IBAction)setLocationButtonTouched:(UIButton *)sender
{
    @try {
        
        if (!internetConnection)
        {
            HIDE_LOADINGINDICATOR
            [[[UIAlertView alloc] initWithTitle:@""
                                        message:@"تعذر الأتصال بالأنترنت"
                                       delegate:nil
                              cancelButtonTitle:@"تم"
                              otherButtonTitles:nil] show];
        }
        else
        {
            scrollEnabled = YES;
            SendMailButton.frame = CGRectMake(SendMailButton.frame.origin.x, SendMailButton.frame.origin.y + MKmapView.frame.size.height, SendMailButton.frame.size.width, SendMailButton.frame.size.height);
            [locationBtn setUserInteractionEnabled:NO];
            if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [locationManager requestWhenInUseAuthorization];
            }
            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            [locationManager startUpdatingLocation];
            
            [UIView animateWithDuration:1.0
                                  delay:0.2
                                options:UIViewAnimationOptionTransitionCrossDissolve
                             animations:^
             {
                 ScrollView.contentSize=CGSizeMake(self.frame.size.width, self.frame.size.height + 30);
                 LocationView.frame = CGRectMake(LocationView.frame.origin.x, LocationView.frame.origin.y, LocationView.frame.size.width, 190);
                 
                 [MKmapView setHidden:NO];
                 
             } completion:nil];
        }
        
    }
    @catch (NSException *exception) {
        
    }
}

- (void)requestAlwaysAuthorization
{
    @try {
        CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
        
        if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusDenied) {
            NSString *title;
            title = (status == kCLAuthorizationStatusDenied) ? @"تأكد من فتح خدمة تحديد المواقع" : @"";
            NSString *message = @"تأكد فتح خدمة الموقع من الإعدادات ";
            
            locationAlertView = [[UIAlertView alloc] initWithTitle:title
                                                                message:message
                                                               delegate:self
                                                      cancelButtonTitle:@"إلغاء"
                                                      otherButtonTitles:@"الإعدادات", nil];
            [locationAlertView show];
        }
        else if (status == kCLAuthorizationStatusNotDetermined)
        {
            [locationManager requestAlwaysAuthorization];
        }
    }
    @catch (NSException *exception)
    {
        
    }
}


-(void)handleSendReportDataResult:(id)result
{
    @try {
        HIDE_LOADINGINDICATOR;
        if (result)
        {
            if (result == false)
            {
                [[[UIAlertView alloc] initWithTitle:@""
                                            message:@"حدث خطأ أثناء الأرسال"
                                           delegate:nil
                                  cancelButtonTitle:@"تم"
                                  otherButtonTitles:nil] show];
            }
            else
            {
                [[[UIAlertView alloc] initWithTitle:@""
                                            message:@"تم ارسال الطلب بنجاح"
                                           delegate:nil
                                  cancelButtonTitle:@"تم"
                                  otherButtonTitles:nil] show];
            }
        }
        else
        {
            
        }
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)sendReportData
{
    @try {
        NSString *requestTypeString = RequestTypeLabal.text;
        NSString *productTypeString =FoodTypeLabel.text;
        NSString *nameString = NameTextField.text;
        NSString *mobileString = MobileNumberTextField.text;
        NSString *cityString = CityTextField.text;
        NSString *emailString = EmailTextField.text;
        NSString *detailString = DetailTextView.text;
        NSString *productImageStr = [self encodeToBase64String:self.ProductImageView.image];
        NSString *location = [NSString stringWithFormat:@"%f,%f",latitude,longitude];
        NSLog(@"%@", location);
        
        id result;
        if ([self ValidateData] && ![requestTypeString isEqual:nil] && ![productTypeString isEqual:nil])
        {
            
            result = [Webservice sendEmailWithName:nameString
                                         mobileNum:mobileString
                                              city:cityString
                                             email:emailString
                                           message:detailString
                                       requestType:requestTypeString
                                       productType:productTypeString
                                             image:productImageStr
                                       coordinates:location];
            
            [self performSelectorOnMainThread:@selector(handleSendReportDataResult:) withObject:result waitUntilDone:NO];
            
            
        }
        else
        {
            HIDE_LOADING
            [self showValidationMessages];
            
        }
        
        
        
        
    }
    @catch (NSException *exception) {
        
    }
    
}
-(IBAction)sendMailButtonTouched:(id)sender
{
    
    SHOW_LOADINGINDICATOR
    
    @try {
        
        if (!internetConnection)
        {
            HIDE_LOADINGINDICATOR
            [[[UIAlertView alloc] initWithTitle:@""
                                        message:@"تعذر الأتصال بالأنترنت"
                                       delegate:nil
                              cancelButtonTitle:@"تم"
                              otherButtonTitles:nil] show];
        }
        else
        {
            [NSThread detachNewThreadSelector:@selector(sendReportData) toTarget:self withObject:nil];
        }
        
        
        
    }
    @catch (NSException *exception) {
        
    }
    
}



-(IBAction)dissmisskeyboard:(id)sender
{
    @try
    {
        [self resignTexts];
        
    }
    @catch (NSException *exception)
    {
        
    }
    
}
#pragma mark - text Fields delegate

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField
{
    @try {
        if(theTextField==NameTextField)
        {
            [MobileNumberTextField becomeFirstResponder];
        }
        else if (theTextField == MobileNumberTextField)
        {
            [CityTextField becomeFirstResponder];
        }
        else if (theTextField == CityTextField)
        {
            CGPoint p=CGPointMake(0,DetailTextView.frame.origin.y);
            [ScrollView setContentOffset:p animated:NO];
            [EmailTextField becomeFirstResponder];
        }
        else if (theTextField == EmailTextField)
        {
            CGPoint p=CGPointMake(0,200);
            [ScrollView setContentOffset:p animated:NO];
            [DetailTextView becomeFirstResponder];
        }
        return YES;
        
    }
    @catch (NSException *exception) {
    }
}

#pragma mark - textView delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    @try {
        
        if([text isEqualToString:@"\n"])
        {
            [DetailTextView resignFirstResponder];
            CGPoint p=CGPointMake(0,0);
            [ScrollView setContentOffset:p animated:YES];
            
            return NO;
        }
        return YES;
    }
    @catch (NSException *exception)
    {
        
    }

}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
}
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    @try {
        CGPoint p=CGPointMake(0,200);
        [ScrollView setContentOffset:p animated:NO];
        
        DetailTextView.text = @"";
        DetailTextView.textColor = [UIColor blackColor];
        return YES;
    }
    @catch (NSException *exception)
    {
        
    }
    
}

-(void) textViewDidChange:(UITextView *)textView
{
    @try {
        if(DetailTextView.text.length == 0)
        {
            DetailTextView.textColor = [UIColor lightGrayColor];
            DetailTextView.text = @"التفاصيل";
            [DetailTextView resignFirstResponder];
        }
        
    }
    @catch (NSException *exception) {
        
    }
    
}


#pragma mark - Action sheet Delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    @try {
        
        if (actionSheet == requestActionSheet)
        {
            [RequestTypeLabal setHidden:NO];
            if (buttonIndex == 0)
            {
                NSDictionary *name = [[[Language getObject] getArrayWithKey:@"RequestList"] objectAtIndex:0];
                RequestTypeLabal.text  = name[@"name"];
            }
            else
            {
                NSDictionary *name = [[[Language getObject] getArrayWithKey:@"RequestList"] objectAtIndex:1];
                RequestTypeLabal.text  = name[@"name"];
            }
        }
        else if (actionSheet == prodctActionSheet)
        {
            [FoodTypeLabel setHidden:NO];
            if (buttonIndex == 0)
            {
                NSDictionary *name = [[[Language getObject] getArrayWithKey:@"ProductList"] objectAtIndex:0];
                FoodTypeLabel.text = name[@"name"];
            }
            else if (buttonIndex == 1)
            {
                NSDictionary *name = [[[Language getObject] getArrayWithKey:@"ProductList"] objectAtIndex:1];
                FoodTypeLabel.text = name[@"name"];
            }
            else if (buttonIndex == 2)
            {
                NSDictionary *name = [[[Language getObject] getArrayWithKey:@"ProductList"] objectAtIndex:2];
                FoodTypeLabel.text = name[@"name"];
            }
            else
            {
                NSDictionary *name = [[[Language getObject] getArrayWithKey:@"ProductList"] objectAtIndex:3];
                FoodTypeLabel.text = name[@"name"];
            }
        }
        else if (actionSheet == productImageActionSheet)
        {
            if (buttonIndex == 0)
            {
                [actionSheet dismissWithClickedButtonIndex:buttonIndex animated:NO];
                [self.delegate presentView:nil atIndex:0];
            }
            else
            {
               // [actionSheet dismissWithClickedButtonIndex:buttonIndex animated:NO];
               // [self.delegate presentView:nil atIndex:1];
            }
        }
        
    }
    @catch (NSException *exception) {
        
    }
    
}



#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"" message:@"خطأ في تحديد الموقع, من فضلك تأكد من فتح خدمة تحديد الموقع"
                               delegate:nil
                               cancelButtonTitle:@"تم" otherButtonTitles:nil];
    [errorAlert show];
}


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    @try {
        CLLocation *currentLocation = newLocation;
        
        if (currentLocation != nil)
        {
            
            longitude = currentLocation.coordinate.longitude;
            latitude = currentLocation.coordinate.latitude;
            MKmapView.delegate = self;
            CLLocationCoordinate2D initialLocation;
            initialLocation.latitude = latitude;
            initialLocation.longitude = longitude;
            MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(initialLocation, 1000, 1000);
            [MKmapView setRegion:region animated:YES];
            
            [MKmapView showsUserLocation];
            
        }
        
    }
    @catch (NSException *exception) {
        
    }
    
}


-(MKCoordinateRegion)initializeMapRegionWithLocation:(CLLocation *)location
{
    @try {
        CLLocationCoordinate2D initialLocation;
        initialLocation.latitude = location.coordinate.latitude;
        initialLocation.longitude = location.coordinate.longitude;
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(initialLocation, 100000, 100000);
        return region;
    }
    @catch (NSException *exception)
    {
        
    }
    
}
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    @try {
        MKmapView.centerCoordinate = userLocation.location.coordinate;
        
    }
    @catch (NSException *exception) {
        
    }
    
}


- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
    {
        ((MKUserLocation *)annotation).title = @"My Current Location";
    }
    return nil;  //return nil to use default blue dot view
}
#pragma mark  - alertView delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    @try {
        if (alertView == locationAlertView)
        {
            if (buttonIndex == 1)
            {
                NSURL *settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
               [[UIApplication sharedApplication] openURL:settingsURL];
                
            }
        }
        
    }
    @catch (NSException *exception)
    {
        
    }
    
    
}

#pragma mark - scroll view and keyboard mehtods

- (void)keyboardWillShowScrollView:(NSNotification *)notification
{
    @try
    {
        if (scrollEnabled)
        {
            CGRect R;
            [[[notification userInfo]objectForKey:UIKeyboardFrameBeginUserInfoKey] getValue:&R];
            ScrollView.contentSize=CGSizeMake(self.frame.size.width, self.frame.size.height + 105);
            CGPoint p = CGPointMake(0 ,170);
            [ScrollView setContentOffset:p animated:NO];
        }
        else if (EmailTextField.tag == 3)
        {
            
            CGRect R;
            [[[notification userInfo]objectForKey:UIKeyboardFrameBeginUserInfoKey] getValue:&R];
            ScrollView.contentSize=CGSizeMake(self.frame.size.width, self.frame.size.height + 105);
            CGPoint p = CGPointMake(0 ,170);
            [ScrollView setContentOffset:p animated:NO];
        }
        
    }
    @catch (NSException *exception) {
        
    }
    
    
    
}

-(void)keyboardWillHideScrollView:(NSNotification *)notification
{
    @try {
        if (scrollEnabled)
        {
            CGRect R;
            [[[notification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&R];
            ScrollView.contentSize=CGSizeMake(self.frame.size.width, self.frame.size.height+ 50);
            CGPoint p=CGPointMake(0,0);
            [ScrollView setContentOffset:p animated:YES];
        }
        else
        {
            CGRect R;
            [[[notification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&R];
            ScrollView.contentSize=CGSizeMake(self.frame.size.width, self.frame.size.height);
            CGPoint p=CGPointMake(0,0);
            [ScrollView setContentOffset:p animated:YES];
        }
        
    }
    @catch (NSException *exception)
    {
        
    }
    
}



@end
