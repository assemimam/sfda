//
//  AwarnessCell.h
//  sfda-ipad
//
//  Created by Assem Imam on 5/27/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AwarnessCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *ArticleNameLabel1;
@property (retain, nonatomic) IBOutlet UILabel *ArticleNameLabel2;
@property (retain, nonatomic) IBOutlet UIButton *ArticleDownloadButton1;
@property (retain, nonatomic) IBOutlet UIButton *ArticleDownloadButton2;
@property (retain, nonatomic) IBOutlet UIView *ArticleView2;
@property (retain, nonatomic) IBOutlet UIImageView *ViewPDFImageView1;
@property (retain, nonatomic) IBOutlet UIImageView *DownloadPDFImageView1;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *AcivityIndicator1;
@property (retain, nonatomic) IBOutlet UIImageView *ViewPDFImageView2;
@property (retain, nonatomic) IBOutlet UIImageView *DownloadPDFImageView2;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator2;

@end
