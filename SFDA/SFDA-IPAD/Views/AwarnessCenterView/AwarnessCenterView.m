//
//  AwarnessCenterView.m
//  sfda-ipad
//
//  Created by Assem Imam on 5/22/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "AwarnessCenterView.h"

#import "Cashing.h"

@interface AwarnessCenterView()
{
    id articlesList;
  
}
@end
@implementation AwarnessCenterView
@synthesize delegate,Sector;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)GetCashedData
{
    
    id result=nil;
    @try {
        
        DB_Field *articleID = [[DB_Field alloc]init];
        articleID.FieldName=@"id";
        articleID.FieldAlias= @"$id";
        
        DB_Field *sectionID = [[DB_Field alloc]init];
        sectionID.FieldName=@"SectionId";
        sectionID.FieldAlias=@"SectionId";
        
        DB_Field *sectionName = [[DB_Field alloc]init];
        sectionName.FieldName=@"sectionName";
        
        
        DB_Field *arabicTitle = [[DB_Field alloc]init];
        arabicTitle.FieldName=@"title_ar";
        arabicTitle.FieldAlias=@"TitleAr";
        
        DB_Field *PDFUrl = [[DB_Field alloc]init];
        PDFUrl.FieldName=@"PDFUrl";
        
        DB_Field *PDFPath = [[DB_Field alloc]init];
        PDFPath.FieldName=@"PDFPath";
        
        DB_Field *Loaded = [[DB_Field alloc]init];
        Loaded.FieldName=@"Loaded";
        

        [[Cashing getObject]setDatabase:@"SDFAipad"];

        
        if (Sector==SFDA_SECTOR) {
          result =[[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:articleID,sectionID,sectionName,arabicTitle,PDFPath,PDFUrl,Loaded, nil] Tables:[NSArray arrayWithObject:@"AwarenessCenter"] Where:nil FromIndex:nil ToIndex:nil];
            
        }
        else{
            NSString*sector=@"" ;
            switch (Sector) {
                case DRUGS_SECTOR:
                {
                    sector=@"Drug";
                }
                    break;
                case FOODS_SECTOR:
                {
                    sector=@"Food";
                }
                    break;
                case MEDICAL_SECTOR:
                {
                    sector=@"Medical";
                }
                    break;
            }
            
            result =[[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:articleID,sectionID,sectionName,arabicTitle,PDFPath,PDFUrl,Loaded, nil] Tables:[NSArray arrayWithObject:@"AwarenessCenter"] Where:[NSString stringWithFormat:@"sectionName='%@'",sector] FromIndex:nil ToIndex:nil];
        }

    }
    @catch (NSException *exception) {
        
    }
    return result;
}

- (void)drawRect:(CGRect)rect
{
    SHOW_LOADINGINDICATOR;
    [NSThread detachNewThreadSelector:@selector(GetArticlesList) toTarget:self withObject:nil];
}
-(void)HandleGetArticlesListResult
{
    @try {
        [NSThread detachNewThreadSelector:@selector(CashArticlesData) toTarget:self withObject:nil];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)HandleCashArticlesDataResult{
    @try {
        HIDE_LOADINGINDICATOR;
        [ArticlesTableView reloadData];
        
    }
    @catch (NSException *exception) {
        
    }
   
}
-(void)CashArticlesData
{
    @try {
        NSString*sector=@"" ;
        NSString *sectorCashedKey=nil;
        switch (Sector) {
            case DRUGS_SECTOR:
            {
                sector=@"Drug";
                 sectorCashedKey=AWARNESS_DRUGS_CASH_KEY;
            }
                break;
            case FOODS_SECTOR:
            {
                sector=@"Food";
                 sectorCashedKey=AWARNESS_FOOD_CASH_KEY;
            }
                break;
            case MEDICAL_SECTOR:
            {
                sector=@"Medical";
                 sectorCashedKey=AWARNESS_MEDICAL_CASH_KEY;
            }
                break;
            case SFDA_SECTOR:
            {
                sector=@"SFDA";
                sectorCashedKey=AWARNESS_SFDA_CASH_KEY;
            }
                break;
        }

        NSMutableArray *cashedData = [[NSMutableArray alloc]init];
        for (id article in articlesList) {
            DB_Field *articleID = [[DB_Field alloc]init];
            articleID.FieldDataType=FIELD_DATA_TYPE_NUMBER;
            articleID.IS_PRIMARY_KEY=YES;
            articleID.FieldName=@"id";
            articleID.FieldValue=[article objectForKey:@"$id"];
            
            DB_Field *sectionID = [[DB_Field alloc]init];
            sectionID.FieldDataType=FIELD_DATA_TYPE_NUMBER;
            sectionID.IS_PRIMARY_KEY=YES;
            sectionID.FieldName=@"SectionId";
            sectionID.FieldValue=[article objectForKey:@"SectionId"];
            
            DB_Field *sectionName = [[DB_Field alloc]init];
            sectionName.FieldDataType=FIELD_DATA_TYPE_TEXT;
            sectionName.FieldName=@"sectionName";
            sectionName.FieldValue=sector;
            
            DB_Field *PDFUrl= [[DB_Field alloc]init];
            PDFUrl.FieldDataType=FIELD_DATA_TYPE_TEXT;
            PDFUrl.FieldName=@"PDFUrl";
            PDFUrl.FieldValue=[article objectForKey:@"PDFUrl"];;
            
            
            DB_Field *arabicTitle = [[DB_Field alloc]init];
            arabicTitle.FieldDataType=FIELD_DATA_TYPE_TEXT;
            arabicTitle.FieldName=@"title_ar";
            arabicTitle.FieldValue=[article objectForKey:@"TitleAr"];
            
            DB_Field *englishTitle = [[DB_Field alloc]init];
            englishTitle.FieldDataType=FIELD_DATA_TYPE_TEXT;
            englishTitle.FieldName=@"title_en";
            englishTitle.FieldValue=[article objectForKey:@"TitleEn"];
            
            DB_Recored *record = [[DB_Recored alloc]init];
            record.Fields=[NSMutableArray arrayWithObjects:articleID,sectionName,arabicTitle,englishTitle,sectionID,PDFUrl, nil];
            [cashedData addObject:record];
        }
      [[Cashing getObject] setDatabase:@"SDFAipad"];
      BOOL Success=  [[Cashing getObject]CashData:cashedData inTable:@"AwarenessCenter"];
        if (Success) {
            [[NSUserDefaults standardUserDefaults]setObject:[NSDate date] forKey:sectorCashedKey];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
        }
       articlesList=  [self GetCashedData];
      [self performSelectorOnMainThread:@selector(HandleCashArticlesDataResult) withObject:nil waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
  
}
-(void)GetArticlesList
{
    @try {
         NSString *sectorCashedKey=@"";
         NSString*sector=@"" ;

        if (Sector==SFDA_SECTOR) {
            sectorCashedKey=AWARNESS_SFDA_CASH_KEY;
            if ([[NSUserDefaults standardUserDefaults]objectForKey:AWARNESS_SFDA_CASH_KEY]) {
                int  munites = ceil([[NSDate date] timeIntervalSinceDate:[[NSUserDefaults standardUserDefaults]objectForKey:AWARNESS_SFDA_CASH_KEY]]/60);
                if (munites>180) {
                    articlesList=[Webservice GetAllAwarnessCenterArticles];
                    if ([articlesList count]==0) {
                        [self performSelectorOnMainThread:@selector(HandleGetArticlesListResult) withObject:nil waitUntilDone:NO];
                    }
                    else{
                      articlesList=  [self GetCashedData];
                        [self performSelectorOnMainThread:@selector(HandleCashArticlesDataResult) withObject:nil waitUntilDone:NO];
                    }
                }
                else{
                     articlesList=  [self GetCashedData];
                     [self performSelectorOnMainThread:@selector(HandleCashArticlesDataResult) withObject:nil waitUntilDone:NO];
                }
            }
            else{
                articlesList=[Webservice GetAllAwarnessCenterArticles];
                [self performSelectorOnMainThread:@selector(HandleGetArticlesListResult) withObject:nil waitUntilDone:NO];
            }
           
            return;
        }
       
        
        switch (Sector) {
            case DRUGS_SECTOR:
            {
                sector=@"Drug";
                sectorCashedKey=AWARNESS_DRUGS_CASH_KEY;
            }
                break;
            case FOODS_SECTOR:
            {
                sector=@"Food";
                sectorCashedKey=AWARNESS_FOOD_CASH_KEY;
            }
                break;
            case MEDICAL_SECTOR:
            {
                sector=@"Medical";
                sectorCashedKey=AWARNESS_MEDICAL_CASH_KEY;
            }
                break;
            case SFDA_SECTOR:
            {
               
            }
                break;
        }
        if ([[NSUserDefaults standardUserDefaults]objectForKey:sectorCashedKey]) {
            int  munites = ceil([[NSDate date] timeIntervalSinceDate:[[NSUserDefaults standardUserDefaults]objectForKey:sectorCashedKey]]/60);
            if (munites>180) {
                articlesList=[Webservice GetAwarnessCenterArticlesBySector:sector];
                if ([articlesList count]==0) {
                     articlesList=  [self GetCashedData];
                     [self performSelectorOnMainThread:@selector(HandleCashArticlesDataResult) withObject:nil waitUntilDone:NO];
                }
                else{
                    
                   [self performSelectorOnMainThread:@selector(HandleGetArticlesListResult) withObject:nil waitUntilDone:NO];
                }
            }
            else{
                 articlesList=  [self GetCashedData];
                 [self performSelectorOnMainThread:@selector(HandleCashArticlesDataResult) withObject:nil waitUntilDone:NO];
            }
        }
        else{
            articlesList=[Webservice GetAwarnessCenterArticlesBySector:sector];
            
            [self performSelectorOnMainThread:@selector(HandleGetArticlesListResult) withObject:nil waitUntilDone:NO];
        }
       
    }
    @catch (NSException *exception) {
        
    }
}
-(void)ArticleDownloadButtonAction:(UIButton*)sender
{
    AwarnessCell *cell;
    @try {
        if (sender.tag>0) {
            int rowIndex=0;
            if (sender.tag%2==0) {
                rowIndex = sender.tag /2;
            }
            else{
                rowIndex = (sender.tag -1)/2;
            }
            cell=(AwarnessCell*)[ArticlesTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:rowIndex inSection:0]];
        }
        else{
            cell=(AwarnessCell*)[ArticlesTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
        }
        
        if (self.delegate) {
            [self.delegate didSelectArticle: [articlesList objectAtIndex:sender.tag] AtCell:cell AtIndex:sender.tag];
        }
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AwarnessCell*cell;
    @try {
         int index=((indexPath.row)*2)+1;
        NSLog(@"index %i ,  index path %i",index,indexPath.row);
        //cell= (AwarnessCell*)[tableView dequeueReusableCellWithIdentifier:@"cleArticle"];
        if (cell==nil) {
            cell=(AwarnessCell*)[[[NSBundle mainBundle] loadNibNamed:@"AwarnessCell" owner:nil options:nil]objectAtIndex:0];
        }
        cell.ArticleNameLabel1.font = [UIFont fontWithName:@"GESSTwoBold-Bold" size:13];
        cell.ArticleNameLabel2.font = [UIFont fontWithName:@"GESSTwoBold-Bold" size:13];
       
         if ( index<[articlesList count]) {
            
            cell.ArticleNameLabel1.text = [[articlesList objectAtIndex:index-1]objectForKey:@"TitleAr"];
            cell.ArticleNameLabel2.text = [[articlesList objectAtIndex:index]objectForKey:@"TitleAr"];
            if ([[[articlesList objectAtIndex:index]objectForKey:@"Loaded"] intValue]==1) {
                [cell.ViewPDFImageView2 setHidden:NO];
                [cell.DownloadPDFImageView2 setHidden:YES];
            }
            else{
                [cell.ViewPDFImageView2 setHidden:YES];
                [cell.DownloadPDFImageView2 setHidden:NO];
            }
            if ([[[articlesList objectAtIndex:index -1 ]objectForKey:@"Loaded"] intValue]==1) {
                [cell.ViewPDFImageView1 setHidden:NO];
                [cell.DownloadPDFImageView1 setHidden:YES];
            }
            else{
                [cell.ViewPDFImageView1 setHidden:YES];
                [cell.DownloadPDFImageView1 setHidden:NO];
            }
            cell.ArticleDownloadButton1.tag=index-1;
            cell.ArticleDownloadButton2.tag=index;
            [cell.ArticleDownloadButton1 addTarget:self action:@selector(ArticleDownloadButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.ArticleDownloadButton2 addTarget:self action:@selector(ArticleDownloadButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            
        }
         else{
             
             [cell.ArticleView2 setHidden:YES];
             
             cell.ArticleNameLabel1.text = [[articlesList objectAtIndex:index -1]objectForKey:@"TitleAr"];
             cell.ArticleDownloadButton1.tag=index -1;
             [cell.ArticleDownloadButton1 addTarget:self action:@selector(ArticleDownloadButtonAction:) forControlEvents:UIControlEventTouchUpInside];
             
             if ([[[articlesList objectAtIndex:index -1]objectForKey:@"Loaded"] intValue]==1) {
                 [cell.ViewPDFImageView1 setHidden:NO];
                 [cell.DownloadPDFImageView1 setHidden:YES];
             }
             else{
                 [cell.ViewPDFImageView1 setHidden:YES];
                 [cell.DownloadPDFImageView1 setHidden:NO];
             }
         }
        
    }
    @catch (NSException *exception) {
        
    }
    
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int noOfRows=0;
    @try {
        if (articlesList && [articlesList count]>0) {
            if ([articlesList count]>2) {
                noOfRows = floor(([articlesList count]-1)/2)+1;
                
            }
            else{
                noOfRows = 1;
            }
            
        }
    }
    @catch (NSException *exception) {
        
    }
    
    return noOfRows;
}

@end
