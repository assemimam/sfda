//
//  AwarnessCenterView.h
//  sfda-ipad
//
//  Created by Assem Imam on 5/22/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//
@protocol AwarnessDelegate <NSObject>
@optional
-(void)didSelectArticle:(id)article AtCell:(UIView*)cell AtIndex:(int)index;
@end
#import <UIKit/UIKit.h>
#import "LibrariesConstants.h"
#import "AwarnessCell.h"
@interface AwarnessCenterView : UIView<UITableViewDataSource>
{
    __weak IBOutlet UITableView *ArticlesTableView;
}
@property(nonatomic)SECTOR_TYPE Sector;
@property(assign)id<AwarnessDelegate> delegate;
@end
