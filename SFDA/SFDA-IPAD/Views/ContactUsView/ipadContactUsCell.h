//
//  ContactUsCell.h
//  sfda-ipad
//
//  Created by Assem Imam on 5/22/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ipadContactUsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *contactName;
@property (weak, nonatomic) IBOutlet UIImageView *contactImage;
@end
