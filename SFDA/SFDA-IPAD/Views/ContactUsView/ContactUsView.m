//
//  ContactUsView.m
//  sfda-ipad
//
//  Created by Assem Imam on 5/22/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "ContactUsView.h"
#import "ipadContactUsCell.h"
#import "LibrariesConstants.h"
#import "Cashing.h"
#import "Database.h"

@implementation ContactUsView
@synthesize delegate;
-(id)GetContactUSCashedData
{
    @try {
        DB_Field *fldNameAr= [[DB_Field alloc]init];
        fldNameAr.FieldName=@"name";
        fldNameAr.FieldAlias= @"NameAr";
        
        DB_Field *fldValueAr = [[DB_Field alloc]init];
        fldValueAr.FieldName=@"value";
        fldValueAr.FieldAlias= @"ValueAr";
        
        DB_Field *fldSectionId= [[DB_Field alloc]init];
        fldSectionId.FieldName=@"SectionId";
        fldSectionId.FieldAlias= @"SectionId";
        
        [[Cashing getObject]setDatabase:@"SDFAipad"];
        id result =[[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldNameAr,fldValueAr,fldSectionId, nil] Tables:[NSArray arrayWithObject:@"ContactUs"] Where:nil FromIndex:nil ToIndex:nil];
        
        return result;


    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)CashContactUsData
{
    @try {
        NSMutableArray *recordList = [[NSMutableArray alloc]init];
        for (id contactUS in optionsList) {
            
            DB_Recored *contactUsRecored =[[DB_Recored alloc]init];
            
            DB_Field *fldNameAr= [[DB_Field alloc]init];
            fldNameAr.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldNameAr.FieldName=@"name";
            fldNameAr.FieldValue= [contactUS objectForKey:@"NameAr"];
            
            DB_Field *fldValueAr = [[DB_Field alloc]init];
            fldValueAr.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldValueAr.IS_PRIMARY_KEY=YES;
            fldValueAr.FieldName=@"value";
            fldValueAr.FieldValue= [contactUS objectForKey:@"ValueAr"];
            
            DB_Field *fldSectionId= [[DB_Field alloc]init];
            fldSectionId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
            fldSectionId.IS_PRIMARY_KEY=YES;
            fldSectionId.FieldName=@"SectionId";
            fldSectionId.FieldValue= [contactUS objectForKey:@"SectionId"];
            
            
            contactUsRecored.Fields = [[NSMutableArray alloc]initWithObjects:fldNameAr,fldValueAr,fldSectionId ,nil];
            [recordList addObject:contactUsRecored];

        }
        
        
        [[Cashing getObject]setDatabase:@"SDFAipad"];
        [[Database getObject]setDatabase:@"SDFAipad"];
        [[Database getObject]DeleteFromTable:@"ContactUs" Where:nil];
        
        BOOL Success = [[Cashing getObject] CashData:recordList inTable:@"ContactUs"];
        if (Success) {
            [[NSUserDefaults standardUserDefaults]setObject:[NSDate date] forKey:CONTACT_US_CASH_KEY];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }

    }
    @catch (NSException *exception) {
        
    }
}
-(void)drawRect:(CGRect)rect{
    @try {
        sdfaOptionsList = [[NSMutableArray alloc]init];
        foodOptionsList = [[NSMutableArray alloc]init];
        drugOptionsList = [[NSMutableArray alloc]init];
        medicalOptionsList = [[NSMutableArray alloc]init];
        sectionList = [[Language getObject] getArrayWithKey:@"ContactSectionList"];

        SHOW_LOADINGINDICATOR;
        [NSThread detachNewThreadSelector:@selector(GetContactUsData) toTarget:self withObject:nil];
    }
    @catch (NSException *exception) {
        
    }
 
}
-(void)awakeFromNib{
    SHOW_LOADINGINDICATOR
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)ReloadData
{
    HIDE_LOADINGINDICATOR
    if(optionsList == nil || [optionsList count] ==0){
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"يرجى التحقق من اتصالك مع الإنترنت" delegate:self cancelButtonTitle:@"موافق" otherButtonTitles:nil];
//        [alert show];
        
    }
    
    for (int i=0; i<[optionsList count]; i++) {
        
        NSInteger sectionId = [[[optionsList objectAtIndex:i] objectForKey:@"SectionId"] intValue];
        
        if(sectionId == 1){
            [sdfaOptionsList addObject:[optionsList objectAtIndex:i]];
        }else if(sectionId == 2){
            [foodOptionsList addObject:[optionsList objectAtIndex:i]];
        }else if(sectionId == 3){
            [drugOptionsList addObject:[optionsList objectAtIndex:i]];
        }else if(sectionId == 4){
            [medicalOptionsList addObject:[optionsList objectAtIndex:i]];
        }
    }
    HIDE_LOADINGINDICATOR
    [ContactsTableView reloadData];
}


-(void)HandleGetContactUsDataResult
{
    @try {
        
         [self ReloadData];
 
         [NSThread detachNewThreadSelector:@selector(CashContactUsData) toTarget:self withObject:nil];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)GetContactUsData
{
    @try {
        if (!internetConnection) {
            optionsList =[self GetContactUSCashedData];
            [self performSelectorOnMainThread:@selector(ReloadData) withObject:nil waitUntilDone:NO];
            return;
        }
        if ([[NSUserDefaults standardUserDefaults]objectForKey:CONTACT_US_CASH_KEY]) {
            int  munites = ceil([[NSDate date] timeIntervalSinceDate:[[NSUserDefaults standardUserDefaults]objectForKey:CONTACT_US_CASH_KEY]]/60);
            if (munites>180) {
                optionsList =[Webservice GetContactUsData];
                if ([optionsList count]>0) {
                    [self performSelectorOnMainThread:@selector(HandleGetContactUsDataResult) withObject:nil waitUntilDone:NO];
                }
                else
                {
                     optionsList =[self GetContactUSCashedData];
                    [self performSelectorOnMainThread:@selector(ReloadData) withObject:nil waitUntilDone:NO];
                    
                }
                
            }
            else{
                optionsList =[self GetContactUSCashedData];
                [self performSelectorOnMainThread:@selector(ReloadData) withObject:nil waitUntilDone:NO];
            }
        }
        else
        {
            optionsList =[Webservice GetContactUsData];
             [self performSelectorOnMainThread:@selector(HandleGetContactUsDataResult) withObject:nil waitUntilDone:NO];
        }

       
    }
    @catch (NSException *exception) {
        
    }
    
}
#pragma  mark tableview datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     ipadContactUsCell *cell =(ipadContactUsCell*) [tableView dequeueReusableCellWithIdentifier:@"cleOption"];
    @try {
        if (cell==nil) {
            cell =(ipadContactUsCell*) [[[NSBundle mainBundle]loadNibNamed:@"ipadContactUsCell" owner:nil options:nil]objectAtIndex:0];
        }
        
        if(indexPath.section ==0){
            currentArray = sdfaOptionsList;
        }else if(indexPath.section ==1){
            currentArray = foodOptionsList;
        }else if(indexPath.section == 2){
            currentArray = drugOptionsList;
        }else if(indexPath.section ==3){
            currentArray = medicalOptionsList;
        }
        
        NSString *name,*value;
        name = [[currentArray objectAtIndex:indexPath.row] objectForKey:@"NameAr"];
        value = [[currentArray objectAtIndex:indexPath.row] objectForKey:@"ValueAr"];
        if ([[Language getObject]getLanguage]==languageArabic) {
            
            if([name isEqualToString:@"رقم الهاتف"] || [name isEqualToString:@"رقم الهاتف 1"] || [name isEqualToString:@"البريد"]||[name isEqualToString:@"لموقع الإلكترونى"] ){
                cell.contactName.text = value;
                
                //Spicial case for Email contact
                if ([name isEqualToString:@"البريد"]) {
                    cell.contactName.text=@"البريد الإلكتروني العام";
                }
                
                
            }else if([name isEqualToString:@"تحويلة 1"]){
                //phone number +" ,"+ extenstion
                NSString *firstPhoneNumber =[[sdfaOptionsList objectAtIndex:0]objectForKey:@"ValueAr" ];
                firstPhoneNumber = [firstPhoneNumber substringFromIndex:[firstPhoneNumber rangeOfString:@"+"].location +1];
                cell.contactName.text = [[firstPhoneNumber stringByAppendingString:@"+     تحويلة : "] stringByAppendingString:value];
            }
            else{
                cell.contactName.text = name;
            }
            
            //////// for Ar image handling
            if ([[Language getObject]getLanguage]==languageArabic) {
                if([name isEqualToString:@"رقم الهاتف"] || [name isEqualToString:@"رقم الهاتف 1"]||[name isEqualToString:@"تحويلة 1"]){
                    cell.contactImage.image = [UIImage imageNamed:@"callic2.png"];
                }else if([name isEqualToString:@"البريد"]  || [name isEqualToString:@"مراسلة الرئيس التنفيذي"] ||[name isEqualToString: @"اسأل المتحدث الرسمي"]){
                    cell.contactImage.image = [UIImage imageNamed:@"mailic.png"];
                }else if([name isEqualToString:@"الموقع الإلكتروني"]){
                    cell.contactImage.image = [UIImage imageNamed:@"ieic.png"];
                } else if([name isEqualToString:@"موقع الهيئة على الخريطة"])
                    cell.contactImage.image = [UIImage imageNamed:@"navic.png"];
                else if([name rangeOfString:@"تويتر"].length >0){
                    cell.contactImage.image = [UIImage imageNamed:@"twitteric.png"];
                }else if([name rangeOfString:@"الهيئة على الفيسبوك"].length >0){
                    cell.contactImage.image = [UIImage imageNamed:@"fbic.png"];
                }
            }
            
        }
        cell.contactName.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:13];

    }
    @catch (NSException *exception) {
        
    }
   
    return cell;
}

#pragma  mark tableview delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        
        if(indexPath.section ==0){
            currentArray = sdfaOptionsList;
        }else if(indexPath.section ==1){
            currentArray = foodOptionsList;
        }else if(indexPath.section == 2){
            currentArray = drugOptionsList;
        }else if(indexPath.section ==3){
            currentArray = medicalOptionsList;
        }

        NSString *name,*value;
        if ([[Language getObject]getLanguage]==languageArabic) {
            name = [[currentArray objectAtIndex:indexPath.row] objectForKey:@"NameAr"];
            value = [[currentArray objectAtIndex:indexPath.row] objectForKey:@"ValueAr"];
        }
        else{
            name = [[currentArray objectAtIndex:indexPath.row] objectForKey:@"NameEn"];
            value = [[currentArray objectAtIndex:indexPath.row] objectForKey:@"ValueEn"];
        }
        
        NSMutableDictionary *contact = [NSMutableDictionary dictionaryWithObjectsAndKeys:name,@"name",value,@"value", nil];
        if (self.delegate) {
            [self.delegate didClickAtContact:contact];
        }
    }
    @catch (NSException *exception) {
        
    }
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    @try {
        
        if(optionsList!=nil && [optionsList count] > 0){
            if(section ==0){
                return  [sdfaOptionsList count];
            }else if(section ==1){
                return  [foodOptionsList count];
            }else if(section == 2){
                return  [drugOptionsList count];
            }else if(section ==3){
                return  [medicalOptionsList count];
            }
        }
        
 
    }
    @catch (NSException *exception) {
        
    }
       return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 60;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(optionsList!=nil && [optionsList count] > 0){
        
        UIView *headerView;
        NSString *headerText = [sectionList objectAtIndex:section];
        
        float headerWidth = 320.0f;
        float padding = 10.0f;
        
        headerView = [[UIView alloc] initWithFrame:CGRectMake(300, 0.0f, headerWidth, 60)];
        headerView.autoresizesSubviews = YES;
        headerView.backgroundColor = [UIColor colorWithRed:92/255.0f  green:98/255.0f blue:106/255.0f alpha:1];
        
        
        // create the label centered in the container, then set the appropriate autoresize mask
        UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, headerWidth, 30)];
        headerLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;
        if([[Language getObject] getLanguage] == languageArabic){
            headerLabel.textAlignment = UITextAlignmentRight;
        }else{
            headerLabel.textAlignment = UITextAlignmentLeft;
        }
        
        headerLabel.text = headerText;
        headerLabel.textColor = [UIColor whiteColor];
        headerLabel.backgroundColor = [UIColor clearColor];
        headerLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
        CGSize textSize = [headerText sizeWithFont:headerLabel.font constrainedToSize:headerLabel.frame.size lineBreakMode:NSLineBreakByWordWrapping];
        headerLabel.frame = CGRectMake(headerWidth - (textSize.width + padding), headerLabel.frame.origin.y, textSize.width, headerLabel.frame.size.height);
        
        [headerView addSubview:headerLabel];
        
        return headerView;
    }
    
    return nil;


}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(optionsList != nil  &&[optionsList count] > 0){
        return 4;
    }else{
        return 0;
    }
    
}


@end
