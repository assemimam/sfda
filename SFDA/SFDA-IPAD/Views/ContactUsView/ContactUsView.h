//
//  ContactUsView.h
//  sfda-ipad
//
//  Created by Assem Imam on 5/22/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//
@protocol ContactUsDelegate <NSObject>

@optional
-(void)didClickAtContact:(id)contact;

@end
#import <UIKit/UIKit.h>

@interface ContactUsView : UIView<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UITableView *ContactsTableView;
    NSMutableArray *sdfaOptionsList;
    NSMutableArray *foodOptionsList;
    NSMutableArray *drugOptionsList;
    NSMutableArray *medicalOptionsList;
    NSMutableArray *optionsList;
    NSMutableArray *currentArray;
    NSArray *sectionList;
}
@property(assign)id<ContactUsDelegate>delegate;
@end
