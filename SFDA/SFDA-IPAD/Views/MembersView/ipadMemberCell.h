//
//  MemberCell.h
//  sfda-ipad
//
//  Created by Assem Imam on 5/21/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ipadMemberCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *MemberName;
@property (weak, nonatomic) IBOutlet UILabel *MemberPosition;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *LoadingActivityIndicator;
@property (weak, nonatomic) IBOutlet UIImageView *MemberImage;
@end
