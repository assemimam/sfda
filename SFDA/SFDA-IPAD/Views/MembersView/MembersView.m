//
//  MembersView.m
//  sfda-ipad
//
//  Created by Assem Imam on 5/21/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "MembersView.h"
#import "ipadMemberCell.h"
#import "LibrariesConstants.h"

@interface MembersView ()
{
      NSArray *membersList;
}
@end
@implementation MembersView
-(void)awakeFromNib{
    @try {
        SHOW_LOADINGINDICATOR;
        [NSThread detachNewThreadSelector:@selector(GetMembersList) toTarget:self withObject:nil];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)HandleGetMembersCashedDataResult
{
    @try {
        HIDE_LOADINGINDICATOR
        [MembersTableView reloadData];
    }
    @catch (NSException *exception) {
        
    }
  
}
-(id)GetMembersCashedData
{
    @try {
        DB_Field *fldId = [[DB_Field alloc]init];
        fldId.FieldName=@"Id";
        fldId.FieldAlias=@"$id";
        
        DB_Field *fldName = [[DB_Field alloc]init];
        fldName.FieldName=@"Name";
        
        
        DB_Field *fldTitle = [[DB_Field alloc]init];
        fldTitle.FieldName=@"Title";
        
        

        
        
        DB_Field *fldImagePath = [[DB_Field alloc]init];
        fldImagePath.FieldName=@"ImagePath";
       
        
        DB_Field *fldOrder = [[DB_Field alloc]init];
        fldOrder.FieldName=@"Rank";
        [[Cashing getObject]setDatabase:@"SDFAipad"];
        [[Database getObject] setDatabase:@"SDFAipad"];

        id result =[[Cashing getObject]getDataFromCashWithSelectColumns:[NSMutableArray arrayWithObjects:fldId,fldName,fldTitle,fldImagePath,fldOrder, nil] Tables:[NSArray arrayWithObject:@"members"] Where:nil FromIndex:nil ToIndex:nil];
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Order" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        [result sortUsingDescriptors:sortDescriptors];
        
        return result;

    }
    @catch (NSException *exception) {
        
    }
}
-(void)HandleGetMembersList
{
    @try {
        HIDE_LOADINGINDICATOR
        [MembersTableView reloadData];
        [NSThread detachNewThreadSelector:@selector(CashMembersData:) toTarget:self withObject:membersList];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)CashMembersData:(id)data
{
    @try {
        NSMutableArray *cashedData = [[NSMutableArray alloc]init];
        for (id item in data) {
            DB_Recored *memberRecored =[[DB_Recored alloc]init];
            
            DB_Field *fldId = [[DB_Field alloc]init];
            fldId.FieldName=@"Id";
            fldId.FieldDataType= FIELD_DATA_TYPE_NUMBER;
            fldId.IS_PRIMARY_KEY=YES;
            fldId.FieldValue=[item objectForKey:@"$id"];
            
            DB_Field *fldName = [[DB_Field alloc]init];
            fldName.FieldName=@"Name";
            fldName.FieldDataType= FIELD_DATA_TYPE_TEXT;
            fldName.FieldValue=[item objectForKey:@"Name"];
            
            DB_Field *fldTitle = [[DB_Field alloc]init];
            fldTitle.FieldName=@"Title";
            fldTitle.FieldDataType= FIELD_DATA_TYPE_TEXT;
            fldTitle.FieldValue=[NSNull null]== [item objectForKey:@"Title"]?@" ":[item objectForKey:@"Title"];
            
            DB_Field *fldURL = [[DB_Field alloc]init];
            fldURL.FieldName=@"URL";
            fldURL.FieldDataType= FIELD_DATA_TYPE_TEXT;
            fldURL.FieldValue=[item objectForKey:@"URL"];
            
            DB_Field *fldImagePath = [[DB_Field alloc]init];
            fldImagePath.FieldName=@"ImagePath";
            fldImagePath.FieldDataType= FIELD_DATA_TYPE_IMAGE;
            fldImagePath.FieldValue=[item objectForKey:@"URL"];
            
            DB_Field *fldOrder = [[DB_Field alloc]init];
            fldOrder.FieldName=@"Rank";
            fldOrder.FieldDataType= FIELD_DATA_TYPE_TEXT;
            fldOrder.FieldValue=[NSString stringWithFormat:@"%@" ,[item objectForKey:@"Order"]];
            
            memberRecored.Fields = [NSMutableArray arrayWithObjects:fldId,fldName,fldTitle,fldURL,fldImagePath,fldOrder, nil];
            [cashedData  addObject:memberRecored];
        }
        [[Cashing getObject]setDatabase:@"SDFAipad"];
        [[Database getObject] setDatabase:@"SDFAipad"];
        [[Database getObject] DeleteFromTable:@"members" Where:nil];
        
        BOOL Success = [[Cashing getObject] CashData:cashedData inTable:@"members"];
        if (Success) {
            [[NSUserDefaults standardUserDefaults]setObject:[NSDate date] forKey:MEMBERS_CASH_KEY];
            [[NSUserDefaults standardUserDefaults]synchronize];
//            membersList =  [self GetMembersCashedData];
//            [self performSelectorOnMainThread:@selector(HandleGetMembersCashedDataResult) withObject:nil waitUntilDone:NO];
        }

        
  
    }
    @catch (NSException *exception) {
        
    }
}
-(void)GetMembersList
{
    @try {
        if ([[NSUserDefaults standardUserDefaults]objectForKey:MEMBERS_CASH_KEY]) {
            int  munites = ceil([[NSDate date] timeIntervalSinceDate:[[NSUserDefaults standardUserDefaults]objectForKey:MEMBERS_CASH_KEY]]/60);
            if (munites>180) {
                membersList = [Webservice GetMembersBoard];
                if ([membersList count]>0) {
                    
                    [self performSelectorOnMainThread:@selector(HandleGetMembersList) withObject:nil waitUntilDone:NO];
                }
                else{
                    membersList = [self GetMembersCashedData];
                    [self performSelectorOnMainThread:@selector(HandleGetMembersCashedDataResult) withObject:nil waitUntilDone:NO];
                }
            }
            else{
                membersList = [self GetMembersCashedData];
                [self performSelectorOnMainThread:@selector(HandleGetMembersCashedDataResult) withObject:nil waitUntilDone:NO];
            }
        }
        else{
            membersList = [Webservice GetMembersBoard];
            if ([membersList count]>0) {
                [self performSelectorOnMainThread:@selector(HandleGetMembersList) withObject:nil waitUntilDone:NO];
            }
            else{
                membersList = [self GetMembersCashedData];
                [self performSelectorOnMainThread:@selector(HandleGetMembersCashedDataResult) withObject:nil waitUntilDone:NO];
            }
        }

    }
    @catch (NSException *exception) {
        
    }
    
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
#pragma -mark uitableview datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [membersList count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ipadMemberCell*cell;// =(ipadMemberCell*)[tableView dequeueReusableCellWithIdentifier:@"cleMember"];
    if (cell==nil) {
        cell = (ipadMemberCell*)[[[NSBundle mainBundle]loadNibNamed:@"ipadMemberCell" owner:nil options:nil]objectAtIndex:0];
    }
    cell.MemberName.text = [[membersList objectAtIndex:indexPath.row] objectForKey:@"Name"];
    cell.MemberName.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
    cell.MemberPosition.text = [[membersList objectAtIndex:indexPath.row] objectForKey:@"Title"]&& [NSNull null]!=[[membersList objectAtIndex:indexPath.row] objectForKey:@"Title"] ? [[membersList objectAtIndex:indexPath.row] objectForKey:@"Title"]:@"";
    cell.MemberPosition.font=[UIFont fontWithName:@"GESSTwoLight-Light" size:13];
    
    if ([[membersList objectAtIndex:indexPath.row] objectForKey:@"URL"]) {
        NSURL*NewsImageUrl =[NSURL URLWithString: [[membersList objectAtIndex:indexPath.row] objectForKey:@"URL"]];
         [cell.LoadingActivityIndicator startAnimating];
         [cell.LoadingActivityIndicator setHidden:NO];
        if (NewsImageUrl) {
            [[ImageCache sharedInstance]downloadImageAtURL:NewsImageUrl completionHandler:^(UIImage *image) {
                
                if (image) {
                    cell.MemberImage.image = image;
                }
                else{
                    cell.MemberImage.image = [UIImage imageNamed:@"maindefaultimg.png"];
                }
                [cell.LoadingActivityIndicator stopAnimating];
            }];
        }
    }
    else//from cash
    {
        @autoreleasepool {
            [cell.LoadingActivityIndicator startAnimating];
            [cell.LoadingActivityIndicator setHidden:NO];
            
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                 NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *imagePath=[[membersList objectAtIndex:indexPath.row] objectForKey:@"ImagePath"];
            documentsDirectory =[documentsDirectory stringByAppendingFormat:@"/%@",imagePath];

            [[ImageCache sharedInstance] downloadImageAtURL:[NSURL fileURLWithPath:documentsDirectory]completionHandler:^(UIImage *image) {
                if (image) {
                    cell.MemberImage.image = image;
                }
                else{
                    cell.MemberImage.image = [UIImage imageNamed:@"maindefaultimgrows.png"];
                }
            }];
            
        }

        
    }

    return  cell;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
