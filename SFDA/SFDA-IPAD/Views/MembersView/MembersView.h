//
//  MembersView.h
//  sfda-ipad
//
//  Created by Assem Imam on 5/21/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MembersView : UIView<UITableViewDataSource>
{
    
    __weak IBOutlet UITableView *MembersTableView;
}
@end
