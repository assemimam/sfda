//
//  MemberCell.m
//  sfda-ipad
//
//  Created by Assem Imam on 5/21/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "ipadMemberCell.h"

@implementation ipadMemberCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
