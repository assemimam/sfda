//
//  QuestionCell.h
//  sfda-ipad
//
//  Created by Assem Imam on 5/28/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ipadQuestionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *QuestionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *QuestionImageView;
@end
