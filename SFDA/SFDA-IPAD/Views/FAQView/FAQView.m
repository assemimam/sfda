//
//  FAQView.m
//  sfda-ipad
//
//  Created by Assem Imam on 5/22/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "FAQView.h"
#import "ipadQuestionCell.h"
#import  "Cashing.h"
#import "Database.h"

@interface FAQView ()
{
    NSMutableArray *questionsList;
    NSString *currentSectionName;
    NSString *currentSectionCachingKey;
    NSInteger expandedRowIndex;
    NSMutableArray *arrCells;
}
@end
@implementation FAQView
@synthesize Sector;
-(id)GetCashedQuestionsListBySection:(NSString*)sector
{
    @try {
        DB_Field *fldqQuestion= [[DB_Field alloc]init];
        fldqQuestion.FieldName=@"question";
        fldqQuestion.FieldAlias= @"QuestionAr";
        
        DB_Field *fldAnswer = [[DB_Field alloc]init];
        fldAnswer.FieldName=@"answer";
        fldAnswer.FieldAlias= @"AnswerAr";
        
        DB_Field *fldSectionName= [[DB_Field alloc]init];
        fldSectionName.FieldName=@"sectionName";
        
        [[Cashing getObject]setDatabase:@"SDFAipad"];
        id result =[[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldqQuestion,fldAnswer,fldSectionName, nil] Tables:[NSArray arrayWithObject:@"question"] Where:[NSString stringWithFormat:@"sectionName='%@'",sector] FromIndex:nil ToIndex:nil];
        
        return result;
        
        
        
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)CashQuestionsList:(id)data
{
    @try {
        NSString*sector =@"";
        NSString *cashed_sector_key=nil;
        switch (Sector) {
            case DRUGS_SECTOR:
            {
                sector=@"Drug";
                cashed_sector_key=FAQ_DRUG_CASH_KEY;
            }
                break;
            case FOODS_SECTOR:
            {
                sector=@"Food";
                cashed_sector_key=FAQ_FOOD_CASH_KEY;
            }
                break;
            case MEDICAL_SECTOR:
            {
                sector=@"Medical";
                cashed_sector_key=FAQ_MEDICAL_CASH_KEY;
            }
                break;
            case SFDA_SECTOR:
            {
                sector=@"SFDA";
            }
                break;
        }
        
        
        NSMutableArray *recordList = [[NSMutableArray alloc]init];
        for (id Question in data) {
            
            DB_Recored *contactUsRecored =[[DB_Recored alloc]init];
            
            DB_Field *fldqQuestion= [[DB_Field alloc]init];
            fldqQuestion.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldqQuestion.IS_PRIMARY_KEY=YES;
            fldqQuestion.FieldName=@"question";
            fldqQuestion.FieldValue= [Question objectForKey:@"QuestionAr"];
            
            DB_Field *fldAnswer = [[DB_Field alloc]init];
            fldAnswer.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldAnswer.FieldName=@"answer";
            fldAnswer.FieldValue= [Question objectForKey:@"AnswerAr"];
            
            DB_Field *fldSectionName= [[DB_Field alloc]init];
            fldSectionName.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldSectionName.IS_PRIMARY_KEY=YES;
            fldSectionName.FieldName=@"sectionName";
            fldSectionName.FieldValue= sector;
            
            
            contactUsRecored.Fields = [[NSMutableArray alloc]initWithObjects:fldSectionName,fldqQuestion,fldAnswer ,nil];
            [recordList addObject:contactUsRecored];
            
        }
        
        
        [[Cashing getObject]setDatabase:@"SDFAipad"];
        [[Database getObject]setDatabase:@"SDFAipad"];
        [[Database getObject]DeleteFromTable:@"question" Where:[NSString stringWithFormat:@"sectionName='%@'",sector]];
        
        BOOL Success = [[Cashing getObject] CashData:recordList inTable:@"question"];
        if (Success) {
            [[NSUserDefaults standardUserDefaults]setObject:[NSDate date] forKey:cashed_sector_key];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
    }
    @catch (NSException *exception) {
        
    }
    
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)awakeFromNib
{
    
    expandedRowIndex = -1;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didChangePreferredContentSize:)
                                                 name:UIContentSizeCategoryDidChangeNotification object:nil];
    
    
    questionsTableView.rowHeight = UITableViewAutomaticDimension;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIContentSizeCategoryDidChangeNotification
                                                  object:nil];
}


- (void)didChangePreferredContentSize:(NSNotification *)notification
{
    [questionsTableView reloadData];
}


- (void)drawRect:(CGRect)rect
{
    @try {
        
        questionsTableView.decelerationRate =0.152072;
        SHOW_LOADINGINDICATOR
        [NSThread detachNewThreadSelector:@selector(GetQuestionsList) toTarget:self withObject:nil];
        
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)HandleGetQuestionsListResult
{
    @try {
        
        HIDE_LOADINGINDICATOR;
        [questionsTableView reloadData];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)GetQuestionsList
{
    @try {
        NSString*sector =@"";
        NSString *cashed_sector_key=nil;
        switch (Sector) {
            case DRUGS_SECTOR:
            {
                sector=@"Drug";
                cashed_sector_key=FAQ_DRUG_CASH_KEY;
            }
                break;
            case FOODS_SECTOR:
            {
                sector=@"Food";
                cashed_sector_key=FAQ_FOOD_CASH_KEY;
            }
                break;
            case MEDICAL_SECTOR:
            {
                sector=@"Medical";
                cashed_sector_key=FAQ_MEDICAL_CASH_KEY;
            }
                break;
            case SFDA_SECTOR:
            {
                sector=@"SFDA";
            }
                break;
        }
        id result;
        if (!internetConnection) {
            result=[self GetCashedQuestionsListBySection:sector];
            HIDE_LOADINGINDICATOR
        }
        else{
            if ([[NSUserDefaults standardUserDefaults]objectForKey:cashed_sector_key]) {
                int  munites = ceil([[NSDate date] timeIntervalSinceDate:[[NSUserDefaults standardUserDefaults]objectForKey:cashed_sector_key]]/60);
                if (munites>180) {
                    result=[Webservice GetQuestionsBySector:sector];
                    if ([result count]==0) {
                        result=[self GetCashedQuestionsListBySection:sector];
                    }
                    else{
                        [NSThread detachNewThreadSelector:@selector(CashQuestionsList:) toTarget:self withObject:result];
                    }
                }
                else{
                    result=[self GetCashedQuestionsListBySection:sector];
                }
            }
            else{
                result=[Webservice GetQuestionsBySector:sector];
                [NSThread detachNewThreadSelector:@selector(CashQuestionsList:) toTarget:self withObject:result];
            }
        }
        
        NSMutableArray *itemsList = [[NSMutableArray alloc]init];
        for (id item in result) {
            NSMutableDictionary *ParentItem =[[NSMutableDictionary alloc]init];
            NSMutableDictionary *SubItem =[[NSMutableDictionary alloc]init];
            [SubItem setObject:[item objectForKey:@"AnswerAr"] forKey:@"Name"];
            [SubItem setObject:@"1" forKey:@"level"];
            
            [ParentItem setObject:[item objectForKey:@"QuestionAr"] forKey:@"Name"];
            [ParentItem setObject:@"0" forKey:@"level"];
            [ParentItem setObject:[NSArray arrayWithObject:SubItem] forKey:@"SubItems"];
            [ParentItem setObject:@"0" forKey:@"expanded"];
            [itemsList addObject:ParentItem];
            
        }
        
        questionsList = [itemsList mutableCopy];
        [self performSelectorOnMainThread:@selector(HandleGetQuestionsListResult) withObject:nil waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [questionsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *Question= [[[questionsList objectAtIndex:indexPath.row] objectForKey:@"Name"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    ipadQuestionCell *cell ;//= (ipadQuestionCell*)[tableView dequeueReusableCellWithIdentifier:@"cleQuestion"];
    if (cell==nil){
        cell = [[[NSBundle mainBundle]loadNibNamed:@"ipadQuestionCell" owner:nil options:nil]objectAtIndex:0];
    }
    
    NSMutableAttributedString* TitleAttrString = [[NSMutableAttributedString  alloc]initWithString:Question];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setLineSpacing:15];
    [style setAlignment:NSTextAlignmentRight];
    [TitleAttrString addAttribute:NSParagraphStyleAttributeName
                            value:style
                            range:NSMakeRange(0, Question.length)];
    
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = [UIColor clearColor];
    cell.selectedBackgroundView = bgView;
    cell.QuestionLabel.attributedText = TitleAttrString;
    cell.QuestionLabel.textColor = [UIColor blackColor];
    
    [cell setIndentationLevel:[[[questionsList objectAtIndex:indexPath.row] objectForKey:@"level"] intValue]];
    cell.indentationWidth = 25;
    
    float indentPoints = cell.indentationLevel * cell.indentationWidth;
    
    cell.contentView.frame = CGRectMake(indentPoints,cell.contentView.frame.origin.y,cell.contentView.frame.size.width - indentPoints,cell.contentView.frame.size.height);
    
    NSDictionary *d1=[questionsList objectAtIndex:indexPath.row] ;
    
    if([d1 objectForKey:@"SubItems"])
    {
        cell.QuestionLabel.font = [UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
        cell.QuestionImageView.alpha = 1.0;
        cell.QuestionImageView.tag=indexPath.row;
        // [cell.QuestionImageView addTarget:self action:@selector(showSubItems:) forControlEvents:UIControlEventTouchUpInside];
        if ([[[questionsList objectAtIndex:indexPath.row] objectForKey:@"expanded"]intValue]==1) {
            if (self.Sector==FOODS_SECTOR) {
                
                [cell.QuestionImageView setImage:[UIImage imageNamed:@"faqfoodic.png"] ];
                
            }
            else if (self.Sector==MEDICAL_SECTOR){
                
                [cell.QuestionImageView setImage:[UIImage imageNamed:@"faqselctedic.png"] ];
                
            }
            else if (self.Sector==DRUGS_SECTOR){
                
                [cell.QuestionImageView setImage:[UIImage imageNamed:@"faqdrugic.png"] ];
                
            }
            
            
        }
        
    }
    else
    {
        [cell.QuestionImageView setImage:[UIImage imageNamed:@"faqic.png"] ];
        cell.QuestionLabel.font = [UIFont fontWithName:@"GESSTwoLight-Light" size:15];
        cell.QuestionImageView.alpha = 0.0;
    }
    return cell;
    
}


-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 104.0;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSString *Title= [[[questionsList objectAtIndex:indexPath.row] objectForKey:@"Name"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//
//    NSMutableAttributedString* TitleAttrString = [[NSMutableAttributedString  alloc]initWithString:Title==nil?@"":Title];
//    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
//    [style setLineSpacing:15];
//    //NSLog(@"title %@at index %i",Title,indexPath.row);
//    [style setAlignment:NSTextAlignmentRight];
//    [TitleAttrString addAttribute:NSParagraphStyleAttributeName
//                            value:style
//                            range:NSMakeRange(0, Title.length)];
//    [TitleAttrString addAttribute:NSFontAttributeName
//                            value:[UIFont fontWithName:@"GESSTwoBold-Bold" size:20]
//                            range:NSMakeRange(0, Title.length)];
//
//
//    CGSize QuestionSize = [Title sizeWithFont:[UIFont fontWithName:@"GESSTwoBold-Bold" size:27] constrainedToSize:CGSizeMake(tableView.frame.size.width , 2000000000000.0f)];
//
//    return  MAX( QuestionSize.height,40);
//
//
//}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic=[questionsList objectAtIndex:indexPath.row];
    if([dic objectForKey:@"SubItems"])
    {
        NSArray *arr=[dic objectForKey:@"SubItems"];
        BOOL isTableExpanded=NO;
        ipadQuestionCell *cell=(ipadQuestionCell*)[tableView cellForRowAtIndexPath:indexPath];
        for(NSDictionary *subitems in arr )
        {
            NSInteger index=[questionsList indexOfObjectIdenticalTo:subitems];
            isTableExpanded=(index>0 && index!=NSIntegerMax);
            if(isTableExpanded) break;
        }
        
        if(isTableExpanded)
        {
            [self CollapseRows:arr];
            [[questionsList objectAtIndex:indexPath.row] setObject:@"0" forKey:@"expanded"];
            [cell.QuestionImageView setImage:[UIImage imageNamed:@"faqic.png"] ];
            
        }
        else
        {
            [[questionsList objectAtIndex:indexPath.row] setObject:@"1" forKey:@"expanded"];
            if (self.Sector==FOODS_SECTOR) {
                [cell.QuestionImageView setImage:[UIImage imageNamed:@"faqfoodic.png"] ];
            }
            else if (self.Sector==MEDICAL_SECTOR){
                [cell.QuestionImageView setImage:[UIImage imageNamed:@"faqselctedic.png"]];
            }
            else if (self.Sector==DRUGS_SECTOR){
                
                [cell.QuestionImageView setImage:[UIImage imageNamed:@"faqdrugic.png"] ];
                
            }
            
            NSUInteger count=indexPath.row+1;
            arrCells=[NSMutableArray array];
            for(NSDictionary *dInner in arr )
            {
                [arrCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
                [questionsList insertObject:dInner atIndex:count++];
            }
            [questionsTableView insertRowsAtIndexPaths:arrCells withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}

-(void)CollapseRows:(NSArray*)ar
{
    for(NSDictionary *dInner in ar )
    {
        NSUInteger indexToRemove=[questionsList indexOfObjectIdenticalTo:dInner];
        ipadQuestionCell *cell=(ipadQuestionCell*)[questionsTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexToRemove inSection:0]];
        
        NSArray *arInner=[dInner objectForKey:@"SubItems"];
        if(arInner && [arInner count]>0)
        {
            [self CollapseRows:arInner];
            [cell.QuestionImageView setImage:[UIImage imageNamed:@"faqic.png"] ];
        }
        
        if([questionsList indexOfObjectIdenticalTo:dInner]!=NSNotFound)
        {
            [questionsList removeObjectIdenticalTo:dInner];
            [questionsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:
                                                        [NSIndexPath indexPathForRow:indexToRemove inSection:0]
                                                        ]
                                      withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}



-(void)showSubItems :(UIButton*) sender
{
    UIButton *btn = (UIButton*)sender;
    CGRect buttonFrameInTableView = [btn convertRect:btn.bounds toView:questionsTableView];
    ipadQuestionCell *cell = (ipadQuestionCell*)[questionsTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    
    
    NSDictionary *d=[questionsList objectAtIndex:sender.tag] ;
    NSArray *arr=[d objectForKey:@"SubItems"];
    if([d objectForKey:@"SubItems"])
    {
        BOOL isTableExpanded=NO;
        for(NSDictionary *subitems in arr )
        {
            NSInteger index=[questionsList indexOfObjectIdenticalTo:subitems];
            isTableExpanded=(index>0 && index!=NSIntegerMax);
            if(isTableExpanded) break;
        }
        
        if(isTableExpanded)
        {
            [self CollapseRows:arr];
            [[questionsList objectAtIndex:sender.tag] setObject:@"0" forKey:@"expanded"];
        }
        else
        {
            [[questionsList objectAtIndex:sender.tag] setObject:@"1" forKey:@"expanded"];
            if (self.Sector==FOODS_SECTOR) {
                if ([[cell.QuestionImageView image] isEqual:[UIImage imageNamed:@"faqfoodic.png"]])
                {
                    [cell.QuestionImageView setImage:[UIImage imageNamed:@"faqic.png"] ];
                }
                else
                {
                    [cell.QuestionImageView setImage:[UIImage imageNamed:@"faqfoodic.png"]] ;
                }
            }
            else if (self.Sector==MEDICAL_SECTOR){
                if ([[cell.QuestionImageView image] isEqual:[UIImage imageNamed:@"faqselctedic.png"]])
                {
                    [cell.QuestionImageView setImage:[UIImage imageNamed:@"faqic.png"] ];
                }
                else
                {
                    [cell.QuestionImageView setImage:[UIImage imageNamed:@"faqselctedic.png"] ];
                }
            }
            else if (self.Sector==DRUGS_SECTOR){
                if ([[cell.QuestionImageView image] isEqual:[UIImage imageNamed:@"faqdrugic.png"]])
                {
                    [cell.QuestionImageView setImage:[UIImage imageNamed:@"faqic.png"] ];
                }
                else
                {
                    [cell.QuestionImageView setImage:[UIImage imageNamed:@"faqdrugic.png"] ];
                }
            }
            
            
            NSUInteger count=sender.tag+1;
            arrCells=[NSMutableArray array];
            for(NSDictionary *dInner in arr )
            {
                [arrCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
                [questionsList insertObject:dInner atIndex:count++];
            }
            [questionsTableView insertRowsAtIndexPaths:arrCells withRowAnimation:UITableViewRowAnimationNone];
        }
    }
    
    
}


@end
