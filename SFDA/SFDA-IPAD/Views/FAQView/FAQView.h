//
//  FAQView.h
//  sfda-ipad
//
//  Created by Assem Imam on 5/22/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LibrariesConstants.h"
@interface FAQView : UIView<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UITableView *questionsTableView;
}
@property(nonatomic)SECTOR_TYPE Sector;
@end
