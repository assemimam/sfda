//
//  QuestionCell.m
//  sfda-ipad
//
//  Created by Assem Imam on 5/28/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "ipadQuestionCell.h"

@implementation ipadQuestionCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
