//
//  ImportantLinkCell.h
//  sfda-ipad
//
//  Created by Assem Imam on 6/24/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImportantLinkCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *LinkTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *LinkLabel;

@end
