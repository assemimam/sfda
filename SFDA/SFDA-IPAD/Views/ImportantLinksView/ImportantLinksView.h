//
//  ImportantLinksView.h
//  sfda-ipad
//
//  Created by Assem Imam on 5/26/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//
@protocol ImportantLinksDelegate <NSObject>
@optional;
-(void)didSelectLink:(id)link;
@end
#import <UIKit/UIKit.h>

@interface ImportantLinksView : UIView<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UITableView *ImportantLinksTableView;
}
@property(assign)id<ImportantLinksDelegate>delegate;
@end
