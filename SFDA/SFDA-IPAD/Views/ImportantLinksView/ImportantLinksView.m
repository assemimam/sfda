//
//  ImportantLinksView.m
//  sfda-ipad
//
//  Created by Assem Imam on 5/26/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "ImportantLinksView.h"
#import "LibrariesConstants.h"
#import "Cashing.h"
#import "Database.h"
#import  "ImportantLinkCell.h"

@interface ImportantLinksView ()
{
    NSMutableArray *optionsList;
    NSArray *sectionList;
    NSMutableArray *sfdaLinksList;
    NSMutableArray *generalLinksList;
}
@end
@implementation ImportantLinksView
@synthesize delegate;
-(id)GetCashedLinks
{
    @try {
        DB_Field *fldTitle_ar= [[DB_Field alloc]init];
        fldTitle_ar.FieldName=@"title_ar";
        fldTitle_ar.FieldAlias=@"TitleAr";
        
        DB_Field *fldSectionId = [[DB_Field alloc]init];
        fldSectionId.FieldName=@"SectionId";
        fldSectionId.FieldAlias= @"Category";
        
        DB_Field *fldLink = [[DB_Field alloc]init];
        fldLink.FieldName=@"link";
        fldLink.FieldAlias= @"Link";
        
        [[Cashing getObject]setDatabase:@"SDFAipad"];
        id result =[[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldTitle_ar,fldSectionId,fldLink, nil] Tables:[NSArray arrayWithObject:@"ImportantLinks"] Where:nil FromIndex:nil ToIndex:nil];
        
        return result;
        
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)CashImportantLinks:(id)linksList
{
    @try {
        
        NSMutableArray *recordList = [[NSMutableArray alloc]init];
        for (id link in linksList) {
            
            DB_Recored *linksRecored =[[DB_Recored alloc]init];
            
            DB_Field *fldTitle_ar= [[DB_Field alloc]init];
            fldTitle_ar.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldTitle_ar.IS_PRIMARY_KEY=YES;
            fldTitle_ar.FieldName=@"title_ar";
            fldTitle_ar.FieldValue= [link objectForKey:@"TitleAr"];
            
            DB_Field *fldSectionId = [[DB_Field alloc]init];
            fldSectionId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
            fldSectionId.IS_PRIMARY_KEY=YES;
            fldSectionId.FieldName=@"SectionId";
            fldSectionId.FieldValue= [link objectForKey:@"Category"];
            
            DB_Field *fldLink = [[DB_Field alloc]init];
            fldLink.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldLink.FieldName=@"link";
            fldLink.FieldValue= [link objectForKey:@"Link"];
            linksRecored.Fields = [NSMutableArray arrayWithObjects:fldLink,fldSectionId,fldTitle_ar, nil];
            [recordList addObject:linksRecored];
        }
        
        [[Cashing getObject]setDatabase:@"SDFAipad"];
        [[Database getObject]setDatabase:@"SDFAipad"];
        [[Database getObject]DeleteFromTable:@"ImportantLinks" Where:nil];
        
        BOOL Success = [[Cashing getObject] CashData:recordList inTable:@"ImportantLinks"];
        if (Success) {
            [[NSUserDefaults standardUserDefaults]setObject:[NSDate date] forKey:LINKS_CASH_KEY];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        
        
    }
    @catch (NSException *exception) {
        
    }

}
-(void)drawRect:(CGRect)rect{
    @try {
        sectionList = [[Language getObject]getArrayWithKey:@"ImportanLinksList"];
        SHOW_LOADINGINDICATOR;
        [NSThread detachNewThreadSelector:@selector(GetImportantLinks) toTarget:self withObject:nil];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)awakeFromNib
{
    
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)HandleGetCashedLinksResult
{
    @try {
        HIDE_LOADINGINDICATOR
        [ImportantLinksTableView reloadData];
    }
    @catch (NSException *exception) {
        
    }
    
}

-(void)HandleGetImportantLinksResult
{
    @try {
        HIDE_LOADINGINDICATOR
        [ImportantLinksTableView reloadData];
          [NSThread detachNewThreadSelector:@selector(CashImportantLinks:) toTarget:self withObject:optionsList];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)GetImportantLinks
{
    @try {
        sfdaLinksList = [[NSMutableArray alloc]init];
        generalLinksList = [[NSMutableArray alloc]init];
        if (!internetConnection) {
            optionsList = [self GetCashedLinks];
            if ([optionsList count]>0) {
                for (int i=0; i<[optionsList count]; i++) {
                    NSInteger sectionId = [[[optionsList objectAtIndex:i] objectForKey:@"Category"] intValue];
                    if(sectionId == 1){
                        [sfdaLinksList addObject:[optionsList objectAtIndex:i]];
                    }else if(sectionId == 0){
                        [generalLinksList addObject:[optionsList objectAtIndex:i]];
                    }
                }
                
            }
            [self performSelectorOnMainThread:@selector(HandleGetCashedLinksResult) withObject:nil waitUntilDone:NO];
        }
        else{
            if ([[NSUserDefaults standardUserDefaults]objectForKey:LINKS_CASH_KEY]) {
                int  munites = ceil([[NSDate date] timeIntervalSinceDate:[[NSUserDefaults standardUserDefaults]objectForKey:LINKS_CASH_KEY]]/60);
                if (munites>180) {
                    optionsList = [Webservice GetImportantLinksList];
                    if ([optionsList count]>0) {
                        for (int i=0; i<[optionsList count]; i++) {
                            
                            NSInteger sectionId = [[[optionsList objectAtIndex:i] objectForKey:@"Category"] intValue];
                            
                            if(sectionId == 1){
                                [sfdaLinksList addObject:[optionsList objectAtIndex:i]];
                            }else if(sectionId == 0){
                                [generalLinksList addObject:[optionsList objectAtIndex:i]];
                            }
                        }
                        
                        [self performSelectorOnMainThread:@selector(HandleGetImportantLinksResult) withObject:nil waitUntilDone:NO];
                    }
                    else{
                        optionsList = [self GetCashedLinks];
                        if ([optionsList count]>0) {
                            for (int i=0; i<[optionsList count]; i++) {
                                NSInteger sectionId = [[[optionsList objectAtIndex:i] objectForKey:@"Category"] intValue];
                                if(sectionId == 1){
                                    [sfdaLinksList addObject:[optionsList objectAtIndex:i]];
                                }else if(sectionId == 0){
                                    [generalLinksList addObject:[optionsList objectAtIndex:i]];
                                }
                            }
                            [self performSelectorOnMainThread:@selector(HandleGetCashedLinksResult) withObject:nil waitUntilDone:NO];
                        }
                    }
                }
                else{
                    optionsList = [self GetCashedLinks];
                    if ([optionsList count]>0) {
                        for (int i=0; i<[optionsList count]; i++) {
                            NSInteger sectionId = [[[optionsList objectAtIndex:i] objectForKey:@"Category"] intValue];
                            if(sectionId == 1){
                                [sfdaLinksList addObject:[optionsList objectAtIndex:i]];
                            }else if(sectionId == 0){
                                [generalLinksList addObject:[optionsList objectAtIndex:i]];
                            }
                        }
                         [self performSelectorOnMainThread:@selector(HandleGetCashedLinksResult) withObject:nil waitUntilDone:NO];
                    }
                }
            }
            else{
                optionsList = [Webservice GetImportantLinksList];
                if ([optionsList count]>0) {
                    for (int i=0; i<[optionsList count]; i++) {
                        
                        NSInteger sectionId = [[[optionsList objectAtIndex:i] objectForKey:@"Category"] intValue];
                        
                        if(sectionId == 1){
                            [sfdaLinksList addObject:[optionsList objectAtIndex:i]];
                        }else if(sectionId == 0){
                            [generalLinksList addObject:[optionsList objectAtIndex:i]];
                        }
                    }
                    
                    [self performSelectorOnMainThread:@selector(HandleGetImportantLinksResult) withObject:nil waitUntilDone:NO];
                }
            

            }
        }
    }
    @catch (NSException *exception) {
        
    }
}
#pragma  mark tableview datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ImportantLinkCell *cell =(ImportantLinkCell*) [tableView dequeueReusableCellWithIdentifier:@"cleLink"];
    @try {
        if (cell==nil) {
            cell =(ImportantLinkCell*)[[[NSBundle  mainBundle]loadNibNamed:@"ImportantLinkCell" owner:nil options:nil]objectAtIndex:0];
        }
        
        NSString *title;
        
        if(indexPath.section == 0){
            if ([[Language getObject]getLanguage]==languageArabic) {
                title = [[sfdaLinksList objectAtIndex:indexPath.row] objectForKey:@"TitleAr"];
            }
            else{
                title = [[sfdaLinksList objectAtIndex:indexPath.row] objectForKey:@"TitleEn"];
            }
        }else{
            if ([[Language getObject]getLanguage]==languageArabic) {
                title = [[generalLinksList objectAtIndex:indexPath.row] objectForKey:@"TitleAr"];
            }
            else{
                title = [[generalLinksList objectAtIndex:indexPath.row] objectForKey:@"TitleEn"];
            }
        }
        
        if([[Language getObject] getLanguage] == languageArabic){
            cell.textLabel.textAlignment = NSTextAlignmentRight;
        }else{
            cell.textLabel.textAlignment = NSTextAlignmentLeft;
        }
        cell.LinkTitleLabel.font=[UIFont fontWithName:@"GESSTwoLight-Light" size:15];
        cell.LinkLabel.font=[UIFont systemFontOfSize:11];
        cell.LinkTitleLabel.text = title;
        cell.LinkLabel.text =[[generalLinksList objectAtIndex:indexPath.row] objectForKey:@"Link"] ;
    }
    @catch (NSException *exception) {
        
    }
    
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(optionsList!=nil && [optionsList count] > 0){
        if(section ==0){
            return  [sfdaLinksList count];
        }else if(section ==1){
            return  [generalLinksList count];
        }
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 60;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 78;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if(optionsList!=nil && [optionsList count] > 0){
        
        UIView *headerView;
        
        
        NSString *headerText = [sectionList objectAtIndex:section];
        
        float headerWidth = 320.0f;
        float padding = 10.0f;
        
        headerView = [[UIView alloc] initWithFrame:CGRectMake(300, 0.0f, headerWidth, 60)];
        headerView.autoresizesSubviews = YES;
        headerView.backgroundColor = [UIColor colorWithRed:92/255.0f  green:98/255.0f blue:106/255.0f alpha:1];
        
        
        // create the label centered in the container, then set the appropriate autoresize mask
        UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, headerWidth, 30)];
        headerLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;
        if([[Language getObject] getLanguage] == languageArabic){
            headerLabel.textAlignment = NSTextAlignmentRight;
        }else{
            headerLabel.textAlignment = NSTextAlignmentLeft;
        }
        
        headerLabel.text = headerText;
        headerLabel.textColor = [UIColor whiteColor];
        headerLabel.backgroundColor = [UIColor clearColor];
        headerLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
        CGSize textSize = [headerText sizeWithFont:headerLabel.font constrainedToSize:headerLabel.frame.size lineBreakMode:NSLineBreakByWordWrapping];
        headerLabel.frame = CGRectMake(headerWidth - (textSize.width + padding), headerLabel.frame.origin.y, textSize.width, headerLabel.frame.size.height);
        
        [headerView addSubview:headerLabel];
        
        return headerView;
    }
    
    return nil;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(optionsList != nil && [optionsList count] >0){
        return 2;
    }else{
        return 0;
    }
}
#pragma  mark tableview delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        if (!internetConnection) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            HIDE_LOADINGINDICATOR
            return;
        }
        NSString *link;
        
        if(indexPath.section == 0){
            link = [[sfdaLinksList objectAtIndex:indexPath.row] objectForKey:@"Link"];
        }else{
            link = [[generalLinksList objectAtIndex:indexPath.row] objectForKey:@"Link"];
        }
        
        if (self.delegate) {
            [self.delegate didSelectLink:link];
        }
    }
    @catch (NSException *exception) {
        
    }
    
    
}



@end
