//
//  WebView.m
//  sfda-ipad
//
//  Created by Assem Imam on 5/22/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "CustomWebView.h"
#import "LibrariesConstants.h"

@implementation CustomWebView
@synthesize Url;
-(void)awakeFromNib{

    
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

#pragma -mark uiwebview delegate
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    @try {
        HIDE_LOADINGINDICATOR;
    }
    @catch (NSException *exception) {
        
    }
   
}
-(void)drawRect:(CGRect)rect
{
    
    @try {
        

        if (!internetConnection) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            HIDE_LOADINGINDICATOR
            return;
        }
        SHOW_LOADINGINDICATOR
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.Url]];
        [WebViewHolder loadRequest:request];
         
    }
    @catch (NSException *exception) {
        
    }
    

}
@end
