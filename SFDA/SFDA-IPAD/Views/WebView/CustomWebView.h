//
//  WebView.h
//  sfda-ipad
//
//  Created by Assem Imam on 5/22/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomWebView : UIView<UIWebViewDelegate>
{
    __weak IBOutlet UIWebView *WebViewHolder;
}
@property(nonatomic,retain) NSString*Url;
@end
