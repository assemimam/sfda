//
//  AboutCell.m
//  sfda-ipad
//
//  Created by Assem Imam on 5/26/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "AboutCell.h"

@implementation AboutCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
    
    self.TitleLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.TitleLabel.frame);
    self.SubTitleLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.SubTitleLabel.frame);
}

@end
