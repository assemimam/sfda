//
//  AboutView.m
//  sfda-ipad
//
//  Created by Assem Imam on 5/22/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "AboutView.h"
#import "AboutCell.h"
#import "Cashing.h"
#import "Database.h"

@interface AboutView()
{
    id aboutList;
    ArabicConverter* converter;
}
@end
@implementation AboutView
@synthesize Sector;


-(void)drawRect:(CGRect)rect{
    @try {
         converter= [[ArabicConverter alloc] init];
        SHOW_LOADINGINDICATOR;
        [NSThread detachNewThreadSelector:@selector(GetAboutInformation) toTarget:self withObject:nil];
    }
    @catch (NSException *exception) {
        
    }
    
}

-(void)awakeFromNib
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didChangePreferredContentSize:)
                                                 name:UIContentSizeCategoryDidChangeNotification object:nil];
    
    
    AboutTableView.rowHeight = UITableViewAutomaticDimension;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIContentSizeCategoryDidChangeNotification
                                                  object:nil];
}


- (void)didChangePreferredContentSize:(NSNotification *)notification
{
    [AboutTableView reloadData];
}

-(void)HandleGetAboutCashedData
{
    @try {
        HIDE_LOADINGINDICATOR;
        [AboutTableView reloadData];
    }
    @catch (NSException *exception) {
        
    }
}
-(id)GetAboutCashedData
{
    @try {
        NSString*sector=@"";
        NSString *cashed_sector_key=nil;
        
        switch (Sector) {
            case DRUGS_SECTOR:
            {
                sector=@"Drug";
                cashed_sector_key=ABOUT_DRUGS_CASH_KEY;
            }
                break;
            case FOODS_SECTOR:
            {
                sector=@"Food";
                cashed_sector_key=ABOUT_FOOD_CASH_KEY;
            }
                break;
            case MEDICAL_SECTOR:
            {
                sector=@"Medical";
                cashed_sector_key=ABOUT_MEDICAL_CASH_KEY;
            }
                break;
            case SFDA_SECTOR:
            {
                sector=@"SFDA";
                cashed_sector_key=ABOUT_SFDA_CASH_KEY;
            }
                break;
        }
        
        
        DB_Field *fldTitle = [[DB_Field alloc]init];
        fldTitle.FieldName=@"title";
        fldTitle.FieldAlias= @"TitleAr";
        
        DB_Field *fldDescr = [[DB_Field alloc]init];
        fldDescr.FieldName=@"descr";
        fldDescr.FieldAlias= @"DesAr";
        
        DB_Field *fldSectionName = [[DB_Field alloc]init];
        fldSectionName.FieldName=@"sectionName";
        [[Cashing getObject]setDatabase:@"SDFAipad"];
        id result =[[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldDescr,fldSectionName,fldTitle, nil] Tables:[NSArray arrayWithObject:@"About"] Where:[NSString stringWithFormat:@"sectionName='%@'",sector]  FromIndex:nil ToIndex:nil];
        
        return result;
        
    }
    @catch (NSException *exception) {
        
    }
   
}
-(void)cashAboutData
{
    @try {
        NSString*sector=@"";
        NSString *cashed_sector_key=nil;
        
        switch (Sector) {
            case DRUGS_SECTOR:
            {
                sector=@"Drug";
                cashed_sector_key=ABOUT_DRUGS_CASH_KEY;
            }
                break;
            case FOODS_SECTOR:
            {
                sector=@"Food";
                cashed_sector_key=ABOUT_FOOD_CASH_KEY;
            }
                break;
            case MEDICAL_SECTOR:
            {
                sector=@"Medical";
                cashed_sector_key=ABOUT_MEDICAL_CASH_KEY;
            }
                break;
            case SFDA_SECTOR:
            {
                sector=@"SFDA";
                cashed_sector_key=ABOUT_SFDA_CASH_KEY;
            }
                break;
        }
    
        NSMutableArray *recordList = [[NSMutableArray alloc]init];
        for (id about in aboutList) {
            DB_Recored *newsRecored =[[DB_Recored alloc]init];
            
            DB_Field *fldTitle = [[DB_Field alloc]init];
            fldTitle.FieldDataType= FIELD_DATA_TYPE_TEXT;
            fldTitle.IS_PRIMARY_KEY=YES;
            fldTitle.FieldName=@"title";
            fldTitle.FieldValue = [about objectForKey:@"TitleAr"];
            
            DB_Field *fldDescr = [[DB_Field alloc]init];
            fldDescr.FieldDataType= FIELD_DATA_TYPE_TEXT;
            fldDescr.FieldName=@"descr";
            fldDescr.FieldValue = [about objectForKey:@"DesAr"];
            
            DB_Field *fldSectionName = [[DB_Field alloc]init];
            fldSectionName.FieldDataType= FIELD_DATA_TYPE_TEXT;
            fldSectionName.FieldName=@"sectionName";
            fldSectionName.IS_PRIMARY_KEY=YES;
            fldSectionName.FieldValue = sector;
            
            newsRecored.Fields = [[NSMutableArray alloc]initWithObjects:fldTitle,fldDescr,fldSectionName, nil];
            [recordList addObject:newsRecored];
        }
        [[Cashing getObject]setDatabase:@"SDFAipad"];
        [[Database getObject]setDatabase:@"SDFAipad"];
        [[Database getObject]DeleteFromTable:@"About" Where:[NSString stringWithFormat:@"sectionName='%@'",sector]];
        
        BOOL Success = [[Cashing getObject] CashData:recordList inTable:@"About"];
        if (Success) {
            [[NSUserDefaults standardUserDefaults]setObject:[NSDate date] forKey:cashed_sector_key];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        
 
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)HandleGetAboutInformationResult
{
    @try {
        HIDE_LOADINGINDICATOR;
        
        [AboutTableView reloadData];
        if ([aboutList count]>0) {
            [NSThread detachNewThreadSelector:@selector(cashAboutData) toTarget:self withObject:nil];
            
        }
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)GetAboutInformation
{
    @try {
        NSString*sector=@"";
        NSString *sectorCashedKey=nil;
        
        switch (Sector) {
            case DRUGS_SECTOR:
            {
                sector=@"Drug";
                sectorCashedKey=ABOUT_DRUGS_CASH_KEY;
            }
                break;
            case FOODS_SECTOR:
            {
                sector=@"Food";
                sectorCashedKey=ABOUT_FOOD_CASH_KEY;
            }
                break;
            case MEDICAL_SECTOR:
            {
                sector=@"Medical";
                sectorCashedKey=ABOUT_MEDICAL_CASH_KEY;
            }
                break;
            case SFDA_SECTOR:
            {
                sector=@"SFDA";
                sectorCashedKey=ABOUT_SFDA_CASH_KEY;
            }
                break;
            }
        if ([[NSUserDefaults standardUserDefaults]objectForKey:sectorCashedKey]) {
            int  munites = ceil([[NSDate date] timeIntervalSinceDate:[[NSUserDefaults standardUserDefaults]objectForKey:sectorCashedKey]]/60);
            if (munites>180) {
                aboutList = [Webservice GetAboutInformationBySector:sector];
                if ([aboutList count]>0) {
                   [self performSelectorOnMainThread:@selector(HandleGetAboutInformationResult) withObject:nil waitUntilDone:NO];
                }
                else{
                    aboutList = [self GetAboutCashedData];
                    [self performSelectorOnMainThread:@selector(HandleGetAboutCashedData) withObject:nil waitUntilDone:NO];
                }
                
            }
            else{
                aboutList = [self GetAboutCashedData];
                [self performSelectorOnMainThread:@selector(HandleGetAboutCashedData) withObject:nil waitUntilDone:NO];
            }
        }
        else{
            aboutList = [Webservice GetAboutInformationBySector:sector];
            if ([aboutList count]>0) {
                [self performSelectorOnMainThread:@selector(HandleGetAboutInformationResult) withObject:nil waitUntilDone:NO];
            }
            else{
                aboutList = [self GetAboutCashedData];
                [self performSelectorOnMainThread:@selector(HandleGetAboutCashedData) withObject:nil waitUntilDone:NO];
            }
        }
       
    }
    @catch (NSException *exception) {
        
    }
    
}
#pragma -mark uitableview datasource methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    @try {
        return  [aboutList count];
    }
    @catch (NSException *exception) {
        
    }
   
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AboutCell*cell =(AboutCell*)[tableView dequeueReusableCellWithIdentifier:@"cleAbout"];
    @try {
        if (cell==nil) {
            cell = (AboutCell*)[[[NSBundle mainBundle]loadNibNamed:@"AboutCell" owner:nil options:nil]objectAtIndex:0];
        }
        cell.TitleLabel.textAlignment=NSTextAlignmentRight;
        cell.SubTitleLabel.textAlignment=NSTextAlignmentRight;
        cell.TitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.TitleLabel.numberOfLines=0;
        cell.SubTitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.SubTitleLabel.numberOfLines=0;
        cell.TitleLabel.text= [converter convertArabic:[[[aboutList objectAtIndex:indexPath.row]objectForKey:@"TitleAr"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        
        NSMutableAttributedString* subTitleAttrString = [[NSMutableAttributedString  alloc]initWithString:[[[aboutList objectAtIndex:indexPath.row]objectForKey:@"DesAr"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        [style setLineSpacing:15];
        [style setAlignment:NSTextAlignmentRight];
        [subTitleAttrString addAttribute:NSParagraphStyleAttributeName
                           value:style
                           range:NSMakeRange(0, [[[aboutList objectAtIndex:indexPath.row]objectForKey:@"DesAr"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length)];
        
        cell.SubTitleLabel.attributedText =  subTitleAttrString;
        cell.TitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:17];
        cell.SubTitleLabel.font=[UIFont fontWithName:@"GESSTwoLight-Light" size:15];
        
    }
    @catch (NSException *exception) {
        
    }
    return  cell;
}
#pragma -mark tableview delegate methods

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 88.0;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSInteger height=0;
//    @try {
//        NSString *descr;
//        descr =[[[aboutList objectAtIndex:indexPath.row] objectForKey:@"DesAr"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//        CGSize subtitleSize = [descr sizeWithFont:[UIFont fontWithName:@"GESSTwoBold-Bold" size:21] constrainedToSize:CGSizeMake(tableView.frame.size.width , 200000000000000000000.0f)];
//        height = subtitleSize.height+50;
//        
//        //NSLog(@" heigt for index path row %d  = %d",indexPath.row,height);
//    }
//    @catch (NSException *exception) {
//        
//    }
//    
//    return height;
//
//    
//}
@end
