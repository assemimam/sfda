//
//  AboutCell.h
//  sfda-ipad
//
//  Created by Assem Imam on 5/26/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *TitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *SubTitleLabel;

@end
