//
//  NewsItemView.h
//  sfda-ipad
//
//  Created by Assem Imam on 6/2/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsItemView : UIView
@property (weak, nonatomic) IBOutlet UILabel *NewsDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *NewsTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *NewsImageView;
@property (weak, nonatomic) IBOutlet UILabel *NewsDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *ContainerScrollView;

@end
