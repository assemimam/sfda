//
//  NewsView.h
//  sfda-ipad
//
//  Created by Assem Imam on 5/21/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

@protocol NewsDelegate <NSObject>
@optional
-(void)didFinishLoadingNews:(id)newsList;
-(void)didSelectNewsItem:(id)item atIndex:(int)index;
-(void)didSelectNewsHeader:(id)item;
@end

#import <UIKit/UIKit.h>
#import "LibrariesConstants.h"
typedef enum
{
    OPEN_TYPE_FOR_SEARCH,
    OPEN_TYPE_REGULAR
}NewsOpenType ;
@interface NewsView : UIView<AllNewsTyesDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *NewsActivityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *NewsDescritionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *NewsImageView;
@property (weak, nonatomic) IBOutlet UIView *NewsHeaderView;
@property (weak, nonatomic) IBOutlet UILabel *NewsTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *NewsDateLabel;
@property (weak, nonatomic) IBOutlet UITableView *NewsTableView;
@property(assign) id<NewsDelegate> delegate;
@property(nonatomic)SECTOR_TYPE Sector;
@property(nonatomic)NewsOpenType OpenType;
@property(nonatomic,retain)NSString*NewsSearchTitle;
-(void)LoadNews;
@end
