//
//  NewsCell.h
//  sfda-ipad
//
//  Created by Assem Imam on 5/21/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *NewsView2;
@property (weak, nonatomic) IBOutlet UIView *NewsView1;
@property (weak, nonatomic) IBOutlet UIButton *NewsBacButton2;
@property (weak, nonatomic) IBOutlet UIImageView *NewsImageView2;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *NewsActivityIndicator2;
@property (weak, nonatomic) IBOutlet UILabel *NewsTitleLabel2;
@property (weak, nonatomic) IBOutlet UILabel *NewsDateLabel2;
@property (weak, nonatomic) IBOutlet UILabel *NewsTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *NewsDateLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *NewsActivityIndicator;
@property (weak, nonatomic) IBOutlet UIImageView *NewsImageView;
@property (weak, nonatomic) IBOutlet UIView *NewsMarkerView;
@property (weak, nonatomic) IBOutlet UIView *NewsmarkerView2;
@property (weak, nonatomic) IBOutlet UIButton *NewsBackButton;

@end
