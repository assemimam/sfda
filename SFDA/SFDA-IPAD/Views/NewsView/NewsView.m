///
//  NewsView.m
//  sfda-ipad
//
//  Created by Assem Imam on 5/21/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "NewsView.h"
#import "NewsCell.h"
#import "Cashing.h"
#import "Database.h"

@interface NewsView()
{
    id NewsList;
    id AllNewsList;
    NewsType CurrentNewsType;
    ArabicConverter *converter;
}
@end
@implementation NewsView
@synthesize Sector,delegate,OpenType,NewsSearchTitle;
-(void)awakeFromNib{
    UITapGestureRecognizer *tapRecognizer =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapRecognizerAction)];
    tapRecognizer.numberOfTapsRequired=1;
    [self.NewsHeaderView addGestureRecognizer:tapRecognizer];
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */
#pragma -mark cashing methods
-(void)cashNewsData:(id)newsList
{
    @try {
        NSString*sector =@"";
        NSString *cashed_sector_key=nil;
        switch (Sector) {
            case DRUGS_SECTOR:
            {
                sector=@"Drug";
                cashed_sector_key=NEWS_DRUGS_CASH_KEY;
                
            }
                break;
            case FOODS_SECTOR:
            {
                sector=@"Food";
                cashed_sector_key= NEWS_FOOD_CASH_KEY;
            }
                break;
            case MEDICAL_SECTOR:
            {
                sector=@"Medical";
                cashed_sector_key =NEWS_MEDICAL_CASH_KEY;
            }
                break;
            case SFDA_SECTOR:
            {
                sector=@"SFDA";
                cashed_sector_key=NEWS_SFDA_CASH_KEY;
            }
                break;
            case ALL:
            {
                sector=@"latestnews";
                cashed_sector_key=NEWS_ALL_CASH_KEY;
                
            }
                break;
        }
        
        
        NSMutableArray *recordList = [[NSMutableArray alloc]init];
        for (id news in newsList) {
            DB_Recored *newsRecored =[[DB_Recored alloc]init];
            
            DB_Field *fldNewsId = [[DB_Field alloc]init];
            fldNewsId.FieldDataType= FIELD_DATA_TYPE_NUMBER;
            fldNewsId.FieldName=@"id";
            fldNewsId.IS_PRIMARY_KEY=YES;
            fldNewsId.FieldValue = [news objectForKey:@"$id"];
            
            DB_Field *fldTitleAr = [[DB_Field alloc]init];
            fldTitleAr.FieldDataType= FIELD_DATA_TYPE_TEXT;
            fldTitleAr.FieldName=@"title_ar";
            fldTitleAr.FieldValue = [news objectForKey:@"TitleAr"];
            
            DB_Field *fldTitleEn = [[DB_Field alloc]init];
            fldTitleEn.FieldDataType= FIELD_DATA_TYPE_TEXT;
            fldTitleEn.FieldName=@"title_en";
            fldTitleEn.FieldValue = [news objectForKey:@"TitleEn"];
            
            DB_Field *fldContentAr = [[DB_Field alloc]init];
            fldContentAr.FieldDataType= FIELD_DATA_TYPE_TEXT;
            fldContentAr.FieldName=@"content_ar";
            fldContentAr.FieldValue = [news objectForKey:@"DescriptionAr"];
            
            DB_Field *fldContentEn = [[DB_Field alloc]init];
            fldContentEn.FieldDataType= FIELD_DATA_TYPE_TEXT;
            fldContentEn.FieldName=@"content_en";
            fldContentEn.FieldValue = [news objectForKey:@"DescriptionEn"];
            
            DB_Field *fldCategoryAr = [[DB_Field alloc]init];
            fldCategoryAr.FieldDataType= FIELD_DATA_TYPE_TEXT;
            fldCategoryAr.FieldName=@"cat_ar";
            fldCategoryAr.FieldValue = [news objectForKey:@"CategoryAr"];
            
            DB_Field *fldCategoryEn = [[DB_Field alloc]init];
            fldCategoryEn.FieldDataType= FIELD_DATA_TYPE_TEXT;
            fldCategoryEn.FieldName=@"cat_en";
            fldCategoryEn.FieldValue = [news objectForKey:@"CategoryEn"];
            
            DB_Field *fldPublishDate = [[DB_Field alloc]init];
            fldPublishDate.FieldDataType= FIELD_DATA_TYPE_TEXT;
            fldPublishDate.FieldName=@"PubDate";
            fldPublishDate.FieldValue = [news objectForKey:@"PubDate"];
            
            DB_Field *fldImagePath= [[DB_Field alloc]init];
            fldImagePath.FieldDataType= FIELD_DATA_TYPE_IMAGE;
            fldImagePath.FieldName=@"image_path";
            fldImagePath.FieldValue = [news objectForKey:@"ImageUrl"];
            
            DB_Field *fldSector = [[DB_Field alloc]init];
            fldSector.FieldDataType= FIELD_DATA_TYPE_TEXT;
            fldSector.FieldName=@"sector";
            fldSector.IS_PRIMARY_KEY=YES;
            fldSector.FieldValue = sector;
            
            DB_Field *fldNewsUrl = [[DB_Field alloc]init];
            fldNewsUrl.FieldDataType= FIELD_DATA_TYPE_TEXT;
            fldNewsUrl.FieldName=@"link";
            fldNewsUrl.FieldValue = [news objectForKey:@"Url"];
            
            newsRecored.Fields = [[NSMutableArray alloc]initWithObjects:fldNewsId,fldTitleAr,fldTitleEn,fldContentAr,fldContentEn,fldCategoryAr,fldCategoryEn,fldPublishDate,fldImagePath,fldSector,fldNewsUrl, nil];
            [recordList addObject:newsRecored];
        }
        [[Cashing getObject]setDatabase:@"SDFAipad"];
        [[Database getObject] setDatabase:@"SDFAipad"];
        [[Database getObject] DeleteFromTable:@"news" Where:[NSString stringWithFormat:@"sector='%@'",sector]];
        
        BOOL Success = [[Cashing getObject] CashData:recordList inTable:@"news"];
        if (Success) {
            [[NSUserDefaults standardUserDefaults]setObject:[NSDate date] forKey:cashed_sector_key];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        NewsList = [self GetNewsCashedData];
        AllNewsList = [NewsList copy];
        if (delegate) {
            [self.delegate didFinishLoadingNews:NewsList];
        }
        
    }
    @catch (NSException *exception) {
        
    }
}
-(void)tapRecognizerAction
{
    @try {
        if (self.delegate) {
            [self.delegate didSelectNewsHeader:[NewsList objectAtIndex:0]];
        }
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)HandleGetNewsCashedDataResult
{
    @try {
        HIDE_LOADINGINDICATOR;
        if ([NewsList count]>0) {
            
            NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"MM/dd/yyyy hh:mm:ss a"];
            [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
            NSDateFormatter *stringFormatter =[[NSDateFormatter alloc]init];
            [stringFormatter setDateFormat:@"yyyy/MM/dd"];
            [stringFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
            id FirstNews = [NewsList objectAtIndex:0];
            
            
            [self setHidden:NO];
            self.NewsTitleLabel.text =[FirstNews objectForKey:@"TitleAr"];
            NSDate*newsDate =[formatter dateFromString:[FirstNews objectForKey:@"PubDate"]];
            self.NewsDescritionLabel.text =[FirstNews objectForKey:@"DescriptionAr"];
            self.NewsDateLabel.text =[stringFormatter stringFromDate:newsDate];
            self.NewsTitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:17];
            self.NewsDateLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:12];
            self.NewsDescritionLabel.font=[UIFont fontWithName:@"GESSTwoLight-Light" size:15];
            if ([FirstNews objectForKey:@"ImageUrl"])
            {
                NSURL*NewsImageUrl =[NSURL URLWithString: [FirstNews objectForKey:@"ImageUrl"]];
                [self.NewsActivityIndicator startAnimating];
                
                
                if (NewsImageUrl)
                {
                    [[ImageCache sharedInstance]downloadImageAtURL:NewsImageUrl completionHandler:^(UIImage *image) {
                        [self.NewsActivityIndicator stopAnimating];
                        if (image) {
                            self.NewsImageView.image = image;
                        }
                        else{
                            self.NewsImageView.image = [UIImage imageNamed:@"maindefaultimg.png"];
                        }
                        [self.NewsActivityIndicator stopAnimating];
                    }];
                    
                }
                
            }
            else//from cash
            {
                @autoreleasepool {
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                         NSUserDomainMask, YES);
                    NSString *documentsDirectory = [paths objectAtIndex:0];
                    NSString *imagePath=[FirstNews objectForKey:@"image_path"];
                    documentsDirectory =[documentsDirectory stringByAppendingFormat:@"/%@",imagePath];
                    
                    [[ImageCache sharedInstance] downloadImageAtURL:[NSURL fileURLWithPath:documentsDirectory] completionHandler:^(UIImage *image) {
                        if (image) {
                            self.NewsImageView.image = image;
                        }
                        else{
                            self.NewsImageView.image = [UIImage imageNamed:@"maindefaultimgrows.png"];
                        }
                        [self.NewsActivityIndicator stopAnimating];
                    }];
                    
                }
                
            }
            [self.NewsTableView reloadData];
            
            
            if (delegate) {
                [self.delegate didFinishLoadingNews:NewsList];
            }
            
        }
        
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)HandleGetNewsDataResult
{
    @try {
        
        HIDE_LOADINGINDICATOR;
        if ([NewsList count]>0) {
            
            [NSThread detachNewThreadSelector:@selector(cashNewsData:) toTarget:self withObject:NewsList];
            
            NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"MM/dd/yyyy hh:mm:ss a"];
            [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
            NSDateFormatter *stringFormatter =[[NSDateFormatter alloc]init];
            [stringFormatter setDateFormat:@"yyyy/MM/dd"];
            [stringFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
            id FirstNews = [NewsList objectAtIndex:0];
            
            
            [self setHidden:NO];
            self.NewsTitleLabel.text =[FirstNews objectForKey:@"TitleAr"];
            self.NewsDescritionLabel.text =[FirstNews objectForKey:@"DescriptionAr"];
            NSDate*newsDate =[formatter dateFromString:[FirstNews objectForKey:@"PubDate"]];
            
            self.NewsDateLabel.text =[stringFormatter stringFromDate:newsDate];
            self.NewsTitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:17];
            self.NewsDescritionLabel.font=[UIFont fontWithName:@"GESSTwoLight-Light" size:15];
            self.NewsDateLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:12];
            if ([FirstNews objectForKey:@"ImageUrl"]) {
                NSURL*NewsImageUrl =[NSURL URLWithString: [FirstNews objectForKey:@"ImageUrl"]];
                [self.NewsActivityIndicator startAnimating];
                if (NewsImageUrl) {
                    [[ImageCache sharedInstance]downloadImageAtURL:NewsImageUrl completionHandler:^(UIImage *image) {
                        [self.NewsActivityIndicator stopAnimating];
                        if (image) {
                            self.NewsImageView.image = image;
                        }
                        else{
                            self.NewsImageView.image = [UIImage imageNamed:@"maindefaultimg.png"];
                        }
                    }];
                }
                
            }
            else//from cash
            {
                @autoreleasepool {
                    [[ImageCache sharedInstance] downloadImageAtURL:[NSURL fileURLWithPath:[FirstNews objectForKey:@"image_path"]] completionHandler:^(UIImage *image) {
                        if (image) {
                            self.NewsImageView.image = image;
                        }
                        else{
                            self.NewsImageView.image = [UIImage imageNamed:@"maindefaultimgrows.png"];
                        }
                    }];
                    
                }
                
                
            }
            [self.NewsTableView reloadData];
            
            
            
        }
        
    }
    @catch (NSException *exception) {
        
    }
    
}
-(id)GetNewsCashedData
{
    @try {
        
        NSString*sector =@"";
        switch (Sector) {
            case DRUGS_SECTOR:
            {
                sector=@"Drug";
                
            }
                break;
            case FOODS_SECTOR:
            {
                sector=@"Food";
                
            }
                break;
            case MEDICAL_SECTOR:
            {
                sector=@"Medical";
                
            }
                break;
            case SFDA_SECTOR:
            {
                sector=@"SFDA";
                
            }
                break;
            case ALL:
            {
                sector=@"latestnews";
                
            }
                break;
        }
        
        
        DB_Field *fldNewsId = [[DB_Field alloc]init];
        fldNewsId.FieldName=@"id";
        fldNewsId.FieldAlias=@"$id";
        
        DB_Field *fldNewsSerial = [[DB_Field alloc]init];
        fldNewsSerial.FieldName=@"serial";
        
        
        DB_Field *fldTitleAr = [[DB_Field alloc]init];
        fldTitleAr.FieldName=@"title_ar";
        fldTitleAr.FieldAlias=@"TitleAr";
        
        DB_Field *fldTitleEn = [[DB_Field alloc]init];
        fldTitleEn.FieldName=@"title_en";
        fldTitleEn.FieldAlias=@"TitleEn";
        
        DB_Field *fldContentAr = [[DB_Field alloc]init];
        fldContentAr.FieldName=@"content_ar";
        fldContentAr.FieldAlias=@"DescriptionAr";
        
        DB_Field *fldContentEn = [[DB_Field alloc]init];
        fldContentEn.FieldName=@"content_en";
        fldContentEn.FieldAlias=@"DescriptionEn";
        
        DB_Field *fldCategoryAr = [[DB_Field alloc]init];
        fldCategoryAr.FieldName=@"cat_ar";
        fldCategoryAr.FieldAlias=@"CategoryAr";
        
        DB_Field *fldCategoryEn = [[DB_Field alloc]init];
        fldCategoryEn.FieldName=@"cat_en";
        fldCategoryEn.FieldAlias=@"CategoryEn";
        
        DB_Field *fldPublishDate = [[DB_Field alloc]init];
        fldPublishDate.FieldName=@"PubDate";
        fldPublishDate.FieldAlias=@"PubDate";
        
        DB_Field *fldImagePath= [[DB_Field alloc]init];
        fldImagePath.FieldName=@"image_path";
        
        
        DB_Field *fldSector = [[DB_Field alloc]init];
        fldSector.FieldName=@"sector";
        fldSector.FieldAlias=@"sector";
        
        DB_Field *fldNewsUrl = [[DB_Field alloc]init];
        fldNewsUrl.FieldName=@"link";
        fldNewsUrl.FieldAlias=@"Url";
        
        DB_Field *fldFavourite = [[DB_Field alloc]init];
        fldFavourite.FieldName=@"is_favourite";
        
        
        
        [[Cashing getObject]setDatabase:@"SDFAipad"];
        id result =[[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldNewsSerial,fldNewsId,fldTitleAr,fldTitleEn,fldContentAr,fldContentEn,fldCategoryAr,fldCategoryEn,fldSector,fldPublishDate,fldImagePath,fldNewsUrl,fldFavourite, nil] Tables:[NSArray arrayWithObject:@"news"] Where:[NSString stringWithFormat:@"sector='%@'",sector] FromIndex:nil ToIndex:nil];
        
        return result;
        
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)GetNewsData
{
    @try {
        NSString *sectorCashedKey=@"";
        NSString*sector =@"";
        switch (Sector) {
            case DRUGS_SECTOR:
            {
                sector=@"Drug";
                sectorCashedKey=NEWS_DRUGS_CASH_KEY;
            }
                break;
            case FOODS_SECTOR:
            {
                sector=@"Food";
                sectorCashedKey=NEWS_FOOD_CASH_KEY;
            }
                break;
            case MEDICAL_SECTOR:
            {
                sector=@"Medical";
                sectorCashedKey=NEWS_MEDICAL_CASH_KEY;
            }
                break;
            case SFDA_SECTOR:
            {
                sector=@"SFDA";
                sectorCashedKey=NEWS_SFDA_CASH_KEY;
            }
                break;
            case ALL:
            {
                sector=@"latestnews";
                sectorCashedKey=NEWS_ALL_CASH_KEY;
            }
                break;
        }
        
        
        if ([[NSUserDefaults standardUserDefaults]objectForKey:sectorCashedKey]) {
            int  munites = ceil([[NSDate date] timeIntervalSinceDate:[[NSUserDefaults standardUserDefaults]objectForKey:sectorCashedKey]]/60);
            if (munites>180) {
                NewsList = [Webservice GetNewsListBySector:sector];
                if ([NewsList count]>0) {
                    AllNewsList = [NewsList copy];
                    [self performSelectorOnMainThread:@selector(HandleGetNewsDataResult) withObject:nil waitUntilDone:NO];
                }
                else{
                    NewsList = [self GetNewsCashedData];
                    AllNewsList = [NewsList copy];
                    [self performSelectorOnMainThread:@selector(HandleGetNewsCashedDataResult) withObject:nil waitUntilDone:NO];
                }
                
            }
            else{
                NewsList = [self GetNewsCashedData];
                AllNewsList = [NewsList copy];
                [self performSelectorOnMainThread:@selector(HandleGetNewsCashedDataResult) withObject:nil waitUntilDone:NO];
                
            }
        }
        else{
            NewsList = [Webservice GetNewsListBySector:sector];
            if ([NewsList count]>0) {
                AllNewsList = [NewsList copy];
                
                [self performSelectorOnMainThread:@selector(HandleGetNewsDataResult) withObject:nil waitUntilDone:NO];
            }
            else{
                NewsList = [self GetNewsCashedData];
                AllNewsList = [NewsList copy];
                [self performSelectorOnMainThread:@selector(HandleGetNewsCashedDataResult) withObject:nil waitUntilDone:NO];
            }
        }
        
        
    }
    @catch (NSException *exception) {
        
    }
    
}

-(void)LoadNews
{
    @try {
        SHOW_LOADINGINDICATOR
        
        [NSThread detachNewThreadSelector:@selector(GetNewsData) toTarget:self withObject:nil];
        
    }
    @catch (NSException *exception) {
        
    }
    
}
#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewsCell*cell;
    int index=(indexPath.row+1)*2;
    NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM/dd/yyyy hh:mm:ss a"];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    NSDateFormatter *stringFormatter =[[NSDateFormatter alloc]init];
    [stringFormatter setDateFormat:@"yyyy/MM/dd"];
    [stringFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    @try {
        //cell= (NewsCell*)[tableView dequeueReusableCellWithIdentifier:@"cleNews"];
        if (cell==nil) {
            cell=(NewsCell*)[[[NSBundle mainBundle] loadNibNamed:@"NewsCell" owner:nil options:nil]objectAtIndex:0];
        }
        cell.NewsTitleLabel.font = [UIFont fontWithName:@"GESSTwoBold-Bold" size:13];
        cell.NewsTitleLabel2.font = [UIFont fontWithName:@"GESSTwoBold-Bold" size:13];
        cell.NewsDateLabel.font = [UIFont fontWithName:@"GESSTwoLight-Light" size:13];
        cell.NewsDateLabel2.font = [UIFont fontWithName:@"GESSTwoLight-Light" size:13];
        
        NSDate *firstDate =[formatter dateFromString:[[NewsList objectAtIndex:index -1]objectForKey:@"PubDate"]];
        
        if ( index<=[NewsList count]-1) {
            
            NSDate *seconedDate =[formatter dateFromString:[[NewsList objectAtIndex:index]objectForKey:@"PubDate"]];
            
            if (Sector==SFDA_SECTOR) {
                cell.NewsMarkerView.backgroundColor=[UIColor colorWithRed:92/255.0f  green:98/255.0f blue:106/255.0f alpha:1];
                cell.NewsmarkerView2.backgroundColor=[UIColor colorWithRed:92/255.0f  green:98/255.0f blue:106/255.0f alpha:1];
            }
            
            
            cell.NewsMarkerView.backgroundColor= self.NewsHeaderView.backgroundColor;
            cell.NewsmarkerView2.backgroundColor= self.NewsHeaderView.backgroundColor;
            
            if ([[NewsList objectAtIndex:index -1]objectForKey:@"ImageUrl"]) {
                [cell.NewsActivityIndicator startAnimating];
                NSURL*firstNewsImageUrl =[NSURL URLWithString: [[NewsList objectAtIndex:index -1]objectForKey:@"ImageUrl"]];
                if (firstNewsImageUrl) {
                    [[ImageCache sharedInstance]downloadImageAtURL:firstNewsImageUrl completionHandler:^(UIImage *image) {
                        [cell.NewsActivityIndicator stopAnimating];
                        if (image) {
                            cell.NewsImageView.image = image;
                        }
                        else{
                            cell.NewsImageView.image = [UIImage imageNamed:@"maindefaultimgrows.png"];
                        }
                    }];
                }
            }
            else{//from cashe
                @autoreleasepool {
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                         NSUserDomainMask, YES);
                    NSString *documentsDirectory = [paths objectAtIndex:0];
                    NSString *imagePath=[[NewsList objectAtIndex:index -1]objectForKey:@"image_path"];
                    documentsDirectory =[documentsDirectory stringByAppendingFormat:@"/%@",imagePath];
                    
                    
                    
                    [[ImageCache sharedInstance] downloadImageAtURL:[NSURL fileURLWithPath:documentsDirectory]completionHandler:^(UIImage *image) {
                        if (image) {
                            cell.NewsImageView.image = image;
                        }
                        else{
                            cell.NewsImageView.image = [UIImage imageNamed:@"maindefaultimgrows.png"];
                        }
                    }];
                
                }
            }
            
            if ([[NewsList objectAtIndex:index ]objectForKey:@"ImageUrl"]) {
                [cell.NewsActivityIndicator2 startAnimating];
                NSURL*seconedNewsImageUrl =[NSURL URLWithString: [[NewsList objectAtIndex:index ]objectForKey:@"ImageUrl"]];
                if (seconedNewsImageUrl) {
                    [[ImageCache sharedInstance]downloadImageAtURL:seconedNewsImageUrl completionHandler:^(UIImage *image) {
                        [cell.NewsActivityIndicator2 stopAnimating];
                        if (image) {
                            cell.NewsImageView2.image = image;
                        }
                        else{
                            cell.NewsImageView2.image = [UIImage imageNamed:@"maindefaultimgrows.png"];
                        }
                    }];
                }
                
                
            }
            else{//from cashe
                @autoreleasepool {
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                         NSUserDomainMask, YES);
                    NSString *documentsDirectory = [paths objectAtIndex:0];
                    NSString *imagePath=[[NewsList objectAtIndex:index]objectForKey:@"image_path"];
                    documentsDirectory =[documentsDirectory stringByAppendingFormat:@"/%@",imagePath];
                    
                    
                    
                    [[ImageCache sharedInstance] downloadImageAtURL:[NSURL fileURLWithPath:documentsDirectory]completionHandler:^(UIImage *image) {
                        if (image) {
                            cell.NewsImageView2.image = image;
                        }
                        else{
                            cell.NewsImageView2.image = [UIImage imageNamed:@"maindefaultimgrows.png"];
                        }
                    }];
                }
                
            }
            
            cell.NewsMarkerView.backgroundColor= self.NewsHeaderView.backgroundColor;
            cell.NewsmarkerView2.backgroundColor= self.NewsHeaderView.backgroundColor;
            if (Sector==ALL) {
                if (CurrentNewsType==ALL_NEWS ) {
                    if([[[NewsList objectAtIndex:index -1]objectForKey:@"CategoryAr"]  rangeOfString:@"تنبيه"].location!= NSNotFound  || [[[NewsList objectAtIndex:index -1]objectForKey:@"CategoryEn"]rangeOfString:@"تنبيه"].location!= NSNotFound){
                        cell.NewsMarkerView.backgroundColor=[UIColor colorWithRed:215/255.0f  green:45/255.0f blue:40/255.0f alpha:1];
                    }
                    if([[[NewsList objectAtIndex:index ]objectForKey:@"CategoryAr"] rangeOfString:@"تنبيه"].location!= NSNotFound  || [[[NewsList objectAtIndex:index ]objectForKey:@"CategoryEn"]rangeOfString:@"تنبيه"].location!= NSNotFound){
                        cell.NewsmarkerView2.backgroundColor=[UIColor colorWithRed:215/255.0f  green:45/255.0f blue:40/255.0f alpha:1];
                    }
                }
            }
            
            cell.NewsTitleLabel.text = [[NewsList objectAtIndex:index -1]objectForKey:@"TitleAr"];
            cell.NewsTitleLabel2.text = [[NewsList objectAtIndex:index]objectForKey:@"TitleAr"];
            cell.NewsDateLabel.text = [stringFormatter stringFromDate:firstDate];
            cell.NewsDateLabel2.text = [stringFormatter stringFromDate:seconedDate];
            cell.NewsBackButton.tag=index -1;
            cell.NewsBacButton2.tag=index ;
            [cell.NewsBackButton addTarget:self action:@selector(didSelectNewsCellAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.NewsBacButton2 addTarget:self action:@selector(didSelectNewsCellAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        else {
            [cell.NewsView2 setHidden:YES];
            cell.NewsMarkerView.backgroundColor= self.NewsHeaderView.backgroundColor;
            if (Sector==ALL) {
                if (CurrentNewsType==ALL_NEWS) {
                    
                    if([[[NewsList objectAtIndex:index -1] objectForKey:@"CategoryAr"] rangeOfString:@"تنبيه"].location!= NSNotFound  || [[[NewsList objectAtIndex:index -1]objectForKey:@"CategoryEn"]rangeOfString:@"تنبيه"].location!= NSNotFound){
                        cell.NewsMarkerView.backgroundColor=[UIColor colorWithRed:215/255.0f  green:45/255.0f blue:40/255.0f alpha:1];
                    }
                    
                }
                
            }
            cell.NewsTitleLabel.text = [[NewsList objectAtIndex:index-1 ]objectForKey:@"TitleAr"];
            cell.NewsDateLabel.text = [stringFormatter stringFromDate:firstDate];
            cell.NewsBackButton.tag=index-1 ;
            [cell.NewsBackButton addTarget:self action:@selector(didSelectNewsCellAction:) forControlEvents:UIControlEventTouchUpInside];
            if ([[NewsList objectAtIndex:index -1]objectForKey:@"ImageUrl"]) {
                [cell.NewsActivityIndicator startAnimating];
                NSURL*firstNewsImageUrl =[NSURL URLWithString: [[NewsList objectAtIndex:index -1]objectForKey:@"ImageUrl"]];
                if (firstNewsImageUrl) {
                    [[ImageCache sharedInstance]downloadImageAtURL:firstNewsImageUrl completionHandler:^(UIImage *image) {
                        [cell.NewsActivityIndicator stopAnimating];
                        if (image) {
                            cell.NewsImageView.image = image;
                        }
                        else{
                            cell.NewsImageView.image = [UIImage imageNamed:@"maindefaultimgrows.png"];
                        }
                    }];
                }
                
                
            }
            else{//from cashe
                @autoreleasepool {
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                         NSUserDomainMask, YES);
                    NSString *documentsDirectory = [paths objectAtIndex:0];
                    NSString *imagePath=[[NewsList objectAtIndex:index -1]objectForKey:@"image_path"];
                    documentsDirectory =[documentsDirectory stringByAppendingFormat:@"/%@",imagePath];
                    
                    
                    
                    [[ImageCache sharedInstance] downloadImageAtURL:[NSURL fileURLWithPath:documentsDirectory]completionHandler:^(UIImage *image) {
                        if (image) {
                            cell.NewsImageView2.image = image;
                        }
                        else{
                            cell.NewsImageView2.image = [UIImage imageNamed:@"maindefaultimgrows.png"];
                        }
                    }];
                }
            }
            
        }
        
        if (CurrentNewsType==SFDA_NEWS) {
            cell.NewsMarkerView.backgroundColor=[UIColor colorWithRed:92/255.0f  green:98/255.0f blue:106/255.0f alpha:1];
            cell.NewsmarkerView2.backgroundColor=[UIColor colorWithRed:92/255.0f  green:98/255.0f blue:106/255.0f alpha:1];
        }
        if (CurrentNewsType==ALL_WRNING_NEWS) {
            cell.NewsMarkerView.backgroundColor=[UIColor colorWithRed:215/255.0f  green:45/255.0f blue:40/255.0f alpha:1];
            cell.NewsmarkerView2.backgroundColor=[UIColor colorWithRed:215/255.0f  green:45/255.0f blue:40/255.0f alpha:1];
        }
        
    }
    @catch (NSException *exception) {
        
    }
    
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int noOfRows=0;
    @try {
        if (NewsList && [NewsList count]>0) {
            noOfRows = floor(([NewsList count]-1)/2)+1;
            if (([NewsList count]-1) %2==0) {
                noOfRows=  floor((([NewsList count]-1)/2));
            }
        }
    }
    @catch (NSException *exception) {
        
    }
    
    return noOfRows;
}
-(void)didSelectNewsCellAction:(UIButton*)sender
{
    @try {
        if (self.delegate) {
            [self.delegate didSelectNewsItem:[NewsList objectAtIndex:sender.tag] atIndex:sender.tag];
        }
    }
    @catch (NSException *exception) {
        
    }
    
}
#pragma -mark allnews types delegate
-(void)didSelectNewsType:(NewsType)type{
    @try {
        self.NewsTableView.dataSource= self;
        CurrentNewsType = type;
        
        switch (type) {
            case ALL_NEWS:
            {
                NewsList = [AllNewsList mutableCopy];
                [self.NewsTableView reloadData];
            }
                break;
            case SFDA_NEWS:
            {
                NSMutableArray *sfdaNewsList =[[NSMutableArray alloc]init];
                for (id newsItem in [AllNewsList copy]) {
                    if([[newsItem objectForKey:@"CategoryAr"] rangeOfString:@"الرئيسية"].location!= NSNotFound  || [[newsItem objectForKey:@"CategoryEn"] rangeOfString:@"الرئيسية"].location!= NSNotFound){
                        [sfdaNewsList addObject:newsItem];
                        
                    }
                }
                NewsList = [sfdaNewsList copy];
                
                [self.NewsTableView reloadData];
                
            }
                break;
            case ALL_WRNING_NEWS:
            {
                NSMutableArray *worningNewsList =[[NSMutableArray alloc]init];
                for (id newsItem in [AllNewsList copy]) {
                    if([[newsItem objectForKey:@"CategoryAr"] rangeOfString:@"تنبيه"].location!= NSNotFound  || [[newsItem objectForKey:@"CategoryEn"] rangeOfString:@"تنبيه"].location!= NSNotFound){
                        [worningNewsList addObject:newsItem];
                        
                    }
                }
                NewsList = [worningNewsList mutableCopy];
                [self.NewsTableView reloadData];
                
            }
                break;
                
        }
    }
    @catch (NSException *exception) {
        
    }
    
}




@end
