//
//  EmploymentView.h
//  sfda-ipad
//
//  Created by Assem Imam on 6/26/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmploymentView : UIView
{
    
    __weak IBOutlet UIButton *HelpButton;
    __weak IBOutlet UIButton *UpdateInfomationButton;
    __weak IBOutlet UIButton *NewUserButton;
    __weak IBOutlet UITextView *DescriptionTextView;
    __weak IBOutlet UILabel *TitleLabel;
}
- (IBAction)NewUserAction:(UIButton *)sender;
- (IBAction)UdateInformationAction:(UIButton *)sender;
- (IBAction)HelpAction:(UIButton *)sender;

@end
