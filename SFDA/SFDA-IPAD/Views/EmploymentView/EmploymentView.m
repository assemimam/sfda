//
//  EmploymentView.m
//  sfda-ipad
//
//  Created by Assem Imam on 6/26/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "EmploymentView.h"
#import "Language.h"

@implementation EmploymentView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)drawRect:(CGRect)rect
{
    @try {
        
        NSMutableAttributedString* subTitleAttrString = [[NSMutableAttributedString  alloc]initWithString:DescriptionTextView.text];
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        [style setLineSpacing:13];
        [style setAlignment:NSTextAlignmentRight];
        [subTitleAttrString addAttribute:NSParagraphStyleAttributeName
                                   value:style
                                   range:NSMakeRange(0, DescriptionTextView.text.length)];
        DescriptionTextView.attributedText = subTitleAttrString;

        TitleLabel.font = [UIFont fontWithName:@"GESSTwoBold-Bold" size:17];
        DescriptionTextView.font=[UIFont fontWithName:@"GESSTwoLight-Light" size:15];
        NewUserButton.titleLabel.font= [UIFont fontWithName:@"GESSTwoBold-Bold" size:10];
        UpdateInfomationButton.titleLabel.font= [UIFont fontWithName:@"GESSTwoBold-Bold" size:10];
        HelpButton.titleLabel.font= [UIFont fontWithName:@"GESSTwoBold-Bold" size:10];
    }
    @catch (NSException *exception) {
        
    }
   
}


- (IBAction)NewUserAction:(UIButton *)sender {
    @try {
        if (!internetConnection) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
           
            return;
        }
        
         [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://www.sfda.gov.sa/ar/about/Pages/sectors_brief.aspx"]];
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)UdateInformationAction:(UIButton *)sender {
    @try {
        if (!internetConnection) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            
            return;
        }
        
         [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"https://old.sfda.gov.sa/services/career/Application_ar_Personal.aspx?opt=newupd"]];
        
    }
    @catch (NSException *exception) {
        
    }
   
}

- (IBAction)HelpAction:(UIButton *)sender {
    @try {
        if (!internetConnection) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            
            return;
        }
         [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://old.sfda.gov.sa/pages/hrfeedback_.aspx"]];
        
    }
    @catch (NSException *exception) {
        
    }
    
}
@end
