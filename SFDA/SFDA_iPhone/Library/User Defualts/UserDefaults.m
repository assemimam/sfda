//
//  UserDefaults.m
//  Islam Guide
//
//  Created by Hani on 7/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "UserDefaults.h"

@implementation UserDefaults

#pragma -
#pragma Add Object

+ (void)addObject:(id)objectValue withKey:(NSString *)objectKey ifKeyNotExists:(BOOL)keyCheck{
	if ((objectKey != nil) && !keyCheck) {
		[[NSUserDefaults standardUserDefaults] setObject:objectValue forKey:objectKey];
		[[NSUserDefaults standardUserDefaults] synchronize];
	} else if (objectKey != nil) {
		NSObject *returnObject = [[NSUserDefaults standardUserDefaults] objectForKey:objectKey];
		if (returnObject == nil) {
			[[NSUserDefaults standardUserDefaults] setObject:objectValue forKey:objectKey];
			[[NSUserDefaults standardUserDefaults] synchronize];
		}
	}
}

#pragma -
#pragma Get Object

+ (NSData *)getDataWithKey:(NSString *)dataKey{
	NSData *userData = nil;
	
	if (dataKey != nil) {
		NSObject *returnObject = [[NSUserDefaults standardUserDefaults] objectForKey:dataKey];
		if ([returnObject isKindOfClass:[NSData class]]) {
			userData = (NSData *)returnObject;
		}
	}
	
	return userData;
}

+ (NSString *)getStringWithKey:(NSString *)stringKey{
	NSString *userData = nil;
	
	if (stringKey != nil) {
		NSObject *returnObject = [[NSUserDefaults standardUserDefaults] objectForKey:stringKey];
		if ([returnObject isKindOfClass:[NSString class]]) {
			userData = (NSString *)returnObject;
		}
	}
	
	return userData;
}

+ (NSNumber *)getNumberWithKey:(NSString *)numberKey{
	NSNumber *userData = nil;
	
	if (numberKey != nil) {
		NSObject *returnObject = [[NSUserDefaults standardUserDefaults] objectForKey:numberKey];
		if ([returnObject isKindOfClass:[NSNumber class]]) {
			userData = (NSNumber *)returnObject;
		}
	}
	
	return userData;
}

+ (NSDate *)getDateWithKey:(NSString *)dateKey{
	NSDate *userData = nil;
	
	if (dateKey != nil) {
		NSObject *returnObject = [[NSUserDefaults standardUserDefaults] objectForKey:dateKey];
		if ([returnObject isKindOfClass:[NSDate class]]) {
			userData = (NSDate *)returnObject;
		}
	}
	
	return userData;
}

+ (NSArray *)getArrayWithKey:(NSString *)arrayKey{
	NSArray *userData = nil;
	
	if (arrayKey != nil) {
		NSObject *returnObject = [[NSUserDefaults standardUserDefaults] objectForKey:arrayKey];
		if ([returnObject isKindOfClass:[NSArray class]]) {
			userData = (NSArray *)returnObject;
		}
	}
	
	return userData;
}

+ (NSDictionary *)getDictionaryWithKey:(NSString *)dictionaryKey{
	NSDictionary *userData = nil;
	
	if (dictionaryKey != nil) {
		NSObject *returnObject = [[NSUserDefaults standardUserDefaults] objectForKey:dictionaryKey];
		if ([returnObject isKindOfClass:[NSDictionary class]]) {
			userData = (NSDictionary *)returnObject;
		}
	}
	
	return userData;
}

#pragma -
#pragma Check for Object

+ (BOOL)checkForKey:(NSString *)checkKey{
	BOOL checkResult = YES;
	
	NSObject *returnObject = [[NSUserDefaults standardUserDefaults] objectForKey:checkKey];
	if (returnObject == nil) {
		checkResult = NO;
	}
	
	return checkResult;
}

#pragma -
#pragma Insert Object in existing object

+ (void)insertObject:(id)objectValue inArrayWithKey:(NSString *)arrayKey{
	if (arrayKey != nil) {
		NSObject *returnObject = [[NSUserDefaults standardUserDefaults] objectForKey:arrayKey];
		if (returnObject == nil) {
			[[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObject:objectValue] forKey:arrayKey];
		} else if ([returnObject isKindOfClass:[NSArray class]]) {
			NSArray *oldArray = (NSArray *)returnObject;
			[[NSUserDefaults standardUserDefaults] setObject:[oldArray arrayByAddingObject:objectValue] forKey:arrayKey];
		}
	}
}

+ (void)insertObject:(id)objectValue withKey:(NSString *)objectKey inDictionaryWithKey:(NSString *)dictionaryKey{
	if ((objectKey != nil) && (dictionaryKey != nil)) {
		NSObject *returnObject = [[NSUserDefaults standardUserDefaults] objectForKey:dictionaryKey];
		if (returnObject == nil) {
			[[NSUserDefaults standardUserDefaults] setObject:[NSDictionary dictionaryWithObject:objectValue forKey:objectKey] forKey:dictionaryKey];
		} else if ([returnObject isKindOfClass:[NSDictionary class]]) {
			NSDictionary *oldDictionary = (NSDictionary *)returnObject;
			NSMutableDictionary *newDictionary = [NSMutableDictionary dictionaryWithDictionary:oldDictionary];
			[newDictionary setObject:objectValue forKey:objectKey];
			[[NSUserDefaults standardUserDefaults] setObject:[NSDictionary dictionaryWithDictionary:newDictionary] forKey:dictionaryKey];
		}
	}
}

@end
