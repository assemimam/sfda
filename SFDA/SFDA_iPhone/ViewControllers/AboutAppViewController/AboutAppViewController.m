//
//  AboutAppViewController.m
//  SFDA
//
//  Created by AYASHI on 11/13/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "AboutAppViewController.h"
#import "NavigationBar.h"
#import "AppDelegate.h"
#import "Global.h"

@interface AboutAppViewController ()
@property (weak, nonatomic) IBOutlet UITextView *aboutTextView;

@end

@implementation AboutAppViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
       // Do any additional setup after loading the view from its nib.
     self.aboutTextView.font =[UIFont fontWithName:@"GESSTwoLight-Light" size:17];
    VersionLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:12];
    SupportLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:12];
    EmailButton.titleLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:12];
}

-(void)viewDidAppear:(BOOL)animated{
    
//    AppDelegate*   del =(AppDelegate*) [[UIApplication sharedApplication]delegate];
    NavigationBar*navigationBarView =  (NavigationBar*) [del.window viewWithTag:1000000];
//    NavigationBar * navigationBarView = (NavigationBar *) del.window.rootViewController.navigationItem.titleView;
    [navigationBarView.btnRefresh setHidden:YES];
    [navigationBarView.btnBack setHidden:NO];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateViews];
}

- (void)updateViews
{
    CGRect frame = [self navigationFrame];
    
    if(!IS_DEVICE_RUNNING_IOS_7_AND_ABOVE)
        frame.origin.y = 0.0;
    

    [_aboutTextView setFrame:CGRectMake(_aboutTextView.frame.origin.x, frame.origin.y + frame.size.height, _aboutTextView.frame.size.width, _aboutTextView.frame.size.height)];
}

- (IBAction)openMail:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setSubject:@""];
        UIImage *myImage = [UIImage imageNamed:@"logo.png"];
        NSData *imageData = UIImagePNGRepresentation(myImage);
        [controller addAttachmentData:imageData mimeType:@"image/png" fileName:@"logo"];
        [controller setToRecipients:[NSArray arrayWithObjects:@"smart-app@sfda.gov.sa",nil]];
        [controller setMessageBody:@"" isHTML:NO];
        [[del.window viewWithTag:1000000] setHidden:TRUE];
        if (controller) [self presentModalViewController:controller animated:YES];
        
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"ادخل بريدك الالكترونى"
                                                       delegate:nil
                                              cancelButtonTitle:@"موافق"
                                              otherButtonTitles: nil];
        [alert show];
       
    }
    
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    
    UIAlertView * alert;
	switch (result)
	{
		case MFMailComposeResultCancelled:
            
			NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued");
			break;
		case MFMailComposeResultSaved:
			NSLog(@"Mail saved: you saved the email message in the Drafts folder");
			break;
		case MFMailComposeResultSent:
        {
            alert = [[UIAlertView alloc]initWithTitle:@"الدعم الفني" message:@"لقد تم ارسال الايمل بنجاح " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
			break;
		case MFMailComposeResultFailed:
        {
            alert = [[UIAlertView alloc]initWithTitle:@"الدعم الفني" message:@"لم يتم ارسال الايميل حاول مره اخري " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
			break;
		default:
			NSLog(@"Mail not sent");
			break;
	}
    
	[self dismissModalViewControllerAnimated:YES];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
