//
//  AboutAppViewController.h
//  SFDA
//
//  Created by AYASHI on 11/13/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "MasterViewController.h"
@interface AboutAppViewController : MasterViewController<MFMailComposeViewControllerDelegate>
{
    
    __weak IBOutlet UIButton *EmailButton;
    __weak IBOutlet UILabel *SupportLabel;
    __weak IBOutlet UILabel *VersionLabel;
}
- (IBAction)openMail:(id)sender;
@end
