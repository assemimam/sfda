//
//  PDFViewerViewController.m
//  SFDA
//
//  Created by Assem Imam on 10/9/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "PDFViewerViewController.h"
#import "NavigationBar.h"
#import "Global.h"

@interface PDFViewerViewController ()
{
    AppDelegate*del;
    id _selectedPdf;
}
@end

@implementation PDFViewerViewController
@synthesize SelectedPDF,sectorName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    HIDE_LOADING
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    del = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NavigationBar*navBar2 =  (NavigationBar*) [del.window viewWithTag:1000000];
    if (navBar2) {
        [navBar2.btnBack setHidden:NO];
        [del.window bringSubviewToFront:navBar2];
        
        
        Global* global = [Global getInstance];
        [navBar2.btnMenu setImage:[global currentSectorMenuBtn] forState:UIControlStateNormal];
    }


    
    // load pdf again
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *Documentpath = [paths objectAtIndex:0];
    NSString *TargetPath = [Documentpath stringByAppendingPathComponent:@"AwarnessPDF"];
    TargetPath = [Documentpath stringByAppendingPathComponent:self.sectorName];
    
    NSString *fileName= [TargetPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.pdf",[_selectedPdf objectForKey:@"$id"]] ];
    
    
    //this will set the image when loading is finished
    dispatch_async(dispatch_get_main_queue(), ^{
        //save image file
        
            NSURL *url = [[NSURL alloc]initFileURLWithPath:fileName isDirectory:NO];
            [wvPdfViewer loadRequest:[NSURLRequest requestWithURL:url]];
        
        
    });
    [self updateViews];
}

- (void)updateViews
{
    CGRect frame = [self navigationFrame];
    
    if(!IS_DEVICE_RUNNING_IOS_7_AND_ABOVE)
        frame.origin.y = 0.0;
    
    [wvPdfViewer setFrame:CGRectMake(wvPdfViewer.frame.origin.x, frame.origin.y + frame.size.height, wvPdfViewer.frame.size.width, wvPdfViewer.frame.size.height - frame.origin.y)];
}

- (void)viewDidLoad
{
    SHOW_LOADING
    _selectedPdf = self.SelectedPDF;
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *Documentpath = [paths objectAtIndex:0];
    NSString *TargetPath = [Documentpath stringByAppendingPathComponent:@"AwarnessPDF"];
     TargetPath = [Documentpath stringByAppendingPathComponent:self.sectorName];
   TargetPath= [TargetPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.pdf",[self.SelectedPDF objectForKey:@"$id"]]];
    NSFileManager *Fmanager = [NSFileManager defaultManager];
    
    if ([Fmanager fileExistsAtPath:TargetPath isDirectory:NO]) {
        
        NSURL *url = [[NSURL alloc]initFileURLWithPath:TargetPath isDirectory:NO];
        [wvPdfViewer loadRequest:[NSURLRequest requestWithURL:url]];
    
        return;
    }
    
    
    dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    //this will start the image loading in bg
    dispatch_async(concurrentQueue, ^{
         NSString *url = [[[self.SelectedPDF objectForKey:@"PDFUrl"] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding ]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *pdfUrl = [NSURL URLWithString:url];
        NSURLRequest* imgRequest = [NSURLRequest requestWithURL:pdfUrl];
        NSData *pdfData = [[NSData alloc]initWithData:[NSURLConnection sendSynchronousRequest:imgRequest returningResponse:nil error:nil]];
        imgRequest=nil;
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *Documentpath = [paths objectAtIndex:0];
        NSString *TargetPath = [Documentpath stringByAppendingPathComponent:@"AwarnessPDF"];
         TargetPath = [Documentpath stringByAppendingPathComponent:self.sectorName];
        NSFileManager *Fmanager = [NSFileManager defaultManager];
        
        if ([Fmanager fileExistsAtPath:TargetPath isDirectory:nil]) {

       

        }
        else
        {
            BOOL ret =[Fmanager createDirectoryAtPath:TargetPath withIntermediateDirectories:YES attributes:nil error:nil];
            if (ret) {
                
            };
        }
        
        NSString *fileName= [TargetPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.pdf",[_selectedPdf objectForKey:@"$id"]] ];
        
        
        //this will set the image when loading is finished
        dispatch_async(dispatch_get_main_queue(), ^{
            //save image file
            if (pdfData!=nil &&pdfData.length > 0 ) {
                
                [pdfData writeToFile:fileName atomically:YES];
                NSURL *url = [[NSURL alloc]initFileURLWithPath:fileName isDirectory:NO];
                [wvPdfViewer loadRequest:[NSURLRequest requestWithURL:url]];
            }
            
            
        });
    });

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    wvPdfViewer = nil;
    [super viewDidUnload];
}
@end
