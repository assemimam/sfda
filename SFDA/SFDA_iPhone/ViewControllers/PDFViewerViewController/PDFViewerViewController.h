//
//  PDFViewerViewController.h
//  SFDA
//
//  Created by Assem Imam on 10/9/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#import "MasterViewController.h"

@interface PDFViewerViewController : MasterViewController<UIWebViewDelegate>
{
    
    __weak IBOutlet UIWebView *wvPdfViewer;
}
@property(nonatomic)id SelectedPDF;
@property(nonatomic,retain)NSString* sectorName;
@end
