//
//  MemberViewController.h
//  SFDA
//
//  Created by Assem Imam on 11/4/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"
@interface MemberViewController : MasterViewController<UITableViewDataSource>
{
    
    __weak IBOutlet UITableView *MembersTableView;
    __weak IBOutlet UIImageView *imgTitle;
}
@end
