//
//  AboutViewController.m
//  SFDA
//
//  Created by yehia on 9/26/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "AboutViewController.h"
#import "ASIHTTPRequest.h"
#import "QuestionsCustomCell.h"
#import "Language.h"
#import "JSON.h"
#import "JSONKit.h"
#import "Global.h"
#import "FMDatabase.h"
#import "AppDelegate.h"
#import "NavigationBar.h"
#import "MemberViewController.h"

@interface AboutViewController ()
{
    NSMutableArray *optionsList;
    ASIHTTPRequest *req;
    Global *global;
    NSString *currentSectionName;
    NSString *currentSectionCachingKey;
    AppDelegate*del;
    float cellHeight;
    BOOL IS_ABOUT;
    NSString * paramter1; ///to  save paramter to check it in numberOfRowAtIndexPath 
}

@end

@implementation AboutViewController
@synthesize tvAbout;
@synthesize parameter;
@synthesize sectorTileImgView;

#pragma  mark tableview datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id cell;
    if(IS_ABOUT){
        if (indexPath.row== [optionsList count] -1 ) {
            cell =(UITableViewCell*) [tableView dequeueReusableCellWithIdentifier:@"cleOption2"];
            if (cell==nil) {
                cell =[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cleOption2"];
            }
            
            UIButton *btn =[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 320, 65)];
            [btn setImage:[UIImage imageNamed:@"boardBtn.png"] forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(gotoMembersList) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:btn];
            
            
        }
        else
        {
             cell =(QuestionsCustomCell*) [tableView dequeueReusableCellWithIdentifier:@"cleOption"];
            if (cell==nil) {
                cell =(QuestionsCustomCell*) [[[NSBundle mainBundle]loadNibNamed:[[Language getObject]getStringWithKey:@"QuestionsCustomCell"] owner:nil options:nil]objectAtIndex:0];
            }
            
            NSString *title,*descr;
            
            if ([[Language getObject]getLanguage]==languageArabic) {
                title = [[optionsList objectAtIndex:indexPath.row] objectForKey:@"TitleAr"];
                descr =[[optionsList objectAtIndex:indexPath.row] objectForKey:@"DesAr"];
            }else{
                title = [[optionsList objectAtIndex:indexPath.row] objectForKey:@"TitleEn"];
                descr =[[optionsList objectAtIndex:indexPath.row] objectForKey:@"DesEn"];
            }
            
            [((QuestionsCustomCell*)cell).questionTextView setDelegate:self];
            [((QuestionsCustomCell*)cell).answerTextView setDelegate:self];
            
            ((QuestionsCustomCell*)cell).questionTextView.text = title;
            ((QuestionsCustomCell*)cell).answerTextView.text = descr;

            if(!IS_DEVICE_RUNNING_IOS_7_AND_ABOVE) {
                [((QuestionsCustomCell*)cell).questionTextView sizeToFit];
                [((QuestionsCustomCell*)cell).answerTextView sizeToFit];
            }
            
            cellHeight  =((QuestionsCustomCell*)cell).questionTextView.frame.size.height + ((QuestionsCustomCell*)cell).answerTextView.frame.size.height;
                
        }
        
    }
else
{
    cell =(QuestionsCustomCell*) [tableView dequeueReusableCellWithIdentifier:@"cleOption"];
    if (cell==nil) {
        cell =(QuestionsCustomCell*) [[[NSBundle mainBundle]loadNibNamed:[[Language getObject]getStringWithKey:@"QuestionsCustomCell"] owner:nil options:nil]objectAtIndex:0];
    }
    
    NSString *title,*descr;
    
    if ([[Language getObject]getLanguage]==languageArabic) {
        title = [[optionsList objectAtIndex:indexPath.row] objectForKey:@"TitleAr"];
        descr =[[optionsList objectAtIndex:indexPath.row] objectForKey:@"DesAr"];
    }else{
        title = [[optionsList objectAtIndex:indexPath.row] objectForKey:@"TitleEn"];
        descr =[[optionsList objectAtIndex:indexPath.row] objectForKey:@"DesEn"];
    }
    
    [((QuestionsCustomCell*)cell).questionTextView setDelegate:self];
    [((QuestionsCustomCell*)cell).answerTextView setDelegate:self];
    
    ((QuestionsCustomCell*)cell).questionTextView.text = title;
    ((QuestionsCustomCell*)cell).answerTextView.text = descr;
    
    
    if(!IS_DEVICE_RUNNING_IOS_7_AND_ABOVE) {
        [((QuestionsCustomCell*)cell).questionTextView sizeToFit];
        [((QuestionsCustomCell*)cell).answerTextView sizeToFit];
    }
    
    cellHeight  =((QuestionsCustomCell*)cell).questionTextView.frame.size.height + ((QuestionsCustomCell*)cell).answerTextView.frame.size.height;
    }


    ((QuestionsCustomCell*)cell).questionTextView.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
    ((QuestionsCustomCell*)cell).answerTextView.font=[UIFont fontWithName:@"GESSTwoLight-Light" size:15];
    return cell;

}

-(void)gotoMembersList
{
    MemberViewController *vc_member = [[MemberViewController alloc]init];
    [self.navigationController pushViewController:vc_member animated:YES];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if ([paramter1 isEqualToString:@"SFDA"]) {
        
        return [optionsList count]-1; // -1 to remove last cell "SFDA board member"
    }
    return [optionsList count] ; 
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger height=0;
    NSString *title,*descr;
    if(IS_ABOUT){
        if (indexPath.row== [optionsList count] -1)
        {
            height=60;
        }
        else
        {
            if ([[Language getObject]getLanguage]==languageArabic) {
                title = [[optionsList objectAtIndex:indexPath.row] objectForKey:@"TitleAr"];
                descr =[[optionsList objectAtIndex:indexPath.row] objectForKey:@"DesAr"];
            }else{
                title = [[optionsList objectAtIndex:indexPath.row] objectForKey:@"TitleEn"];
                descr =[[optionsList objectAtIndex:indexPath.row] objectForKey:@"DesEn"];
            }
            
            
            CGSize questSize = [title sizeWithFont:[UIFont boldSystemFontOfSize:17.0f] constrainedToSize:CGSizeMake(self.tvAbout.frame.size.width , 20.0f)];
            CGSize answSize = [descr sizeWithFont:[UIFont systemFontOfSize:17.0f] constrainedToSize:CGSizeMake(self.tvAbout.frame.size.width , 20000000.0f)];
            
            height = questSize.height+answSize.height + 35;
            
            NSLog(@" heigt for index path row %d  = %d",indexPath.row,height);
        }
       
        
    }
    else
    {
        if ([[Language getObject]getLanguage]==languageArabic) {
            title = [[optionsList objectAtIndex:indexPath.row] objectForKey:@"TitleAr"];
            descr =[[optionsList objectAtIndex:indexPath.row] objectForKey:@"DesAr"];
        }else{
            title = [[optionsList objectAtIndex:indexPath.row] objectForKey:@"TitleEn"];
            descr =[[optionsList objectAtIndex:indexPath.row] objectForKey:@"DesEn"];
        }
        
        
        CGSize questSize = [title sizeWithFont:[UIFont boldSystemFontOfSize:17.0f] constrainedToSize:CGSizeMake(self.tvAbout.frame.size.width , 20.0f)];
        CGSize answSize = [descr sizeWithFont:[UIFont systemFontOfSize:17.0f] constrainedToSize:CGSizeMake(self.tvAbout.frame.size.width , 20000000.0f)];
        
        height = questSize.height+answSize.height + 35;
        
        NSLog(@" heigt for index path row %d  = %d",indexPath.row,height);
    }
  

    return height;
    
    
}

#pragma  mark tableview delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}


-(void)getData
{
    [((AppDelegate*)[[UIApplication sharedApplication]delegate]) ShowLoading:YES];
    if([parameter isEqualToString:@"SFDA"]){
        IS_ABOUT=YES;
        currentSectionCachingKey = ABOUT_SECTION_SFDA_TAG;
    }else if([parameter isEqualToString:@"Food"]){
        currentSectionCachingKey = ABOUT_SECTION_FOOD_TAG;
    }else if([parameter isEqualToString:@"Medical"]){
        currentSectionCachingKey = ABOUT_SECTION_MEDI_TAG;
    }else if([parameter isEqualToString:@"Drug"]){
        currentSectionCachingKey = ABOUT_SECTION_DRUG_TAG;
    }
    
    currentSectionName = parameter;
    parameter =[@"?sectionName=" stringByAppendingString:currentSectionName];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:currentSectionCachingKey]){
        
        [self aboutGetAllData];
        
        NSLog(@"%d",[optionsList count]);
        
        [tvAbout reloadData];
        HIDE_LOADING
    }else{
        @try {
            
          
            
            SHOW_LOADING
            NSString *url = [[NSString stringWithFormat:@"%@AdditionalInfo", SERVER_IP] stringByAppendingString:parameter];
            req =[[ASIHTTPRequest alloc]initWithURL:[NSURL URLWithString:url]];
            [req setCompletionBlock:^{
                
                
                
                NSString *response=  [req responseString];
                
                NSLog(@"res===>: %@",response);
                
                id result = [response JSONValue];
                optionsList=result;
                
                if(optionsList != nil && [optionsList count]>0){
                    
                    [self deleteData];
                    NSLog(@" currentSectionCachingKey %@",currentSectionCachingKey);
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:currentSectionCachingKey];
                    
                }
                
                for(NSDictionary *obj in optionsList)
                    
                {
                    if([[Language getObject] getLanguage] == languageArabic){
                        
                        [self insertAboutData:[obj objectForKey:@"TitleAr"] :[obj objectForKey:@"DesAr"]];
                        
                    }else{
                        
                        [self insertAboutData:[obj objectForKey:@"TitleEn"] :[obj objectForKey:@"DesEn"] ];
                        
                    }
                    
                }
                if(IS_ABOUT){
                    [optionsList addObject:@"1"];
                }
                HIDE_LOADING
                [ tvAbout reloadData];
                
                
                
            }];
            [req setFailedBlock:^{
                
                HIDE_LOADING
                [self aboutGetAllData];
                
                if(optionsList != nil && [optionsList count]>0){
                    NSLog(@"%d",[optionsList count]);
                    
                    [tvAbout reloadData];
                }else{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"يرجى التحقق من اتصالك مع الإنترنت" delegate:self cancelButtonTitle:@"موافق" otherButtonTitles:nil];
                    [alert show];
                    NSLog(@"%@",[req error]);
                }
            }];
            [req startAsynchronous];
        }
        @catch (NSException *exception) {
            
        };
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    del =(AppDelegate*) [[UIApplication sharedApplication]delegate];
    
    NavigationBar*navigationBarView =  (NavigationBar*) [del.window viewWithTag:1000000];
//    NavigationBar * navigationBarView = (NavigationBar *) del.window.rootViewController.navigationItem.titleView;
    if (navigationBarView) {
        [navigationBarView.btnBack setHidden:NO];
        global = [Global getInstance];
        [global setCurrentSectorMenuBtn:[UIImage imageNamed:@"menu_button.png"]];
        [global setCurrentSectorImage:nil];
        [navigationBarView.btnMenu setImage:[UIImage imageNamed:@"menu_button.png"] forState:UIControlStateNormal];
        [del.window bringSubviewToFront:navigationBarView];
    }

  
    [self updateViews];
}

- (void)updateViews
{
    CGRect frame = [self navigationFrame];
    
    if(!IS_DEVICE_RUNNING_IOS_7_AND_ABOVE)
        frame.origin.y = 0.0;
    
    frame.origin.y += frame.size.height;
    
    [tvAbout setFrame:CGRectMake(tvAbout.frame.origin.x, frame.origin.y, tvAbout.frame.size.width, tvAbout.frame.size.height)];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    paramter1= [NSString stringWithString:parameter];
    // Do any additional setup after loading the view from its nib.
    IS_ABOUT =NO;
    [self performSelector:@selector(getData) withObject:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTvAbout:nil];
    [self setParameter:nil];
    [super viewDidUnload];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    if(![req isCancelled]) {
        // Cancels an asynchronous request
        [req cancel];
        // Cancels an asynchronous request, clearing all delegates and blocks first
        [req clearDelegatesAndCancel];
    }
    
    [super viewWillDisappear:animated];
}

#pragma  mark DataBase functions
- (void)insertAboutData:(NSString*)title :(NSString*)content{
    
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    NSString* lang = @"en";
    if([[Language getObject] getLanguage] == languageArabic)
        lang = @"ar";
    
    
    NSString *insertQuery = [@"insert into About " stringByAppendingFormat:@" VALUES ('%@', '%@','%@','%@')", title,content,currentSectionName,lang];
    BOOL insterted = [database executeUpdate:insertQuery];
    
    NSLog(insterted ? @"Yes" : @"No");
    
    [database close];
}

-(void) aboutGetAllData{
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    NSString *sqlSelectQuery = [@"SELECT * FROM About where lang = 'en' and sectionName='" stringByAppendingString:[currentSectionName stringByAppendingString:@"'"]];
    if([[Language getObject] getLanguage] == languageArabic)
        sqlSelectQuery = [@"SELECT * FROM About where lang = 'ar' and sectionName='" stringByAppendingString:[currentSectionName stringByAppendingString:@"'"]];
    
    
    optionsList = [[NSMutableArray alloc] init];
    
    // Query result
    FMResultSet *resultsWithNameLocation = [database executeQuery:sqlSelectQuery];
    while([resultsWithNameLocation next]) {
        NSString *about_title = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"title"]];
        NSString *about_content = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"descr"]];
        
        NSMutableDictionary *aboutDict = [[NSMutableDictionary alloc]init];
        if([[Language getObject] getLanguage] == languageArabic){
            [aboutDict setObject:about_title forKey:@"TitleAr"];
            [aboutDict setObject:about_content forKey:@"DesAr"];
        }else{
            [aboutDict setObject:about_title forKey:@"TitleEn"];
            [aboutDict setObject:about_content forKey:@"DesEn"];
        }
        
        [optionsList addObject:aboutDict ];
        
        // loading your data into the array, dictionaries.
    }
     if(IS_ABOUT){
    [optionsList addObject:@"1"];
     }
    [database close];
}

- (void)deleteData {
    
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    NSString *deleteQuery = [@"DELETE FROM About where lang = 'en' and sectionName='" stringByAppendingString:[currentSectionName stringByAppendingString:@"'"]];
    if([[Language getObject] getLanguage] == languageArabic)
        deleteQuery = [@"DELETE FROM About where lang = 'ar' and sectionName='" stringByAppendingString:[currentSectionName stringByAppendingString:@"'"]];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    BOOL deleted = [database executeUpdate:deleteQuery];
    NSLog(deleted ? @"Yes" : @"No");
    [database close];
}

@end
