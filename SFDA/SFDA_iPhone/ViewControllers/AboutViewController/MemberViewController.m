//
//  MemberViewController.m
//  SFDA
//
//  Created by Assem Imam on 11/4/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "MemberViewController.h"
#import "memberCell.h"
#import "Language.h"
#import "AppDelegate.h"
#import "NavigationBar.h"
#import "Global.h"

@interface MemberViewController ()
{
    NSArray *membersList;
    AppDelegate *del;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation MemberViewController
-(void)HandleGetMembersCashedDataResult
{
    @try {
        HIDE_LOADING
        [MembersTableView reloadData];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(id)GetMembersCashedData
{
    @try {
        DB_Field *fldId = [[DB_Field alloc]init];
        fldId.FieldName=@"Id";
        fldId.FieldAlias=@"$id";
        
        DB_Field *fldName = [[DB_Field alloc]init];
        fldName.FieldName=@"Name";
        
        
        DB_Field *fldTitle = [[DB_Field alloc]init];
        fldTitle.FieldName=@"Title";
 
        DB_Field *fldImagePath = [[DB_Field alloc]init];
        fldImagePath.FieldName=@"ImagePath";
        
        
        DB_Field *fldOrder = [[DB_Field alloc]init];
        fldOrder.FieldName=@"Rank";
        [[Cashing getObject]setDatabase:@"SDFAipad"];
        [[Database getObject] setDatabase:@"SDFAipad"];

        id result =[[Cashing getObject]getDataFromCashWithSelectColumns:[NSMutableArray arrayWithObjects:fldId,fldName,fldTitle,fldImagePath,fldOrder, nil] Tables:[NSArray arrayWithObject:@"members"] Where:nil FromIndex:nil ToIndex:nil];
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Order" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        [result sortUsingDescriptors:sortDescriptors];
        
        return result;
        
    }
    @catch (NSException *exception) {
        
    }
}
-(void)HandleGetMembersList
{
    @try {
        [NSThread detachNewThreadSelector:@selector(CashMembersData:) toTarget:self withObject:membersList];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)CashMembersData:(id)data
{
    @try {
        NSMutableArray *cashedData = [[NSMutableArray alloc]init];
        for (id item in data) {
            DB_Recored *memberRecored =[[DB_Recored alloc]init];
            
            DB_Field *fldId = [[DB_Field alloc]init];
            fldId.FieldName=@"Id";
            fldId.FieldDataType= FIELD_DATA_TYPE_NUMBER;
            fldId.IS_PRIMARY_KEY=YES;
            fldId.FieldValue=[item objectForKey:@"$id"];
            
            DB_Field *fldName = [[DB_Field alloc]init];
            fldName.FieldName=@"Name";
            fldName.FieldDataType= FIELD_DATA_TYPE_TEXT;
            fldName.FieldValue=[item objectForKey:@"Name"];
            
            DB_Field *fldTitle = [[DB_Field alloc]init];
            fldTitle.FieldName=@"Title";
            fldTitle.FieldDataType= FIELD_DATA_TYPE_TEXT;
            fldTitle.FieldValue=[NSNull null]== [item objectForKey:@"Title"]?@" ":[item objectForKey:@"Title"];
            
            DB_Field *fldURL = [[DB_Field alloc]init];
            fldURL.FieldName=@"URL";
            fldURL.FieldDataType= FIELD_DATA_TYPE_TEXT;
            fldURL.FieldValue=[item objectForKey:@"URL"];
            
            DB_Field *fldImagePath = [[DB_Field alloc]init];
            fldImagePath.FieldName=@"ImagePath";
            fldImagePath.FieldDataType= FIELD_DATA_TYPE_IMAGE;
            fldImagePath.FieldValue=[item objectForKey:@"URL"];
            
            DB_Field *fldOrder = [[DB_Field alloc]init];
            fldOrder.FieldName=@"Rank";
            fldOrder.FieldDataType= FIELD_DATA_TYPE_TEXT;
            fldOrder.FieldValue=[NSString stringWithFormat:@"%@" ,[item objectForKey:@"Order"]];
            
            memberRecored.Fields = [NSMutableArray arrayWithObjects:fldId,fldName,fldTitle,fldURL,fldImagePath,fldOrder, nil];
            [cashedData  addObject:memberRecored];
        }
        [[Cashing getObject]setDatabase:@"SDFAipad"];
        [[Database getObject] setDatabase:@"SDFAipad"];
        [[Database getObject] DeleteFromTable:@"members" Where:nil];
        
        BOOL Success = [[Cashing getObject] CashData:cashedData inTable:@"members"];
        if (Success) {
            [[NSUserDefaults standardUserDefaults]setObject:[NSDate date] forKey:MEMBERS_CASH_KEY];
            [[NSUserDefaults standardUserDefaults]synchronize];
            membersList =  [self GetMembersCashedData];
            [self performSelectorOnMainThread:@selector(HandleGetMembersCashedDataResult) withObject:nil waitUntilDone:NO];
        }
        
        
        
    }
    @catch (NSException *exception) {
        
    }
}
-(void)GetMembersList
{
    @try {
        if ([[NSUserDefaults standardUserDefaults]objectForKey:MEMBERS_CASH_KEY]) {
            int  munites = ceil([[NSDate date] timeIntervalSinceDate:[[NSUserDefaults standardUserDefaults]objectForKey:MEMBERS_CASH_KEY]]/60);
            if (munites>180) {
                membersList = [Webservice GetMembersBoard];
                if ([membersList count]>0) {
                    
                    [self performSelectorOnMainThread:@selector(HandleGetMembersList) withObject:nil waitUntilDone:NO];
                }
                else{
                    membersList = [self GetMembersCashedData];
                    [self performSelectorOnMainThread:@selector(HandleGetMembersCashedDataResult) withObject:nil waitUntilDone:NO];
                }
            }
            else{
                membersList = [self GetMembersCashedData];
                [self performSelectorOnMainThread:@selector(HandleGetMembersCashedDataResult) withObject:nil waitUntilDone:NO];
            }
        }
        else{
            membersList = [Webservice GetMembersBoard];
            if ([membersList count]>0) {
                [self performSelectorOnMainThread:@selector(HandleGetMembersList) withObject:nil waitUntilDone:NO];
            }
            else{
                membersList = [self GetMembersCashedData];
                [self performSelectorOnMainThread:@selector(HandleGetMembersCashedDataResult) withObject:nil waitUntilDone:NO];
            }
        }
        
    }
    @catch (NSException *exception) {
        
    }
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    SHOW_LOADING;
    [NSThread detachNewThreadSelector:@selector(GetMembersList) toTarget:self withObject:nil];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    del =(AppDelegate*) [[UIApplication sharedApplication]delegate];
    
    NavigationBar*navBar2 =  (NavigationBar*) [del.window viewWithTag:1000000];
    if (navBar2) {
        [navBar2.btnBack setHidden:NO];
        Global*  global = [Global getInstance];
        [global setCurrentSectorMenuBtn:[UIImage imageNamed:@"menu_button.png"]];
        [global setCurrentSectorImage:nil];
        [navBar2.btnMenu setImage:[UIImage imageNamed:@"menu_button.png"] forState:UIControlStateNormal];
        [del.window bringSubviewToFront:navBar2];
    }

    [self updateViews];
    
}

- (void)updateViews
{
    CGRect frame = [self navigationFrame];
    
    if(!IS_DEVICE_RUNNING_IOS_7_AND_ABOVE)
        frame.origin.y = 0.0;
    

    [imgTitle setFrame:CGRectMake(imgTitle.frame.origin.x, frame.origin.y + frame.size.height, imgTitle.frame.size.width, imgTitle.frame.size.height)];
    
    CGRect tableRect = _tableView.frame;
    tableRect.origin.y = imgTitle.frame.origin.y + imgTitle.frame.size.height;
    _tableView.frame = tableRect;
}

- (void)viewDidUnload {
    imgTitle = nil;
    [super viewDidUnload];
}
#pragma -mark uitableview datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [membersList count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    memberCell*cell =(memberCell*)[tableView dequeueReusableCellWithIdentifier:@"cleMember"];
    if (cell==nil) {
        cell = (memberCell*)[[[NSBundle mainBundle]loadNibNamed:[[Language getObject]getStringWithKey:@"memberCell"]owner:nil options:nil]objectAtIndex:0];
    }
    cell.MemberName.text = [[membersList objectAtIndex:indexPath.row] objectForKey:@"Name"];
     cell.MemberName.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
    cell.MemberPosition.text = [[membersList objectAtIndex:indexPath.row] objectForKey:@"Title"];
    cell.MemberPosition.font=[UIFont fontWithName:@"GESSTwoLight-Light" size:13];
    if ([[membersList objectAtIndex:indexPath.row] objectForKey:@"URL"]) {
        NSURL*NewsImageUrl =[NSURL URLWithString: [[membersList objectAtIndex:indexPath.row] objectForKey:@"URL"]];
        [cell.LoadingActivityIndicator startAnimating];
        if (NewsImageUrl) {
            [[ImageCache sharedInstance]downloadImageAtURL:NewsImageUrl completionHandler:^(UIImage *image) {
                [cell.LoadingActivityIndicator stopAnimating];
                if (image) {
                    cell.MemberImage.image = image;
                }
                else{
                    cell.MemberImage.image = [UIImage imageNamed:@"maindefaultimgrows~ipad.png"];
                }
            }];
        }
    }
    else//from cash
    {
        @autoreleasepool {
            [cell.LoadingActivityIndicator startAnimating];
            [[ImageCache sharedInstance] downloadImageAtURL:[NSURL fileURLWithPath:[[membersList objectAtIndex:indexPath.row] objectForKey:@"ImagePath"]] completionHandler:^(UIImage *image) {
                if (image) {
                    cell.MemberImage.image= image;
                }
                else{
                    cell.MemberImage.image= [UIImage imageNamed:@"maindefaultimgrows~ipad.png"];
                }
                [cell.LoadingActivityIndicator stopAnimating];
            }];
        }
    }
  
    return  cell;
}

@end
