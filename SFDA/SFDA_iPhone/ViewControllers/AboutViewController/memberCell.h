//
//  memberCell.h
//  SFDA
//
//  Created by Assem Imam on 11/4/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface memberCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *MemberName;
@property (weak, nonatomic) IBOutlet UILabel *MemberPosition;
@property (weak, nonatomic) IBOutlet UIImageView *MemberImage;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *LoadingActivityIndicator;

@end
