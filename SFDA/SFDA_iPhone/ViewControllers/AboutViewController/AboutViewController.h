//
//  AboutViewController.h
//  SFDA
//
//  Created by yehia on 9/26/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"

@interface AboutViewController : MasterViewController<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tvAbout;
@property (weak, nonatomic) NSString *parameter;
@property (weak, nonatomic) IBOutlet UIImageView *sectorTileImgView;

@end
