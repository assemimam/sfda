//
//  MasterViewController.m
//  SFDA
//
//  Created by Assem Imam on 9/3/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "MasterViewController.h"
#import "SectorViewController.h"
#import "NewsViewController.h"
#import "Language.h"
#import "NewsDetailsViewController.h"
#import "EServiceViewController.h"
#import "Global.h"
#import "MainScreenViewController.h"
#import "MainScreenNewViewController.h"
#import "AboutAppViewController.h"


@interface MasterViewController ()
{
    
}
@end

@implementation MasterViewController

#pragma mark popup delegate
-(void)didSelectItemAtIndex:(int)index{
    
    Global *global = [Global getInstance];
    switch (index) {
        case SFDA_TAG:
        {
            [global setCurrentSectorImage:nil ];
            [global setCurrentSectorMenuBtn:[UIImage imageNamed:@"home button main screen"]];
            [global setCurrentSectorColor:nil];
            [global setCurrentSectorSubImage:nil];
            
            if([[self.navigationController topViewController] class] != [MainScreenViewController class]){
                MainScreenViewController *old_main = [[MainScreenViewController alloc] init];
                [self.navigationController pushViewController:old_main animated:YES];
            }
        }
            break;
        case NEWS_TAG:
        {
            [global setCurrentSectorImage:[UIImage imageNamed:@"news tile .png" ]];
            [global setCurrentSectorMenuBtn:[UIImage imageNamed:@"home button news.png"]];
            [global setCurrentSectorColor:[global colorFromHexString:@"#5b4f6b"]];
            [global setCurrentSectorSubImage:nil];
            
            if([[self.navigationController topViewController] class] != [NewsViewController class]){
                NSString *nibName = [[Language getObject] getStringWithKey:@"NewsViewController"];
                
                NewsViewController *contact = [[NewsViewController alloc]initWithNibName:nibName bundle:nil];
                [self.navigationController pushViewController:contact animated:YES];
            }
        }
            break;
        case FOODS_SECTOR_TAG:
        {
            [global setCurrentSectorImage:[UIImage imageNamed:@"food green tile.png" ]];
            [global setCurrentSectorMenuBtn:[UIImage imageNamed:@"home button good.png"]];
            [global setCurrentSectorColor:[global colorFromHexString:@"#3b7350"]];
            [global setCurrentSectorSubImage:[UIImage imageNamed:@"food sub green tile.png" ]];
            
            SectorViewController* s_sector = (SectorViewController*)[self.navigationController topViewController];
            
            if([[self.navigationController topViewController] class] != [SectorViewController class] || s_sector.Sector != FOODS_SECTOR){
                SectorViewController*VC_Sector = [[SectorViewController alloc]init];
                VC_Sector.Sector=FOODS_SECTOR;
                [self.navigationController pushViewController:VC_Sector animated:YES];
            }
        }
            break;
        case MEDICAL_SECTOR_TAG:
        {
            
            [global setCurrentSectorImage:[UIImage imageNamed:@"medical orrange tile.png" ]];
            [global setCurrentSectorMenuBtn:[UIImage imageNamed:@"home button medical.png"]];
            [global setCurrentSectorColor:[global colorFromHexString:@"#a66136"]];
            [global setCurrentSectorSubImage:[UIImage imageNamed:@"medical sub orrange tile.png" ]];
            
            SectorViewController* s_sector = (SectorViewController*)[self.navigationController topViewController];
            
            if([[self.navigationController topViewController] class] != [SectorViewController class] || s_sector.Sector != MEDICAL_SECTOR){
                SectorViewController*VC_Sector = [[SectorViewController alloc]init];
                VC_Sector.Sector=MEDICAL_SECTOR;
                [self.navigationController pushViewController:VC_Sector animated:YES];
            }
            
        }
            break;
        case E_SERVICES_TAG:
        {
            
            [global setCurrentSectorImage:[UIImage imageNamed:@"serviceSecTitle.png" ]];
            [global setCurrentSectorMenuBtn:[UIImage imageNamed:@"home button main service.png"]];
            [global setCurrentSectorColor:[global colorFromHexString:@"#483e3d"]];
            [global setCurrentSectorSubImage:nil];

            
                       
            if([[self.navigationController topViewController] class] != [EServiceViewController class]){
                
                NSString *nibName = [[Language getObject] getStringWithKey:@"EServiceViewController"];
                
                EServiceViewController *service = [[EServiceViewController alloc]initWithNibName:nibName bundle:nil];
                [self.navigationController pushViewController:service animated:YES];
                
            }
            
        }
            break;
        case SETTINGS_TAG:
        {
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:NEWS_ALL_MAIN_TAG];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:NEWS_SCROLL_MAIN_TAG];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:NEWS_SCROLL_SFDA_TAG];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:NEWS_SCROLL_DRUG_TAG];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:NEWS_SCROLL_FOOD_TAG];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:NEWS_SCROLL_MEDI_TAG];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:ABOUT_SECTION_SFDA_TAG];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:ABOUT_SECTION_FOOD_TAG];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:ABOUT_SECTION_DRUG_TAG];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:ABOUT_SECTION_MEDI_TAG];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:AWARENESS_SECTION_SFDA_TAG];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:AWARENESS_SECTION_FOOD_TAG];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:AWARENESS_SECTION_DRUG_TAG];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:AWARENESS_SECTION_MEDI_TAG];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:QUESTION_SECTION_FOOD_TAG];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:QUESTION_SECTION_DRUG_TAG];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:QUESTION_SECTION_MEDI_TAG];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:CONTACT_US_SECTION_TAG];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IMPORTANT_LINKS_SECTION_TAG];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:E_SERVICE_SECTION_TAG];
            
            
            MainScreenNewViewController *mainScreen =   (MainScreenNewViewController*)[self.navigationController topViewController];
            [mainScreen getNewsData];
            

        }
            break;
        case DRUGS_SECTOR_TAG:
        {
            [global setCurrentSectorImage:[UIImage imageNamed:@"drug blue tile.png" ]];
            [global setCurrentSectorMenuBtn:[UIImage imageNamed:@"hoe button drug.png" ]];
            [global setCurrentSectorColor:[global colorFromHexString:@"#0973ba"]];
            [global setCurrentSectorSubImage:[UIImage imageNamed:@"drug sub blue tile.png" ]];
            
            SectorViewController* s_sector = (SectorViewController*)[self.navigationController topViewController];
            
            if([[self.navigationController topViewController] class] != [SectorViewController class] || s_sector.Sector != DRUGS_SECTOR){
                SectorViewController*VC_Sector = [[SectorViewController alloc]init];
                VC_Sector.Sector=DRUGS_SECTOR;
                [self.navigationController pushViewController:VC_Sector animated:YES];
            }
        }
            break;
        case ABOUT_APP_TAG:
        {
            if([[self.navigationController topViewController] class] != [AboutAppViewController class]){
                
                NSString *nibName = [[Language getObject] getStringWithKey:@"AboutAppViewController"];
                
                AboutAppViewController *aboutApp = [[AboutAppViewController alloc]initWithNibName:nibName bundle:nil];
                [self.navigationController pushViewController:aboutApp animated:YES];
            }
        }
            break;
    }
    
    _navBar =  (NavigationBar*) [del.window viewWithTag:1000000];
    [_navBar.btnMenu setImage:[global currentSectorMenuBtn] forState:UIControlStateNormal];
    
    
    [[del.window viewWithTag:2000000] removeFromSuperview];
    SHOW_POPUP=NO;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    @try {
         [super viewWillAppear:animated];
        
        self.navigationController.navigationBarHidden = YES;
        del =(AppDelegate*) [[UIApplication sharedApplication]delegate];
        for (UIView *view in [del.window subviews]) {
            if (view.tag ==1000000 ) {
                [view removeFromSuperview];
            }
        }
        _navBar =  (NavigationBar*) [del.window viewWithTag:1000000];
        if (_navBar) {
            [_navBar removeFromSuperview];
        }
        Global *global = [Global getInstance];
        if([global currentSectorMenuBtn] == nil){
            [global setCurrentSectorMenuBtn:[UIImage imageNamed:@"menu_button.png"]];
        }
        
       
        _navBar =(NavigationBar*) [[[NSBundle mainBundle] loadNibNamed:@"NavigationBar" owner:nil options:nil]objectAtIndex:0];
        
        if(IS_DEVICE_RUNNING_IOS_7_AND_ABOVE)
            [_navBar setFrame:CGRectMake(0, 0, _navBar.frame.size.width, _navBar.frame.size.height + 20)];
        else
            [_navBar setFrame:CGRectMake(0, [[UIApplication sharedApplication] statusBarFrame].size.height, _navBar.frame.size.width, _navBar.frame.size.height)];
        
        _navBar.tag = 1000000;
        [_navBar.btnMenu addTarget:self action:@selector(menuButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_navBar.btnBack addTarget:self action:@selector(menuButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_navBar.btnBack setHidden:YES];
        [_navBar.btnMenu setImage:[global currentSectorMenuBtn] forState:UIControlStateNormal];
        [del.window addSubview:_navBar];
        [del.window bringSubviewToFront:_navBar];
    }
    @catch (NSException *exception) {
        
    }
   
   
    
    
}

- (CGRect)navigationFrame
{
    if(IS_DEVICE_RUNNING_IOS_7_AND_ABOVE)
        return CGRectMake(0, 20, _navBar.frame.size.width, _navBar.frame.size.height - 20);
    else
        return CGRectMake(0, 0, _navBar.frame.size.width, _navBar.frame.size.height);
//    return self.navBar.frame;
}

- (void)viewDidLoad
{
    SHOW_POPUP =NO;
    [super viewDidLoad];
    
    if(IS_DEVICE_RUNNING_IOS_7_AND_ABOVE) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
	// Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}
-(IBAction)menuButtonClicked:(UIButton*)sender
{
    @try {
        del =(AppDelegate*) [[UIApplication sharedApplication]delegate];
        switch (sender.tag) {
            case MENU_BUTTON_TAG:
            {
                //HIDE_LOADING
                if (SHOW_POPUP) {
                    [[del.window viewWithTag:2000000] removeFromSuperview];
                    SHOW_POPUP=NO;
                }
                else
                {
                    PopUpView* popUpMenu =(PopUpView*) [[[NSBundle mainBundle] loadNibNamed:@"PopUpView" owner:nil options:nil]objectAtIndex:0];
                    [popUpMenu setFrame:CGRectMake(0, 53, popUpMenu.frame.size.width, popUpMenu.frame.size.height)];
                    popUpMenu.tag = 2000000;
                    popUpMenu.delegate=self;
                    if ([[UIScreen mainScreen] bounds].size.height == 568.0f)
                    {
                        [popUpMenu.tvOptions setFrame:CGRectMake(popUpMenu.tvOptions.frame.origin.x, popUpMenu.tvOptions.frame.origin.y, popUpMenu.tvOptions.frame.size.width, 440)];
                        
                    }
                    else
                    {
                        [popUpMenu.tvOptions setFrame:CGRectMake(popUpMenu.tvOptions.frame.origin.x, popUpMenu.tvOptions.frame.origin.y, popUpMenu.tvOptions.frame.size.width, 338)];
                    }
                    
                    if([[self.navigationController topViewController] class] != [MainScreenNewViewController class]){
                        
                        
                        NSMutableArray *array = [NSMutableArray arrayWithArray:[[Language getObject]getArrayWithKey:@"PopUpMenuOptions"]];
                        NSLog(@"%d",[array count]);
                        [array removeObjectAtIndex:[array count]-1];
                        NSLog(@"%d",[array count]);
                        [popUpMenu updateData:array];
                        [array removeObjectAtIndex:[array count]-1];
                        NSLog(@"%d",[array count]);
                        [popUpMenu updateData:array];
                        
                        
                    }
                    
                    // remove sesrch bar border
                    [[popUpMenu.searchBar.subviews objectAtIndex:0] removeFromSuperview];
                    
                    // change search bar search icon
                    UITextField *searchField = nil;
                    for (UIView *subview in popUpMenu.searchBar.subviews) {
                        if ([subview isKindOfClass:[UITextField class]]) {
                            searchField = (UITextField *)subview;
                            break;
                        }
                    }
                    if (searchField) {
                        UIImage *image = [UIImage imageNamed: @"search icon.png"];
                        UIImageView *iView = [[UIImageView alloc] initWithImage:image];
                        iView.frame = CGRectMake(10, 10, 24, 24);
                        searchField.leftView = iView;
                        
                        
                        [searchField setEnablesReturnKeyAutomatically:NO];
                        searchField.delegate=self;
                        [searchField setEnabled:TRUE];
                        
                        [searchField setBackground:[UIImage imageNamed:@"menu tile.png"]];
                        
                    }
                    
                    [del.window addSubview:popUpMenu];
                    [del.window bringSubviewToFront:popUpMenu];
                    [popUpMenu reloadData];
                    
                    SHOW_POPUP=YES;
                }
                
            }
                break;
            case BACK_BUTTON_TAG:
            {
                HIDE_LOADING
                if (self.navigationController.viewControllers.count > 0) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
                
            }
            case REFRESH_BUTTON_TAG:
            {
                
            }
                break;
        }

    }
    @catch (NSException *exception) {
        
    }
    
    
    
}
#pragma Mark uitextfield delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillDisappear:(BOOL)animated{
    if (SHOW_POPUP) {
        [[del.window viewWithTag:2000000] removeFromSuperview];
        SHOW_POPUP=NO;
    }
}

@end
