//
//  DrugDetailsViewController.m
//  SFDA
//
//  Created by Assem Imam on 10/26/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "MedicalEQPDetailsViewController.h"
#import "Global.h"
#import "NavigationBar.h"
#import "ASIHTTPRequest.h"
#import "JSON.h"
#import "Language.h"

@interface MedicalEQPDetailsViewController ()
{
    AppDelegate*del;
    ASIHTTPRequest *req;
}

@property (weak, nonatomic) IBOutlet UILabel *mdmaListing;
@property (weak, nonatomic) IBOutlet UILabel *brandNumber;
@property (weak, nonatomic) IBOutlet UILabel *productDisc;
@property (weak, nonatomic) IBOutlet UILabel *autherizationNum;
@property (weak, nonatomic) IBOutlet UILabel *autherizationValidity;
@property (weak, nonatomic) IBOutlet UILabel *phone;
@property (weak, nonatomic) IBOutlet UILabel *fax;
@property (weak, nonatomic) IBOutlet UILabel *authorizedRepresentive;

@end

@implementation MedicalEQPDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    del =(AppDelegate*) [[UIApplication sharedApplication]delegate];
    
    NavigationBar*navBar2 =  (NavigationBar*) [del.window viewWithTag:1000000];
    if (navBar2) {
        [navBar2.btnBack setHidden:NO];
        [del.window bringSubviewToFront:navBar2];
    }

    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
//    [svContainer setContentSize:CGSizeMake(svContainer.frame.size.width, 620)];
    
    SHOW_LOADING
    NSString *url = [NSString stringWithFormat:@"%@MedicalProductDetails?productId=%@", SERVER_IP, self.productId];
    req =[[ASIHTTPRequest alloc]initWithURL:[NSURL URLWithString:url]];
    
    [req setCompletionBlock:^{
        NSString *response=  [req responseString];
        
        id result = [response JSONValue];
        HIDE_LOADING
        if (result==nil) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"msgNoData"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
        }
        else{
            NSLog(@"tttttt");
            self.authorizedRepresentive.text = [[result objectAtIndex:0]objectForKey:@"AR_Name"];
            self.autherizationValidity.text = [[result objectAtIndex:0]objectForKey:@"Authorization_Validity"];
            self.autherizationNum.text = [[result objectAtIndex:0]objectForKey:@"Authorization_Number"];
            self.brandNumber.text = [[result objectAtIndex:0]objectForKey:@"Brand_Name"];
            self.fax.text = [[result objectAtIndex:0]objectForKey:@"FAX"];
            self.mdmaListing.text = [[result objectAtIndex:0]objectForKey:@"Listing_Number"];
            self.phone.text = [[result objectAtIndex:0]objectForKey:@"PHONE"];
            self.productDisc.text = [[result objectAtIndex:0]objectForKey:@"Product_Description"];
        }
        
        
    }];
    [req setFailedBlock:^{
        [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"msgNoData"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
        
    }];

    [req startAsynchronous];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [super viewDidUnload];
}
@end
