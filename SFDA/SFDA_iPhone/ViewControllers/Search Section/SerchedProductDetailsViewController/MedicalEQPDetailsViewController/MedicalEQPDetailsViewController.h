//
//  DrugDetailsViewController.h
//  SFDA
//
//  Created by Assem Imam on 10/26/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"

@interface MedicalEQPDetailsViewController : MasterViewController

@property(nonatomic,retain)NSNumber *productId;

@end
