//
//  DrugDetailsViewController.m
//  SFDA
//
//  Created by Assem Imam on 10/26/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "DrugDetailsViewController.h"
#import "Global.h"
#import "NavigationBar.h"
#import "ASIHTTPRequest.h"
#import "JSON.h"
#import "Language.h"

@interface DrugDetailsViewController ()
{
    AppDelegate*del;
    ASIHTTPRequest *req;
}
@end

@implementation DrugDetailsViewController
@synthesize DrugRegistrationNo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    del =(AppDelegate*) [[UIApplication sharedApplication]delegate];
    
    NavigationBar*navBar2 =  (NavigationBar*) [del.window viewWithTag:1000000];
    if (navBar2) {
        [navBar2.btnBack setHidden:NO];
        [del.window bringSubviewToFront:navBar2];
    }
    
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [svContainer setContentSize:CGSizeMake(svContainer.frame.size.width, 620)];
    
    SHOW_LOADING
    req =[[ASIHTTPRequest alloc]initWithURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@DrugDetails?registrationNo=", SERVER_IP] stringByAppendingString:self.DrugRegistrationNo]]];
    
    [req setCompletionBlock:^{
        NSString *response=  [req responseString];
        
        id result = [response JSONValue];
        HIDE_LOADING
        if (result==nil) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"msgNoData"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
        }
        else{
            UnitOfStrength= [NSMutableString new];
            UnitOfVolume= [NSMutableString new];
            registrationNo.text = [[result objectAtIndex:0]objectForKey:@"RegistrationNo"];
            comercialName.text=[[result objectAtIndex:0]objectForKey:@"TradeName"];
            scientificName.text=[[result objectAtIndex:0]objectForKey:@"GenericName"];
            storageConditions.text=[[result objectAtIndex:0]objectForKey:@"StorageConditions"];
            code1.text=[[result objectAtIndex:0]objectForKey:@"ATCCode1"];
            code2.text=[[result objectAtIndex:0]objectForKey:@"ATCCode2"];
            UnitOfStrength= [[result objectAtIndex:0]objectForKey:@"UnitOfStrength"];
            UnitOfVolume=[[result objectAtIndex:0]objectForKey:@"UnitOfVolume"];
            NSLog(@"%@",UnitOfStrength);
            NSLog(@"%@",UnitOfVolume);
            size.text =[[result objectAtIndex:0]objectForKey:@"Volume"];
            usageMethod.text=[[result objectAtIndex:0]objectForKey:@"RouteOfAdministration"];
            boxSize.text =[[result objectAtIndex:0]objectForKey:@"PackageSize"];
            boxType.text =[[result objectAtIndex:0]objectForKey:@"PackageType"];
            publicPrice.text=[[result objectAtIndex:0]objectForKey:@"PublicPrice"];
            validityPeriod.text =[[result objectAtIndex:0]objectForKey:@"ShelfLife"];
            monitoring.text=[[result objectAtIndex:0]objectForKey:@"ProductControl"];
            PharmaceuticalForm.text=[[result objectAtIndex:0]objectForKey:@"DosageForm"];
            Concentration.text=[[result objectAtIndex:0]objectForKey:@"StrengthValue"];
            ManufacturerName.text=[[result objectAtIndex:0]objectForKey:@"ManufacturerName"];
            LegalStatus.text=[[result objectAtIndex:0]objectForKey:@"LegalStatus"];
            size.text= [size.text stringByAppendingString:UnitOfVolume];
           Concentration.text= [Concentration.text stringByAppendingString:UnitOfStrength];
        }
        
        
    }];
    [req setFailedBlock:^{
        [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"msgNoData"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
        
    }];

    [req startAsynchronous];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    scientificName = nil;
    comercialName = nil;
    registrationNo = nil;
    PharmaceuticalForm = nil;
    usageMethod = nil;
    code1 = nil;
    code2 = nil;
    monitoring = nil;
    size = nil;
    Concentration = nil;
    boxType = nil;
    boxSize = nil;
    validityPeriod = nil;
    storageConditions = nil;
    publicPrice = nil;
    svContainer = nil;
    [super viewDidUnload];
}
@end
