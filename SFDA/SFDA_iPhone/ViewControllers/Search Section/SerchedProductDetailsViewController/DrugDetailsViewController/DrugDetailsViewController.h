//
//  DrugDetailsViewController.h
//  SFDA
//
//  Created by Assem Imam on 10/26/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"

@interface DrugDetailsViewController : MasterViewController
{
    NSMutableString * UnitOfStrength;
    NSMutableString * UnitOfVolume;
    __weak IBOutlet UILabel * LegalStatus;
    __weak IBOutlet UILabel * ManufacturerName;
    __weak IBOutlet UIScrollView *svContainer;
    __weak IBOutlet UILabel *storageConditions;
    __weak IBOutlet UILabel *PharmaceuticalForm;
    __weak IBOutlet UILabel *publicPrice;
    __weak IBOutlet UILabel *validityPeriod;
    __weak IBOutlet UILabel *boxSize;
    __weak IBOutlet UILabel *boxType;
    __weak IBOutlet UILabel *Concentration;
    __weak IBOutlet UILabel *size;
    __weak IBOutlet UILabel *monitoring;
    __weak IBOutlet UILabel *code2;
    __weak IBOutlet UILabel *code1;
    __weak IBOutlet UILabel *usageMethod;
    __weak IBOutlet UILabel *registrationNo;
    __weak IBOutlet UILabel *comercialName;
    __weak IBOutlet UILabel *scientificName;
}
@property(nonatomic,retain)NSString*DrugRegistrationNo;
@end
