//
//  DrugSearchResultViewController.m
//  SFDA
//
//  Created by Assem Imam on 10/26/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "DrugSearchResultViewController.h"
#import "DrugDetailsViewController.h"
#import "DrugSearchResultCell.h"
#import "Language.h"
#import "NavigationBar.h"
#import "AppDelegate.h"
#import "Global.h"

@interface DrugSearchResultViewController ()
{
    AppDelegate*del;
}
@end

@implementation DrugSearchResultViewController

#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DrugSearchResultCell *cell = (DrugSearchResultCell*)[tableView dequeueReusableCellWithIdentifier:@"cleDrugSearch"];
    if (cell==nil) {
        cell = (DrugSearchResultCell*)[[[NSBundle mainBundle]loadNibNamed:[[Language getObject]getStringWithKey:@"DrugSearchResultCell"] owner:nil options:nil]objectAtIndex:0];
    }
    
    cell.drugCommercialName.text = [[self.searchResultList objectAtIndex:indexPath.row]objectForKey:@"TradeName"];
    cell.drugScientificName.text = [[self.searchResultList objectAtIndex:indexPath.row]objectForKey:@"GenericName"];
    cell.drugSerialNumber.text = [[self.searchResultList objectAtIndex:indexPath.row]objectForKey:@"RegistrationNo"];
    //cell.selectionStyle=UITableViewCellSelectionStyleGray;
    cell.selectedBackgroundView.backgroundColor = [UIColor colorWithRed:159.0f/255.0f green:221.0f/255.0f blue:255.0f/255.0f alpha:1.0f];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DrugDetailsViewController *vc_drugDetails = [[DrugDetailsViewController alloc]initWithNibName:@"DrugDetailsViewController" bundle:nil];
    vc_drugDetails.DrugRegistrationNo = [[self.searchResultList objectAtIndex:indexPath.row]objectForKey:@"RegistrationNo"];
    [self.navigationController pushViewController:vc_drugDetails animated:YES];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
