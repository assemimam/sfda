//
//  DrugSearchResultViewController.h
//  SFDA
//
//  Created by Assem Imam on 10/26/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"

@interface ParentSearchResultViewController : MasterViewController <UITableViewDataSource,UITableViewDelegate>
@property id searchResultList;
@end
