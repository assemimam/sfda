//
//  DrugSearchResultViewController.m
//  SFDA
//
//  Created by Assem Imam on 10/26/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "ParentSearchResultViewController.h"
#import "DrugDetailsViewController.h"
#import "DrugSearchResultCell.h"
#import "Language.h"
#import "NavigationBar.h"
#import "AppDelegate.h"
#import "Global.h"

@interface ParentSearchResultViewController ()
{
    AppDelegate*del;
    __weak IBOutlet UITableView *tableView;
}
@end

@implementation ParentSearchResultViewController
@synthesize searchResultList;

#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    // Overriden in child
    UITableViewCell *cell = nil;
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  [self.searchResultList count];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
// Overriden in child
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    del =(AppDelegate*) [[UIApplication sharedApplication]delegate];
    HIDE_LOADING
    NavigationBar*navBar2 =  (NavigationBar*) [del.window viewWithTag:1000000];
    if (navBar2) {
        [navBar2.btnBack setHidden:NO];
        [del.window bringSubviewToFront:navBar2];
    }

    
    [self updateViews];
}

- (void)updateViews
{
    CGRect frame = [self navigationFrame];
    
    if(!IS_DEVICE_RUNNING_IOS_7_AND_ABOVE)
        frame.origin.y = 0.0;
    
    [tableView setFrame:CGRectMake(tableView.frame.origin.x, frame.origin.y + frame.size.height, tableView.frame.size.width, tableView.frame.size.height - frame.origin.y)];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
