//
//  DrugSearchResultViewController.m
//  SFDA
//
//  Created by Assem Imam on 10/26/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "MedicalEQPSearchResultViewController.h"
#import "MedicalEQPDetailsViewController.h"
#import "MedicalSearchResultCell.h"
#import "Language.h"
#import "NavigationBar.h"
#import "AppDelegate.h"
#import "Global.h"

@interface MedicalEQPSearchResultViewController ()
{
    AppDelegate*del;
}
@end

@implementation MedicalEQPSearchResultViewController

#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MedicalSearchResultCell *cell = (MedicalSearchResultCell*)[tableView dequeueReusableCellWithIdentifier:@"cleMedicalSearch"];
    if (cell==nil) {
        cell = (MedicalSearchResultCell*)[[[NSBundle mainBundle]loadNibNamed:[[Language getObject]getStringWithKey:@"MedicalSearchResultCell"] owner:nil options:nil]objectAtIndex:0];
    }
    
    cell.mdmaListingName.text = [[self.searchResultList objectAtIndex:indexPath.row]objectForKey:@"Listing_Number"];
    cell.medicalCommercialName.text = [[self.searchResultList objectAtIndex:indexPath.row]objectForKey:@"Brand_Name"];
    cell.medicalManufacturerNumber.text = [[self.searchResultList objectAtIndex:indexPath.row]objectForKey:@"Legal_Manufacturer"];
    cell.medicalSerialNumber.text = [[self.searchResultList objectAtIndex:indexPath.row]objectForKey:@"Model_Number"];
    //cell.selectionStyle=UITableViewCellSelectionStyleGray;
    cell.selectedBackgroundView.backgroundColor = [UIColor colorWithRed:236.0f/255.0f green:237.0f/255.0f blue:238.0f/255.0f alpha:1.0f];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MedicalEQPDetailsViewController *vc_drugDetails = [[MedicalEQPDetailsViewController alloc]init];
    vc_drugDetails.productId = [[self.searchResultList objectAtIndex:indexPath.row]objectForKey:@"Product_id"];
    [self.navigationController pushViewController:vc_drugDetails animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  116;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
