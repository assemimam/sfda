//
//  BarcodePickerViewController.m
//  SFDA
//
//  Created by Assem Imam on 5/7/14.
//  Copyright (c) 2014 NUITEX. All rights reserved.
//

#import "BarcodePickerViewController.h"
#import "FoodSearchListViewController.h"

@interface BarcodePickerViewController ()

@end

@implementation BarcodePickerViewController
@synthesize BarcodeImage,BarcodeText;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    BarcodeImageView.image=self.BarcodeImage;
    BarcodeNumberLabel.text= self.BarcodeText;
    SearchButton.titleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
    TitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)SearchAction:(UIButton *)sender {
    @try {
        if (!internetConnection) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            
            return;
        }

        if (BarcodeNumberLabel.text.length>0) {
            FoodSearchListViewController *vc_foodSearchList =[[FoodSearchListViewController alloc]initWithNibName:@"FoodSearchListViewController" bundle:nil];
            vc_foodSearchList.ProductSrial= BarcodeNumberLabel.text;
            vc_foodSearchList.GET_FORM_BARCODE=YES;
            [self.navigationController pushViewController:vc_foodSearchList animated:YES];
        }
        
    }
    @catch (NSException *exception) {
        
    }
   
}
@end
