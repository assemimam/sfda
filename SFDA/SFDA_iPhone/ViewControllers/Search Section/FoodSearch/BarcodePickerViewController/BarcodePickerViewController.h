//
//  BarcodePickerViewController.h
//  SFDA
//
//  Created by Assem Imam on 5/7/14.
//  Copyright (c) 2014 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BarcodePickerViewController : UIViewController
{
    
    __weak IBOutlet UILabel *TitleLabel;
    __weak IBOutlet UIButton *SearchButton;
    __weak IBOutlet UILabel *BarcodeNumberLabel;
    __weak IBOutlet UIImageView *BarcodeImageView;
}
@property(nonatomic,retain)NSString*BarcodeText;
@property(nonatomic,retain)UIImage*BarcodeImage;
- (IBAction)SearchAction:(UIButton *)sender;
@end
