//
//  MainFoodSearchViewController.h
//  SFDA
//
//  Created by Assem Imam on 5/7/14.
//  Copyright (c) 2014 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarReaderViewController.h"
#import "MasterViewController.h"

@interface MainFoodSearchViewController : MasterViewController<UITextFieldDelegate,ZBarReaderDelegate>
{
    
    __weak IBOutlet UITextField *SearchNumberTextField;
    __weak IBOutlet UIButton *ScanBarcodeButton;
}
- (IBAction)ScanBarcodeAction:(id)sender;
- (IBAction)SearchAction:(UIButton *)sender;

@end
