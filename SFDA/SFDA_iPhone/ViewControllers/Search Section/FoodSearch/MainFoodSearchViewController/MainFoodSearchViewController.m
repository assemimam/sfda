//
//  MainFoodSearchViewController.m
//  SFDA
//
//  Created by Assem Imam on 5/7/14.
//  Copyright (c) 2014 NUITEX. All rights reserved.
//

#import "MainFoodSearchViewController.h"
#import "FoodSearchListViewController.h"
#import "Global.h"
#import "BarcodeScannerViewController.h"
#import "BarcodePickerViewController.h"

@interface MainFoodSearchViewController ()<BarCodeScannerDelegate>
{
    
    __weak IBOutlet UILabel *TitleLabel;
    __weak IBOutlet UILabel *barcodeLabel;
}
@end

@implementation MainFoodSearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    del =(AppDelegate*) [[UIApplication sharedApplication]delegate];
    
    NavigationBar*navigationBarView =  (NavigationBar*) [del.window viewWithTag:1000000];
    //    NavigationBar * navigationBarView = (NavigationBar *) del.window.rootViewController.navigationItem.titleView;
    if (navigationBarView) {
        [navigationBarView.btnBack setHidden:NO];
        
        [del.window bringSubviewToFront:navigationBarView];
    }

    
  
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(DismissKeyBoard:)];
    tapGestureRecognizer.numberOfTapsRequired=1;
    [self.view addGestureRecognizer:tapGestureRecognizer];
     //SearchNumberTextField.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
    barcodeLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
    TitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
    
    UIView *inputAccessoryView =[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    UIToolbar *keyboardToolbar =[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    keyboardToolbar.barStyle = UIBarStyleDefault;
    keyboardToolbar.tintColor = [UIColor darkGrayColor];
    
    UIBarButtonItem *barButtonItemSearch = [[UIBarButtonItem alloc]  initWithTitle:[[Language getObject]getStringWithKey:@"Search"] style:UIBarButtonItemStyleBordered target:self action:@selector(SearchAction:) ];
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *barButtonItemCancel = [[UIBarButtonItem alloc]  initWithTitle:[[Language getObject]getStringWithKey:@"CancelTitle"] style:UIBarButtonItemStyleBordered target:self action:@selector(DismissSearchKeyBoard) ];
    NSArray *items = [[NSArray alloc] initWithObjects: barButtonItemCancel,flexibleItem,barButtonItemSearch, nil];
    
    [keyboardToolbar setItems:items];
    
    [inputAccessoryView addSubview:keyboardToolbar];
    [SearchNumberTextField setInputAccessoryView:inputAccessoryView];

}
-(void)DismissSearchKeyBoard
{
    @try {
        [SearchNumberTextField resignFirstResponder];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)DismissKeyBoard:(UIGestureRecognizer*)recognizer
{
    @try {
        [SearchNumberTextField resignFirstResponder];
    }
    @catch (NSException *exception) {
        
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)ScanBarcodeAction:(id)sender {
    @try {
       [[UIApplication sharedApplication]setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
        UIView* _navBar =  (NavigationBar*) [del.window viewWithTag:1000000];
        [_navBar setHidden:YES];
        // force view class to load so it may be referenced directly from NIB
        [ZBarReaderView class];
        BarcodeScannerViewController *vc_barcodecanner =[[BarcodeScannerViewController alloc]init];
        vc_barcodecanner.delegate=self;
        [self.navigationController presentModalViewController: vc_barcodecanner
                                animated: YES];
    }
    @catch (NSException *exception) {
        
    }
    
}
#pragma -mark barcode delegate
-(void)didFinishPickingBarcode:(id)barcode ForBarcodeImage:(UIImage *)barcodeImage
{
    @try {
        if (barcode) {
            [self.navigationController dismissModalViewControllerAnimated:YES];
            BarcodePickerViewController *vc_barcodePicker =[[BarcodePickerViewController alloc]init];
            vc_barcodePicker.BarcodeImage= barcodeImage;
            vc_barcodePicker.BarcodeText=barcode;
            [self.navigationController pushViewController:vc_barcodePicker animated:YES];
            
        }
    }
    @catch (NSException *exception) {
        
    }
    
}
- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
 
    UIView* _navBar =  (NavigationBar*) [del.window viewWithTag:1000000];
    [_navBar setHidden:NO];

    
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
      
        break;
    
    
    //resultText.text = symbol.data;
    
   
   // resultImage.image =
    [info objectForKey: UIImagePickerControllerOriginalImage];
    
  
    [reader dismissModalViewControllerAnimated: YES];
}


- (IBAction)SearchAction:(UIButton *)sender {
    @try {
        [SearchNumberTextField resignFirstResponder];
        if (!internetConnection) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            HIDE_LOADING
            return;
        }
        SHOW_LOADING;
        FoodSearchListViewController*vc_foodSearchList =[[FoodSearchListViewController alloc]initWithNibName:@"FoodSearchListViewController" bundle:nil];
        vc_foodSearchList.ProductSrial=SearchNumberTextField.text;
        vc_foodSearchList.GET_FORM_BARCODE=NO;
        [self.navigationController pushViewController:vc_foodSearchList animated:YES];
    }
    @catch (NSException *exception) {
        
    }
    
}

#pragma -mark uitextfield delegate methods
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 20) ? NO : YES;
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [SearchNumberTextField resignFirstResponder];
    if (!internetConnection) {
        [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
        HIDE_LOADING
      return YES;
    }
    SHOW_LOADING;
    FoodSearchListViewController*vc_foodSearchList =[[FoodSearchListViewController alloc]init];
    vc_foodSearchList.ProductSrial=SearchNumberTextField.text;
    vc_foodSearchList.GET_FORM_BARCODE=NO;
    [self.navigationController pushViewController:vc_foodSearchList animated:YES];
    return YES;
}


@end
