//
//  FoodSearchDetailsViewController.m
//  SFDA
//
//  Created by Assem Imam on 5/7/14.
//  Copyright (c) 2014 NUITEX. All rights reserved.
//

#import "FoodSearchDetailsViewController.h"
#import "FoodDetailsCell.h"
#import "Language.h"

@interface FoodSearchDetailsViewController ()
{
   
}
@end

@implementation FoodSearchDetailsViewController
@synthesize ProductDetails,SerialNumber;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    del =(AppDelegate*) [[UIApplication sharedApplication]delegate];
    NavigationBar*navBar2 =  (NavigationBar*) [del.window viewWithTag:1000000];
    if (navBar2) {
        [navBar2.btnBack setHidden:NO];
        [del.window bringSubviewToFront:navBar2];
    }
    
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    HeaderLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
    FooterLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
    TitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
    TitleLabel.text= SerialNumber;
    [ProductDetailsTableView reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma  mark tableview datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FoodDetailsCell *cell ;
    @try {
        cell=(FoodDetailsCell*) [tableView dequeueReusableCellWithIdentifier:@"cleFood"];
        if (cell==nil) {
            cell =(FoodDetailsCell*) [[[NSBundle mainBundle]loadNibNamed:[[Language getObject]getStringWithKey:@"FoodDetailsCell"] owner:nil options:nil]objectAtIndex:0];
        }
        
        NSString *title,*value;

        switch (indexPath.row) {
            case 0:
            {
                title = [[Language getObject]getStringWithKey:@"ProductState"];
                value = [[Language getObject]getStringWithKey:@"FoodProduct"];
            }
                break;
            case 1:
            {
            
                title = [[Language getObject]getStringWithKey:@"TradeName"];
                value = [NSNull null]!= [ProductDetails objectForKey:@"TradeName"]&&[ProductDetails objectForKey:@"TradeName"]&&[[ProductDetails objectForKey:@"TradeName"] stringByReplacingOccurrencesOfString:@" " withString:@""].length>0?[ProductDetails objectForKey:@"TradeName"]:@"لا يوجد";
            }
                break;
            case 2:
            {
                title = [[Language getObject]getStringWithKey:@"OrginCountry"];
                value =  [NSNull null]!=[ProductDetails objectForKey:@"OrginCountry "]&&[ProductDetails objectForKey:@"OrginCountry "]&&[[ProductDetails objectForKey:@"OrginCountry "] stringByReplacingOccurrencesOfString:@" " withString:@""].length>0?[ProductDetails objectForKey:@"OrginCountry "]:@"لا يوجد";
            }
                break;
            case 3:
            {
                title = [[Language getObject]getStringWithKey:@"PackagingTypes"];
                value = [NSNull null]!= [ProductDetails objectForKey:@"PackagingTypes"]&&[ProductDetails objectForKey:@"PackagingTypes"]&&[[ProductDetails objectForKey:@"PackagingTypes"] stringByReplacingOccurrencesOfString:@" " withString:@""].length>0?[ProductDetails objectForKey:@"PackagingTypes"]:@"لا يوجد";
            }
                break;
            case 4:
            {
                title = [[Language getObject]getStringWithKey:@"ShelfLife"];
                value =  [NSNull null]!=[ProductDetails objectForKey:@"ShelfLife"] && [ProductDetails objectForKey:@"ShelfLife"]&&[[ProductDetails objectForKey:@"ShelfLife"] stringByReplacingOccurrencesOfString:@" " withString:@""].length>0?[ProductDetails objectForKey:@"ShelfLife"]:@"لا يوجد";
            }
                break;
            case 5:
            {
                title = [[Language getObject]getStringWithKey:@"StorageTemperatures"];
                value =  [NSNull null]!=[ProductDetails objectForKey:@"StorageTemperatures"]&&[ProductDetails objectForKey:@"StorageTemperatures"]&&[[ProductDetails objectForKey:@"StorageTemperatures"] stringByReplacingOccurrencesOfString:@" " withString:@""].length>0?[ProductDetails objectForKey:@"StorageTemperatures"]:@"لا يوجد";
            }
                break;
            case 6:
            {
                title = [[Language getObject]getStringWithKey:@"UsageMethods"];
                value = [NSNull null]!=[ProductDetails objectForKey:@"UsageMethods"]&&[ProductDetails objectForKey:@"UsageMethods"]&&[[ProductDetails objectForKey:@"UsageMethods"] stringByReplacingOccurrencesOfString:@" " withString:@""].length>0?[ProductDetails objectForKey:@"UsageMethods"]:@"لا يوجد";
            }
                break;
            case 7:
            {
                title = [[Language getObject]getStringWithKey:@"AgeGroups"];
                value = [NSNull null]!= [ProductDetails objectForKey:@"AgeGroups"]&&[ProductDetails objectForKey:@"AgeGroups"]&&[[ProductDetails objectForKey:@"AgeGroups"] stringByReplacingOccurrencesOfString:@" " withString:@""].length>0?[ProductDetails objectForKey:@"AgeGroups"]:@"لا يوجد";
            }
                break;
            case 8:
            {
                title = [[Language getObject]getStringWithKey:@"ItemWeight"];
                value = [NSNull null]!= [ProductDetails objectForKey:@"ItemWeight"]&&[ProductDetails objectForKey:@"ItemWeight"]?[NSString stringWithFormat:@"%0.1f",[[ProductDetails objectForKey:@"ItemWeight"]floatValue]]:@"لا يوجد";
            }
                break;
            case 9:
            {
                title = [[Language getObject]getStringWithKey:@"ItemWeightUnit"];
                value = [NSNull null]!= [ProductDetails objectForKey:@"ItemWeightUnit"]&&[ProductDetails objectForKey:@"ItemWeightUnit"]&&[[ProductDetails objectForKey:@"ItemWeightUnit"] stringByReplacingOccurrencesOfString:@" " withString:@""].length>0?[ProductDetails objectForKey:@"ItemWeightUnit"]:@"لا يوجد";
            }
                break;
            case 10:
            {
                title = [[Language getObject]getStringWithKey:@"Warnings"];
                value = [NSNull null]!= [ProductDetails objectForKey:@"Warnings"]&& [ProductDetails objectForKey:@"Warnings"]&&[[ProductDetails objectForKey:@"Warnings"] stringByReplacingOccurrencesOfString:@" " withString:@""].length>0?[ProductDetails objectForKey:@"Warnings"]:@"لا يوجد";
            }
                break;
        }

        cell.TitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
        cell.ValueLabel.font=[UIFont fontWithName:@"GESSTwoLight-Light" size:15];
        cell.TitleLabel.text = title;
        cell.ValueLabel.text = value;
    }
    @catch (NSException *exception) {
        
    }
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 11;
}
#pragma  mark tableview delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger height=0;
    NSString *title,*value;
    @try {
        switch (indexPath.row) {
            case 0:
            {
                title = [[Language getObject]getStringWithKey:@"ProductState"];
                value = [[Language getObject]getStringWithKey:@"FoodProduct"];
            }
                break;
            case 1:
            {
                
                title = [[Language getObject]getStringWithKey:@"TradeName"];
                value = [NSNull null]!= [ProductDetails objectForKey:@"TradeName"]&&[ProductDetails objectForKey:@"TradeName"]&&[[ProductDetails objectForKey:@"TradeName"] stringByReplacingOccurrencesOfString:@" " withString:@""].length>0?[ProductDetails objectForKey:@"TradeName"]:@"لا يوجد";
            }
                break;
            case 2:
            {
                title = [[Language getObject]getStringWithKey:@"OrginCountry"];
                value =  [NSNull null]!=[ProductDetails objectForKey:@"OrginCountry "]&&[ProductDetails objectForKey:@"OrginCountry "]&&[[ProductDetails objectForKey:@"OrginCountry "] stringByReplacingOccurrencesOfString:@" " withString:@""].length>0?[ProductDetails objectForKey:@"OrginCountry "]:@"لا يوجد";
            }
                break;
            case 3:
            {
                title = [[Language getObject]getStringWithKey:@"PackagingTypes"];
                value = [NSNull null]!= [ProductDetails objectForKey:@"PackagingTypes"]&&[ProductDetails objectForKey:@"PackagingTypes"]&&[[ProductDetails objectForKey:@"PackagingTypes"] stringByReplacingOccurrencesOfString:@" " withString:@""].length>0?[ProductDetails objectForKey:@"PackagingTypes"]:@"لا يوجد";
            }
                break;
            case 4:
            {
                title = [[Language getObject]getStringWithKey:@"ShelfLife"];
                value =  [NSNull null]!=[ProductDetails objectForKey:@"ShelfLife"] && [ProductDetails objectForKey:@"ShelfLife"]&&[[ProductDetails objectForKey:@"ShelfLife"] stringByReplacingOccurrencesOfString:@" " withString:@""].length>0?[ProductDetails objectForKey:@"ShelfLife"]:@"لا يوجد";
            }
                break;
            case 5:
            {
                title = [[Language getObject]getStringWithKey:@"StorageTemperatures"];
                value =  [NSNull null]!=[ProductDetails objectForKey:@"StorageTemperatures"]&&[ProductDetails objectForKey:@"StorageTemperatures"]&&[[ProductDetails objectForKey:@"StorageTemperatures"] stringByReplacingOccurrencesOfString:@" " withString:@""].length>0?[ProductDetails objectForKey:@"StorageTemperatures"]:@"لا يوجد";
            }
                break;
            case 6:
            {
                title = [[Language getObject]getStringWithKey:@"UsageMethods"];
                value = [NSNull null]!=[ProductDetails objectForKey:@"UsageMethods"]&&[ProductDetails objectForKey:@"UsageMethods"]&&[[ProductDetails objectForKey:@"UsageMethods"] stringByReplacingOccurrencesOfString:@" " withString:@""].length>0?[ProductDetails objectForKey:@"UsageMethods"]:@"لا يوجد";
            }
                break;
            case 7:
            {
                title = [[Language getObject]getStringWithKey:@"AgeGroups"];
                value = [NSNull null]!= [ProductDetails objectForKey:@"AgeGroups"]&&[ProductDetails objectForKey:@"AgeGroups"]&&[[ProductDetails objectForKey:@"AgeGroups"] stringByReplacingOccurrencesOfString:@" " withString:@""].length>0?[ProductDetails objectForKey:@"AgeGroups"]:@"لا يوجد";
            }
                break;
            case 8:
            {
                title = [[Language getObject]getStringWithKey:@"ItemWeight"];
                value = [NSNull null]!= [ProductDetails objectForKey:@"ItemWeight"]&&[ProductDetails objectForKey:@"ItemWeight"]?[NSString stringWithFormat:@"%0.1f",[[ProductDetails objectForKey:@"ItemWeight"]floatValue]]:@"لا يوجد";
            }
                break;
            case 9:
            {
                title = [[Language getObject]getStringWithKey:@"ItemWeightUnit"];
                value = [NSNull null]!= [ProductDetails objectForKey:@"ItemWeightUnit"]&&[ProductDetails objectForKey:@"ItemWeightUnit"]&&[[ProductDetails objectForKey:@"ItemWeightUnit"] stringByReplacingOccurrencesOfString:@" " withString:@""].length>0?[ProductDetails objectForKey:@"ItemWeightUnit"]:@"لا يوجد";
            }
                break;
            case 10:
            {
                title = [[Language getObject]getStringWithKey:@"Warnings"];
                value = [NSNull null]!= [ProductDetails objectForKey:@"Warnings"]&& [ProductDetails objectForKey:@"Warnings"]&&[[ProductDetails objectForKey:@"Warnings"] stringByReplacingOccurrencesOfString:@" " withString:@""].length>0?[ProductDetails objectForKey:@"Warnings"]:@"لا يوجد";
            }
                break;
        }
        

        CGSize titleSize = [title sizeWithFont:[UIFont fontWithName:@"GESSTwoBold-Bold" size:25] constrainedToSize:CGSizeMake(ProductDetailsTableView.frame.size.height , 20000000000.0f)];
        CGSize valueSize = [value sizeWithFont:[UIFont fontWithName:@"GESSTwoBold-Bold" size:25] constrainedToSize:CGSizeMake(ProductDetailsTableView.frame.size.height , 20000000000.0f)];
        if (titleSize.height>valueSize.height) {
            height=titleSize.height;
        }
        else{
            height=valueSize.height;
        }
        
        height =height+5;
        NSLog(@" heigt for index path row %d  = %d",indexPath.row,height);

    }
    @catch (NSException *exception) {
        
    }

    return MAX(height,65);
    
    
}



@end
