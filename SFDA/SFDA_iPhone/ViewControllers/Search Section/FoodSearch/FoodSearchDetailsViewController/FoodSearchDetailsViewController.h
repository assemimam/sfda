//
//  FoodSearchDetailsViewController.h
//  SFDA
//
//  Created by Assem Imam on 5/7/14.
//  Copyright (c) 2014 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"

@interface FoodSearchDetailsViewController : MasterViewController<UITableViewDataSource,UITabBarDelegate>
{
    
    __weak IBOutlet UILabel *TitleLabel;
    __weak IBOutlet UITableView *ProductDetailsTableView;
    __weak IBOutlet UILabel *FooterLabel;
    __weak IBOutlet UILabel *HeaderLabel;
}
@property(nonatomic,retain)NSString*SerialNumber;
@property(nonatomic,retain) id ProductDetails;
@end
