//
//  FoodSearchCell.h
//  SFDA
//
//  Created by Assem Imam on 5/7/14.
//  Copyright (c) 2014 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodSearchCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *ProductNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *CompanyNameLabel;

@end
