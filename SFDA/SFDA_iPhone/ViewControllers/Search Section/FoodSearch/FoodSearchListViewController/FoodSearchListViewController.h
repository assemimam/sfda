//
//  FoodSearchListViewController.h
//  SFDA
//
//  Created by Assem Imam on 5/7/14.
//  Copyright (c) 2014 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"
@interface FoodSearchListViewController : MasterViewController<UIAlertViewDelegate>
{
    
    __weak IBOutlet UILabel *SearchLabel;
    __weak IBOutlet UILabel *TitleLabel;
    __weak IBOutlet UIButton *BarcodeButton;
    __weak IBOutlet UITableView *SearchResultsTableView;
}
@property BOOL GET_FORM_BARCODE;
@property(nonatomic,retain)NSString*ProductSrial;
- (IBAction)GoBackAction:(id)sender;
@end
