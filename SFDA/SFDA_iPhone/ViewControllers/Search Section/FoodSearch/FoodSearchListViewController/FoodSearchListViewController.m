//
//  FoodSearchListViewController.m
//  SFDA
//
//  Created by Assem Imam on 5/7/14.
//  Copyright (c) 2014 NUITEX. All rights reserved.
//

#import "FoodSearchListViewController.h"
#import "Language.h"
#import "FoodSearchCell.h"
#import "BarcodePickerViewController.h"
#import "Global.h"
#import "ASIHTTPRequest.h"
#import "JSON.h"
#import "FoodSearchDetailsViewController.h"

@interface FoodSearchListViewController ()
{
     ASIHTTPRequest *req;
     NSArray* ProductList;
}
@end

@implementation FoodSearchListViewController
@synthesize ProductSrial,GET_FORM_BARCODE;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    del =(AppDelegate*) [[UIApplication sharedApplication]delegate];
    NavigationBar*navBar2 =  (NavigationBar*) [del.window viewWithTag:1000000];
    if (navBar2) {
        [navBar2.btnBack setHidden:NO];
        [del.window bringSubviewToFront:navBar2];
    }

    
   
}
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    if (GET_FORM_BARCODE) {
        NSMutableArray *assignedViewControllers=[[NSMutableArray alloc]init];
        NSMutableArray *viewControllers = [self.navigationController.viewControllers mutableCopy];
        for (id viewController in viewControllers) {
            if ([viewController class]!=[BarcodePickerViewController class]) {
                [assignedViewControllers addObject:viewController];
            }
        }
        self.navigationController.viewControllers = assignedViewControllers;

    }
    if (!internetConnection) {
        [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
        HIDE_LOADING
        return;
    }
    [SearchResultsTableView setHidden:YES];
    SearchLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:25];
    TitleLabel.text =[NSString stringWithFormat:@"نتائج البحث عن %@",self.ProductSrial];
    TitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
    [BarcodeButton setTitle:self.ProductSrial forState:UIControlStateNormal];
    SHOW_LOADING;
    [NSThread detachNewThreadSelector:@selector(GetProductData) toTarget:self withObject:nil];
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)ReloadTableData
{
    @try {
        
        if (ProductList==nil) {
            
//            [[[UIAlertView alloc]initWithTitle:@"قطاع الغذاء" message:@"غير مدرج حاليا" delegate:self cancelButtonTitle:@"موافق" otherButtonTitles:nil, nil]show ];
            HIDE_LOADING
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"msgNoData"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];

        }
         [SearchResultsTableView setHidden:NO];
        [SearchResultsTableView reloadData];
        HIDE_LOADING
    }
    @catch (NSException *exception) {
        
    }
   
}

-(void)GetProductData
{
    @try {
        //[self performSelectorOnMainThread:@selector(ShowLoadingView) withObject:nil waitUntilDone:NO];
        req =[[ASIHTTPRequest alloc]initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?barcode=%@", FOOD_SEARCH_URL,self.ProductSrial]]];

        [req setCompletionBlock:^{
            NSString *response=  [req responseString];
            
            id result = [response JSONValue];
            ProductList=result;
            [self performSelectorOnMainThread:@selector(ReloadTableData) withObject:nil waitUntilDone:NO];

        }];
        
        [req startAsynchronous];
    }
    @catch (NSException *exception) {
        
    }
    
}
#pragma  mark tableview datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    FoodSearchCell *cell;
    @try {
        
        cell=(FoodSearchCell*) [tableView dequeueReusableCellWithIdentifier:@"cleFood"];
        if (cell==nil) {
            cell =(FoodSearchCell*) [[[NSBundle mainBundle]loadNibNamed:[[Language getObject]getStringWithKey:@"FoodSearchCell"] owner:nil options:nil]objectAtIndex:0];
        }
        NSLog(@"cell name%@",[[Language getObject]getStringWithKey:@"FoodSearchCell"]);
        NSString *productName,*companyName;
        companyName = [[ProductList objectAtIndex:indexPath.row]objectForKey:@"EstablishmentInfo.ArabicEstablishmentName"];
        productName= [[ProductList objectAtIndex:indexPath.row]objectForKey:@"ItemDescription"];
        cell.ProductNameLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
        cell.CompanyNameLabel.font=[UIFont fontWithName:@"GESSTwoLight-Light" size:12];
        NSLog(@"company length %i",companyName.length);
        if ([[Language getObject]getLanguage]==languageArabic) {
            cell.ProductNameLabel.textAlignment= NSTextAlignmentRight;
            cell.CompanyNameLabel.textAlignment= NSTextAlignmentRight;
        }
        else{
            cell.ProductNameLabel.textAlignment= NSTextAlignmentLeft;
            cell.CompanyNameLabel.textAlignment= NSTextAlignmentLeft;
        }

        cell.ProductNameLabel.text = productName;
        cell.CompanyNameLabel.text = companyName;

    }
    @catch (NSException *exception) {
        
    }

    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [ProductList count];
}
#pragma  mark tableview delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger height=0 ;
    @try {
        NSString *productName,*companyName;
        productName= [[ProductList objectAtIndex:indexPath.row]objectForKey:@"EstablishmentInfo.ArabicEstablishmentName"];
        companyName= [[ProductList objectAtIndex:indexPath.row]objectForKey:@"ItemDescription"];
        CGSize productNameSize = [productName sizeWithFont:[UIFont fontWithName:@"GESSTwoBold-Bold" size:30] constrainedToSize:CGSizeMake(SearchResultsTableView.frame.size.height , 20000000000.0f)];
        CGSize companyNameSize = [companyName sizeWithFont:[UIFont fontWithName:@"GESSTwoLight-Light" size:30] constrainedToSize:CGSizeMake(SearchResultsTableView.frame.size.height , 20000000000.0f)];

        height = productNameSize.height+companyNameSize.height+10;
        
        NSLog(@" heigt for index path row %d  = %d",indexPath.row,height);
    }
    @catch (NSException *exception) {
        
    }

    return height;
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        id selectedProduct = [ProductList objectAtIndex:indexPath.row];
        FoodSearchDetailsViewController *vc_foodDetail = [[FoodSearchDetailsViewController alloc]initWithNibName:@"FoodSearchDetailsViewController" bundle:nil];
        vc_foodDetail.ProductDetails=selectedProduct;
        vc_foodDetail.SerialNumber= ProductSrial;
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [self.navigationController pushViewController:vc_foodDetail animated:YES];
        
    }
    @catch (NSException *exception) {
        
    }

}

- (IBAction)GoBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma -mark uialertview delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    @try {
        [self.navigationController popViewControllerAnimated:YES];
    }
    @catch (NSException *exception) {
        
    }
    
}
@end
