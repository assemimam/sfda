//
//  DrugSearchViewController.m
//  SFDA
//
//  Created by Assem Imam on 10/26/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "ParentSearchViewController.h"
#import "AppDelegate.h"
#import "Global.h"
#import "NavigationBar.h"

#import "JSON.h"
#import "Language.h"
#import "ParentSearchResultViewController.h"

@interface ParentSearchViewController ()
{
    AppDelegate *del;
    ASIHTTPRequest *req;
}
@end

@implementation ParentSearchViewController

#pragma -mark uitextfield delegate 
#pragma -mark uitextfield delegate

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
//    
//    NSUInteger newLength = [textField.text length] + [string length] - range.length;
//    return (newLength > 20) ? NO : YES;
//    
//}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    if (!internetConnection) {
        [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
        return YES;
        
    }
    if (txtDrugName.text.length>0) {
        [self DoSearch];
    }
    
    return YES;
}

-(IBAction)DoSearch
{
    [txtDrugName resignFirstResponder];
    
    if (!internetConnection) {
        [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
        return ;
        
    }
    
    if (txtDrugName.text.length==0) {
        return;
    }
    SHOW_LOADING
    req = [self getRequest];
    
    [req setCompletionBlock:^{
        NSString *response=  [req responseString];
        
        id result = [response JSONValue];
         HIDE_LOADING
        if (result==nil) {
            HIDE_LOADING
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"msgNoData"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
        }
        else{
            ParentSearchResultViewController *vc_searchResult = [self searchResultsViewController];
            vc_searchResult.searchResultList = result;
            [self.navigationController pushViewController:vc_searchResult animated:YES];
        }
        
       
    }];
    [req setFailedBlock:^{
        HIDE_LOADING
        [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"msgNoData"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
        
    }];
    
   
    [req startAsynchronous];


}

- (ASIHTTPRequest *)getRequest {
    //just to make override in childs
    return nil;
}

- (ParentSearchResultViewController *)searchResultsViewController
{
    // Overriden in child
    return nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    del =(AppDelegate*) [[UIApplication sharedApplication]delegate];
    HIDE_LOADING
    NavigationBar*navBar2 =  (NavigationBar*) [del.window viewWithTag:1000000];
    if (navBar2) {
        [navBar2.btnBack setHidden:NO];
        [del.window bringSubviewToFront:navBar2];
    }

  
    [self updateViews];
}

- (void)updateViews
{
    CGRect frame = [self navigationFrame];
    
    if(!IS_DEVICE_RUNNING_IOS_7_AND_ABOVE)
        frame.origin.y = 0.0;
    
    [headerImg setFrame:CGRectMake(headerImg.frame.origin.x, frame.origin.y + frame.size.height, headerImg.frame.size.width, headerImg.frame.size.height)];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

          - (void)viewDidUnload {
              txtDrugName = nil;
              [super viewDidUnload];
          }
@end
