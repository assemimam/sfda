//
//  DrugSearchViewController.h
//  SFDA
//
//  Created by Assem Imam on 10/26/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "MasterViewController.h"

@interface ParentSearchViewController : MasterViewController <UITextFieldDelegate>
{
    __weak IBOutlet UITextField *txtDrugName;
    __weak IBOutlet UIImageView *headerImg;
    
}

-(IBAction)DoSearch;
- (ASIHTTPRequest *)getRequest;

@end
