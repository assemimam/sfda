//
//  DrugSearchViewController.h
//  SFDA
//
//  Created by Assem Imam on 10/26/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentSearchViewController.h"

@interface DrugSearchViewController : ParentSearchViewController
{
    
    __weak IBOutlet UILabel *TitleLabel;
}
@end
