//
//  DrugSearchViewController.m
//  SFDA
//
//  Created by Assem Imam on 10/26/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "MedicalEquipSearchViewController.h"
#import "AppDelegate.h"
#import "ASIHTTPRequest.h"
#import "JSON.h"
#import "Language.h"
#import "Global.h"
#import "MedicalEQPSearchResultViewController.h"

@interface MedicalEquipSearchViewController ()
{
    AppDelegate *del;
    ASIHTTPRequest *req;
}
@end

@implementation MedicalEquipSearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (ASIHTTPRequest *)getRequest
{
    return [[ASIHTTPRequest alloc]initWithURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@MedicalProductsList?query=", SERVER_IP] stringByAppendingString:txtDrugName.text]]];
}

- (ParentSearchResultViewController *)searchResultsViewController
{
    // Overriden in child
    return [[MedicalEQPSearchResultViewController alloc] initWithNibName:@"ParentSearchResultViewController" bundle:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    TitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
  [super viewDidUnload];
}
@end
