//
//  MainScreenNewViewController.h
//  SFDA
//
//  Created by yehia on 9/28/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "MasterViewController.h"

@interface MainScreenNewViewController : MasterViewController<UIScrollViewDelegate>

{
    
    __weak IBOutlet UILabel *lblLoading;
    __weak IBOutlet UIActivityIndicatorView *aiLoading;
    __weak IBOutlet UIView *vLoading;
}
@property (weak, nonatomic) IBOutlet UIView *newsView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
- (IBAction)btnNews:(id)sender;
- (IBAction)btnSFDA:(id)sender;
- (IBAction)btnDrug:(id)sender;
- (IBAction)btnFood:(id)sender;
- (IBAction)btnEService:(id)sender;
- (IBAction)btnMedical:(id)sender;
-(void)getNewsData;
-(void)refreshData;


@end
