//
//  MainScreenNewViewController.m
//  SFDA
//
//  Created by yehia on 9/28/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "MainScreenNewViewController.h"
#import "Language.h"
#import "JSON.h"
#import "JSONKit.h"
#import "ASIHTTPRequest.h"
#import "Global.h"
#import "NewsDetailsViewController.h"
#import "MainScreenViewController.h"
#import "SectorViewController.h"
#import "EServiceViewController.h"
#import "NewsViewController.h"
#import "Global.h"
#import "FMDatabase.h"
#import "NewsSclidingView.h"
@interface MainScreenNewViewController ()
{
    NSMutableArray *newsList;
    ASIHTTPRequest *req;
    UIImage*SelectedPageImage;
    UIImage*DefaultPageImage;
    UIView *pagerView;
    int indexCount;
    UIScrollView *svImages;
    Global *global;
    __weak IBOutlet UIImageView *bottomBar;
}
@end

@implementation MainScreenNewViewController
@synthesize newsView;
@synthesize scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    DefaultPageImage = [UIImage imageNamed:@"WhiteO.png"];
    SelectedPageImage = [UIImage imageNamed:@"OrangeO.png"];
    
    [scrollView setContentSize:CGSizeMake(320,365)];
    NSUserDefaults *userdefults= [NSUserDefaults standardUserDefaults];
    double lasttime=[userdefults doubleForKey:@"timeKey"];
    double now= [[NSDate date] timeIntervalSince1970];
    double diff= now-lasttime;
    
    diff/=60;
    diff/=60;
    if (diff>=3) {
        [self refreshData];
    }
    else
    [self getNewsData];
    
//    [self setEdgesForExtendedLayout:UIRectEdgeNone];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    del =(AppDelegate*) [[UIApplication sharedApplication]delegate];
    HIDE_LOADING
    NavigationBar*navBar2 =  (NavigationBar*) [del.window viewWithTag:1000000];
  
        [navBar2.btnBack setHidden:YES];
        [del.window bringSubviewToFront:navBar2];
        global = [Global getInstance];
        [global setCurrentSectorMenuBtn:[UIImage imageNamed:@"menu_button"]];
        [global setCurrentSectorImage:nil];
        [navBar2.btnMenu setImage:[global currentSectorMenuBtn] forState:UIControlStateNormal];
        [navBar2.btnRefresh addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventTouchUpInside];
 

    

}

- (void)updateViews
{
    CGRect frame = [self navigationFrame];
    
    if(!IS_DEVICE_RUNNING_IOS_7_AND_ABOVE)
        frame.origin.y = 0.0;
    
    [newsView setFrame:CGRectMake(newsView.frame.origin.x, frame.origin.y + frame.size.height, newsView.frame.size.width, newsView.frame.size.height)];
//    [self.navBar setBackgroundColor:[UIColor redColor]];
    
    CGRect tableRect = scrollView.frame;
    tableRect.origin.y = newsView.frame.origin.y + newsView.frame.size.height;
    tableRect.size.height = [UIScreen mainScreen].bounds.size.height - tableRect.origin.y - bottomBar.frame.size.height;
    scrollView.frame = tableRect;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma News_Horizontal_Scroll_View
-(void)RefreshSwitcher
{
    for (UIView *view in [self.newsView subviews]) {
        [view removeFromSuperview];
    }
    float Button_X = 0;
    float newsViewHeight = self.newsView.frame.size.height;
    svImages = [[UIScrollView alloc] initWithFrame:CGRectMake(0, (newsViewHeight - 100.0) / 2.0, 320, 100)];
    svImages.pagingEnabled = YES;
    svImages.delegate=self;
    [svImages setBackgroundColor:[UIColor clearColor]];
    [svImages setShowsHorizontalScrollIndicator:NO];
    int index = 0;
    if ([newsList count] > 0) {
        
        if([[Language getObject] getLanguage] == languageArabic){
            for(int i=[newsList count]-1;i>=0;i--){
                [self drawNewsViewAt:Button_X :i];
                Button_X+= 320;
            }
        }else{
            for (NSDictionary *dict in newsList) {
                if (index > [newsList count]) {
                    index = 0;
                }
                [self drawNewsViewAt:Button_X :index];
                Button_X+= 320;
                index++;
            }
        }
        
        [self DrawPager];
        [svImages setContentSize:CGSizeMake(((320 +2) * [newsList count]), 95)];
        [svImages setContentOffset:CGPointMake(([newsList count]-1)*320,0) animated:YES];
        [newsView addSubview:svImages];
    }
    
    //hide loading view
    [aiLoading stopAnimating];
    [vLoading setHidden:YES];
    [self.view sendSubviewToBack:vLoading];
}

-(void)drawNewsViewAt:(int)x :(int)index{

    
    NewsSclidingView *newsSclidingView =(NewsSclidingView*) [[[NSBundle mainBundle]loadNibNamed:@"NewsSclidingView" owner:nil options:nil]objectAtIndex:0];
    
    if([[Language getObject] getLanguage] == languageArabic){
        [newsSclidingView.newsTitle setText:[[newsList objectAtIndex:index] objectForKey:@"TitleAr"]];
    }else{
        [newsSclidingView.newsTitle setText:[[newsList objectAtIndex:index] objectForKey:@"TitleEn"]];
    }
   
    newsSclidingView.newsTitle.font = [UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[newsList objectAtIndex:index] objectForKey:@"ImageUrl"]]];
        if (imageData==nil|| imageData.length==0) {
            imageData = [NSData dataWithContentsOfFile:[[newsList objectAtIndex:index] objectForKey:@"imagePath"]];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            newsSclidingView.newsImage.image = [UIImage imageWithData:imageData];
        });
    });
    
    
    
    NSArray* dateArray = [[[newsList objectAtIndex:index] objectForKey:@"PubDate"]componentsSeparatedByString: @" "];
    [newsSclidingView.newsDate  setText:[dateArray objectAtIndex:0]];
    
    
   
    [newsSclidingView setFrame:CGRectMake(x, 0, svImages.frame.size.width, svImages.frame.size.height)];
    newsSclidingView.backgroundColor = [UIColor clearColor];
    newsSclidingView.tag =  index; //[[[newsList objectAtIndex:index] objectForKey:@"$id"] intValue];
    
    
    UITapGestureRecognizer *singleTouch = [[UITapGestureRecognizer alloc]
                                           initWithTarget:self action:@selector(handleSingleTouch:)];
    singleTouch.numberOfTapsRequired = 1;
    [newsSclidingView addGestureRecognizer:singleTouch];
    [newsSclidingView setBackgroundColor:[UIColor clearColor]];
    [newsSclidingView setUserInteractionEnabled:YES];
    
    

  
    [svImages addSubview:newsSclidingView];
          
}

-(void)DrawPager{
    
    float ImageWidth=7;
    float ImageHeight=7;
    float Image_X=305;
    
    
    pagerView = [[UIView alloc] initWithFrame:CGRectMake(0, svImages.frame.size.height + svImages.frame.origin.y, 100, 10)];
    pagerView.backgroundColor = [UIColor clearColor];
    
    for (int page =[newsList count]; page>=1 ; page--) {
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(Image_X, 0, ImageWidth, ImageHeight)]  ;
        img.tag = page;
        if (page == 1) {
            img.image = SelectedPageImage;
        }
        else {
            img.image = DefaultPageImage;
        }
        
        [pagerView addSubview:img];
        Image_X-=(ImageWidth+5);
    }
    
    [newsView addSubview:pagerView];
   // [newsView bringSubviewToFront:pagerView];
}

-(void)SetPagerSelectePage:(NSInteger)index
{
    
    for (int page = 1; page<= [newsList count]; page++) {
        [(UIImageView*) [pagerView viewWithTag:page] setImage:DefaultPageImage];
    }
    [(UIImageView*)  [pagerView viewWithTag:index+1] setImage:SelectedPageImage];
}

-(void)handleSingleTouch:(UITapGestureRecognizer*)sender
{
    NSLog(@"handle bgad %d",sender.view.tag);
    
    NSString *nibName = [[Language getObject] getStringWithKey:@"NewsDetailsViewController"];
    [global setCurrentSectorImage:[UIImage imageNamed:@"orgSecTitle.png"]];
    NewsDetailsViewController *newsDetails = [[NewsDetailsViewController alloc]initWithNibName:nibName bundle:nil];
    newsDetails.newsObj = [newsList objectAtIndex:sender.view.tag];
    [self.navigationController pushViewController:newsDetails animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)_scrollView {
    if (_scrollView == self.scrollView) {
        return;
    }
    @try {
        
        float scrollViewWidth = _scrollView.frame.size.width;
        float scrollContentSizeWidth = _scrollView.contentSize.width;
        float scrollOffset = _scrollView.contentOffset.x;
        
        if (scrollOffset + scrollViewWidth == scrollContentSizeWidth)
        {
            [svImages setContentOffset:CGPointMake(([newsList count]-1)*320,0) animated:YES];
            //            indexCount = (int)(imagesScrollView.contentOffset.x /imagesScrollView.frame.size.width);
            //            //  [delegate didMoveImageAtIndex:(NSInteger)[self.ImageIDS  objectAtIndex:indexCount]];
            //            [delegate didMoveImageAtIndex:(NSInteger)indexCount];
        }else{
            indexCount = (int)(_scrollView.contentOffset.x /_scrollView.frame.size.width);
            //[svImages didMoveImageAtIndex:(NSInteger)indexCount];
            [self SetPagerSelectePage:(NSInteger)indexCount];
        }
        
        
        
        
        //        SLIDING_ENBLED = NO;
        //NSLog(@"did scroll   %i",indexCount);
        
        
        // [delegate didMoveAdAtIndex:(NSInteger)[self.ImageIDS  objectAtIndex:indexCount]];
        
    }
    @catch (NSException *exception) {
        
    }
    
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)_scrollView

{
    if (_scrollView == self.scrollView) {
        return;
    }
    @try {
    
        ////////NSLog(@"did end   %i",indexCount);
        indexCount = (int)(_scrollView.contentOffset.x /_scrollView.frame.size.width);
        // //////NSLog(@"did scroll   %i",indexCount);
        
        //[delegate didMoveImageAtIndex:(NSInteger)indexCount];
        //[delegate didMoveImageAtIndex:(NSInteger)[self.ImageIDS  objectAtIndex:indexCount]];
        // [self setSlidingEnabled:YES];
        // SLIDING_ENBLED = YES;
    }
    @catch (NSException *exception) {
        
    }
    
    
}

-(void)getNewsData
{
    
    for (UIView *view in [self.newsView subviews]) {
        [view removeFromSuperview];
    }
    
    //show loading view
    lblLoading.text = [[Language getObject]getStringWithKey:@"LoadingTitle"];
    [aiLoading startAnimating];
    [vLoading setHidden:NO];
    [self.view bringSubviewToFront:vLoading];
    
    
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:NEWS_SCROLL_MAIN_TAG]){
        [self newsGetAllData];
        NSLog(@"%d",[newsList count]);
       // [self performSelectorOnMainThread:@selector(RefreshSwitcher) withObject:nil waitUntilDone:NO];
        [self RefreshSwitcher];
        
    }else{
        if (!internetConnection) {
            //hide loading view
            
           
            
            [aiLoading stopAnimating];
            [vLoading setHidden:YES];
            [self.view sendSubviewToBack:vLoading];
            HIDE_LOADING
            return;
        }
        
        req =[[ASIHTTPRequest alloc]initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/News?Section=SFDA&count=20", SERVER_IP]]];
        
        __weak ASIHTTPRequest *reqWeak = req;
        
        
        [req setCompletionBlock:^{
            NSString *response=  [reqWeak responseString];
            
            id result = [response JSONValue];
            newsList=result;
            
            if(newsList != nil && [newsList count]>0){
                [self deleteData];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:NEWS_SCROLL_MAIN_TAG];
            }
            
            for(NSDictionary *obj in newsList)
            {
                
                NSString *url = [obj objectForKey:@"ImageUrl"];
                
                dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                //this will start the image loading in bg
                dispatch_async(concurrentQueue, ^{
                    
                    NSURL *imgUrl = [NSURL URLWithString:url];
                    NSURLRequest* imgRequest = [NSURLRequest requestWithURL:imgUrl];
                    NSData *newsImageData = [[NSData alloc]initWithData:[NSURLConnection sendSynchronousRequest:imgRequest returningResponse:nil error:nil]];
                    imgRequest=nil;
                    
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    NSString *Documentpath = [paths objectAtIndex:0];
                    NSString *TargetPath = [Documentpath stringByAppendingPathComponent:@"NewsScrollImages"];
                    
                    NSFileManager *Fmanager = [NSFileManager defaultManager];
                    
                    if ([Fmanager fileExistsAtPath:TargetPath isDirectory:nil]) {
                        
                    }
                    else
                    {
                        BOOL ret =[Fmanager createDirectoryAtPath:TargetPath withIntermediateDirectories:YES attributes:nil error:nil];
                        if (ret) {
                            
                        };
                    }
                    
                    NSString *fileName= [TargetPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",[obj objectForKey:@"$id"]] ];

                    //this will set the image when loading is finished
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //save image file
                        if (newsImageData!=nil &&newsImageData.length > 0 ) {
        
                            [newsImageData writeToFile:fileName atomically:YES];
                            
                            NSMutableDictionary *newsDic =[[NSMutableDictionary alloc]init];
                            for (NSString *key in [obj allKeys]) {
                                [newsDic setValue:[obj objectForKey:key] forKey:key];
                            }
                            
                            [newsDic setValue:fileName forKey:@"ImagePath"];

//                            [self insertNewsInformation:newsDic];
//                            [self newsGetAllData];
                            [self performSelectorOnMainThread:@selector(insertNewsInformation:) withObject:newsDic waitUntilDone:NO];
                        }
                                               
                    });
                });
   
            }

//             [self performSelectorOnMainThread:@selector(RefreshSwitcher) withObject:nil waitUntilDone:NO];
            [self RefreshSwitcher];
        }];
        [req setFailedBlock:^{
            NSLog(@"error net");
        }];
        [req startAsynchronous];
    }
    NSUserDefaults * userdefualt=[NSUserDefaults standardUserDefaults];
    [userdefualt setDouble:[[NSDate date] timeIntervalSince1970] forKey:@"timeKey"];
}

-(void)insertNewsInformation:(id)obj
{
    
    [self insertNewsData:[obj objectForKey:@"$id"] :[obj objectForKey:@"TitleAr"] :[obj objectForKey:@"TitleEn"] :[obj objectForKey:@"DescriptionAr"] :[obj objectForKey:@"DescriptionEn"] :[obj objectForKey:@"Url"] :[obj objectForKey:@"PubDate"]:[obj objectForKey:@"ImagePath"]:[obj objectForKey:@"ImageUrl"]];
    
   
}
- (IBAction)btnSFDA:(id)sender {
    [global setCurrentSectorImage:nil ];
    [global setCurrentSectorMenuBtn:[UIImage imageNamed:@"menu_button.png"]];
    [global setCurrentSectorColor:nil];
    [global setCurrentSectorSubImage:nil];
    
    MainScreenViewController *old_main = [[MainScreenViewController alloc] init];
    [self.navigationController pushViewController:old_main animated:YES];
}

- (IBAction)btnDrug:(id)sender {
    
    [global setCurrentSectorImage:[UIImage imageNamed:@"drugSecTitle.png" ]];
    [global setCurrentSectorMenuBtn:[UIImage imageNamed:@"hoe button drug.png" ]];
    [global setCurrentSectorColor:[global colorFromHexString:@"#0973ba"]];
    [global setCurrentSectorSubImage:[UIImage imageNamed:@"drug sub blue tile.png" ]];
    
    SectorViewController*VC_Sector = [[SectorViewController alloc]init];
    VC_Sector.Sector=DRUGS_SECTOR;
    [self.navigationController pushViewController:VC_Sector animated:YES];
}

- (IBAction)btnFood:(id)sender {
    [global setCurrentSectorImage:[UIImage imageNamed:@"foodSecTitle.png" ]];
    [global setCurrentSectorMenuBtn:[UIImage imageNamed:@"home button good.png"]];
    [global setCurrentSectorColor:[global colorFromHexString:@"#3b7350"]];
    [global setCurrentSectorSubImage:[UIImage imageNamed:@"food sub green tile.png" ]];
    
    SectorViewController*VC_Sector = [[SectorViewController alloc]init];
    VC_Sector.Sector=FOODS_SECTOR;
    [self.navigationController pushViewController:VC_Sector animated:YES];
}

- (IBAction)btnEService:(id)sender {
    [global setCurrentSectorImage:[UIImage imageNamed:@"serviceSecTitle.png" ]];
    [global setCurrentSectorMenuBtn:[UIImage imageNamed:@"home button main service.png"]];
    [global setCurrentSectorColor:[global colorFromHexString:@"#483e3d"]];
    [global setCurrentSectorSubImage:nil];
    
    NSString *nibName = [[Language getObject] getStringWithKey:@"EServiceViewController"];
    
    EServiceViewController *service = [[EServiceViewController alloc]initWithNibName:nibName bundle:nil];
    [self.navigationController pushViewController:service animated:YES];
}

- (IBAction)btnNews:(id)sender {
    [global setCurrentSectorImage:[UIImage imageNamed:@"newSecTitle.png" ]];
    [global setCurrentSectorMenuBtn:[UIImage imageNamed:@"home button news.png"]];
    [global setCurrentSectorColor:[global colorFromHexString:@"#5b4f6b"]];
    [global setCurrentSectorSubImage:nil];
    
    NSString *nibName = [[Language getObject] getStringWithKey:@"NewsViewController"];
    
    NewsViewController *contact = [[NewsViewController alloc]initWithNibName:nibName bundle:nil];
    [self.navigationController pushViewController:contact animated:YES];
}

-(IBAction)btnMedical:(id)sender{
    [global setCurrentSectorImage:[UIImage imageNamed:@"medSecTitle.png" ]];
    [global setCurrentSectorMenuBtn:[UIImage imageNamed:@"home button medical.png"]];
    [global setCurrentSectorColor:[global colorFromHexString:@"#a66136"]];
    [global setCurrentSectorSubImage:[UIImage imageNamed:@"medical sub orrange tile.png" ]];
    
    SectorViewController*VC_Sector = [[SectorViewController alloc]init];
    VC_Sector.Sector=MEDICAL_SECTOR;
    [self.navigationController pushViewController:VC_Sector animated:YES];
}
- (void)viewDidUnload {
    [self setScrollView:nil];
    vLoading = nil;
    aiLoading = nil;
    lblLoading = nil;
    [super viewDidUnload];
}

#pragma  mark DataBase functions
- (void)insertNewsData:(NSString*)nid :(NSString*)title_ar :(NSString*)title_en :(NSString*)content_ar :(NSString*)content_en :(NSString*)link :(NSString*)date :(NSString*)image_path :(NSString*)image_url {
    
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    NSString* lang = @"en";
    if([[Language getObject] getLanguage] == languageArabic)
        lang = @"ar";
    
    
    NSString *insertQuery = [@"insert into news_scroll " stringByAppendingFormat:@" VALUES ('%@', '%@','%@','%@','%@','%@','%@','%@','%@','%@')",link,date,@"Main",nid,image_path,image_url,title_ar,title_en,content_ar,content_en];
    BOOL insterted = [database executeUpdate:insertQuery];
    
    NSLog(insterted ? @"Yes" : @"No");
    
    [database close];
}

-(void) newsGetAllData{
    // Getting the database path.
    [newsList removeAllObjects];
    newsList = nil;
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    NSString *sqlSelectQuery = @"SELECT * FROM news_scroll where  section='Main' order by id asc";
    
    
    NSLog(@"%@",sqlSelectQuery);
    
    newsList = [[NSMutableArray alloc] init];
    
    // Query result
    FMResultSet *resultsWithNameLocation = [database executeQuery:sqlSelectQuery];
    while([resultsWithNameLocation next]) {
        NSString *news_title_ar = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"title_ar"]];
        NSString *news_title_en = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"title_en"]];
        NSString *news_content_ar = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"content_ar"]];
        NSString *news_content_en = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"content_en"]];
        NSString *news_link = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"link"]];
        NSString *news_date = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"PubDate"]];
        NSString *news_id = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"id"]];
        NSString *image_path = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"image_path"]];
        
        NSMutableDictionary *newsDict = [[NSMutableDictionary alloc]init];
        if([[Language getObject] getLanguage] == languageArabic){
            [newsDict setValue:news_title_ar forKey:@"TitleAr"];
            [newsDict setValue:news_content_ar forKey:@"DescriptionAr"];
        }else{
            [newsDict setValue:news_title_en forKey:@"TitleEn"];
            [newsDict setValue:news_content_en forKey:@"DescriptionEn"];
        }
        
        [newsDict setValue:news_link forKey:@"Url"];
        [newsDict setValue:news_date forKey:@"PubDate"];
        [newsDict setValue:news_id forKey:@"$id"];

           [newsDict setValue:image_path forKey:@"imagePath"];
        [newsList addObject:newsDict ];
        
        // loading your data into the array, dictionaries.
    }
    [database close];
}

- (void)deleteData {
    
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    NSString *deleteQuery = @"DELETE FROM news_scroll where section='Main'";
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    BOOL deleted = [database executeUpdate:deleteQuery];
    NSLog(deleted ? @"Yes" : @"No");
    [database close];
}
-(void)refreshData
{
    
    if (!internetConnection)
    {
        [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
        
        //[self.ContainerView setUserInteractionEnabled:YES];
        
        HIDE_LOADING
        //hide loading view
        [aiLoading stopAnimating];
        [vLoading setHidden:YES];
        [self.view sendSubviewToBack:vLoading];
    }
    

    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:NEWS_ALL_MAIN_TAG];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:NEWS_SCROLL_MAIN_TAG];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:NEWS_SCROLL_SFDA_TAG];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:NEWS_SCROLL_DRUG_TAG];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:NEWS_SCROLL_FOOD_TAG];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:NEWS_SCROLL_MEDI_TAG];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:ABOUT_SECTION_SFDA_TAG];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:ABOUT_SECTION_FOOD_TAG];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:ABOUT_SECTION_DRUG_TAG];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:ABOUT_SECTION_MEDI_TAG];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:AWARENESS_SECTION_SFDA_TAG];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:AWARENESS_SECTION_FOOD_TAG];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:AWARENESS_SECTION_DRUG_TAG];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:AWARENESS_SECTION_MEDI_TAG];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:QUESTION_SECTION_FOOD_TAG];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:QUESTION_SECTION_DRUG_TAG];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:QUESTION_SECTION_MEDI_TAG];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:CONTACT_US_SECTION_TAG];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IMPORTANT_LINKS_SECTION_TAG];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:E_SERVICE_SECTION_TAG];
    
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:MEMBERS_CASH_KEY];
    [[NSUserDefaults standardUserDefaults]  synchronize];
    
    [self getNewsData];
}

- (void)viewDidAppear:(BOOL)animated
{
    if(IS_DEVICE_RUNNING_IOS_7_AND_ABOVE)
        [self updateViews];
    
    [super viewDidAppear:animated];
}

@end
