//
//  CollabsableQuestionsViewColtroller.m
//  SFDA
//
//  Created by Assem Imam on 5/13/14.
//  Copyright (c) 2014 NUITEX. All rights reserved.
//

#import "CollabsableQuestionsViewColtroller.h"
#import "ASIHTTPRequest.h"
#import "QuestionCell.h"
#import "Language.h"
#import "JSON.h"
#import "JSONKit.h"
#import "FMDatabase.h"
#import "Global.h"

@interface CollabsableQuestionsViewColtroller ()
{
    NSMutableArray *optionsList;
    ASIHTTPRequest *req;
    NSString *currentSectionName;
    NSString *currentSectionCachingKey;
    NSInteger expandedRowIndex;
    NSMutableArray *arrCells;
    QuestionCell *currentCell;
}

@end

@implementation CollabsableQuestionsViewColtroller
@synthesize parameter;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    expandedRowIndex = -1;
   [self performSelector:@selector(getData) withObject:nil];
    currentCell=(QuestionCell*) [[[NSBundle mainBundle]loadNibNamed:@"QuestionCell_ar" owner:nil options:nil]objectAtIndex:0];
    questionsTableView.decelerationRate =0.152072;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NavigationBar *navBar2 =  (NavigationBar*) [del.window viewWithTag:1000000];
    if (navBar2) {
        [navBar2.btnBack setHidden:NO];
        [del.window bringSubviewToFront:navBar2];
    }
    
    [self updateViews];
}
-(void)viewWillDisappear:(BOOL)animated{
    
    if(![req isCancelled]) {
        // Cancels an asynchronous request
        [req cancel];
        // Cancels an asynchronous request, clearing all delegates and blocks first
        [req clearDelegatesAndCancel];
    }
    
    [super viewWillDisappear:animated];
}

#pragma -ark uiviewcontroller methods
- (void)updateViews
{
    CGRect frame = [self navigationFrame];
    
    if(!IS_DEVICE_RUNNING_IOS_7_AND_ABOVE)
        frame.origin.y = 0.0;
    
    [questionsTableView setFrame:CGRectMake(questionsTableView.frame.origin.x, frame.origin.y + frame.size.height, questionsTableView.frame.size.width, questionsTableView.frame.size.height - frame.origin.y)];
}
-(void)getData
{
    
    @try{
        
        
        if([parameter isEqualToString:@"Food"]){
            currentSectionCachingKey = QUESTION_SECTION_FOOD_TAG;
        }else if([parameter isEqualToString:@"Medical"]){
            currentSectionCachingKey = QUESTION_SECTION_MEDI_TAG;
        }else if([parameter isEqualToString:@"Drug"]){
            currentSectionCachingKey = QUESTION_SECTION_DRUG_TAG;
        }
        
        currentSectionName = parameter;
        parameter =[@"?sectionName=" stringByAppendingString:currentSectionName];
        
        
        if([[NSUserDefaults standardUserDefaults] boolForKey:currentSectionCachingKey]){
            
            [self QuestionGetAllData];
            
            NSLog(@"%d",[optionsList count]);
            
            [questionsTableView reloadData];
        }else{
            NSString *url = [[NSString stringWithFormat:@"%@FAQs", SERVER_IP] stringByAppendingString:parameter];
            NSLog(@"%@",url);
            if (!internetConnection) {
                [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
                HIDE_LOADING
                return;
            }
            
            SHOW_LOADING
            req =[[ASIHTTPRequest alloc]initWithURL:[NSURL URLWithString:url]];
            
            [req setCompletionBlock:^{
                NSString *response=  [req responseString];
                
                id result = [response JSONValue];
                NSMutableArray *itemsList = [[NSMutableArray alloc]init];
                for (id item in result) {
                    NSMutableDictionary *ParentItem =[[NSMutableDictionary alloc]init];
                    NSMutableDictionary *SubItem =[[NSMutableDictionary alloc]init];
                    [SubItem setObject:[item objectForKey:@"AnswerAr"] forKey:@"Name"];
                    [SubItem setObject:@"1" forKey:@"level"];
                    
                    [ParentItem setObject:[item objectForKey:@"QuestionAr"] forKey:@"Name"];
                    [ParentItem setObject:@"0" forKey:@"level"];
                    [ParentItem setObject:[NSArray arrayWithObject:SubItem] forKey:@"SubItems"];
                    [ParentItem setObject:@"0" forKey:@"expanded"];
                    [itemsList addObject:ParentItem];
                    
                }
                optionsList=itemsList;
                
                if(result != nil && [result count]>0){
                    [self deleteData];
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:currentSectionCachingKey];
                    
                }
                
                for (int i=0; i<[result count]; i++) {
                    
                    
                    NSString *qAr = [[result objectAtIndex:i] objectForKey:@"QuestionAr"];
                    NSString *qEn = [[result objectAtIndex:i] objectForKey:@"QuestionEn"];
                    
                    NSString *ansAr = [[result objectAtIndex:i] objectForKey:@"AnswerAr"];
                    NSString *ansEn = [[result objectAtIndex:i] objectForKey:@"AnswerEn"];
                    
                    if([[Language getObject] getLanguage] == languageArabic){
                        [self insertQuestionData:qAr :ansAr];
                    }else{
                        [self insertQuestionData:qEn :ansEn];
                    }
                }
                
                
                HIDE_LOADING
                [questionsTableView reloadData];
                
            }];
            [req setFailedBlock:^{
                
                HIDE_LOADING
                
                [self QuestionGetAllData];
                
                if(optionsList != nil && [optionsList count] >0){
                    
                    [questionsTableView reloadData];
                }else{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"يرجى التحقق من اتصالك مع الإنترنت" delegate:self cancelButtonTitle:@"موافق" otherButtonTitles:nil];
                    [alert show];
                    NSLog(@"%@",[req error]);
                }
            }];
            [req startAsynchronous];
        }
        
    }@catch (NSException *ex) {
        
    }
}

#pragma -mark uitable view methods
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [optionsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *Question= [[[optionsList objectAtIndex:indexPath.row] objectForKey:@"Name"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    QuestionCell *cell; //= (QuestionCell*)[tableView dequeueReusableCellWithIdentifier:@"cleQuestion"];
    if (cell==nil){
        cell = (QuestionCell*)[[[NSBundle mainBundle]loadNibNamed:@"QuestionCell_ar" owner:nil options:nil]objectAtIndex:0];
    }
    
    NSMutableAttributedString* TitleAttrString = [[NSMutableAttributedString  alloc]initWithString:Question];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setLineSpacing:15];
    [style setAlignment:NSTextAlignmentRight];
    [TitleAttrString addAttribute:NSParagraphStyleAttributeName
                            value:style
                            range:NSMakeRange(0, Question.length)];
    
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = [UIColor clearColor];
    cell.selectedBackgroundView = bgView;
    cell.questionTextView.text = Question;
    cell.questionTextView.textColor = [UIColor blackColor];
    [cell setIndentationLevel:[[[optionsList objectAtIndex:indexPath.row] objectForKey:@"level"] intValue]];
    cell.indentationWidth = 25;
    
    float indentPoints = cell.indentationLevel * cell.indentationWidth;
    
    cell.contentView.frame = CGRectMake(indentPoints,cell.contentView.frame.origin.y,cell.contentView.frame.size.width - indentPoints,cell.contentView.frame.size.height);
    
    NSDictionary *d1=[optionsList objectAtIndex:indexPath.row] ;
    
    if([d1 objectForKey:@"SubItems"])
    {
        cell.questionTextView.font = [UIFont fontWithName:@"GESSTwoBold-Bold" size:13];
        cell.QuestionImageView.alpha = 1.0;
        cell.QuestionImageView.tag=indexPath.row;
        // [cell.QuestionImageView addTarget:self action:@selector(showSubItems:) forControlEvents:UIControlEventTouchUpInside];
        if ([[[optionsList objectAtIndex:indexPath.row] objectForKey:@"expanded"]intValue]==1) {
            if([currentSectionName isEqualToString:@"Food"]){
                
                [cell.QuestionImageView setImage:[UIImage imageNamed:@"faqgreenselcted.png"] ];
                
            }
            if([currentSectionName isEqualToString:@"Medical"]){
                
                [cell.QuestionImageView setImage:[UIImage imageNamed:@"faqselcted.png"] ];
                
            }
            if([currentSectionName isEqualToString:@"Drug"]){
                [cell.QuestionImageView setImage:[UIImage imageNamed:@"faqdrugic.png"] ];
            }
        }
        
    }
    else
    {
        [cell.QuestionImageView setImage:[UIImage imageNamed:@"faq.png"] ];
        cell.questionTextView.font = [UIFont fontWithName:@"GESSTwoLight-Light" size:15];
        cell.QuestionImageView.alpha = 0.0;
    }
  
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *Title= [[optionsList objectAtIndex:indexPath.row] objectForKey:@"Name"] ;

    
    CGSize QuestionSize  =  [Title sizeWithFont:[UIFont fontWithName:@"GESSTwoLight-Light" size:16] constrainedToSize:CGSizeMake(280, MAXFLOAT)];
    
    return  MAX(QuestionSize.height+20,40);
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic=[optionsList objectAtIndex:indexPath.row];
    if([dic objectForKey:@"SubItems"])
    {
        NSArray *arr=[dic objectForKey:@"SubItems"];
        BOOL isTableExpanded=NO;
        QuestionCell *cell=(QuestionCell*)[tableView cellForRowAtIndexPath:indexPath];
        for(NSDictionary *subitems in arr )
        {
            NSInteger index=[optionsList indexOfObjectIdenticalTo:subitems];
            isTableExpanded=(index>0 && index!=NSIntegerMax);
            if(isTableExpanded) break;
        }
        
        if(isTableExpanded)
        {
            [self CollapseRows:arr];
            [[optionsList objectAtIndex:indexPath.row] setObject:@"0" forKey:@"expanded"];
            [cell.QuestionImageView setImage:[UIImage imageNamed:@"faq.png"] ];
            
        }
        else
        {
            [[optionsList objectAtIndex:indexPath.row] setObject:@"1" forKey:@"expanded"];
            if([currentSectionName isEqualToString:@"Food"]){
                [cell.QuestionImageView setImage:[UIImage imageNamed:@"faqgreenselcted.png"] ];
            }
           if([currentSectionName isEqualToString:@"Medical"]){
                [cell.QuestionImageView setImage:[UIImage imageNamed:@"faqselcted.png"]];
            }
            if([currentSectionName isEqualToString:@"Drug"]){
                [cell.QuestionImageView setImage:[UIImage imageNamed:@"faqdrugic.png"]];
            }

            
            NSUInteger count=indexPath.row+1;
            arrCells=[NSMutableArray array];
            for(NSDictionary *dInner in arr )
            {
                [arrCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
                [optionsList insertObject:dInner atIndex:count++];
            }
            [questionsTableView insertRowsAtIndexPaths:arrCells withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}

-(void)CollapseRows:(NSArray*)ar
{
	for(NSDictionary *dInner in ar )
    {
		NSUInteger indexToRemove=[optionsList indexOfObjectIdenticalTo:dInner];
        QuestionCell *cell=(QuestionCell*)[questionsTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexToRemove inSection:0]];
        
		NSArray *arInner=[dInner objectForKey:@"SubItems"];
		if(arInner && [arInner count]>0)
        {
			[self CollapseRows:arInner];
            [cell.QuestionImageView setImage:[UIImage imageNamed:@"faq.png"] ];
		}
		
		if([optionsList indexOfObjectIdenticalTo:dInner]!=NSNotFound)
        {
			[optionsList removeObjectIdenticalTo:dInner];
			[questionsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:
                                                        [NSIndexPath indexPathForRow:indexToRemove inSection:0]
                                                        ]
                                      withRowAnimation:UITableViewRowAnimationNone];
        }
	}
}



-(void)showSubItems :(UIButton*) sender
{
    UIButton *btn = (UIButton*)sender;
    CGRect buttonFrameInTableView = [btn convertRect:btn.bounds toView:questionsTableView];
    QuestionCell *cell = (QuestionCell*)[questionsTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    
    
    NSDictionary *d=[optionsList objectAtIndex:sender.tag] ;
    NSArray *arr=[d objectForKey:@"SubItems"];
    if([d objectForKey:@"SubItems"])
    {
        BOOL isTableExpanded=NO;
        for(NSDictionary *subitems in arr )
        {
            NSInteger index=[optionsList indexOfObjectIdenticalTo:subitems];
            isTableExpanded=(index>0 && index!=NSIntegerMax);
            if(isTableExpanded) break;
        }
        
        if(isTableExpanded)
        {
            [self CollapseRows:arr];
            [[optionsList objectAtIndex:sender.tag] setObject:@"0" forKey:@"expanded"];
        }
        else
        {
            [[optionsList objectAtIndex:sender.tag] setObject:@"1" forKey:@"expanded"];
             if([currentSectionName isEqualToString:@"Food"]){
                if ([[cell.QuestionImageView image] isEqual:[UIImage imageNamed:@"faqgreenselcted.png"]])
                {
                    [cell.QuestionImageView setImage:[UIImage imageNamed:@"faq.png"] ];
                }
                else
                {
                    [cell.QuestionImageView setImage:[UIImage imageNamed:@"faqgreenselcted.png"]] ;
                }
            }
            if([currentSectionName isEqualToString:@"Medical"]){
                if ([[cell.QuestionImageView image] isEqual:[UIImage imageNamed:@"faqselcted.png"]])
                {
                    [cell.QuestionImageView setImage:[UIImage imageNamed:@"faq.png"] ];
                }
                else
                {
                    [cell.QuestionImageView setImage:[UIImage imageNamed:@"faqselcted.png"] ];
                }
            }
            if([currentSectionName isEqualToString:@"Drug"]){
                if ([[cell.QuestionImageView image] isEqual:[UIImage imageNamed:@"faqselcted.png"]])
                {
                    [cell.QuestionImageView setImage:[UIImage imageNamed:@"faq.png"] ];
                }
                else
                {
                    [cell.QuestionImageView setImage:[UIImage imageNamed:@"faqdrugic.png"] ];
                }
            }
            
            
            NSUInteger count=sender.tag+1;
            arrCells=[NSMutableArray array];
            for(NSDictionary *dInner in arr )
            {
                [arrCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
                [optionsList insertObject:dInner atIndex:count++];
            }
            [questionsTableView insertRowsAtIndexPaths:arrCells withRowAnimation:UITableViewRowAnimationNone];
        }
    }
    
    
}

#pragma  mark DataBase functions
- (void)insertQuestionData:(NSString*)name :(NSString*)value{
    
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    NSString* lang = @"en";
    if([[Language getObject] getLanguage] == languageArabic)
        lang = @"ar";
    
    
    NSString *insertQuery = [@"insert into question " stringByAppendingFormat:@" VALUES ('%@','%@', '%@','%@')", name,value,currentSectionName,lang];
    BOOL insterted = [database executeUpdate:insertQuery];
    
    NSLog(insterted ? @"Yes" : @"No");
    
    [database close];
}

-(void) QuestionGetAllData{
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    NSString *sqlSelectQuery = [[@"SELECT * FROM question where lang='en' and sectionName='" stringByAppendingString:currentSectionName] stringByAppendingString:@"'"];
    if([[Language getObject] getLanguage] == languageArabic)
        sqlSelectQuery = [[@"SELECT * FROM question where lang='ar' and sectionName='" stringByAppendingString:currentSectionName] stringByAppendingString:@"'"];
    
    NSLog(@" query ==> %@",sqlSelectQuery);
    
    optionsList = [[NSMutableArray alloc] init];
    
    // Query result
    FMResultSet *resultsWithNameLocation = [database executeQuery:sqlSelectQuery];
    
    while([resultsWithNameLocation next]) {
        NSString *question_name = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"question"]];
        NSString *question_answer = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"answer"]];
        
        NSMutableDictionary *ParentItem =[[NSMutableDictionary alloc]init];
        NSMutableDictionary *SubItem =[[NSMutableDictionary alloc]init];
        [SubItem setObject:question_answer forKey:@"Name"];
        [SubItem setObject:@"1" forKey:@"level"];
        
        [ParentItem setObject:question_name forKey:@"Name"];
        [ParentItem setObject:@"0" forKey:@"level"];
        [ParentItem setObject:[NSArray arrayWithObject:SubItem] forKey:@"SubItems"];
        [ParentItem setObject:@"0" forKey:@"expanded"];
        [optionsList addObject:ParentItem ];
        
        // loading your data into the array, dictionaries.
    }
    [database close];
}

- (void)deleteData {
    
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    NSString *deleteQuery = [[@"DELETE FROM question where lang='en' and sectionName='" stringByAppendingString:currentSectionName] stringByAppendingString:@"'"];
    if([[Language getObject] getLanguage] == languageArabic)
        deleteQuery = [[@"DELETE FROM question where lang='ar' and sectionName='" stringByAppendingString:currentSectionName] stringByAppendingString:@"'"];
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    BOOL sss = [database executeUpdate:deleteQuery];
    
    NSLog(sss ? @"Yes" : @"No");
    [database close];
}
@end
