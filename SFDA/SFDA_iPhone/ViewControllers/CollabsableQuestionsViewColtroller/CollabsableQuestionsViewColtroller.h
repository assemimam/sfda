//
//  CollabsableQuestionsViewColtroller.h
//  SFDA
//
//  Created by Assem Imam on 5/13/14.
//  Copyright (c) 2014 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"
@interface CollabsableQuestionsViewColtroller : MasterViewController<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate>
{
    
    __weak IBOutlet UITableView *questionsTableView;
}

@property (weak, nonatomic) NSString *parameter;
@end
