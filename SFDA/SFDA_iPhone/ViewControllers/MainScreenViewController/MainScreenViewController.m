//
//  MainScreenViewController.m
//  SFDA
//
//  Created by Assem Imam on 9/3/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "MainScreenViewController.h"
#import "MainViewCustomCell.h"
#import "Language.h"
#import "ContactUsViewController.h"
#import "AwarenessCenterViewController.h"
#import "ImportantLinksViewController.h"
#import "JSON.h"
#import "JSONKit.h"
#import "ASIHTTPRequest.h"
#import "NewsDetailsViewController.h"
#import "AboutViewController.h"
#import "Global.h"
#import "FMDatabase.h"
#import "WebViewController.h"
#import "NewsSclidingView.h"
#import "MemberViewController.h"
#import "ReportViewController.h"

@interface MainScreenViewController ()
{
    NSArray *optionsList;
    NSMutableArray *newsList;
    ASIHTTPRequest *req;
    UIImage*SelectedPageImage;
    UIImage*DefaultPageImage;
    UIView *pagerView;
    int indexCount;
    UIScrollView *svImages;
}
@property (weak, nonatomic) IBOutlet UIImageView *bottomBar;
@property (weak, nonatomic) IBOutlet UIView *tableViewOwner;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation MainScreenViewController
@synthesize newsView;

#pragma  mark tableview datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MainViewCustomCell *cell =(MainViewCustomCell*) [tableView dequeueReusableCellWithIdentifier:@"cleOption"];
    if (cell==nil) {
        cell =(MainViewCustomCell*) [[[NSBundle mainBundle]loadNibNamed:[[Language getObject]getStringWithKey:@"MainViewCustomCell"] owner:nil options:nil]objectAtIndex:0];
        
    }
    cell.imgIcon.image = [UIImage imageNamed:[[optionsList objectAtIndex:indexPath.row] objectForKey:@"image"]];
    cell.OptionTitle.text = [[optionsList objectAtIndex:indexPath.row] objectForKey:@"option"];
    
    cell.backgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"main screen tile.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    cell.selectedBackgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"main screen tile.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  [optionsList count];
    
}
#pragma  mark tableview delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == 0){
        AboutViewController *about = [[AboutViewController alloc]initWithNibName:@"AboutViewController" bundle:nil  ];
        about.parameter = @"SFDA";
        [self.navigationController pushViewController:about animated:YES];
        
    }
    else if(indexPath.row == 1){
        MemberViewController *vc_members = [[MemberViewController alloc]init];
        [self.navigationController pushViewController:vc_members animated:YES];
    }
    else if(indexPath.row == 2){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.sfda.gov.sa/ar/about/pages/career.aspx"]];
    }
    else if (indexPath.row==3) {

        ReportViewController *reportViewController = [[ReportViewController alloc] initWithNibName:@"ReportViewController" bundle:nil];
        [self.navigationController pushViewController:reportViewController animated:YES];
        
       // WebViewController *vc_web = [[WebViewController alloc]init];
      //  vc_web.url = @"http://sfdaportal.azurewebsites.net/sendemail";
       // [self.navigationController pushViewController:vc_web animated:YES];
    }
    else if(indexPath.row == 4){
        AwarenessCenterViewController *awreness = [[AwarenessCenterViewController alloc]init];
        Global *global = [Global getInstance];
        [global setCurrentSectorImage:[UIImage imageNamed:@"orgSecTitle.png"]];
        awreness.parameter = @"SFDA";
        [self.navigationController pushViewController:awreness animated:YES];
    }else if(indexPath.row == 5){
        ImportantLinksViewController *links = [[ImportantLinksViewController alloc]initWithNibName:@"ImportantLinksViewController" bundle:nil];
        [self.navigationController pushViewController:links animated:YES];
    }
    else if(indexPath.row == 6){
        NSString *nibName = [[Language getObject] getStringWithKey:@"ContactUsViewController"];
        
        ContactUsViewController *contact = [[ContactUsViewController alloc]initWithNibName:nibName bundle:nil];
        [self.navigationController pushViewController:contact animated:YES];
        
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    del =(AppDelegate*) [[UIApplication sharedApplication]delegate];
    NavigationBar*navBar2 =  (NavigationBar*) [del.window viewWithTag:1000000];
    if (navBar2) {
        [navBar2.btnBack setHidden:NO];
        [navBar2.btnRefresh setHidden:YES];
        [del.window bringSubviewToFront:navBar2];
        Global *global = [Global getInstance];
        [global setCurrentSectorMenuBtn:[UIImage imageNamed:@"menu_button.png"]];
        [global setCurrentSectorImage:nil];
        [navBar2.btnMenu setImage:[global currentSectorMenuBtn] forState:UIControlStateNormal];
    }

    
    [self updateViews];
}

- (void)updateViews
{
    CGRect frame = [self navigationFrame];
    
    if(!IS_DEVICE_RUNNING_IOS_7_AND_ABOVE)
        frame.origin.y = 0.0;
    

    [newsView setFrame:CGRectMake(newsView.frame.origin.x, frame.origin.y + frame.size.height, newsView.frame.size.width, newsView.frame.size.height)];
    
    CGRect tableRect = _tableViewOwner.frame;
    tableRect.origin.y = newsView.frame.origin.y + newsView.frame.size.height;
    _tableViewOwner.frame = tableRect;
    
    tableRect.origin.y = 0.0;
    tableRect.size.height = [[UIScreen mainScreen] bounds].size.height - _tableViewOwner.frame.origin.y - _bottomBar.frame.size.height;
    _tableView.frame = tableRect;
    
    CGRect bottomBarRect = _bottomBar.frame;
    bottomBarRect.origin.y = tableRect.size.height;
    _bottomBar.frame = bottomBarRect;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    DefaultPageImage = [UIImage imageNamed:@"WhiteO.png"];
    SelectedPageImage = [UIImage imageNamed:@"OrangeO.png"];
    
    optionsList = [[NSArray alloc]init];
    optionsList = [[Language getObject]getArrayWithKey:@"MainMenuOptions"];
    // Do any additional setup after loading the view from its nib.
    
    [self getNewsData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma News_Horizontal_Scroll_View
-(void)RefreshSwitcher
{
    float Button_X = 0;
    float newsViewHeight = self.newsView.frame.size.height;
    svImages = [[UIScrollView alloc] initWithFrame:CGRectMake(0, (newsViewHeight - 100.0) / 2.0, 320, 95)];
    svImages.pagingEnabled = YES;
    svImages.delegate=self;
    [svImages setBackgroundColor:[UIColor clearColor]];
    //[svImages setScrollEnabled:NO];
    [svImages setShowsHorizontalScrollIndicator:NO];
    int index = 0;
    if ([newsList count] > 0) {
        
        if([[Language getObject] getLanguage] == languageArabic){
            for(int i=[newsList count]-1;i>=0;i--){
                [self drawNewsViewAt:Button_X :i];
                Button_X+= 320;
            }
        }else{
            for (NSDictionary *dict in newsList) {
                if (index > [newsList count]) {
                    index = 0;
                }
                [self drawNewsViewAt:Button_X :index];
                Button_X+= 320;
                index++;
            }
        }
        
        [self DrawPager];
        [svImages setContentSize:CGSizeMake(((320 +2) * [newsList count]), 95)];
        [svImages setContentOffset:CGPointMake(([newsList count]-1)*320,0) animated:YES];
        [newsView addSubview:svImages];
    }
    
    //hide loading view
    [aiLoading stopAnimating];
    [vLoading setHidden:YES];
    [self.view sendSubviewToBack:vLoading];
}

-(void)drawNewsViewAt:(int)x :(int)index{
    
    
    NewsSclidingView *newsSclidingView =(NewsSclidingView*) [[[NSBundle mainBundle]loadNibNamed:@"NewsSclidingView" owner:nil options:nil]objectAtIndex:0];
    
    if([[Language getObject] getLanguage] == languageArabic){
        [newsSclidingView.newsTitle setText:[[newsList objectAtIndex:index] objectForKey:@"TitleAr"]];
    }else{
        [newsSclidingView.newsTitle setText:[[newsList objectAtIndex:index] objectForKey:@"TitleEn"]];
    }
    
    newsSclidingView.newsTitle.font = [UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[newsList objectAtIndex:index] objectForKey:@"ImageUrl"]]];
        if (imageData==nil|| imageData.length==0) {
            imageData = [NSData dataWithContentsOfFile:[[newsList objectAtIndex:index] objectForKey:@"imagePath"]];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            newsSclidingView.newsImage.image = [UIImage imageWithData:imageData];
        });
    });
    
    
    
    NSArray* dateArray = [[[newsList objectAtIndex:index] objectForKey:@"PubDate"]componentsSeparatedByString: @" "];
    [newsSclidingView.newsDate  setText:[dateArray objectAtIndex:0]];
    
    
    
    [newsSclidingView setFrame:CGRectMake(x, 0, svImages.frame.size.width, svImages.frame.size.height)];
    newsSclidingView.backgroundColor = [UIColor clearColor];
    newsSclidingView.tag = index;// [[[newsList objectAtIndex:index] objectForKey:@"$id"] intValue];
    
    
    UITapGestureRecognizer *singleTouch = [[UITapGestureRecognizer alloc]
                                           initWithTarget:self action:@selector(handleSingleTouch:)];
    singleTouch.numberOfTapsRequired = 1;
    [newsSclidingView addGestureRecognizer:singleTouch];
    [newsSclidingView setBackgroundColor:[UIColor clearColor]];
    [newsSclidingView setUserInteractionEnabled:YES];
    
    
    
    
    [svImages addSubview:newsSclidingView];
    
}

-(void)DrawPager{
    
    float ImageWidth=7;
    float ImageHeight=7;
    float Image_X=305;
    
    
    pagerView = [[UIView alloc] initWithFrame:CGRectMake(0, svImages.frame.size.height + svImages.frame.origin.y, 100, 10)];
    pagerView.backgroundColor = [UIColor clearColor];
    
    for (int page =[newsList count]; page>=1 ; page--) {
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(Image_X, 0, ImageWidth, ImageHeight)]  ;
        img.tag = page;
        if (page == 1) {
            img.image = SelectedPageImage;
        }
        else {
            img.image = DefaultPageImage;
        }
        
        [pagerView addSubview:img];
        Image_X-=(ImageWidth+5);
    }
    
    [newsView addSubview:pagerView];
    // [newsView bringSubviewToFront:pagerView];
}

-(void)SetPagerSelectePage:(NSInteger)index
{
    
    for (int page = 1; page<= [newsList count]; page++) {
        [(UIImageView*) [pagerView viewWithTag:page] setImage:DefaultPageImage];
    }
    [(UIImageView*)  [pagerView viewWithTag:index+1] setImage:SelectedPageImage];
}

-(void)handleSingleTouch:(UITapGestureRecognizer*)sender
{
    NSLog(@"handle bgad %d",sender.view.tag);
    
    NSString *nibName = [[Language getObject] getStringWithKey:@"NewsDetailsViewController"];
    Global *global = [Global getInstance];
      [global setCurrentSectorImage:[UIImage imageNamed:@"orgSecTitle.png"]];
    NewsDetailsViewController *newsDetails = [[NewsDetailsViewController alloc]initWithNibName:nibName bundle:nil];
    newsDetails.newsObj = [newsList objectAtIndex:sender.view.tag];
    [self.navigationController pushViewController:newsDetails animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)_scrollView {
    if (_scrollView != svImages) {
        return;
    }
    @try {
        
        float scrollViewWidth = _scrollView.frame.size.width;
        float scrollContentSizeWidth = _scrollView.contentSize.width;
        float scrollOffset = _scrollView.contentOffset.x;
        
        if (scrollOffset + scrollViewWidth == scrollContentSizeWidth)
        {
            [svImages setContentOffset:CGPointMake(([newsList count]-1)*320,0) animated:YES];
            //            indexCount = (int)(imagesScrollView.contentOffset.x /imagesScrollView.frame.size.width);
            //            //  [delegate didMoveImageAtIndex:(NSInteger)[self.ImageIDS  objectAtIndex:indexCount]];
            //            [delegate didMoveImageAtIndex:(NSInteger)indexCount];
        }else{
            indexCount = (int)(_scrollView.contentOffset.x /_scrollView.frame.size.width);
            //[svImages didMoveImageAtIndex:(NSInteger)indexCount];
            [self SetPagerSelectePage:(NSInteger)indexCount];
        }
        
        
        
        
        //        SLIDING_ENBLED = NO;
        //NSLog(@"did scroll   %i",indexCount);
        
        
        // [delegate didMoveAdAtIndex:(NSInteger)[self.ImageIDS  objectAtIndex:indexCount]];
        
    }
    @catch (NSException *exception) {
        
    }
    
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)_scrollView

{
    if (_scrollView != svImages) {
        return;
    }
    @try {
        
        ////////NSLog(@"did end   %i",indexCount);
        indexCount = (int)(_scrollView.contentOffset.x /_scrollView.frame.size.width);
        // //////NSLog(@"did scroll   %i",indexCount);
        
        //[delegate didMoveImageAtIndex:(NSInteger)indexCount];
        //[delegate didMoveImageAtIndex:(NSInteger)[self.ImageIDS  objectAtIndex:indexCount]];
        // [self setSlidingEnabled:YES];
        // SLIDING_ENBLED = YES;
    }
    @catch (NSException *exception) {
        
    }
    
    
}


-(void)getNewsData
{
    
    //show loading view
    lblLoading.text = [[Language getObject]getStringWithKey:@"LoadingTitle"];
    [aiLoading startAnimating];
    [vLoading setHidden:NO];
    [self.view bringSubviewToFront:vLoading];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:NEWS_SCROLL_SFDA_TAG]){
        
        [self newsGetAllData];
        
        NSLog(@"%d",[newsList count]);
        
        [self performSelectorOnMainThread:@selector(RefreshSwitcher) withObject:nil waitUntilDone:NO];
        
        
        
    }else{
        if (!internetConnection) {
            //hide loading view
            [aiLoading stopAnimating];
           // [vLoading setHidden:YES];
            [[[UIAlertView alloc]initWithTitle:nil
                                       message:[[Language getObject]getStringWithKey:@"interNetMessage"]
                                      delegate:nil
                             cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"]
                             otherButtonTitles: nil]show];
            [self.view sendSubviewToBack:vLoading];
            HIDE_LOADING
            return;
        }
        req =[[ASIHTTPRequest alloc]initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@News?Section=SFDA&count=20", SERVER_IP]]];
        
//        [req setCompletionBlock:^{
//            NSString *response=  [req responseString];
//            
//            id result = [response JSONValue];
//            newsList=result;
//            
//            if(newsList != nil && [newsList count]>0){
//                
//                [self deleteData];
//                
//                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:NEWS_SCROLL_SFDA_TAG];
//                
//            }
//            
//            
//            for(NSDictionary *obj in newsList)
//                
//            {
//                [self insertNewsData:[obj objectForKey:@"$id"] :[obj objectForKey:@"TitleAr"] :[obj objectForKey:@"TitleEn"] :[obj objectForKey:@"DescriptionAr"] :[obj objectForKey:@"DescriptionEn"] :[obj objectForKey:@"Url"] :[obj objectForKey:@"PubDate"]:[obj objectForKey:@"ImagePath"]:[obj objectForKey:@"ImageUrl"]];
//            }
//            
//            [self performSelectorOnMainThread:@selector(RefreshSwitcher) withObject:nil waitUntilDone:NO];
//            
//            
//        }];
//        [req setFailedBlock:^{
//            NSLog(@"error net");
//        }];
//        [req startAsynchronous];

        [req setCompletionBlock:^{
            NSString *response=  [req responseString];
            
            id result = [response JSONValue];
            newsList=result;
            
            if(newsList != nil && [newsList count]>0){
                [self deleteData];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:NEWS_SCROLL_SFDA_TAG];
            }
            
            for(NSDictionary *obj in newsList)
            {
                
                NSString *url = [obj objectForKey:@"ImageUrl"];
                
                dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                //this will start the image loading in bg
                dispatch_async(concurrentQueue, ^{
                    
                    NSURL *imgUrl = [NSURL URLWithString:url];
                    NSURLRequest* imgRequest = [NSURLRequest requestWithURL:imgUrl];
                    NSData *newsImageData = [[NSData alloc]initWithData:[NSURLConnection sendSynchronousRequest:imgRequest returningResponse:nil error:nil]];
                    imgRequest=nil;
                    
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    NSString *Documentpath = [paths objectAtIndex:0];
                    NSString *TargetPath = [Documentpath stringByAppendingPathComponent:@"SDFAScrollImages"];
                    
                    NSFileManager *Fmanager = [NSFileManager defaultManager];
                    
                    if ([Fmanager fileExistsAtPath:TargetPath isDirectory:nil]) {
                        
                    }
                    else
                    {
                        BOOL ret =[Fmanager createDirectoryAtPath:TargetPath withIntermediateDirectories:YES attributes:nil error:nil];
                        if (ret) {
                            
                        };
                    }
                    
                    NSString *fileName= [TargetPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",[obj objectForKey:@"$id"]] ];
                    
                    
                    //this will set the image when loading is finished
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //save image file
                        if (newsImageData!=nil &&newsImageData.length > 0 ) {
                            
                            
                            
                            [newsImageData writeToFile:fileName atomically:YES];
                            
                            
                            NSMutableDictionary *newsDic =[[NSMutableDictionary alloc]init];
                            for (NSString *key in [obj allKeys]) {
                                [newsDic setValue:[obj objectForKey:key] forKey:key];
                            }
                            
                            [newsDic setValue:fileName forKey:@"ImagePath"];
                            
                            
                            [self performSelectorOnMainThread:@selector(insertNewsInformation:) withObject:newsDic waitUntilDone:NO];
                        }
                        
                        
                    });
                });
                
                
                
            }
            
            [self performSelectorOnMainThread:@selector(RefreshSwitcher) withObject:nil waitUntilDone:NO];
            
        }];
        [req setFailedBlock:^{
            NSLog(@"error net");
        }];
        [req startAsynchronous];
    
    }
}


- (void)viewDidUnload {
    [self setNewsView:nil];
    [super viewDidUnload];
}

-(void)insertNewsInformation:(id)obj
{
    
    [self insertNewsData:[obj objectForKey:@"$id"] :[obj objectForKey:@"TitleAr"] :[obj objectForKey:@"TitleEn"] :[obj objectForKey:@"DescriptionAr"] :[obj objectForKey:@"DescriptionEn"] :[obj objectForKey:@"Url"] :[obj objectForKey:@"PubDate"]:[obj objectForKey:@"ImagePath"]:[obj objectForKey:@"ImageUrl"]];
    
    
}

#pragma  mark DataBase functions
- (void)insertNewsData:(NSString*)nid :(NSString*)title_ar :(NSString*)title_en :(NSString*)content_ar :(NSString*)content_en :(NSString*)link :(NSString*)date :(NSString*)image_path :(NSString*)image_url {
    
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    NSString* lang = @"en";
    if([[Language getObject] getLanguage] == languageArabic)
        lang = @"ar";
    
    
    NSString *insertQuery = [@"insert into news_scroll " stringByAppendingFormat:@" VALUES ('%@', '%@','%@','%@','%@','%@','%@','%@','%@','%@')",link,date,@"SFDA",nid,image_path,image_url,title_ar,title_en,content_ar,content_en];
    BOOL insterted = [database executeUpdate:insertQuery];

    
    NSLog(insterted ? @"Yes" : @"No");
    
    [database close];
}

-(void) newsGetAllData{
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    NSString *sqlSelectQuery = @"SELECT * FROM news_scroll where section='SFDA' order by id asc";
    
    NSLog(@"%@",sqlSelectQuery);
    
    newsList = [[NSMutableArray alloc] init];
    
    // Query result
    FMResultSet *resultsWithNameLocation = [database executeQuery:sqlSelectQuery];
    while([resultsWithNameLocation next]) {
        NSString *news_title_ar = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"title_ar"]];
        NSString *news_title_en = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"title_en"]];
        NSString *news_content_ar = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"content_ar"]];
        NSString *news_content_en = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"content_en"]];
        NSString *news_link = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"link"]];
        NSString *news_date = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"PubDate"]];
        NSString *news_id = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"id"]];
        NSString *image_path = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"image_path"]];
        
        NSMutableDictionary *newsDict = [[NSMutableDictionary alloc]init];
        if([[Language getObject] getLanguage] == languageArabic){
            [newsDict setValue:news_title_ar forKey:@"TitleAr"];
            [newsDict setValue:news_content_ar forKey:@"DescriptionAr"];
        }else{
            [newsDict setValue:news_title_en forKey:@"TitleEn"];
            [newsDict setValue:news_content_en forKey:@"DescriptionEn"];
        }
        
        [newsDict setValue:news_link forKey:@"Url"];
        [newsDict setValue:news_date forKey:@"PubDate"];
        [newsDict setValue:news_id forKey:@"$id"];
        
        [newsDict setValue:image_path forKey:@"imagePath"];
        [newsList addObject:newsDict ];
        
        // loading your data into the array, dictionaries.
    }
    [database close];
}

- (void)deleteData {
    
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    NSString *deleteQuery = @"DELETE FROM news_scroll where section='SFDA'";
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    BOOL deleted = [database executeUpdate:deleteQuery];
    NSLog(deleted ? @"Yes" : @"No");
    [database close];
}
@end
