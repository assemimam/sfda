//
//  MainScreenViewController.h
//  SFDA
//
//  Created by Assem Imam on 9/3/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"

@interface MainScreenViewController : MasterViewController<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>

{
    __weak IBOutlet UILabel *lblLoading;
    __weak IBOutlet UIActivityIndicatorView *aiLoading;
    __weak IBOutlet UIView *vLoading;
}


@property (weak, nonatomic) IBOutlet UIView *newsView;

@end
