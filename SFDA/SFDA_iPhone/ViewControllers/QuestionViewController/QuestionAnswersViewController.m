//
//  QuestionAnswersViewController.m
//  SFDA
//
//  Created by yehia on 9/14/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "QuestionAnswersViewController.h"
#import "ASIHTTPRequest.h"
#import "QuestionsCustomCell.h"
#import "Language.h"
#import "JSON.h"
#import "JSONKit.h"
#import "FMDatabase.h"
#import "Global.h"

@interface QuestionAnswersViewController ()
{
    NSMutableArray *optionsList;
    ASIHTTPRequest *req;
    NSString *currentSectionName;
    NSString *currentSectionCachingKey;
}

@end

@implementation QuestionAnswersViewController
@synthesize questionsTableView;
@synthesize parameter;

#pragma  mark tableview datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QuestionsCustomCell *cell =(QuestionsCustomCell*) [tableView dequeueReusableCellWithIdentifier:@"cleOption"];
    if (cell==nil) {
        cell =(QuestionsCustomCell*) [[[NSBundle mainBundle]loadNibNamed:[[Language getObject]getStringWithKey:@"QuestionsCustomCell"] owner:nil options:nil]objectAtIndex:0];
    }
    
    NSString *question,*answer;
    
    if ([[Language getObject]getLanguage]==languageArabic) {
        question = [[optionsList objectAtIndex:indexPath.row] objectForKey:@"QuestionAr"];
        answer =[[optionsList objectAtIndex:indexPath.row] objectForKey:@"AnswerAr"];
    }else{
        question = [[optionsList objectAtIndex:indexPath.row] objectForKey:@"QuestionEn"];
        answer =[[optionsList objectAtIndex:indexPath.row] objectForKey:@"AnswerEn"];
    }
    
    cell.questionTextView.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
    cell.answerTextView.font=[UIFont fontWithName:@"GESSTwoLight-Light" size:14];
    [cell.questionTextView setDelegate:self];
    [cell.answerTextView setDelegate:self];
    
    cell.questionTextView.text = question;
    cell.answerTextView.text = answer;
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [optionsList count];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *question,*answer;
    
    if ([[Language getObject]getLanguage]==languageArabic) {
        question = [[optionsList objectAtIndex:indexPath.row] objectForKey:@"QuestionAr"];
        answer =[[optionsList objectAtIndex:indexPath.row] objectForKey:@"AnswerAr"];
    }else{
        question = [[optionsList objectAtIndex:indexPath.row] objectForKey:@"QuestionEn"];
        answer =[[optionsList objectAtIndex:indexPath.row] objectForKey:@"AnswerEn"];
    }
    
    
    CGSize questSize = [question sizeWithFont:[UIFont boldSystemFontOfSize:17.0f] constrainedToSize:CGSizeMake(self.questionsTableView.frame.size.height , 20000000000.0f)];
    CGSize answSize = [answer sizeWithFont:[UIFont systemFontOfSize:21.0f] constrainedToSize:CGSizeMake(self.questionsTableView.frame.size.height , 20000000000.0f)];
    
    NSInteger height = questSize.height+answSize.height+100;
    
    NSLog(@" heigt for index path row %d  = %d",indexPath.row,height);
    
    return height;
    
    
}

#pragma  mark tableview delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}


-(void)getData
{
    
    @try{
        
        
        if([parameter isEqualToString:@"Food"]){
            currentSectionCachingKey = QUESTION_SECTION_FOOD_TAG;
        }else if([parameter isEqualToString:@"Medical"]){
            currentSectionCachingKey = QUESTION_SECTION_MEDI_TAG;
        }else if([parameter isEqualToString:@"Drug"]){
            currentSectionCachingKey = QUESTION_SECTION_DRUG_TAG;
        }
        
        currentSectionName = parameter;
        parameter =[@"?sectionName=" stringByAppendingString:currentSectionName];
        
        
        if([[NSUserDefaults standardUserDefaults] boolForKey:currentSectionCachingKey]){
            
            [self QuestionGetAllData];
            
            NSLog(@"%d",[optionsList count]);
            
            [questionsTableView reloadData];
        }else{
            NSString *url = [[NSString stringWithFormat:@"%@FAQs", SERVER_IP] stringByAppendingString:parameter];
            NSLog(@"%@",url);
            if (!internetConnection) {
                [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
                HIDE_LOADING
                return;
            }
            
            SHOW_LOADING
            req =[[ASIHTTPRequest alloc]initWithURL:[NSURL URLWithString:url]];
            
            [req setCompletionBlock:^{
                NSString *response=  [req responseString];
                
                id result = [response JSONValue];
                optionsList=result;
                
                if(optionsList != nil && [optionsList count]>0){
                    [self deleteData];
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:currentSectionCachingKey];
                    
                }
                
                for (int i=0; i<[optionsList count]; i++) {
                    
                    
                    NSString *qAr = [[optionsList objectAtIndex:i] objectForKey:@"QuestionAr"];
                    NSString *qEn = [[optionsList objectAtIndex:i] objectForKey:@"QuestionEn"];
                    
                    NSString *ansAr = [[optionsList objectAtIndex:i] objectForKey:@"AnswerAr"];
                    NSString *ansEn = [[optionsList objectAtIndex:i] objectForKey:@"AnswerEn"];
                    
                    if([[Language getObject] getLanguage] == languageArabic){
                        [self insertQuestionData:qAr :ansAr];
                    }else{
                        [self insertQuestionData:qEn :ansEn];
                    }
                }
                
                
                HIDE_LOADING
                [questionsTableView reloadData];
                
            }];
            [req setFailedBlock:^{
                
                HIDE_LOADING
                
                [self QuestionGetAllData];
                
                if(optionsList != nil && [optionsList count] >0){
                    
                    [questionsTableView reloadData];
                }else{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"يرجى التحقق من اتصالك مع الإنترنت" delegate:self cancelButtonTitle:@"موافق" otherButtonTitles:nil];
                    [alert show];
                    NSLog(@"%@",[req error]);
                }
            }];
            [req startAsynchronous];
        }
        
    }@catch (NSException *ex) {
        
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NavigationBar *navBar2 =  (NavigationBar*) [del.window viewWithTag:1000000];
    if (navBar2) {
        [navBar2.btnBack setHidden:NO];
        [del.window bringSubviewToFront:navBar2];
    }

    
    [self updateViews];
}

- (void)updateViews
{
    CGRect frame = [self navigationFrame];
    
    if(!IS_DEVICE_RUNNING_IOS_7_AND_ABOVE)
        frame.origin.y = 0.0;
    
    [questionsTableView setFrame:CGRectMake(questionsTableView.frame.origin.x, frame.origin.y + frame.size.height, questionsTableView.frame.size.width, questionsTableView.frame.size.height - frame.origin.y)];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self performSelector:@selector(getData) withObject:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setQuestionsTableView:nil];
    [super viewDidUnload];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    if(![req isCancelled]) {
        // Cancels an asynchronous request
        [req cancel];
        // Cancels an asynchronous request, clearing all delegates and blocks first
        [req clearDelegatesAndCancel];
    }
    
    [super viewWillDisappear:animated];
}

#pragma  mark DataBase functions
- (void)insertQuestionData:(NSString*)name :(NSString*)value{
    
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    NSString* lang = @"en";
    if([[Language getObject] getLanguage] == languageArabic)
        lang = @"ar";
    
    
    NSString *insertQuery = [@"insert into question " stringByAppendingFormat:@" VALUES ('%@','%@', '%@','%@')", name,value,currentSectionName,lang];
    BOOL insterted = [database executeUpdate:insertQuery];
    
    NSLog(insterted ? @"Yes" : @"No");
    
    [database close];
}

-(void) QuestionGetAllData{
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    NSString *sqlSelectQuery = [[@"SELECT * FROM question where lang='en' and sectionName='" stringByAppendingString:currentSectionName] stringByAppendingString:@"'"];
    if([[Language getObject] getLanguage] == languageArabic)
        sqlSelectQuery = [[@"SELECT * FROM question where lang='ar' and sectionName='" stringByAppendingString:currentSectionName] stringByAppendingString:@"'"];
    
    NSLog(@" query ==> %@",sqlSelectQuery);
    
    optionsList = [[NSMutableArray alloc] init];
    
    // Query result
    FMResultSet *resultsWithNameLocation = [database executeQuery:sqlSelectQuery];
    while([resultsWithNameLocation next]) {
        NSString *question_name = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"question"]];
        NSString *question_answer = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"answer"]];
        
        
        NSMutableDictionary *questionDict = [[NSMutableDictionary alloc]init];
        if([[Language getObject] getLanguage] == languageArabic){
            
            [questionDict setValue:question_name forKey:@"QuestionAr"];
            [questionDict setValue:question_answer forKey:@"AnswerAr"];
        }else{
            [questionDict setValue:question_name forKey:@"QuestionEn"];
            [questionDict setValue:question_answer forKey:@"AnswerEn"];
        }
        
        [optionsList addObject:questionDict ];
        
        // loading your data into the array, dictionaries.
    }
    [database close];
}

- (void)deleteData {
    
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    NSString *deleteQuery = [[@"DELETE FROM question where lang='en' and sectionName='" stringByAppendingString:currentSectionName] stringByAppendingString:@"'"];
    if([[Language getObject] getLanguage] == languageArabic)
        deleteQuery = [[@"DELETE FROM question where lang='ar' and sectionName='" stringByAppendingString:currentSectionName] stringByAppendingString:@"'"];
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    BOOL sss = [database executeUpdate:deleteQuery];
    
    NSLog(sss ? @"Yes" : @"No");
    [database close];
}
@end
