//
//  QuestionAnswersViewController.h
//  SFDA
//
//  Created by yehia on 9/14/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"

@interface QuestionAnswersViewController : MasterViewController<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *questionsTableView;
@property (weak, nonatomic) NSString *parameter;
-(NSString*)RetString:(NSString*)arg1 withName:(NSString*)name;
@end
