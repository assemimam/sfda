//
//  AwarenessCenterViewController.h
//  SFDA
//
//  Created by yehia on 9/8/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "MasterViewController.h"
#import "ASIHTTPRequest.h"
#import "PDFViewerViewController.h"

@interface AwarenessCenterViewController : MasterViewController<UITableViewDataSource,UITableViewDelegate>
{
    
    __weak IBOutlet UITableView *tvContacts;
}
@property (weak, nonatomic) IBOutlet UIImageView *sectorTileImgView;
@property(weak,nonatomic)NSString *parameter;

@end
