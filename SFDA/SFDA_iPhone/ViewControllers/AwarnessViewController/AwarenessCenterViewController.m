//
//  AwarenessCenterViewController.m
//  SFDA
//
//  Created by yehia on 9/8/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "AwarenessCenterViewController.h"

#import "AwarenessCustomCell.h"
#import "Language.h"
#import "JSON.h"
#import "JSONKit.h"
#import "FMDatabase.h"
#import "Global.h"

@interface AwarenessCenterViewController ()
{
    NSMutableArray *optionsList;
    ASIHTTPRequest *req;
    Global *global;
    NSString *currentSectionName;
    NSString *currentSectionCachingKey;
}

@end

@implementation AwarenessCenterViewController
@synthesize parameter;
@synthesize sectorTileImgView;


#pragma  mark tableview datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AwarenessCustomCell *cell =(AwarenessCustomCell*) [tableView dequeueReusableCellWithIdentifier:@"cleOption"];
    if (cell==nil) {
        cell =(AwarenessCustomCell*) [[[NSBundle mainBundle]loadNibNamed:[[Language getObject]getStringWithKey:@"AwarenessCustomCell"] owner:nil options:nil]objectAtIndex:0];
        
    }
    
    NSLog(@"cell nib =%@",[[Language getObject]getStringWithKey:@"AwarenessCustomCell"] );
    NSString *title_ar,*title_en;
    
        title_ar = [[optionsList objectAtIndex:indexPath.row] objectForKey:@"TitleAr"];
        title_en = [[optionsList objectAtIndex:indexPath.row] objectForKey:@"TitleEn"];

    if([[Language getObject]getLanguage] == languageArabic){
    cell.lblTitle.text = title_ar;
    }else{
    cell.lblTitle.text = title_en;
    }
     cell.lblTitle.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
    cell.backgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"silver box tile !.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    cell.selectedBackgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"silver box tile !.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [optionsList count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 71;
}

#pragma  mark tableview delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
//    NSString *originalUrl = [[optionsList objectAtIndex:indexPath.row] objectForKey:@"PDFUrl"];
//    
//    NSString* escapedUrl = [originalUrl
//                            stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    
//    NSURL *url = [NSURL URLWithString:escapedUrl];
//    [[UIApplication sharedApplication] openURL:url];
    
    PDFViewerViewController *vc_viewer = [[PDFViewerViewController alloc]init];
    vc_viewer.SelectedPDF = [optionsList objectAtIndex:indexPath.row] ;
    vc_viewer.sectorName= currentSectionName;
    [self.navigationController pushViewController:vc_viewer animated:YES];
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)getData
{
    
    if([parameter isEqualToString:@"SFDA"]){
        currentSectionCachingKey = AWARENESS_SECTION_SFDA_TAG;
    }else if([parameter isEqualToString:@"Food"]){
        currentSectionCachingKey = AWARENESS_SECTION_FOOD_TAG;
    }else if([parameter isEqualToString:@"Medical"]){
        currentSectionCachingKey = AWARENESS_SECTION_MEDI_TAG;
    }else if([parameter isEqualToString:@"Drug"]){
        currentSectionCachingKey = AWARENESS_SECTION_DRUG_TAG;
    }
    
    currentSectionName = parameter;
    if(![parameter isEqualToString:@"SFDA"]){
        parameter =[@"?sectionName=" stringByAppendingString:currentSectionName];
    }else{
        parameter = @"";
    }
    NSLog(@" %@ , %@ ",currentSectionName,parameter);
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:currentSectionCachingKey]){
        
        [self awarenessGetAllData];
        
        NSLog(@"%d",[optionsList count]);
        
        [tvContacts reloadData];
        HIDE_LOADING;
    }else{
        @try {
            if (!internetConnection) {
                [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
                HIDE_LOADING
                return;
            }

        NSString *url = [[NSString stringWithFormat:@"%@AwarenessCenter", SERVER_IP] stringByAppendingString:parameter];
        req =[[ASIHTTPRequest alloc]initWithURL:[NSURL URLWithString:url]];
        
        [req setCompletionBlock:^{
            NSString *response=  [req responseString];
            
            id result = [response JSONValue];
            optionsList=result;
            
            if(optionsList != nil && [optionsList count]>0){
                [self deleteData];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:currentSectionCachingKey];
                
            }
            
            for (NSDictionary *dict in optionsList) {
                
                NSString *title_ar,*title_en;

                title_ar = [dict objectForKey:@"TitleAr"];
                title_en = [dict objectForKey:@"TitleEn"];
                
                [self insertAwarenessData:[dict objectForKey:@"$id"] :[dict objectForKey:@"PDFUrl"]  :title_ar :title_en];
            }
            [tvContacts reloadData];
            HIDE_LOADING;
            
        }];
        [req setFailedBlock:^{
            
            [self awarenessGetAllData];
            HIDE_LOADING;
            if(optionsList == nil || [optionsList count] ==0){
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"يرجى التحقق من اتصالك مع الإنترنت" delegate:self cancelButtonTitle:@"موافق" otherButtonTitles:nil];
                [alert show];
                //            NSLog(@"%@",[req error]);
            }else{
                [tvContacts reloadData];
            }
        }];
        [req startAsynchronous];
        }
        @catch (NSException *exception) {
            
        };
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    del = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    NavigationBar*navBar2 =  (NavigationBar*) [del.window viewWithTag:1000000];
    if (navBar2) {
        [navBar2.btnBack setHidden:NO];
        [del.window bringSubviewToFront:navBar2];
        
        global = [Global getInstance];
        [navBar2.btnMenu setImage:[global currentSectorMenuBtn] forState:UIControlStateNormal];
        [sectorTileImgView setImage:[global currentSectorImage]];
    }

    
    [self updateViews];
    
}

- (void)updateViews
{
    CGRect frame = [self navigationFrame];
    
    if(!IS_DEVICE_RUNNING_IOS_7_AND_ABOVE)
        frame.origin.y = 0.0;
    

    [sectorTileImgView setFrame:CGRectMake(sectorTileImgView.frame.origin.x, frame.origin.y + frame.size.height, sectorTileImgView.frame.size.width, sectorTileImgView.frame.size.height)];
    
    CGRect tableRect = tvContacts.frame;
    tableRect.origin.y = sectorTileImgView.frame.origin.y + sectorTileImgView.frame.size.height;
    tvContacts.frame = tableRect;
}

- (void)viewDidLoad
{
    SHOW_LOADING
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self performSelector:@selector(getData) withObject:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    tvContacts = nil;
    [super viewDidUnload];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    if(![req isCancelled]) {
        // Cancels an asynchronous request
        [req cancel];
        // Cancels an asynchronous request, clearing all delegates and blocks first
        [req clearDelegatesAndCancel];
    }
    
    [super viewWillDisappear:animated];
}


#pragma  mark DataBase functions
- (void)insertAwarenessData:(NSString*)_id :(NSString*)pdfUrl :(NSString*)title_ar :(NSString*)title_en {
    
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    NSString* lang = @"en";
    if([[Language getObject] getLanguage] == languageArabic)
        lang = @"ar";
    
    
    NSString *insertQuery = [@"insert into AwarenessCenter " stringByAppendingFormat:@" VALUES ('%@', '%@','%@','%@','%@')", pdfUrl,currentSectionName,title_ar,title_en,_id];
    BOOL insterted = [database executeUpdate:insertQuery];
    
    NSLog(insterted ? @"Yes" : @"No");
    
    [database close];
}

-(void) awarenessGetAllData{
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    NSString *sqlSelectQuery = [@"SELECT * FROM AwarenessCenter where sectionName='" stringByAppendingString:[currentSectionName stringByAppendingString:@"'"]];
        
    NSLog(@" query ==> %@",sqlSelectQuery);
    
    optionsList = [[NSMutableArray alloc] init];
    
    // Query result
    FMResultSet *resultsWithNameLocation = [database executeQuery:sqlSelectQuery];
    while([resultsWithNameLocation next]) {
        NSString *awareness_title_ar = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"title_ar"]];
        NSString *awareness_title_en = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"title_en"]];
        NSString *awareness_pdf_url = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"PDFUrl"]];
        NSString *awareness_id= [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"id"]];
        
        NSMutableDictionary *awarenessDict = [[NSMutableDictionary alloc]init];

        [awarenessDict setObject:awareness_title_ar forKey:@"TitleAr"];
        [awarenessDict setObject:awareness_title_en forKey:@"TitleEn"];
        [awarenessDict setObject:awareness_pdf_url forKey:@"PDFUrl"];
        [awarenessDict setObject:awareness_id forKey:@"$id"];
        [optionsList addObject:awarenessDict ];
        
        // loading your data into the array, dictionaries.
    }
    [database close];
}

- (void)deleteData {
    
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    NSString *deleteQuery = [@"DELETE FROM AwarenessCenter where sectionName='" stringByAppendingString:[currentSectionName  stringByAppendingString:@"'"]];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    [database executeUpdate:deleteQuery];
    [database close];
    HIDE_LOADING
}


@end
