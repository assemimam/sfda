//
//  EServiceViewController.h
//  SFDA
//
//  Created by yehia on 9/26/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "MasterViewController.h"
#import "ASIHTTPRequest.h"
#import "MasterViewController.h"

@interface EServiceViewController : MasterViewController<UITableViewDataSource,UITableViewDelegate>
{
    
    
    __weak IBOutlet UITableView *tvContacts;
    
    NSMutableArray *sdfaOptionsList;
    NSMutableArray *foodOptionsList;
    NSMutableArray *drugOptionsList;
    NSMutableArray *medicalOptionsList;
}
@property (weak, nonatomic) IBOutlet UIImageView *sectorTileImgView;
@end
