//
//  EServiceViewController.m
//  SFDA
//
//  Created by yehia on 9/26/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "EServiceViewController.h"
#import "EServiceCustomCell.h"
#import "Language.h"
#import "JSON.h"
#import "JSONKit.h"
#import "Global.h"
#import "WebViewController.h"
#import "FMDatabase.h"

@interface EServiceViewController ()

@end

@implementation EServiceViewController
{
    NSMutableArray *optionsList;
    NSMutableArray *currentArray;
    NSArray *sectionList;
    ASIHTTPRequest *req;
    Global *global;
}
@synthesize sectorTileImgView;

#pragma  mark tableview datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EServiceCustomCell *cell =(EServiceCustomCell*) [tableView dequeueReusableCellWithIdentifier:@"cleOption"];
    if (cell==nil) {
        cell =(EServiceCustomCell*) [[[NSBundle mainBundle]loadNibNamed:[[Language getObject]getStringWithKey:@"EServiceCustomCell"] owner:nil options:nil]objectAtIndex:0];
        
    }
    
    
    
    if(indexPath.section ==0){
        currentArray = sdfaOptionsList;
    }else if(indexPath.section ==1){
        currentArray = foodOptionsList;
    }else if(indexPath.section == 2){
        currentArray = drugOptionsList;
    }else if(indexPath.section ==3){
        currentArray = medicalOptionsList;
    }
    
    
    if ([[Language getObject]getLanguage]==languageArabic) {
        NSString *title = [[currentArray objectAtIndex:indexPath.row] objectForKey:@"TitleAr"];
        NSString *s_id = [[currentArray objectAtIndex:indexPath.row] objectForKey:@"SectionId"];
        
        
        cell.lblTitle.text = title;
        
    }
    else{
        NSString *title = [[currentArray objectAtIndex:indexPath.row] objectForKey:@"TitleEn"];
        NSString *s_id = [[currentArray objectAtIndex:indexPath.row] objectForKey:@"SectionId"];
        
        cell.lblTitle.text = title;
    }
    
    cell.lblTitle.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:12];
    cell.backgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"main service tile white.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    cell.selectedBackgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"main service tile white.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    
    return cell;
}

#pragma  mark tableview delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if(indexPath.section ==0){
        currentArray = sdfaOptionsList;
    }else if(indexPath.section ==1){
        currentArray = foodOptionsList;
    }else if(indexPath.section == 2){
        currentArray = drugOptionsList;
    }else if(indexPath.section ==3){
        currentArray = medicalOptionsList;
    }
    
    NSString *urlS = [[currentArray objectAtIndex:indexPath.row] objectForKey:@"Link"];
    
    
    if(![urlS hasPrefix:@"http://"] && ![urlS hasPrefix:@"https://"] ){
        urlS = [@"http://" stringByAppendingString:urlS];
    }
    
    NSLog(@"%@",urlS);
       NSURL *url = [NSURL URLWithString:urlS];
       [[UIApplication sharedApplication] openURL:url];
//    WebViewController *vc_web = [[WebViewController alloc]init];
//    vc_web.url = urlS;
//    [self.navigationController pushViewController:vc_web animated:YES];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(optionsList!=nil && [optionsList count] > 0){
        if(section ==0){
            return  [sdfaOptionsList count];
        }else if(section ==1){
            return  [foodOptionsList count];
        }else if(section == 2){
            return  [drugOptionsList count];
        }else if(section ==3){
            return  [medicalOptionsList count];
        }
    }
    
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 48;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 62;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if(optionsList!=nil && [optionsList count] > 0){
        
        UIView *headerView;
        
        
       // NSString *headerText = [sectionList objectAtIndex:section];
        
        float headerWidth = 320.0f;
        //float padding = 10.0f;
        
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, headerWidth, 48)];
        headerView.autoresizesSubviews = YES;
        headerView.backgroundColor = [UIColor clearColor];
        
//        // create the label centered in the container, then set the appropriate autoresize mask
//        UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, headerWidth, 30)];
//        headerLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;
//        if([[Language getObject] getLanguage] == languageArabic){
//            headerLabel.textAlignment = UITextAlignmentRight;
//        }else{
//            headerLabel.textAlignment = UITextAlignmentLeft;
//        }
//        
//        headerLabel.text = headerText;
//        headerLabel.textColor = [UIColor whiteColor];
//        headerLabel.backgroundColor = [UIColor clearColor];
//        
//        CGSize textSize = [headerText sizeWithFont:headerLabel.font constrainedToSize:headerLabel.frame.size lineBreakMode:UILineBreakModeWordWrap];
//        headerLabel.frame = CGRectMake(headerWidth - (textSize.width + padding), headerLabel.frame.origin.y, textSize.width, headerLabel.frame.size.height);
//        
//        [headerView addSubview:headerLabel];
        UIImageView *headerImage =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 48)];
        if(section ==0){
            headerImage.image = [UIImage imageNamed:@"generalServ.png"];
            //headerView.backgroundColor = [UIColor colorWithRed:173.0f/255 green:168.0f/255 blue:177.0f/255 alpha:1.0];
        }else if(section ==1){
             headerImage.image = [UIImage imageNamed:@"foodServ.png"];
           // headerView.backgroundColor = [UIColor colorWithRed:128.0f/255 green:183.0f/255 blue:150.0f/255 alpha:1.0];
        }else if(section == 2){
             headerImage.image = [UIImage imageNamed:@"drugServ.png"];
          //headerView.backgroundColor = [UIColor colorWithRed:102.0f/255 green:175.0f/255 blue:219.0f/255 alpha:1.0];
        }else if(section ==3){
            headerImage.image = [UIImage imageNamed:@"medServ.png"];
            // headerView.backgroundColor = [UIColor colorWithRed:242.0f/255 green:102.0f/255 blue:37.0f/255 alpha:1.0];
        }
        
       
        return headerImage;
    }
    
    return nil;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if([optionsList count] >0)
        return [sectionList count];
    else
        return 0;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)getData
{
    
    sdfaOptionsList = [[NSMutableArray alloc]init];
    foodOptionsList = [[NSMutableArray alloc]init];
    drugOptionsList = [[NSMutableArray alloc]init];
    medicalOptionsList = [[NSMutableArray alloc]init];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:E_SERVICE_SECTION_TAG]){
        NSLog(@" SAVED NEWS Main");
        SHOW_LOADING
        [self eServiceGetAllData];
        
        
        for (int i=0; i<[optionsList count]; i++) {
            
            NSInteger sectionId = [[[optionsList objectAtIndex:i] objectForKey:@"SectionId"] intValue];
            
            if(sectionId == 1){
                [sdfaOptionsList addObject:[optionsList objectAtIndex:i]];
            }else if(sectionId == 2){
                [foodOptionsList addObject:[optionsList objectAtIndex:i]];
            }else if(sectionId == 3){
                [drugOptionsList addObject:[optionsList objectAtIndex:i]];
            }else if(sectionId == 4){
                [medicalOptionsList addObject:[optionsList objectAtIndex:i]];
            }
        }
        [tvContacts reloadData];
        HIDE_LOADING
        
        
    }else{
        
        if (!internetConnection) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            HIDE_LOADING
            return;
        }
        
        SHOW_LOADING
        req =[[ASIHTTPRequest alloc]initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@PublicService", SERVER_IP]]];
        
        
        
        [req setCompletionBlock:^{
            NSString *response=  [req responseString];
            
            id result = [response JSONValue];
            optionsList=result;
            
            if(optionsList != nil && [optionsList count]>0){
                [self deleteData];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:E_SERVICE_SECTION_TAG];
            }
            
            for (int i=0; i<[optionsList count]; i++) {
                
                NSString *title_ar = [[optionsList objectAtIndex:i] objectForKey:@"TitleAr"];
                NSString *title_en = [[optionsList objectAtIndex:i] objectForKey:@"TitleEn"];
                NSString *link = [[optionsList objectAtIndex:i] objectForKey:@"Link"];
                NSInteger sectionId = [[[optionsList objectAtIndex:i] objectForKey:@"SectionId"] intValue];
                
                if(sectionId == 1){
                    [sdfaOptionsList addObject:[optionsList objectAtIndex:i]];
                }else if(sectionId == 2){
                    [foodOptionsList addObject:[optionsList objectAtIndex:i]];
                }else if(sectionId == 3){
                    [drugOptionsList addObject:[optionsList objectAtIndex:i]];
                }else if(sectionId == 4){
                    [medicalOptionsList addObject:[optionsList objectAtIndex:i]];
                }
                
                [self insertEServiceData:title_ar :title_en :link :[NSString stringWithFormat:@"%d",sectionId]];
                
            }
            [tvContacts reloadData];
            HIDE_LOADING
            
        }];
        [req setFailedBlock:^{
            
            [self eServiceGetAllData];
            HIDE_LOADING
            if(optionsList != nil && [optionsList count] >0){
                for (int i=0; i<[optionsList count]; i++) {
                    
                    NSInteger sectionId = [[[optionsList objectAtIndex:i] objectForKey:@"SectionId"] intValue];
                    
                    if(sectionId == 1){
                        [sdfaOptionsList addObject:[optionsList objectAtIndex:i]];
                    }else if(sectionId == 2){
                        [foodOptionsList addObject:[optionsList objectAtIndex:i]];
                    }else if(sectionId == 3){
                        [drugOptionsList addObject:[optionsList objectAtIndex:i]];
                    }else if(sectionId == 4){
                        [medicalOptionsList addObject:[optionsList objectAtIndex:i]];
                    }
                }
                [tvContacts reloadData];
            }else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"يرجى التحقق من اتصالك مع الإنترنت" delegate:self cancelButtonTitle:@"موافق" otherButtonTitles:nil];
                [alert show];
            }
        }];
        [req startAsynchronous];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    global = [Global getInstance];
    NavigationBar*navBar2 =  (NavigationBar*) [del.window viewWithTag:1000000];
    if (navBar2) {
        [navBar2.btnBack setHidden:NO];
        [del.window bringSubviewToFront:navBar2];
        [navBar2.btnMenu setImage:[global currentSectorMenuBtn] forState:UIControlStateNormal];
    }

    [sectorTileImgView setImage:[global currentSectorImage]];
    [self updateViews];
}

- (void)updateViews
{
    CGRect frame = [self navigationFrame];
    
    if(!IS_DEVICE_RUNNING_IOS_7_AND_ABOVE)
        frame.origin.y = 0.0;
    

    [sectorTileImgView setFrame:CGRectMake(sectorTileImgView.frame.origin.x, frame.origin.y + frame.size.height, sectorTileImgView.frame.size.width, sectorTileImgView.frame.size.height)];
    
    CGRect tableRect = tvContacts.frame;
    tableRect.origin.y = sectorTileImgView.frame.origin.y + sectorTileImgView.frame.size.height;
    tableRect.size.height = [UIScreen mainScreen].bounds.size.height - tableRect.origin.y;
    tvContacts.frame = tableRect;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    sectionList = [[Language getObject] getArrayWithKey:@"EServiceSectionList"];
    
    
    [self performSelector:@selector(getData) withObject:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    tvContacts = nil;
    
    [super viewDidUnload];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    if(![req isCancelled]) {
        // Cancels an asynchronous request
        [req cancel];
        // Cancels an asynchronous request, clearing all delegates and blocks first
        [req clearDelegatesAndCancel];
    }
    
    [super viewWillDisappear:animated];
}

#pragma  mark DataBase functions
- (void)insertEServiceData:(NSString*)title_ar :(NSString*)title_en :(NSString*)link :(NSString*)sectionId {
    
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    
    NSString *insertQuery = [@"insert into EService " stringByAppendingFormat:@" VALUES ('%@', '%@','%@','%@')",title_ar,title_en,link,sectionId];
    BOOL insterted = [database executeUpdate:insertQuery];
    
    NSLog(insterted ? @"Yes" : @"No");
    
    [database close];
}

-(void) eServiceGetAllData{
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    NSString *sqlSelectQuery = @"SELECT * FROM EService";
    
    optionsList = [[NSMutableArray alloc] init];
    
    // Query result
    FMResultSet *resultsWithNameLocation = [database executeQuery:sqlSelectQuery];
    while([resultsWithNameLocation next]) {
        NSString *eservice_title_ar = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"title_ar"]];
        NSString *eservice_title_en = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"title_en"]];
        NSString *eservice_link = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"link"]];
        NSString *eservice_sectionId = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"SectionId"]];
        
        NSMutableDictionary *newsDict = [[NSMutableDictionary alloc]init];
        
        [newsDict setValue:eservice_title_ar forKey:@"TitleAr"];
        [newsDict setValue:eservice_title_en forKey:@"TitleEn"];
        [newsDict setValue:eservice_link forKey:@"Link"];
        [newsDict setValue:eservice_sectionId forKey:@"SectionId"];
        
        [optionsList addObject:newsDict ];
        
        // loading your data into the array, dictionaries.
    }
    [database close];
    
    HIDE_LOADING
}

- (void)deleteData {
    
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    NSString *deleteQuery = @"DELETE FROM EService";
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    BOOL deleted = [database executeUpdate:deleteQuery];
    NSLog(deleted ? @"Yes" : @"No");
    [database close];
}


@end
