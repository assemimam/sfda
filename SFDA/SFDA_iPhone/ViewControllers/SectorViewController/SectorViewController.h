//
//  SectorViewController.h
//  SFDA
//
//  Created by Assem Imam on 9/4/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"


@interface SectorViewController : MasterViewController<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
{
    __weak IBOutlet UIActivityIndicatorView *aiLoading;
    __weak IBOutlet UILabel *lblLoading;
    __weak IBOutlet UIView *vLoading;
    __weak IBOutlet UITableView *tvMenuOptions;
}
@property (weak, nonatomic) IBOutlet UIImageView *sectorTileImgView;
@property(nonatomic) SECTOR_TYPE Sector;
@property (weak, nonatomic) IBOutlet UIView *newsView;

@end
