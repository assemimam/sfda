//
//  SectorViewController.m
//  SFDA
//
//  Created by Assem Imam on 9/4/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "SectorViewController.h"
#import "MainViewCustomCell.h"
#import "Language.h"
#import "AwarenessCenterViewController.h"
#import "QuestionAnswersViewController.h"
#import "CollabsableQuestionsViewColtroller.h"
#import "JSON.h"
#import "JSONKit.h"
#import "ASIHTTPRequest.h"
#import "NewsDetailsViewController.h"
#import "AboutViewController.h"
#import "Global.h"
#import "FMDatabase.h"
#import "WebViewController.h"
#import "DrugSearchViewController.h"
#import "NewsSclidingView.h"
#import "MedicalEquipSearchViewController.h"
#import "MainFoodSearchViewController.h"

@interface SectorViewController ()
{
    NSArray *optionsList;
    
    NSMutableArray *newsList;
    ASIHTTPRequest *req;
    UIImage*SelectedPageImage;
    UIImage*DefaultPageImage;
    UIView *pagerView;
    int indexCount;
    UIScrollView *svImages;
    NSString *currentSectionName;
    NSString *currentSectionCachingKey;
}
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UIImageView *bottomBar;
@end

@implementation SectorViewController
@synthesize Sector;
@synthesize newsView;
@synthesize sectorTileImgView;

#pragma  mark tableview datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MainViewCustomCell *cell =(MainViewCustomCell*) [tableView dequeueReusableCellWithIdentifier:@"cleOption"];
    if (cell==nil) {
        cell =(MainViewCustomCell*) [[[NSBundle mainBundle]loadNibNamed:[[Language getObject]getStringWithKey:@"MainViewCustomCell"] owner:nil options:nil]objectAtIndex:0];
        
    }
    
    cell.imgIcon.image = [UIImage imageNamed:[[optionsList objectAtIndex:indexPath.row] objectForKey:@"image"]];
    cell.OptionTitle.text = [[optionsList objectAtIndex:indexPath.row] objectForKey:@"option"];
    
    cell.backgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"main screen tile.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    cell.selectedBackgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"main screen tile.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  [optionsList count];
    
}
#pragma  mark tableview delegate
#pragma  mark tableview delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row ==0){
        AboutViewController *about = [[AboutViewController alloc]initWithNibName:@"AboutViewController" bundle:nil];
        
        
        switch (self.Sector) {
            case FOODS_SECTOR:
            {
                about.parameter = @"Food";
            }
                break;
            case MEDICAL_SECTOR:
            {
                about.parameter = @"Medical";
            }
                break;
            case DRUGS_SECTOR:
            {
                about.parameter = @"Drug";
            }
                break;
        }
        SHOW_LOADING;
        [self.navigationController pushViewController:about animated:YES];
    }else if(indexPath.row == 1){
        
        if (self.Sector== DRUGS_SECTOR) {
            
            DrugSearchViewController *vc_drugSearch = [[DrugSearchViewController alloc]init];
            [self.navigationController pushViewController:vc_drugSearch animated:YES];
        } else if (self.Sector== MEDICAL_SECTOR) {
            MedicalEquipSearchViewController *vc_drugSearch = [[MedicalEquipSearchViewController alloc]init];
            [self.navigationController pushViewController:vc_drugSearch animated:YES];
        }
        else if (self.Sector== FOODS_SECTOR) {
            MainFoodSearchViewController *vc_foodSearch = [[MainFoodSearchViewController alloc]init];
            [self.navigationController pushViewController:vc_foodSearch animated:YES];
        }
        
    }else if(indexPath.row == 2){
        
        if (self.Sector==DRUGS_SECTOR) {
            NSString *link  =@"https://ade.sfda.gov.sa/";
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:link]];
            
//            WebViewController *vc_web = [[WebViewController alloc]init];
//            vc_web.url = link;
//            [self.navigationController pushViewController:vc_web animated:YES];
        }
        else if (self.Sector==FOODS_SECTOR) {

            AwarenessCenterViewController *awreness = [[AwarenessCenterViewController alloc]init];
            awreness.parameter = @"Food";
            [self.navigationController pushViewController:awreness animated:YES];

        }
        else{
            AwarenessCenterViewController *awreness = [[AwarenessCenterViewController alloc]init];
            awreness.parameter = @"Medical";
            [self.navigationController pushViewController:awreness animated:YES];
        }
    }
    else if(indexPath.row == 3){
        if (self.Sector== DRUGS_SECTOR) {
            AwarenessCenterViewController *awreness = [[AwarenessCenterViewController alloc]init];
            awreness.parameter = @"Drug";
            [self.navigationController pushViewController:awreness animated:YES];
        }
        else{
            NSString *lang = ([[Language getObject] getLanguage] == languageArabic)?@"ar":@"en";
            
            NSString *link  = @"";
            switch (self.Sector) {
                case FOODS_SECTOR:
                {
                     link=[NSString stringWithFormat:@"http://sfda.gov.sa/%@/mobile/food/Pages/default.aspx",lang];

                }
                    break;
                case MEDICAL_SECTOR:
                {
                    link=[NSString stringWithFormat:@"http://sfda.gov.sa/%@/mobile/medicaldevices/Pages/default.aspx",lang];
                    
                }
                    break;
                case DRUGS_SECTOR:
                {
                    link=@"https://ade.sfda.gov.sa/";
                    
                    
                }
                    break;
            }
             [[UIApplication sharedApplication]openURL:[NSURL URLWithString:link]];

//            WebViewController *vc_web = [[WebViewController alloc]init];
//            vc_web.url = link;
//            [self.navigationController pushViewController:vc_web animated:YES];
        }
        
    }else if(indexPath.row == 4){
        CollabsableQuestionsViewColtroller*questionAnswer = [[CollabsableQuestionsViewColtroller alloc]init];
        //QuestionAnswersViewController *questionAnswer = [[QuestionAnswersViewController alloc]init];
        WebViewController * vc_web=[[WebViewController alloc]init];
        switch (self.Sector) {
            case FOODS_SECTOR:
            {
                questionAnswer.parameter = @"Food";
            }
                break;
            case MEDICAL_SECTOR:
            {
                questionAnswer.parameter = @"Medical";
            }
                break;
            case DRUGS_SECTOR:
            {
                NSString *lang = ([[Language getObject] getLanguage] == languageArabic)?@"ar":@"en";
                NSString*link= [NSString stringWithFormat:@"http://sfda.gov.sa/%@/mobile/drug/Pages/default.aspx",lang];
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:link]];
                vc_web.url=link;
            }
                break;
        }
        if (Sector==DRUGS_SECTOR) {
           // [self.navigationController pushViewController:vc_web animated:YES];
        }
        else
            [self.navigationController pushViewController:questionAnswer animated:YES];
        
    }
    else if(indexPath.row == 5){
      
        if (self.Sector==DRUGS_SECTOR) {
              CollabsableQuestionsViewColtroller*questionAnswer = [[CollabsableQuestionsViewColtroller alloc]init];
            questionAnswer.parameter = @"Drug";
             [self.navigationController pushViewController:questionAnswer animated:YES];
        }
    }
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)LoadMenuOptions
{
    switch (self.Sector) {
        case FOODS_SECTOR:
        {
            optionsList = [[Language getObject]getArrayWithKey:@"FoodSectorMenuOptions"];
        }
            break;
        case MEDICAL_SECTOR:
        {
            optionsList = [[Language getObject]getArrayWithKey:@"MedicalServicesSectorMenuOptions"];
        }
            break;
        case DRUGS_SECTOR:
        {
            optionsList = [[Language getObject]getArrayWithKey:@"DrugsSectorMenuOptions"];
        }
            break;
    }
    
    [tvMenuOptions reloadData];
    HIDE_LOADING
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    @try {
        [super viewWillAppear:animated];
        Global *global = [Global getInstance];
        
        switch (self.Sector) {
            case FOODS_SECTOR:
            {
                [global setCurrentSectorImage:[UIImage imageNamed:@"foodSecTitle.png" ]];
                [global setCurrentSectorMenuBtn:[UIImage imageNamed:@"home button good.png"]];
                [global setCurrentSectorColor:[global colorFromHexString:@"#3b7350"]];
                [global setCurrentSectorSubImage:[UIImage imageNamed:@"food sub green tile.png" ]];
            }
                break;
            case MEDICAL_SECTOR:
            {
                [global setCurrentSectorImage:[UIImage imageNamed:@"medSecTitle.png" ]];
                [global setCurrentSectorMenuBtn:[UIImage imageNamed:@"home button medical.png"]];
                [global setCurrentSectorColor:[global colorFromHexString:@"#a66136"]];
                [global setCurrentSectorSubImage:[UIImage imageNamed:@"medical sub orrange tile.png" ]];
            }
                break;
            case DRUGS_SECTOR:
            {
                [global setCurrentSectorImage:[UIImage imageNamed:@"drugSecTitle.png" ]];
                [global setCurrentSectorMenuBtn:[UIImage imageNamed:@"hoe button drug.png" ]];
                [global setCurrentSectorColor:[global colorFromHexString:@"#0973ba"]];
                [global setCurrentSectorSubImage:[UIImage imageNamed:@"drug sub blue tile.png" ]];
            }
                break;
        }
        
        
        del =(AppDelegate*) [[UIApplication sharedApplication]delegate];
        HIDE_LOADING
        NavigationBar*navBar2 =  (NavigationBar*) [del.window viewWithTag:1000000];
        if (navBar2) {
            [navBar2.btnBack setHidden:NO];
            [del.window bringSubviewToFront:navBar2];
            [navBar2.btnMenu setImage:[global currentSectorMenuBtn] forState:UIControlStateNormal];
        }
        

        [sectorTileImgView setImage:[global currentSectorImage]];
        newsView.backgroundColor = [UIColor colorWithPatternImage:[global currentSectorSubImage]];
        
        [self updateViews];
    }
    @catch (NSException *exception) {
        
    }
    
}

- (void)updateViews
{
    CGRect frame = [self navigationFrame];
    
    if(!IS_DEVICE_RUNNING_IOS_7_AND_ABOVE)
        frame.origin.y = 0.0;
    
    
    [sectorTileImgView setFrame:CGRectMake(sectorTileImgView.frame.origin.x, frame.origin.y + frame.size.height, sectorTileImgView.frame.size.width, sectorTileImgView.frame.size.height)];
    
    CGRect newsRect = newsView.frame;
    newsRect.origin.y = sectorTileImgView.frame.origin.y + sectorTileImgView.frame.size.height - 1;
    newsView.frame = newsRect;
    
    CGRect tableRect = _infoView.frame;
    tableRect.origin.y = newsView.frame.origin.y + newsView.frame.size.height;
    _infoView.frame = tableRect;
    
    tableRect.origin.y = 0.0;
    tableRect.size.height = [[UIScreen mainScreen] bounds].size.height - _infoView.frame.origin.y - _bottomBar.frame.size.height;
    tvMenuOptions.frame = tableRect;
    
    CGRect bottomBarRect = _bottomBar.frame;
    bottomBarRect.origin.y = tableRect.size.height;
    _bottomBar.frame = bottomBarRect;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    optionsList = [[NSArray alloc]init];
    
    [self LoadMenuOptions];
    
    if(self.Sector == MEDICAL_SECTOR)
    {
        DefaultPageImage = [UIImage imageNamed:@"WhiteO.png"];
        SelectedPageImage = [UIImage imageNamed:@"yellowO.png"];
    }else{
        DefaultPageImage = [UIImage imageNamed:@"WhiteO.png"];
        SelectedPageImage = [UIImage imageNamed:@"OrangeO.png"];
    }
    
    
    [self getNewsData];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma News_Horizontal_Scroll_View
-(void)RefreshSwitcher
{
    float Button_X = 0;
    float newsViewHeight = self.newsView.frame.size.height;
    svImages = [[UIScrollView alloc] initWithFrame:CGRectMake(0, (newsViewHeight - 100.0) / 2.0, 320, 95)];
    svImages.pagingEnabled = YES;
    svImages.delegate=self;
    [svImages setBackgroundColor:[UIColor clearColor]];
    //[svImages setScrollEnabled:NO];
    [svImages setShowsHorizontalScrollIndicator:NO];
    int index = 0;
    if ([newsList count] > 0) {
        
        if([[Language getObject] getLanguage] == languageArabic){
            for(int i=[newsList count]-1;i>=0;i--){
                [self drawNewsViewAt:Button_X :i];
                Button_X+= 320;
            }
        }else{
            for (NSDictionary *dict in newsList) {
                if (index > [newsList count]) {
                    index = 0;
                }
                [self drawNewsViewAt:Button_X :index];
                Button_X+= 320;
                index++;
            }
        }
        
        [self DrawPager];
        [svImages setContentSize:CGSizeMake(((320 +2) * [newsList count]), 95)];
        [svImages setContentOffset:CGPointMake(([newsList count]-1)*320,0) animated:YES];
        [newsView addSubview:svImages];
    }
    
    
    //hide loading view
    [aiLoading stopAnimating];
    [vLoading setHidden:YES];
    [self.view sendSubviewToBack:vLoading];
}

-(void)drawNewsViewAt:(int)x :(int)index{
    
    
    NewsSclidingView *newsSclidingView =(NewsSclidingView*) [[[NSBundle mainBundle]loadNibNamed:@"NewsSclidingView" owner:nil options:nil]objectAtIndex:0];
    
    if([[Language getObject] getLanguage] == languageArabic){
        [newsSclidingView.newsTitle setText:[[newsList objectAtIndex:index] objectForKey:@"TitleAr"]];
    }else{
        [newsSclidingView.newsTitle setText:[[newsList objectAtIndex:index] objectForKey:@"TitleEn"]];
    }
    
    newsSclidingView.newsTitle.font = [UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[newsList objectAtIndex:index] objectForKey:@"ImageUrl"]]];
        if (imageData==nil|| imageData.length==0) {
            imageData = [NSData dataWithContentsOfFile:[[newsList objectAtIndex:index] objectForKey:@"imagePath"]];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            newsSclidingView.newsImage.image = [UIImage imageWithData:imageData];
        });
    });
    
    
    
    NSArray* dateArray = [[[newsList objectAtIndex:index] objectForKey:@"PubDate"]componentsSeparatedByString: @" "];
    [newsSclidingView.newsDate  setText:[dateArray objectAtIndex:0]];
    
    
    
    [newsSclidingView setFrame:CGRectMake(x, 0, svImages.frame.size.width, svImages.frame.size.height)];
    newsSclidingView.backgroundColor = [UIColor clearColor];
    newsSclidingView.tag = [[[newsList objectAtIndex:index] objectForKey:@"$id"] intValue];
    
    
    UITapGestureRecognizer *singleTouch = [[UITapGestureRecognizer alloc]
                                           initWithTarget:self action:@selector(handleSingleTouch:)];
    singleTouch.numberOfTapsRequired = 1;
    [newsSclidingView addGestureRecognizer:singleTouch];
    [newsSclidingView setBackgroundColor:[UIColor clearColor]];
    [newsSclidingView setUserInteractionEnabled:YES];
    
    
    
    
    [svImages addSubview:newsSclidingView];
    
}

-(void)DrawPager{
    
    float ImageWidth=7;
    float ImageHeight=7;
    float Image_X=305;
    
    
    pagerView = [[UIView alloc] initWithFrame:CGRectMake(0, svImages.frame.size.height + svImages.frame.origin.y, 100, 10)];
    pagerView.backgroundColor = [UIColor clearColor];
    
    for (int page =[newsList count]; page>=1 ; page--) {
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(Image_X, 0, ImageWidth, ImageHeight)]  ;
        img.tag = page;
        if (page == 1) {
            img.image = SelectedPageImage;
        }
        else {
            img.image = DefaultPageImage;
        }
        
        [pagerView addSubview:img];
        Image_X-=(ImageWidth+5);
    }
    
    [newsView addSubview:pagerView];
    // [newsView bringSubviewToFront:pagerView];
}

-(void)SetPagerSelectePage:(NSInteger)index
{
    
    for (int page = 1; page<= [newsList count]; page++) {
        [(UIImageView*) [pagerView viewWithTag:page] setImage:DefaultPageImage];
    }
    [(UIImageView*)  [pagerView viewWithTag:index+1] setImage:SelectedPageImage];
}

-(void)handleSingleTouch:(UITapGestureRecognizer*)sender
{
    NSString *nibName = [[Language getObject] getStringWithKey:@"NewsDetailsViewController"];
    
    NewsDetailsViewController *newsDetails = [[NewsDetailsViewController alloc]initWithNibName:nibName bundle:nil];
    NSLog(@" sender tah = %d , liis sie  = %d",sender.view.tag ,[newsList count]);
    newsDetails.newsObj = [newsList objectAtIndex:sender.view.tag-1];
    [self.navigationController pushViewController:newsDetails animated:YES];
}
- (void)scrollViewDidScroll:(UIScrollView *)_scrollView {
    if (_scrollView != svImages) {
        return;
    }
    @try {
        
        float scrollViewWidth = _scrollView.frame.size.width;
        float scrollContentSizeWidth = _scrollView.contentSize.width;
        float scrollOffset = _scrollView.contentOffset.x;
        
        if (scrollOffset + scrollViewWidth == scrollContentSizeWidth)
        {
            [svImages setContentOffset:CGPointMake(([newsList count]-1)*320,0) animated:YES];
            //            indexCount = (int)(imagesScrollView.contentOffset.x /imagesScrollView.frame.size.width);
            //            //  [delegate didMoveImageAtIndex:(NSInteger)[self.ImageIDS  objectAtIndex:indexCount]];
            //            [delegate didMoveImageAtIndex:(NSInteger)indexCount];
        }else{
            indexCount = (int)(_scrollView.contentOffset.x /_scrollView.frame.size.width);
            //[svImages didMoveImageAtIndex:(NSInteger)indexCount];
            [self SetPagerSelectePage:(NSInteger)indexCount];
        }
        
        
        
        
        //        SLIDING_ENBLED = NO;
        //NSLog(@"did scroll   %i",indexCount);
        
        
        // [delegate didMoveAdAtIndex:(NSInteger)[self.ImageIDS  objectAtIndex:indexCount]];
        
    }
    @catch (NSException *exception) {
        
    }
    
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)_scrollView

{
    if (_scrollView != svImages) {
        return;
    }
    @try {
        
        ////////NSLog(@"did end   %i",indexCount);
        indexCount = (int)(_scrollView.contentOffset.x /_scrollView.frame.size.width);
        // //////NSLog(@"did scroll   %i",indexCount);
        
        //[delegate didMoveImageAtIndex:(NSInteger)indexCount];
        //[delegate didMoveImageAtIndex:(NSInteger)[self.ImageIDS  objectAtIndex:indexCount]];
        // [self setSlidingEnabled:YES];
        // SLIDING_ENBLED = YES;
    }
    @catch (NSException *exception) {
        
    }
    
    
}


-(void)getNewsData
{
    @try {
        //show loading view
        
        lblLoading.text = [[Language getObject]getStringWithKey:@"LoadingTitle"];
        [aiLoading startAnimating];
        [vLoading setHidden:NO];
        [self.view bringSubviewToFront:vLoading];
        
        
        NSString *secName ;
        
        BOOL isLoadedSection;
        
        switch (self.Sector) {
            case FOODS_SECTOR:
            {
                currentSectionName = @"FOOD";
                currentSectionCachingKey = NEWS_SCROLL_FOOD_TAG;
                secName = @"?Section=FOOD&count=20";
                isLoadedSection = [[NSUserDefaults standardUserDefaults] boolForKey:NEWS_SCROLL_FOOD_TAG];
            }
                break;
            case MEDICAL_SECTOR:
            {
                currentSectionName = @"MEDI";
                currentSectionCachingKey = NEWS_SCROLL_MEDI_TAG;
                secName = @"?Section=MEDICAL&count=20";
                isLoadedSection = [[NSUserDefaults standardUserDefaults] boolForKey:NEWS_SCROLL_MEDI_TAG];
            }
                break;
            case DRUGS_SECTOR:
            {
                currentSectionName = @"DRUG";
                currentSectionCachingKey = NEWS_SCROLL_DRUG_TAG;
                secName = @"?Section=DRUG&count=20";
                isLoadedSection = [[NSUserDefaults standardUserDefaults] boolForKey:NEWS_SCROLL_DRUG_TAG];
            }
                break;
        }
        
        if(isLoadedSection){
            
            [self newsGetAllData];
            
            NSLog(@"%d",[newsList count]);
            
            [self performSelectorOnMainThread:@selector(RefreshSwitcher) withObject:nil waitUntilDone:NO];
        }else{
            if (!internetConnection) {
                //hide loading view
                [[[UIAlertView alloc]initWithTitle:nil
                                           message:[[Language getObject]getStringWithKey:@"interNetMessage"]
                                          delegate:nil
                                 cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"]
                                 otherButtonTitles: nil]show];
                [aiLoading stopAnimating];
                //[vLoading setHidden:YES];
                [self.view sendSubviewToBack:vLoading];
                HIDE_LOADING
                return;
            }

            
            req =[[ASIHTTPRequest alloc]initWithURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@News", SERVER_IP] stringByAppendingString:secName]]];
            
            [req setCompletionBlock:^{
                NSString *response=  [req responseString];
                
                id result = [response JSONValue];
                newsList=result;
                
                if(newsList != nil && [newsList count]>0){
                    
                    [self deleteData];
                    NSLog(@" currentSectionCachingKey %@",currentSectionCachingKey);
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:currentSectionCachingKey];
                    
                }
                
                for(NSDictionary *obj in newsList)
                    
                {
                    NSString *url = [obj objectForKey:@"ImageUrl"];
                    
                    dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    //this will start the image loading in bg
                    dispatch_async(concurrentQueue, ^{
                        
                        NSURL *imgUrl = [NSURL URLWithString:url];
                        NSURLRequest* imgRequest = [NSURLRequest requestWithURL:imgUrl];
                        NSData *newsImageData = [[NSData alloc]initWithData:[NSURLConnection sendSynchronousRequest:imgRequest returningResponse:nil error:nil]];
                        imgRequest=nil;
                        
                        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                        NSString *Documentpath = [paths objectAtIndex:0];
                        NSString *TargetPath = [Documentpath stringByAppendingPathComponent:@"NewsScrollImages"];
                        TargetPath = [TargetPath stringByAppendingPathComponent:currentSectionName];
                        
                        NSFileManager *Fmanager = [NSFileManager defaultManager];
                        
                        if ([Fmanager fileExistsAtPath:TargetPath isDirectory:nil]) {
                            
                        }
                        else
                        {
                            BOOL ret =[Fmanager createDirectoryAtPath:TargetPath withIntermediateDirectories:YES attributes:nil error:nil];
                            if (ret) {
                                
                            };
                        }
                        
                        NSString *fileName= [TargetPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",[obj objectForKey:@"$id"]] ];
                        
                        
                        //this will set the image when loading is finished
                        dispatch_async(dispatch_get_main_queue(), ^{
                            //save image file
                            if (newsImageData!=nil &&newsImageData.length > 0 ) {
                                
                                
                                
                                [newsImageData writeToFile:fileName atomically:YES];
                                
                                
                                NSMutableDictionary *newsDic =[[NSMutableDictionary alloc]init];
                                for (NSString *key in [obj allKeys]) {
                                    [newsDic setValue:[obj objectForKey:key] forKey:key];
                                }
                                
                                [newsDic setValue:fileName forKey:@"ImagePath"];
                                
                                
                                [self performSelectorOnMainThread:@selector(insertNewsInformation:) withObject:newsDic waitUntilDone:NO];
                            }
                            
                            
                        });
                    });
                    
                    
                    
                }
                
                [self performSelectorOnMainThread:@selector(RefreshSwitcher) withObject:nil waitUntilDone:NO];
                
                
            }];
            [req setFailedBlock:^{
                NSLog(@"error net");
            }];
            [req startAsynchronous];
        }

    }
    @catch (NSException *exception) {
        
    }
    
    }
-(void)insertNewsInformation:(id)obj
{
    
    [self insertNewsData:[obj objectForKey:@"$id"] :[obj objectForKey:@"TitleAr"] :[obj objectForKey:@"TitleEn"] :[obj objectForKey:@"DescriptionAr"] :[obj objectForKey:@"DescriptionEn"] :[obj objectForKey:@"Url"] :[obj objectForKey:@"PubDate"]:[obj objectForKey:@"ImagePath"]:[obj objectForKey:@"ImageUrl"]];
    
    
}
- (void)viewDidUnload {
    tvMenuOptions = nil;
    [self setSectorTileImgView:nil];
    vLoading = nil;
    aiLoading = nil;
    lblLoading = nil;
    [super viewDidUnload];
}

#pragma  mark DataBase functions


- (void)deleteData {
    
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    NSString *deleteQuery = [[@"DELETE FROM news_scroll where  section='" stringByAppendingString:currentSectionName] stringByAppendingString:@"'"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    BOOL deleted = [database executeUpdate:deleteQuery];
    NSLog(deleted ? @"Yes" : @"No");
    [database close];
}


#pragma  mark DataBase functions
- (void)insertNewsData:(NSString*)nid :(NSString*)title_ar :(NSString*)title_en :(NSString*)content_ar :(NSString*)content_en :(NSString*)link :(NSString*)date :(NSString*)image_path :(NSString*)image_url {
    
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    
    NSString *insertQuery = [@"insert into news_scroll " stringByAppendingFormat:@" VALUES ('%@', '%@','%@','%@','%@','%@','%@','%@','%@','%@')",link,date,currentSectionName,nid,image_path,image_url,title_ar,title_en,content_ar,content_en];
    BOOL insterted = [database executeUpdate:insertQuery];
    
    NSLog(insterted ? @"Yes" : @"No");
    
    [database close];
}

-(void) newsGetAllData{
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    NSString *sqlSelectQuery = [NSString stringWithFormat: @"SELECT * FROM news_scroll where  section='%@' order by id asc", currentSectionName];
    
    
    NSLog(@"%@",sqlSelectQuery);
    
    newsList = [[NSMutableArray alloc] init];
    
    // Query result
    FMResultSet *resultsWithNameLocation = [database executeQuery:sqlSelectQuery];
    while([resultsWithNameLocation next]) {
        NSString *news_title_ar = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"title_ar"]];
        NSString *news_title_en = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"title_en"]];
        NSString *news_content_ar = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"content_ar"]];
        NSString *news_content_en = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"content_en"]];
        NSString *news_link = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"link"]];
        NSString *news_date = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"PubDate"]];
        NSString *news_id = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"id"]];
        NSString *image_path = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"image_path"]];
        
        NSMutableDictionary *newsDict = [[NSMutableDictionary alloc]init];
        if([[Language getObject] getLanguage] == languageArabic){
            [newsDict setValue:news_title_ar forKey:@"TitleAr"];
            [newsDict setValue:news_content_ar forKey:@"DescriptionAr"];
        }else{
            [newsDict setValue:news_title_en forKey:@"TitleEn"];
            [newsDict setValue:news_content_en forKey:@"DescriptionEn"];
        }
        
        [newsDict setValue:news_link forKey:@"Url"];
        [newsDict setValue:news_date forKey:@"PubDate"];
        [newsDict setValue:news_id forKey:@"$id"];
        
        [newsDict setValue:image_path forKey:@"imagePath"];
        [newsList addObject:newsDict ];
        
        // loading your data into the array, dictionaries.
    }
    [database close];
    
}


@end
