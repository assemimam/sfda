//
//  WebViewController.h
//  SFDA
//
//  Created by Assem Imam on 10/9/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"

@interface WebViewController : MasterViewController<UIWebViewDelegate>
{
    
    __weak IBOutlet UIWebView *_webView;
}
@property(nonatomic,retain)NSString *url;
@end
