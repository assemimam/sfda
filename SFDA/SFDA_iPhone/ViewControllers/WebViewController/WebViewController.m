//
//  WebViewController.m
//  SFDA
//
//  Created by Assem Imam on 10/9/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "WebViewController.h"
#import "Global.h"
#import "NavigationBar.h"
#import "AppDelegate.h"
#import "CommonMethods.h"
#import "Language.h"
@interface WebViewController ()
{
    AppDelegate*del;
}
@end

@implementation WebViewController
@synthesize url;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    BOOL barIsAdded = FALSE;
    for (UIView *view in [self.view subviews]) {
        if (view.tag ==1000000 ) {
            barIsAdded = TRUE;
            break;
        }
    }
    if(!barIsAdded) {
        [super viewWillAppear:animated];
    
        del = (AppDelegate*)[[UIApplication sharedApplication]delegate];
        NavigationBar*navBar2 =  (NavigationBar*) [del.window viewWithTag:1000000];
        if (navBar2) {
            [navBar2.btnRefresh setHidden:YES];
            [navBar2.btnBack setHidden:NO];
            //    [del.window bringSubviewToFront:navBar2];
            CGRect frame = navBar2.frame;
            frame.origin.y = 0.0;
            navBar2.frame = frame;
            
            Global* global = [Global getInstance];
            [navBar2.btnMenu setImage:[global currentSectorMenuBtn] forState:UIControlStateNormal];
            [self.view addSubview:navBar2];
        }

    }
//    [del.window sendSubviewToBack:navBar2];
    [self updateViews];
   
}

- (void)updateViews
{
    CGRect frame = [self navigationFrame];
    
    if(!IS_DEVICE_RUNNING_IOS_7_AND_ABOVE)
        frame.origin.y = 0.0;
    
    
    [_webView setFrame:CGRectMake(_webView.frame.origin.x, frame.origin.y + frame.size.height, _webView.frame.size.width, _webView.frame.size.height)];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if (!internetConnection) {
        [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
        HIDE_LOADING
        return;
    }
    SHOW_LOADING
//    [_webView loadRequest:  [NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
    
    
    //for EACH url to load
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.url]];
    
    //[_webView loadRequest:request];

    NSURL *fullURL = [NSURL URLWithString:self.url];
    NSString *base = [NSString stringWithFormat:@"%@://%@",[fullURL scheme], [fullURL host]];
    NSURL *baseURL = [NSURL URLWithString:base];
    
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue] // one should ideally use a different queue here to free main thread and ONLY do the imageView.image setting in Main thread
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if(!error) {
                                   //load webview! with Data maybe!
                                   [_webView loadData:data MIMEType: @"text/html" textEncodingName: @"UTF-8" baseURL:baseURL];
                               }
                           }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    _webView = nil;
    [super viewDidUnload];
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    HIDE_LOADING
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    HIDE_LOADING
}


@end
