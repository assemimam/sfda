//
//  ReportViewController.h
//  SFDA_ALL
//
//  Created by iOS Developer on 2/24/1436 AH.
//  Copyright (c) 1436 assem. All rights reserved.
//

#import "MasterViewController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
@interface ReportViewController : MasterViewController<MKMapViewDelegate , CLLocationManagerDelegate>
{
    __weak IBOutlet UIScrollView  *ScrollView;
    __weak IBOutlet UIView        *TitleView;
    __weak IBOutlet UIView        *LocationView;
    __weak IBOutlet UILabel       *RequestTypeLabal;
    __weak IBOutlet UILabel       *FoodTypeLabel;
    __weak IBOutlet UITextField   *NameTextField;
    __weak IBOutlet UITextField   *MobileNumberTextField;
    __weak IBOutlet UITextField   *EmailTextField;
    __weak IBOutlet UITextField   *CityTextField;
    __weak IBOutlet UITextView    *DetailTextView;
    __weak IBOutlet UIImageView   *ProductImageView;
    __weak IBOutlet MKMapView     *MKmapView;
    __weak IBOutlet UIButton      *SendMailButton,*locationBtn;
    
    
}
@end
