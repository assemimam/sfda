//
//  MasterViewController.h
//  SFDA
//
//  Created by Assem Imam on 9/3/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "NavigationBar.h"
#import "PopUpView.h"
#import "Language.h"
//typedef enum
//{
//    FOODS_SECTOR=0,
//    DRUGS_SECTOR=1,
//    MEDICAL_SECTOR=2
//}
//SECTOR_TYPE;

@interface MasterViewController : UIViewController<UITextFieldDelegate,popUpProtocol>
{
    AppDelegate *del;
    BOOL SHOW_POPUP;
}

@property (nonatomic, strong) NavigationBar* navBar;;

- (CGRect)navigationFrame;

@end
