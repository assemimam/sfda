//
//  NewsDetailsViewController.m
//  SFDA
//
//  Created by yehia on 9/14/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "NewsDetailsViewController.h"
#import "Language.h"
#import "NavigationBar.h"
#import "Global.h"
#import "WebViewController.h"
#include "WebViewController.h"

@interface NewsDetailsViewController ()
{
    __weak IBOutlet UIView *ContainerView;
    Global *global;
}
@end

@implementation NewsDetailsViewController
@synthesize newsObj,lblNewsTitle,lblNewsDesc,lblNewsImg,lblDate,sectorTileImgView;
@synthesize scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
  
    lblNewsTitle.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:16];
    lblNewsDesc.font =[UIFont fontWithName:@"GESSTwoMedium-Medium" size:16];

    if([[Language getObject] getLanguage] == languageArabic){
        lblNewsTitle.text = [newsObj objectForKey:@"TitleAr"];
        lblNewsDesc.text = [[newsObj objectForKey:@"DescriptionAr"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        lblNewsTitle.textAlignment = UITextAlignmentRight;
        lblNewsDesc.textAlignment = UITextAlignmentRight;
        lblDate.textAlignment = UITextAlignmentRight;
        
    }else{
        lblNewsTitle.text = [newsObj objectForKey:@"TitleEn"];
        lblNewsDesc.text = [[newsObj objectForKey:@"DescriptionEn"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        lblNewsTitle.textAlignment = UITextAlignmentLeft;
        lblNewsDesc.textAlignment = UITextAlignmentLeft;
        lblDate.textAlignment = UITextAlignmentLeft;
    }

    
    NSLog(@"desc %@",[[newsObj objectForKey:@"DescriptionAr"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]);
    [lblNewsDesc sizeToFit];
   
    lblDate.text = [newsObj objectForKey:@"PubDate"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[newsObj objectForKey:@"ImageUrl"]]];
        if (imageData==nil || imageData.length==0) {
            imageData = [NSData dataWithContentsOfFile:[newsObj objectForKey:@"imagePath"]];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
          
            lblNewsImg.image = [UIImage imageWithData:imageData];
        });
    });
    
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width,lblNewsDesc.frame.origin.y+ lblNewsDesc.frame.size.height+50)];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    del =(AppDelegate*) [[UIApplication sharedApplication]delegate];
    NavigationBar*navBar2 =  (NavigationBar*) [del.window viewWithTag:1000000];
    if (navBar2) {
        [navBar2.btnBack setHidden:NO];
        [del.window bringSubviewToFront:navBar2];
    }

    global = [Global getInstance];
    [sectorTileImgView setImage:[global currentSectorImage]];
    
    [self updateViews];
}

- (void)updateViews
{
    CGRect frame = [self navigationFrame];
    
    if(!IS_DEVICE_RUNNING_IOS_7_AND_ABOVE)
        frame.origin.y = 0.0;
    

    [sectorTileImgView setFrame:CGRectMake(sectorTileImgView.frame.origin.x, frame.origin.y + frame.size.height, sectorTileImgView.frame.size.width, sectorTileImgView.frame.size.height)];
    
    CGRect tableRect = scrollView.frame;
    tableRect.origin.y = sectorTileImgView.frame.origin.y + sectorTileImgView.frame.size.height;
    scrollView.frame = tableRect;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setLblNewsTitle:nil];
    [self setLblNewsImg:nil];
    [self setLblNewsDesc:nil];
    [self setScrollView:nil];
    [super viewDidUnload];
}
- (IBAction)moreAction:(id)sender {
    //    NSURL *url = [NSURL URLWithString:[newsObj objectForKey:@"Url"]];
    //    [[UIApplication sharedApplication] openURL:url];
    WebViewController *vc_web = [[WebViewController alloc]init];
    vc_web.url = [newsObj objectForKey:@"Url"];
    [self.navigationController pushViewController:vc_web animated:YES];
}


- (IBAction)newsShare:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:@""
                                  delegate:self
                                  cancelButtonTitle:@"إلغاء"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"فيس بوك", @"تويتر",
                                  @"رسالة قصيره",@"بريد الكتروني", nil];
    
    
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet showInView:self.view];
    
}

#pragma Action sheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            WebViewController *vc_web = [[WebViewController alloc]init];
            vc_web.url = [@"http://www.facebook.com/sharer.php?u=" stringByAppendingString:[newsObj objectForKey:@"Url"]];
            [self.navigationController pushViewController:vc_web animated:YES];
        }
            break;
        case 1:
        {
            WebViewController *vc_web = [[WebViewController alloc]init];
            vc_web.url = [@"http://www.twitter.com/share?url=" stringByAppendingString:[newsObj objectForKey:@"Url"]];
            [self.navigationController pushViewController:vc_web animated:YES];
        }
            break;
        case 2:
        {
            [self sendNativeSMS];
        }
            break;
        case 3:
        {
            if ([MFMailComposeViewController canSendMail]){
                 [[del.window viewWithTag:1000000] setHidden:YES];
            
            MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
            controller.mailComposeDelegate = self;
            [controller setSubject:@""];
            [controller setMessageBody:[newsObj objectForKey:@"Url"] isHTML:NO];
           
            if (controller) [self presentModalViewController:controller animated:YES];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"ادخل بريدك الالكترونى"
                                                               delegate:nil
                                                      cancelButtonTitle:@"موافق"
                                                      otherButtonTitles: nil];
                [alert show];

            }
        }
            break;
    }
}

#pragma  mark MFMailCompose delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissModalViewControllerAnimated:YES];
    
    [[del.window viewWithTag:1000000] setHidden:NO];
}


#pragma  mark MFMessage delegate
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{

    [self dismissModalViewControllerAnimated:YES];
    [[del.window viewWithTag:1000000] setHidden:NO];
}

-(void)sendNativeSMS{
    
        
        Class messageClass = (NSClassFromString(@"MFMessageComposeViewController"));
        if (messageClass != nil) {
            // Check whether the current device is configured for sending SMS messages
            if ([messageClass canSendText]) {
                MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
                picker.messageComposeDelegate = self;
                picker.recipients = [NSArray arrayWithObject:@""];
                picker.body = [newsObj objectForKey:@"Url"];
                [[del.window viewWithTag:1000000] setHidden:YES];
                [self presentModalViewController:picker animated:YES];
                
                
            }else{
                UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:nil message:@"غير مسموح بارسال رسائل قصيره" delegate:self cancelButtonTitle:@"الغاء" otherButtonTitles:nil, nil];
                [Alert show];
            }
        }else{
            UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:nil message:@"غير مسموح بارسال رسائل قصيره" delegate:self cancelButtonTitle:@"الغاء" otherButtonTitles:nil, nil];
            [Alert show];
        }
}
@end
