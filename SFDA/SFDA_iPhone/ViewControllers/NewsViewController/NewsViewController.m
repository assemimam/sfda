//
//  NewsViewController.m
//  SFDA
//
//  Created by yehia on 9/14/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "NewsViewController.h"
#import "NewsViewCustomCell.h"
#import "Language.h"
#import "JSON.h"
#import "JSONKit.h"
#import "ASIHTTPRequest.h"
#import "NavigationBar.h"
#import "NewsDetailsViewController.h"
#import "AppRecord.h"
#import "IconDownloader.h"
#import "FMDatabase.h"
#import "Global.h"

#define image_TAG_CellView 990

@interface NewsViewController (){
    NSMutableArray *optionsList;
    ASIHTTPRequest *req;
    Global *global;
}
@property (weak, nonatomic) IBOutlet UIButton *headerImg;
@end

@implementation NewsViewController
@synthesize tvNews;
@synthesize entries,imageDownloadsInProgress;
@synthesize sectorTileImgViewButton;


#pragma  mark tableview datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewsViewCustomCell *cell =(NewsViewCustomCell*) [tableView dequeueReusableCellWithIdentifier:@"cleOption"];
    if (cell==nil) {
        cell =(NewsViewCustomCell*) [[[NSBundle mainBundle]loadNibNamed:[[Language getObject]getStringWithKey:@"NewsViewCustomCell"] owner:nil options:nil]objectAtIndex:0];
        
    }
    
    NSString *title,*date,*categoryAr,*categoryEn;
    
    if ([[Language getObject]getLanguage]==languageArabic) {
        title = [[optionsList objectAtIndex:indexPath.row] objectForKey:@"TitleAr"];
    }else{
        title = [[optionsList objectAtIndex:indexPath.row] objectForKey:@"TitleEn"];
    }
    
    categoryAr = [[optionsList objectAtIndex:indexPath.row]objectForKey:@"CategoryAr"];
    categoryEn = [[optionsList objectAtIndex:indexPath.row]objectForKey:@"CategoryEn"];
    date = [[optionsList objectAtIndex:indexPath.row] objectForKey:@"PubDate"];
    
    cell.lblNewsTitle.text = title;
    cell.lblNewsTitle.font =[UIFont fontWithName:@"GESSTwoLight-Light" size:15];
    cell.lblNewsDate.text = date;
    [cell.aiLoading startAnimating];
    //NSLog(@"path %@",[[optionsList objectAtIndex:indexPath.row] objectForKey:@"imagePath"]);
    UIImageView* thumb = (UIImageView*)[cell viewWithTag:image_TAG_CellView];
    if([[optionsList objectAtIndex:indexPath.row] objectForKey:@"imagePath"]){
        cell.thumbnail.image =[UIImage imageWithData: [NSData dataWithContentsOfFile:[[optionsList objectAtIndex:indexPath.row] objectForKey:@"imagePath"]]];
        [cell.aiLoading stopAnimating];
        [cell.aiLoading setHidden:YES];
    }
    else
    {
        AppRecord * appRecord=[entries objectAtIndex:indexPath.row];
        // Only load cached images; defer new downloads until scrolling ends
        if (!appRecord.appIcon)
        {
            if (self.tvNews.dragging == NO && self.tvNews.decelerating == NO)
            {
                [self startIconDownload:appRecord forIndexPath:indexPath];
            }
            // if a download is deferred or in progress, return a placeholder image
            thumb.image = [UIImage imageNamed:@"new icon.png"];
        }
        else
        {
            
            //        thumb.image=[UIImage imageNamed:@"Icon-Thumbnail"];
            //        [self startIconDownload:appRecord forIndexPath:indexPath];
            thumb.image = appRecord.appIcon;
            [cell.aiLoading stopAnimating];
            [cell.aiLoading setHidden:YES];
        }
        
    }
    //    if ([stringToSearchWithin rangeOfString:s].location != NSNotFound)
    if([categoryAr rangeOfString:@"تنبيه"].location!= NSNotFound  || [categoryEn rangeOfString:@"تنبيه"].location!= NSNotFound){
        cell.iconType.image = [UIImage imageNamed:@"main_news-06.png"];
        //    }else //if([categoryAr rangeOfString:@"عام"].location != NSNotFound  || [categoryEn rangeOfString:@"عام"].location != NSNotFound){
    }else{
        if ([categoryAr rangeOfString:@"الأجهزة الطبية"].location!= NSNotFound) { //medical
            cell.iconType.image = [UIImage imageNamed:@"newsMedIcon.png"];
        }
        else if ([categoryAr rangeOfString:@"الدواء"].location!= NSNotFound){//drug
            cell.iconType.image = [UIImage imageNamed:@"newsDrugIcon.png"];
        }
        else if ([categoryAr rangeOfString:@"الغذاء"].location!= NSNotFound){//food
            cell.iconType.image = [UIImage imageNamed:@"main_news-05.png"];
        }
        else if ([categoryAr rangeOfString:@"الرئيسية"].location!= NSNotFound){//sfda <main>
            cell.iconType.image = [UIImage imageNamed:@"newsOrgIcon.png"];
        }
 
                   
        
    }
    
    cell.backgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"main screen tile.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    cell.selectedBackgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"main screen tile.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [optionsList count];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 118;
}

#pragma  mark tableview delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *nibName = [[Language getObject] getStringWithKey:@"NewsDetailsViewController"];
    [global setCurrentSectorImage:[UIImage imageNamed:@"newSecTitle_noarrow.png"]];
    NewsDetailsViewController *newsDetails = [[NewsDetailsViewController alloc]initWithNibName:nibName bundle:nil];
    newsDetails.newsObj = [optionsList objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:newsDetails animated:YES];
}


-(void)getData
{
    
    
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:NEWS_ALL_MAIN_TAG]){
        NSLog(@" SAVED NEWS Main");
        [self newsGetAllData:@"where cat_ar='تنبيه'"];
        [tvNews reloadData];
    }else{
        NSLog(@" load NEWS Main from inernet");
        
        if (!internetConnection) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            HIDE_LOADING
            return;
        }
        
        req =[[ASIHTTPRequest alloc]initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@News?section=latestnews", SERVER_IP]]];
        [req setCompletionBlock:^{
            NSString *response=  [req responseString];
            
            id result = [response JSONValue];
            optionsList=result;
            
            entries=[[NSMutableArray alloc] init];
            self.imageDownloadsInProgress = [NSMutableDictionary dictionary];
            
            if(optionsList != nil && [optionsList count]>0){
                [self deleteData];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:NEWS_ALL_MAIN_TAG];
            }
            
            for(NSDictionary *obj in optionsList)
            {
                AppRecord * appRecord=[[AppRecord alloc] init];
                appRecord.imageURLString=[obj objectForKey:@"ImageUrl"];
                [entries addObject:appRecord];
                
                NSString *url = [obj objectForKey:@"ImageUrl"];
                
                dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                
                //this will start the image loading in bg
                dispatch_async(concurrentQueue, ^{
                    
                    NSURL *imgUrl = [NSURL URLWithString:url];
                    NSURLRequest* imgRequest = [NSURLRequest requestWithURL:imgUrl];
                    NSData *newsImageData = [[NSData alloc]initWithData:[NSURLConnection sendSynchronousRequest:imgRequest returningResponse:nil error:nil]];
                    imgRequest=nil;
                    
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    NSString *Documentpath = [paths objectAtIndex:0];
                    NSString *TargetPath = [Documentpath stringByAppendingPathComponent:@"NewsImages"];
                    
                    NSFileManager *Fmanager = [NSFileManager defaultManager];
                    
                    if ([Fmanager fileExistsAtPath:TargetPath isDirectory:nil]) {
                        
                    }
                    else
                    {
                        BOOL ret =[Fmanager createDirectoryAtPath:TargetPath withIntermediateDirectories:YES attributes:nil error:nil];
                        if (ret) {
                            
                        };
                    }
                    
                    NSString *fileName= [TargetPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",[obj objectForKey:@"$id"]] ];
                    
                    
                    //this will set the image when loading is finished
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //save image file
                        if (newsImageData!=nil &&newsImageData.length > 0 ) {
                            
                            
                            
                            [newsImageData writeToFile:fileName atomically:YES];
                            
                            
                            NSMutableDictionary *newsDic =[[NSMutableDictionary alloc]init];
                            for (NSString *key in [obj allKeys]) {
                                [newsDic setValue:[obj objectForKey:key] forKey:key];
                            }
                            
                            [newsDic setValue:fileName forKey:@"ImagePath"];
                            
                            
                            [self performSelectorOnMainThread:@selector(insertNewsInformation:) withObject:newsDic waitUntilDone:NO];
                        }
                        
                        
                    });
                });
                
                
                
                
            }
            [tvNews reloadData];
            
            
            
            HIDE_LOADING
            
            
        }];
        [req setFailedBlock:^{
            
            [self newsGetAllData:@"where cat_ar='تنبيه'"];
            
            if(optionsList == nil || [optionsList count] ==0){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"يرجى التحقق من اتصالك مع الإنترنت" delegate:self cancelButtonTitle:@"موافق" otherButtonTitles:nil];
                [alert show];
                //            NSLog(@"%@",[req error]);
            }else{
                NSLog(@"news size = %d",[optionsList count]);
                [tvNews reloadData];
                HIDE_LOADING
            }
        }];
        [req startAsynchronous];
    }
}
-(void)insertNewsInformation:(id)obj
{
    
    NSString *catAr , *catEn;
    
    if([[obj objectForKey:@"CategoryAr"] rangeOfString:@"تنبيه"].location!= NSNotFound  || [[obj objectForKey:@"CategoryEn"] rangeOfString:@"تنبيه"].location!= NSNotFound){
        catAr = @"تنبيه";
        catEn = @"تنبيه";
    }else{
        catAr = [obj objectForKey:@"CategoryAr"];
        catEn = [obj objectForKey:@"CategoryEn"];
    }
    
    [self insertNewsData:[obj objectForKey:@"$id"] :[obj objectForKey:@"TitleAr"] :[obj objectForKey:@"TitleEn"] :[obj objectForKey:@"DescriptionAr"] :[obj objectForKey:@"DescriptionEn"] :[obj objectForKey:@"Url"] :[obj objectForKey:@"PubDate"]:[obj objectForKey:@"ImagePath"]:[obj objectForKey:@"ImageUrl"]:catAr:catEn];
}
#pragma mark -
#pragma mark Table cell image support

- (void)startIconDownload:(AppRecord *)appRecord forIndexPath:(NSIndexPath *)indexPath
{
    IconDownloader *iconDownloader = [imageDownloadsInProgress objectForKey:indexPath];
    if (iconDownloader == nil)
    {
        iconDownloader = [[IconDownloader alloc] init];
        iconDownloader.appRecord = appRecord;
        iconDownloader.indexPathInTableView = indexPath;
        iconDownloader.delegate = self;
        [imageDownloadsInProgress setObject:iconDownloader forKey:indexPath];
        [iconDownloader startDownload];
        
    }
}

// this method is used in case the user scrolled into a set of cells that don't have their app icons yet
- (void)loadImagesForOnscreenRows
{
    if ([optionsList count] > 0)
    {
        NSArray *visiblePaths = [self.tvNews indexPathsForVisibleRows];
        for (NSIndexPath *indexPath in visiblePaths)
        {
            AppRecord *appRecord = [self.entries objectAtIndex:indexPath.row] ;
            
            if (!appRecord.appIcon) // avoid the app icon download if the app already has an icon
            {
                [self startIconDownload:appRecord forIndexPath:indexPath];
            }
        }
    }
}

// called by our ImageDownloader when an icon is ready to be displayed
- (void)appImageDidLoad:(NSIndexPath *)indexPath
{
    IconDownloader *iconDownloader = [imageDownloadsInProgress objectForKey:indexPath];
    if (iconDownloader != nil)
    {
        
        //        UITableViewCell *cell = [self.mytable cellForRowAtIndexPath:iconDownloader.indexPathInTableView];
        //
        //        // Display the newly loaded image
        //        cell.imageView.image = iconDownloader.appRecord.appIcon;
        [tvNews reloadData];
    }
    
    // Remove the IconDownloader from the in progress list.
    // This will result in it being deallocated.
    [imageDownloadsInProgress removeObjectForKey:indexPath];
    
}

// Load images for all onscreen rows when scrolling is finished
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
    {
        [self loadImagesForOnscreenRows];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadImagesForOnscreenRows];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    global = [Global getInstance];
    
//    [global setCurrentSectorImage:[UIImage imageNamed:@"news tile .png" ]];
    [global setCurrentSectorMenuBtn:[UIImage imageNamed:@"home button news.png"]];
    [global setCurrentSectorColor:[global colorFromHexString:@"#5b4f6b"]];
    [global setCurrentSectorSubImage:nil];
    
    NavigationBar *navBar2 =  (NavigationBar*) [del.window viewWithTag:1000000];
    if (navBar2) {
        [del.window bringSubviewToFront:navBar2];
        // [sectorTileImgView setImage:[global currentSectorImage]];
        [navBar2.btnMenu setImage:[global currentSectorMenuBtn] forState:UIControlStateNormal];
    }
    [navBar2.btnBack setHidden:NO];

    [self updateViews];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    SHOW_LOADING
    [self performSelector:@selector(getData) withObject:nil];

}

- (void)updateViews
{
    CGRect frame = [self navigationFrame];
    
    if(!IS_DEVICE_RUNNING_IOS_7_AND_ABOVE)
        frame.origin.y = 0.0;
    

    [_headerImg setFrame:CGRectMake(_headerImg.frame.origin.x, frame.origin.y + frame.size.height, _headerImg.frame.size.width, _headerImg.frame.size.height)];
    
    CGRect tableRect = tvNews.frame;
    tableRect.origin.y = _headerImg.frame.origin.y + _headerImg.frame.size.height;
    tvNews.frame = tableRect;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    tvNews = nil;
    
    [super viewDidUnload];
}


-(void)viewWillDisappear:(BOOL)animated{
    
    if(![req isCancelled]) {
        // Cancels an asynchronous request
        [req cancel];
        // Cancels an asynchronous request, clearing all delegates and blocks first
        [req clearDelegatesAndCancel];
    }
    
    [super viewWillDisappear:animated];
}

#pragma  mark DataBase functions
- (void)insertNewsData:(NSString*)nid :(NSString*)title_ar :(NSString*)title_en :(NSString*)content_ar :(NSString*)content_en :(NSString*)link :(NSString*)date :(NSString*)image_path :(NSString*)image_url :(NSString*)cat_ar :(NSString*)cat_en {
    
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    
    NSString *insertQuery = [@"insert into news_main " stringByAppendingFormat:@" VALUES ('%@', '%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",link,date,nid,image_path,image_url,title_ar,title_en,content_ar,content_en,cat_ar,cat_en];
    BOOL insterted = [database executeUpdate:insertQuery];
    
    NSLog(insterted ? @"Yes" : @"No");
    
    [database close];
}

-(void) newsGetAllData{
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    NSString *sqlSelectQuery = @"SELECT * FROM news_main order by   CAST(id as integer)";
    
    optionsList = [[NSMutableArray alloc] init];
    
    // Query result
    FMResultSet *resultsWithNameLocation = [database executeQuery:sqlSelectQuery];
    while([resultsWithNameLocation next]) {
        NSString *news_title_ar = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"title_ar"]];
        NSString *news_title_en = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"title_en"]];
        NSString *news_content_ar = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"content_ar"]];
        NSString *news_content_en = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"content_en"]];
        NSString *news_link = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"link"]];
        NSString *news_date = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"PubDate"]];
        NSString *news_id = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"id"]];
        NSString *image_path = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"image_path"]];
        NSString *cat_ar = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"cat_ar"]];
        NSString *cat_en = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"cat_en"]];
        NSMutableDictionary *newsDict = [[NSMutableDictionary alloc]init];
        if([[Language getObject] getLanguage] == languageArabic){
            [newsDict setValue:news_title_ar forKey:@"TitleAr"];
            [newsDict setValue:news_content_ar forKey:@"DescriptionAr"];
        }else{
            [newsDict setValue:news_title_en forKey:@"TitleEn"];
            [newsDict setValue:news_content_en forKey:@"DescriptionEn"];
        }
        
        [newsDict setValue:news_link forKey:@"Url"];
        [newsDict setValue:news_date forKey:@"PubDate"];
        [newsDict setValue:news_id forKey:@"$id"];
        [newsDict setValue:cat_ar forKey:@"CategoryAr"];
        [newsDict setValue:cat_en forKey:@"CategoryEn"];
        [newsDict setValue:image_path forKey:@"imagePath"];
        
        [optionsList addObject:newsDict ];
        
        // loading your data into the array, dictionaries.
    }
    [database close];
    
    HIDE_LOADING
}

-(void) newsGetAllData:(NSString*)whereStaremtns{
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    NSString *sqlSelectQuery = [@"SELECT * FROM news_main " stringByAppendingString:whereStaremtns];
    sqlSelectQuery = [sqlSelectQuery stringByAppendingString:@" order by   CAST(id as integer) "];
    NSLog(@"QQQQ %@",[@"SELECT * FROM news_main " stringByAppendingString:whereStaremtns]);
    
    optionsList = [[NSMutableArray alloc] init];
    
    // Query result
    FMResultSet *resultsWithNameLocation = [database executeQuery:sqlSelectQuery];
    while([resultsWithNameLocation next]) {
        NSString *news_title_ar = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"title_ar"]];
        NSString *news_title_en = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"title_en"]];
        NSString *news_content_ar = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"content_ar"]];
        NSString *news_content_en = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"content_en"]];
        NSString *news_link = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"link"]];
        NSString *news_date = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"PubDate"]];
        NSString *news_id = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"id"]];
        NSString *image_path = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"image_path"]];
        NSString *cat_ar = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"cat_ar"]];
        NSString *cat_en = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"cat_en"]];
        NSMutableDictionary *newsDict = [[NSMutableDictionary alloc]init];
        if([[Language getObject] getLanguage] == languageArabic){
            [newsDict setValue:news_title_ar forKey:@"TitleAr"];
            [newsDict setValue:news_content_ar forKey:@"DescriptionAr"];
        }else{
            [newsDict setValue:news_title_en forKey:@"TitleEn"];
            [newsDict setValue:news_content_en forKey:@"DescriptionEn"];
        }
        
        [newsDict setValue:news_link forKey:@"Url"];
        [newsDict setValue:news_date forKey:@"PubDate"];
        [newsDict setValue:news_id forKey:@"$id"];
        [newsDict setValue:cat_ar forKey:@"CategoryAr"];
        [newsDict setValue:cat_en forKey:@"CategoryEn"];
        [newsDict setValue:image_path forKey:@"imagePath"];
        
        [optionsList addObject:newsDict ];
        
        // loading your data into the array, dictionaries.
    }
    [database close];
    
    HIDE_LOADING
}

- (void)deleteData {
    
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    NSString *deleteQuery = @"DELETE FROM news_main";
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    BOOL deleted = [database executeUpdate:deleteQuery];
    NSLog(deleted ? @"Yes" : @"No");
    [database close];
}

- (IBAction)newsFitering:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:@""
                                  delegate:self
                                  cancelButtonTitle:@"إلغاء"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"آخر التحذيرات", @"آخر الأخبار والتحذيرات",
                                  @"أخبار الهيئة", nil];
    
    
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet showInView:self.view];
    
}

#pragma Action sheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
//            NSString* whereS = ;
            [self newsGetAllData:@"where cat_ar='تنبيه'"];
            [tvNews reloadData];
            break;
        case 1:
            [self newsGetAllData];
            [tvNews reloadData];
            break;
        case 2:
            [self newsGetAllData:@"where cat_ar!='تنبيه'"];
            [tvNews reloadData];
            break;
            
    }
}

@end