//
//  NewsDetailsViewController.h
//  SFDA
//
//  Created by yehia on 9/14/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface NewsDetailsViewController : MasterViewController<UIScrollViewDelegate,UIScrollViewDelegate,UIActionSheetDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>

{
}

@property (weak, nonatomic) IBOutlet UIImageView *lblNewsImg;
@property (weak, nonatomic) IBOutlet UIImageView *sectorTileImgView;

@property (weak, nonatomic) IBOutlet UILabel *lblNewsDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblNewsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;

@property(weak,nonatomic)NSDictionary *newsObj;
- (IBAction)moreAction:(id)sender;
- (IBAction)newsShare:(id)sender;
@end
