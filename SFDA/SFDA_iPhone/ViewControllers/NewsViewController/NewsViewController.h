//
//  NewsViewController.h
//  SFDA
//
//  Created by yehia on 9/14/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "MasterViewController.h"
#import "IconDownloader.h"

@interface NewsViewController : MasterViewController<UITableViewDataSource,UITableViewDataSource,IconDownloaderDelegate,UIActionSheetDelegate>
{
    NSMutableDictionary *imageDownloadsInProgress;
    NSMutableArray  *entries;
}
@property (weak, nonatomic) IBOutlet UITableView *tvNews;

@property(nonatomic,retain)NSMutableArray  *entries;
@property (nonatomic,retain)NSMutableDictionary *imageDownloadsInProgress;
@property (weak, nonatomic) IBOutlet UIButton *sectorTileImgViewButton;

- (void)insertNewsData:(NSString*)nid :(NSString*)title :(NSString*)content :(NSString*)link :(NSString*)date;
-(void) newsGetAllData;
- (void)deleteData;
- (IBAction)newsFitering:(id)sender;

@end
