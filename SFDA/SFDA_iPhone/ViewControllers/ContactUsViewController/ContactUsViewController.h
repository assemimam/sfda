//
//  ContactUsViewController.h
//  SFDA
//
//  Created by Assem Imam on 9/4/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "MasterViewController.h"
#import <MessageUI/MFMailComposeViewController.h>

@interface ContactUsViewController : MasterViewController<UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate>
{
    
    __weak IBOutlet UITableView *tvContacts;
    
    NSMutableArray *sdfaOptionsList;
    NSMutableArray *foodOptionsList;
    NSMutableArray *drugOptionsList;
    NSMutableArray *medicalOptionsList;
}


@end
