//
//  ContactUsViewController.m
//  SFDA
//
//  Created by Assem Imam on 9/4/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "ContactUsViewController.h"
#import "contactUsCell.h"
#import "Language.h"
#import "JSON.h"
#import "JSONKit.h"
#import "FMDatabase.h"
#import "Global.h"
#import "WebViewController.h"


@interface ContactUsViewController ()
{
    NSMutableArray *optionsList;
    NSMutableArray *currentArray;
    NSArray *sectionList;
    ASIHTTPRequest *req;
}
@end

@implementation ContactUsViewController

#pragma  mark tableview datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    contactUsCell *cell =(contactUsCell*) [tableView dequeueReusableCellWithIdentifier:@"cleOption"];
    if (cell==nil) {
        cell =(contactUsCell*) [[[NSBundle mainBundle]loadNibNamed:[[Language getObject]getStringWithKey:@"contactUsCell"] owner:nil options:nil]objectAtIndex:0];
        
        
        
    }
    
    if(indexPath.section ==0){
        currentArray = sdfaOptionsList;
    }else if(indexPath.section ==1){
        currentArray = foodOptionsList;
    }else if(indexPath.section == 2){
        currentArray = drugOptionsList;
    }else if(indexPath.section ==3){
        currentArray = medicalOptionsList;
    }
    
    NSString *name,*value;
    
    if ([[Language getObject]getLanguage]==languageArabic) {
        name = [[currentArray objectAtIndex:indexPath.row] objectForKey:@"NameAr"];
        value = [[currentArray objectAtIndex:indexPath.row] objectForKey:@"ValueAr"];
        NSLog(@"name%@",name);
        if([name isEqualToString:@"رقم الهاتف"] || [name isEqualToString:@"رقم الهاتف 1"] || [name isEqualToString:@"البريد"]||[name isEqualToString:@"لموقع الإلكترونى"] ){
            cell.contactName.text = value;
            
            //Spicial case for Email contact
            if ([name isEqualToString:@"البريد"]) {
                cell.contactName.text=@"البريد الإلكتروني العام";
            }
            
            
        }else if([name isEqualToString:@"تحويلة 1"]){
            //phone number +" ,"+ extenstion
            NSString *firstPhoneNumber =[[sdfaOptionsList objectAtIndex:0]objectForKey:@"ValueAr" ];
            firstPhoneNumber = [firstPhoneNumber substringFromIndex:[firstPhoneNumber rangeOfString:@"+"].location +1];
            cell.contactName.text = [[firstPhoneNumber stringByAppendingString:@"+     تحويلة : "] stringByAppendingString:value];
        }
        else{
            cell.contactName.text = name;
        }
        
        //////// for Ar image handling
        if ([[Language getObject]getLanguage]==languageArabic) {
            if([name isEqualToString:@"رقم الهاتف"] || [name isEqualToString:@"رقم الهاتف 1"]||[name isEqualToString:@"تحويلة 1"]){
                cell.contactImage.image = [UIImage imageNamed:@"phoneContactIco.png"];
            }else if([name isEqualToString:@"البريد"]  || [name isEqualToString:@"مراسلة الرئيس التنفيذي"] ||[name isEqualToString: @"اسأل المتحدث الرسمي"]){
                cell.contactImage.image = [UIImage imageNamed:@"mailContactIco.png"];
            }else if([name isEqualToString:@"الموقع الإلكتروني"]){
                cell.contactImage.image = [UIImage imageNamed:@"webContactIco.png"];
            } else if([name isEqualToString:@"موقع الهيئة على الخريطة"])
                cell.contactImage.image = [UIImage imageNamed:@"location.png"];
            else if([name rangeOfString:@"تويتر"].length >0){
                cell.contactImage.image = [UIImage imageNamed:@"twitterContactIco.png"];
            }else if([name rangeOfString:@"الهيئة على الفيسبوك"].length >0){
                cell.contactImage.image = [UIImage imageNamed:@"fbContactIco.png"];
            }
        }
        
    }
    
    
    
    
    cell.backgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"silver box tile !.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    cell.selectedBackgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"silver box tile !.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    
    return cell;
}

#pragma  mark tableview delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if(indexPath.section ==0){
        currentArray = sdfaOptionsList;
    }else if(indexPath.section ==1){
        currentArray = foodOptionsList;
    }else if(indexPath.section == 2){
        currentArray = drugOptionsList;
    }else if(indexPath.section ==3){
        currentArray = medicalOptionsList;
    }
    
    
    
    if ([[Language getObject]getLanguage]==languageArabic) {
        NSString *name = [[currentArray objectAtIndex:indexPath.row] objectForKey:@"NameAr"];
        NSString *value = [[currentArray objectAtIndex:indexPath.row] objectForKey:@"ValueAr"];
        
        
        if([name isEqualToString:@"رقم الهاتف"] || [name isEqualToString:@"رقم الهاتف 1"]){
            
            
            if([[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",value]]])
            {
                
            }
            else{
            }
        }else if([name isEqualToString:@"البريد"]  || [name isEqualToString:@"مراسلة الرئيس التنفيذي"]){
            if ([MFMailComposeViewController canSendMail]) {
                MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                controller.mailComposeDelegate = self;
                [controller setSubject:@""];
                [controller setToRecipients:[NSArray arrayWithObjects:value,nil]];
                [controller setMessageBody:@"" isHTML:NO];
                [[del.window viewWithTag:1000000] setHidden:TRUE];
                if (controller) [self presentModalViewController:controller animated:YES];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"ادخل بريدك الالكترونى"
                                                               delegate:nil
                                                      cancelButtonTitle:@"موافق"
                                                      otherButtonTitles: nil];
                [alert show];

            }
            
        }else if([name isEqualToString:@"لموقع الإلكترونى"]){
            if(![name hasPrefix:@"http://"]){
                value = [@"http://" stringByAppendingString:value];
            }
            NSURL *url = [NSURL URLWithString:value];
            [[UIApplication sharedApplication] openURL:url];
            //            WebViewController *vc_web = [[WebViewController alloc]init];
            //            vc_web.url = value;
            //            [self.navigationController pushViewController:vc_web animated:YES];
            
        }else if([name isEqualToString:@"تحويلة 1"]){
            NSString *firstPhoneNumber =[[sdfaOptionsList objectAtIndex:0]objectForKey:@"ValueAr" ];
            if([[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",firstPhoneNumber]]])
            {
                
            }
            else{
            }
        }
        else{
            if(![value hasPrefix:@"http://"] && ![value hasPrefix:@"https://"] ){
                value = [@"http://" stringByAppendingString:value];
            }
            
            NSLog(@"%@",value);
            NSURL *url = [NSURL URLWithString:value];
            [[UIApplication sharedApplication] openURL:url];
            //            WebViewController *vc_web = [[WebViewController alloc]init];
            //            vc_web.url = value;
            //            [self.navigationController pushViewController:vc_web animated:YES];
        }
    }
    else{
        NSString *name = [[currentArray objectAtIndex:indexPath.row] objectForKey:@"NameEn"];
        NSString *value = [[currentArray objectAtIndex:indexPath.row] objectForKey:@"ValueEn"];
        
        
        if([name isEqualToString:@"Phone Number"]|| [name isEqualToString:@"Phone Number 1"]){
            if([[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",value]]])
            {
                
            }
            else{
            }
        }else if([name isEqualToString:@"Email"] || [name isEqualToString:@"CEO Contacted"]){
            if ([MFMailComposeViewController canSendMail]) {
                MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                controller.mailComposeDelegate = self;
                [controller setSubject:@""];
                [controller setToRecipients:[NSArray arrayWithObjects:value,nil]];
                [controller setMessageBody:@"" isHTML:NO];
                [[del.window viewWithTag:1000000] setHidden:TRUE];
                if (controller) [self presentModalViewController:controller animated:YES];
            }
            else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"ادخل بريدك الالكترونى"
                                                               delegate:nil
                                                      cancelButtonTitle:@"موافق"
                                                      otherButtonTitles: nil];
                [alert show];

              
            }
            
        }else if([name isEqualToString:@"Website"]){
            if(![name hasPrefix:@"http://"]){
                value = [@"http://" stringByAppendingString:value];
            }
            NSURL *url = [NSURL URLWithString:value];
            [[UIApplication sharedApplication] openURL:url];
            //            WebViewController *vc_web = [[WebViewController alloc]init];
            //            vc_web.url = value;
            //            [self.navigationController pushViewController:vc_web animated:YES];
        }
        else if([name isEqualToString:@"Converter 1"]){
            NSString *firstPhoneNumber =[[sdfaOptionsList objectAtIndex:0]objectForKey:@"ValueAr" ];
            if([[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",firstPhoneNumber]]])
            {
                
            }
            else{
            }
        }
        else{
            if(![value hasPrefix:@"http://"] && ![value hasPrefix:@"https://"] ){
                value = [@"http://" stringByAppendingString:value];
            }
            
            NSURL *url = [NSURL URLWithString:value];
            [[UIApplication sharedApplication] openURL:url];
            //            WebViewController *vc_web = [[WebViewController alloc]init];
            //            vc_web.url = value;
            //            [self.navigationController pushViewController:vc_web animated:YES];
        }
    }
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(optionsList!=nil && [optionsList count] > 0){
        if(section ==0){
            return  [sdfaOptionsList count];
        }else if(section ==1){
            return  [foodOptionsList count];
        }else if(section == 2){
            return  [drugOptionsList count];
        }else if(section ==3){
            return  [medicalOptionsList count];
        }
    }
    
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 60;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if(optionsList!=nil && [optionsList count] > 0){
        
        UIView *headerView;
        
        
        NSString *headerText = [sectionList objectAtIndex:section];
        
        float headerWidth = 320.0f;
        float padding = 10.0f;
        
        headerView = [[UIView alloc] initWithFrame:CGRectMake(300, 0.0f, headerWidth, 60)];
        headerView.autoresizesSubviews = YES;
        headerView.backgroundColor = [[Global getInstance] colorFromHexString:@"374a51"];
        
        // create the label centered in the container, then set the appropriate autoresize mask
        UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, headerWidth, 30)];
        headerLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;
        
        headerLabel.text = headerText;
        headerLabel.textColor = [UIColor whiteColor];
        headerLabel.backgroundColor = [UIColor clearColor];
        
        CGSize textSize = [headerText sizeWithFont:headerLabel.font constrainedToSize:headerLabel.frame.size lineBreakMode:UILineBreakModeWordWrap];
        headerLabel.frame = CGRectMake(headerWidth - (textSize.width + padding), headerLabel.frame.origin.y, textSize.width, headerLabel.frame.size.height);
        
        [headerView addSubview:headerLabel];
        
        return headerView;
    }
    
    return nil;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(optionsList != nil  &&[optionsList count] > 0){
        return 4;
    }else{
        return 0;
    }
    
}


#pragma  mark MFMailCompose delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissModalViewControllerAnimated:YES];
    
    [[del.window viewWithTag:1000000] setHidden:FALSE];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)getData
{
    
    sdfaOptionsList = [[NSMutableArray alloc]init];
    foodOptionsList = [[NSMutableArray alloc]init];
    drugOptionsList = [[NSMutableArray alloc]init];
    medicalOptionsList = [[NSMutableArray alloc]init];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:CONTACT_US_SECTION_TAG]){
        
        [self contactGetAllData];
        
        for (int i=0; i<[optionsList count]; i++) {
            
            NSInteger sectionId = [[[optionsList objectAtIndex:i] objectForKey:@"SectionId"] intValue];
            
            if(sectionId == 1){
                [sdfaOptionsList addObject:[optionsList objectAtIndex:i]];
            }else if(sectionId == 2){
                [foodOptionsList addObject:[optionsList objectAtIndex:i]];
            }else if(sectionId == 3){
                [drugOptionsList addObject:[optionsList objectAtIndex:i]];
            }else if(sectionId == 4){
                [medicalOptionsList addObject:[optionsList objectAtIndex:i]];
            }
        }
        
        NSLog(@"news size = %d",[optionsList count]);
        [tvContacts reloadData];
        
        
    }else{
        if (!internetConnection) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            HIDE_LOADING
            return;
        }
        
        SHOW_LOADING
        req =[[ASIHTTPRequest alloc]initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@ContactUs", SERVER_IP]]];
        
        
        
        [req setCompletionBlock:^{
            NSString *response=  [req responseString];
            
            id result = [response JSONValue];
            optionsList=result;
            
            if(optionsList != nil && [optionsList count]>0){
                [self deleteData];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:CONTACT_US_SECTION_TAG];
            }
            
            for (int i=0; i<[optionsList count]; i++) {
                
                NSInteger sectionId = [[[optionsList objectAtIndex:i] objectForKey:@"SectionId"] intValue];
                NSString *nameAr = [[optionsList objectAtIndex:i] objectForKey:@"NameAr"];
                NSString *nameEn = [[optionsList objectAtIndex:i] objectForKey:@"NameEn"];
                
                NSString *valAr = [[optionsList objectAtIndex:i] objectForKey:@"ValueAr"];
                NSString *valEn = [[optionsList objectAtIndex:i] objectForKey:@"ValueEn"];
                
                if(sectionId == 1){
                    [sdfaOptionsList addObject:[optionsList objectAtIndex:i]];
                }else if(sectionId == 2){
                    [foodOptionsList addObject:[optionsList objectAtIndex:i]];
                }else if(sectionId == 3){
                    [drugOptionsList addObject:[optionsList objectAtIndex:i]];
                }else if(sectionId == 4){
                    [medicalOptionsList addObject:[optionsList objectAtIndex:i]];
                }
                
                if([[Language getObject] getLanguage] == languageArabic){
                    [self insertContactData:nameAr :valAr :[NSString stringWithFormat:@"%d",sectionId]];
                }else{
                    [self insertContactData:nameEn :valEn :[NSString stringWithFormat:@"%d",sectionId]];
                }
                
            }
            HIDE_LOADING
            [tvContacts reloadData];
            
        }];
        [req setFailedBlock:^{
            
            [self contactGetAllData];
            HIDE_LOADING
            if(optionsList == nil || [optionsList count] ==0){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"يرجى التحقق من اتصالك مع الإنترنت" delegate:self cancelButtonTitle:@"موافق" otherButtonTitles:nil];
                [alert show];
                //            NSLog(@"%@",[req error]);
            }else{
                
                for (int i=0; i<[optionsList count]; i++) {
                    
                    NSInteger sectionId = [[[optionsList objectAtIndex:i] objectForKey:@"SectionId"] intValue];
                    
                    if(sectionId == 1){
                        [sdfaOptionsList addObject:[optionsList objectAtIndex:i]];
                    }else if(sectionId == 2){
                        [foodOptionsList addObject:[optionsList objectAtIndex:i]];
                    }else if(sectionId == 3){
                        [drugOptionsList addObject:[optionsList objectAtIndex:i]];
                    }else if(sectionId == 4){
                        [medicalOptionsList addObject:[optionsList objectAtIndex:i]];
                    }
                }
                
                NSLog(@"news size = %d",[optionsList count]);
                [tvContacts reloadData];
            }
        }];
        [req startAsynchronous];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NavigationBar*navBar2 =  (NavigationBar*) [del.window viewWithTag:1000000];
    if (navBar2) {
        [navBar2.btnBack setHidden:NO];
        [del.window bringSubviewToFront:navBar2];
    }
    
    
    [self updateViews];
}

- (void)updateViews
{
    CGRect frame = [self navigationFrame];
    
    if(!IS_DEVICE_RUNNING_IOS_7_AND_ABOVE)
        frame.origin.y = 0.0;
    
    [tvContacts setFrame:CGRectMake(tvContacts.frame.origin.x, frame.origin.y + frame.size.height, tvContacts.frame.size.width, tvContacts.frame.size.height)];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    sectionList = [[Language getObject] getArrayWithKey:@"ContactSectionList"];
    
    
    [self performSelector:@selector(getData) withObject:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    tvContacts = nil;
    
    
    [super viewDidUnload];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    if(![req isCancelled]) {
        // Cancels an asynchronous request
        [req cancel];
        // Cancels an asynchronous request, clearing all delegates and blocks first
        [req clearDelegatesAndCancel];
    }
    
    [super viewWillDisappear:animated];
}

#pragma  mark DataBase functions
- (void)insertContactData:(NSString*)name :(NSString*)value :(NSString*)sectionId{
    
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    NSString* lang = @"en";
    if([[Language getObject] getLanguage] == languageArabic)
        lang = @"ar";
    
    
    NSString *insertQuery = [@"insert into ContactUs " stringByAppendingFormat:@" VALUES ('%@','%@', '%@','%@')", name,value,sectionId,lang];
    BOOL insterted = [database executeUpdate:insertQuery];
    
    NSLog(insterted ? @"Yes" : @"No");
    
    [database close];
}

-(void) contactGetAllData{
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    NSString *sqlSelectQuery = @"SELECT * FROM ContactUs where lang='en'";
    if([[Language getObject] getLanguage] == languageArabic)
        sqlSelectQuery = @"SELECT * FROM ContactUs where lang='ar'";
    
    NSLog(@" query ==> %@",sqlSelectQuery);
    
    optionsList = [[NSMutableArray alloc] init];
    
    // Query result
    FMResultSet *resultsWithNameLocation = [database executeQuery:sqlSelectQuery];
    while([resultsWithNameLocation next]) {
        NSString *contactUs_name = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"name"]];
        NSString *contactUs_value = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"value"]];
        NSString *SectionId = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"SectionId"]];
        
        NSMutableDictionary *contactUsDict = [[NSMutableDictionary alloc]init];
        if([[Language getObject] getLanguage] == languageArabic){
            
            [contactUsDict setObject:contactUs_name forKey:@"NameAr"];
            [contactUsDict setObject:contactUs_value forKey:@"ValueAr"];
            [contactUsDict setObject:SectionId forKey:@"SectionId"];
        }else{
            [contactUsDict setObject:contactUs_name forKey:@"NameEn"];
            [contactUsDict setObject:contactUs_value forKey:@"ValueEn"];
            [contactUsDict setObject:SectionId forKey:@"SectionId"];
        }
        
        [optionsList addObject:contactUsDict ];
        
        // loading your data into the array, dictionaries.
    }
    [database close];
}

- (void)deleteData {
    
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    NSString *deleteQuery = @"DELETE FROM ContactUs where lang='en'";
    if([[Language getObject] getLanguage] == languageArabic)
        deleteQuery = @"DELETE FROM ContactUs where lang='ar'";
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    [database executeUpdate:deleteQuery];
    [database close];
}

@end
