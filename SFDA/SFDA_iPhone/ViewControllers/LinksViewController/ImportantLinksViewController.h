//
//  ImportantLinksViewController.h
//  SFDA
//
//  Created by yehia on 9/8/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "MasterViewController.h"

@interface ImportantLinksViewController :  MasterViewController<UITableViewDataSource,UITableViewDelegate>
{
    
    __weak IBOutlet UITableView *tvContacts;
    
    NSMutableArray *sdfaLinksList;
    NSMutableArray *generalLinksList;
    
}

@end
