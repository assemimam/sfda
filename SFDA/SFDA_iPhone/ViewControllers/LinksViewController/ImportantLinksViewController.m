//
//  ImportantLinksViewController.m
//  SFDA
//
//  Created by yehia on 9/8/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "ImportantLinksViewController.h"
#import "ImportanLinksCustomCell.h"
#import "Language.h"
#import "JSON.h"
#import "JSONKit.h"
#import "FMDatabase.h"
#import "Global.h"
#import "WebViewController.h"

@interface ImportantLinksViewController ()
{
    NSMutableArray *optionsList;
    ASIHTTPRequest *req;
    NSArray *sectionList;
}
@end

@implementation ImportantLinksViewController

#pragma  mark tableview datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ImportanLinksCustomCell *cell =(ImportanLinksCustomCell*) [tableView dequeueReusableCellWithIdentifier:@"cleOption"];
    if (cell==nil) {
        cell =(ImportanLinksCustomCell*) [[[NSBundle mainBundle]loadNibNamed:@"ImportanLinksCustomCell" owner:nil options:nil]objectAtIndex:0];
        
    }
    
    NSString *title;
    
    if(indexPath.section == 0){
        if ([[Language getObject]getLanguage]==languageArabic) {
            title = [[sdfaLinksList objectAtIndex:indexPath.row] objectForKey:@"TitleAr"];
        }
        else{
            title = [[sdfaLinksList objectAtIndex:indexPath.row] objectForKey:@"TitleEn"];
        }
    }else{
        if ([[Language getObject]getLanguage]==languageArabic) {
            title = [[generalLinksList objectAtIndex:indexPath.row] objectForKey:@"TitleAr"];
        }
        else{
            title = [[generalLinksList objectAtIndex:indexPath.row] objectForKey:@"TitleEn"];
        }
    }
    
    if([[Language getObject] getLanguage] == languageArabic){
        cell.lblTitle.textAlignment = UITextAlignmentRight;
    }else{
        cell.lblTitle.textAlignment = UITextAlignmentLeft;
    }
     cell.lblTitle.font=[UIFont fontWithName:@"GESSTwoLight-Light" size:15];
    cell.backgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"link tile white.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    cell.selectedBackgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"link tile white.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    
    cell.lblTitle.text = title;
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(optionsList!=nil && [optionsList count] > 0){
    if(section ==0){
        return  [sdfaLinksList count];
    }else if(section ==1){
        return  [generalLinksList count];
    }
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 60;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 76;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if(optionsList!=nil && [optionsList count] > 0){
        
        UIView *headerView;
        
        
        NSString *headerText = [sectionList objectAtIndex:section];
        
        float headerWidth = 320.0f;
        float padding = 10.0f;
        
        headerView = [[UIView alloc] initWithFrame:CGRectMake(300, 0.0f, headerWidth, 60)];
        headerView.autoresizesSubviews = YES;
        headerView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"link tile green.png"]];
        
        // create the label centered in the container, then set the appropriate autoresize mask
        UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, headerWidth, 30)];
        headerLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;
        if([[Language getObject] getLanguage] == languageArabic){
            headerLabel.textAlignment = UITextAlignmentRight;
        }else{
            headerLabel.textAlignment = UITextAlignmentLeft;
        }
        
        headerLabel.text = headerText;
        headerLabel.textColor = [UIColor whiteColor];
        headerLabel.backgroundColor = [UIColor clearColor];
        headerLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
        CGSize textSize = [headerText sizeWithFont:headerLabel.font constrainedToSize:headerLabel.frame.size lineBreakMode:UILineBreakModeWordWrap];
        headerLabel.frame = CGRectMake(headerWidth - (textSize.width + padding), headerLabel.frame.origin.y, textSize.width, headerLabel.frame.size.height);
        
        [headerView addSubview:headerLabel];
        
        return headerView;
    }
    
    return nil;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(optionsList != nil && [optionsList count] >0){
    return 2;
    }else{
        return 0;
    }
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma  mark tableview delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSString *link;
    
    if(indexPath.section == 0){
        link = [[sdfaLinksList objectAtIndex:indexPath.row] objectForKey:@"Link"];
    }else{
        link = [[generalLinksList objectAtIndex:indexPath.row] objectForKey:@"Link"];
    }
    
    
    
       NSURL *url = [NSURL URLWithString:link];
      [[UIApplication sharedApplication] openURL:url];
    
//    WebViewController *vc_web = [[WebViewController alloc]init];
//    vc_web.url = link;
//    [self.navigationController pushViewController:vc_web animated:YES];
    
}


-(void)getData
{
    
    sdfaLinksList = [[NSMutableArray alloc]init];
    generalLinksList = [[NSMutableArray alloc]init];
    
    
    
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:IMPORTANT_LINKS_SECTION_TAG]){
        
        [self getImportantLinksAllData];
        
        for (int i=0; i<[optionsList count]; i++) {
            
            NSInteger sectionId = [[[optionsList objectAtIndex:i] objectForKey:@"Category"] intValue];
            
            if(sectionId == 1){
                [sdfaLinksList addObject:[optionsList objectAtIndex:i]];
            }else if(sectionId == 0){
                [generalLinksList addObject:[optionsList objectAtIndex:i]];
            }
        }
        
        [tvContacts reloadData];
        
        
    }else{
        if (!internetConnection) {
            [[[UIAlertView alloc]initWithTitle:nil message:[[Language getObject]getStringWithKey:@"interNetMessage"] delegate:nil cancelButtonTitle:[[Language getObject]getStringWithKey:@"OkTitle"] otherButtonTitles:nil, nil]show];
            HIDE_LOADING
            //return;
        }

        SHOW_LOADING
        req =[[ASIHTTPRequest alloc]initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@ImportantLink", SERVER_IP]]];
        
    [req setCompletionBlock:^{
        NSString *response=  [req responseString];
        
        id result = [response JSONValue];
        optionsList=result;
        
        if(optionsList != nil && [optionsList count]>0){
            [self deleteData];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IMPORTANT_LINKS_SECTION_TAG];
        }
        
        for (int i=0; i<[optionsList count]; i++) {
            
            NSInteger sectionId = [[[optionsList objectAtIndex:i] objectForKey:@"Category"] intValue];
            
            if(sectionId == 1){
                [sdfaLinksList addObject:[optionsList objectAtIndex:i]];
            }else if(sectionId == 0){
                [generalLinksList addObject:[optionsList objectAtIndex:i]];
            }
            
            NSString *title_ar,*title_en;

            title_ar = [[optionsList objectAtIndex:i] objectForKey:@"TitleAr"];
            title_en = [[optionsList objectAtIndex:i] objectForKey:@"TitleEn"];
            
            [self insertImportantLinksData:title_ar :title_en :[[optionsList objectAtIndex:i] objectForKey:@"Link"] :[[optionsList objectAtIndex:i] objectForKey:@"Category"]];
            
        }
        HIDE_LOADING
        [tvContacts reloadData];
        
    }];
    [req setFailedBlock:^{
        
        [self getImportantLinksAllData];
        HIDE_LOADING
        if(optionsList == nil || [optionsList count] ==0){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"يرجى التحقق من اتصالك مع الإنترنت" delegate:self cancelButtonTitle:@"موافق" otherButtonTitles:nil];
            [alert show];
            //            NSLog(@"%@",[req error]);
        }else{
            
            for (int i=0; i<[optionsList count]; i++) {
                
                NSInteger sectionId = [[[optionsList objectAtIndex:i] objectForKey:@"Category"] intValue];
                
                if(sectionId == 1){
                    [sdfaLinksList addObject:[optionsList objectAtIndex:i]];
                }else if(sectionId == 0){
                    [generalLinksList addObject:[optionsList objectAtIndex:i]];
                }
            }
            
          [tvContacts reloadData];
        }
    }];
    [req startAsynchronous];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NavigationBar*navBar2 =  (NavigationBar*) [del.window viewWithTag:1000000];
    if (navBar2) {
        [navBar2.btnBack setHidden:NO];
        [del.window bringSubviewToFront:navBar2];
    }

    
    [self updateViews];
}

- (void)updateViews
{
    CGRect frame = [self navigationFrame];
    
    if(!IS_DEVICE_RUNNING_IOS_7_AND_ABOVE)
        frame.origin.y = 0.0;
    
    [tvContacts setFrame:CGRectMake(tvContacts.frame.origin.x, frame.origin.y + frame.size.height, tvContacts.frame.size.width, tvContacts.frame.size.height)];

}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    sectionList = [[Language getObject]getArrayWithKey:@"ImportanLinksList"];
    [self performSelector:@selector(getData) withObject:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    tvContacts = nil;
    
    [super viewDidUnload];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    if(![req isCancelled]) {
        // Cancels an asynchronous request
        [req cancel];
        // Cancels an asynchronous request, clearing all delegates and blocks first
        [req clearDelegatesAndCancel];
    }
    
    [super viewWillDisappear:animated];
}

#pragma  mark DataBase functions
- (void)insertImportantLinksData :(NSString*)title_ar  :(NSString*)title_en :(NSString*)link :(NSString*)sectionId{
    
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    NSString* lang = @"en";
    if([[Language getObject] getLanguage] == languageArabic)
        lang = @"ar";
    
    
    NSString *insertQuery = [@"insert into ImportantLinks " stringByAppendingFormat:@" VALUES ('%@','%@', '%@','%@')",link,sectionId,title_ar,title_en];
    BOOL insterted = [database executeUpdate:insertQuery];
    
    NSLog(insterted ? @"Yes" : @"No");
    
    [database close];
}

-(void) getImportantLinksAllData{
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    NSString *sqlSelectQuery = @"SELECT * FROM ImportantLinks";
    
    optionsList = [[NSMutableArray alloc] init];
    
    // Query result
    FMResultSet *resultsWithNameLocation = [database executeQuery:sqlSelectQuery];
    while([resultsWithNameLocation next]) {
        NSString *importantLinks_title_ar = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"title_ar"]];
        NSString *importantLinks_title_en = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"title_en"]];
        
        NSString *importantLinks_link = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"link"]];
        NSString *category = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"SectionId"]];
        
        NSMutableDictionary *importantLinkssDict = [[NSMutableDictionary alloc]init];

        [importantLinkssDict setValue:importantLinks_title_ar forKey:@"TitleAr"];
        [importantLinkssDict setValue:importantLinks_title_en forKey:@"TitleEn"];
        [importantLinkssDict setValue:importantLinks_link forKey:@"Link"];
        [importantLinkssDict setValue:category forKey:@"Category"];
        
        
        [optionsList addObject:importantLinkssDict ];
        
        // loading your data into the array, dictionaries.
    }
    [database close];
}

- (void)deleteData {
    
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"SDFA.db"];
    
    NSString *deleteQuery = @"DELETE FROM ImportantLinks";
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    BOOL deleted = [database executeUpdate:deleteQuery];
    [database close];
}

@end
