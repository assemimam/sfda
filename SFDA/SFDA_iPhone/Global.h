//
//  Global.h
//  SFDA
//
//  Created by yehia on 9/27/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "CommonMethods.h"

//#define PRODUCTION_VERSION

#ifdef PRODUCTION_VERSION
   #define FOOD_SEARCH_URL @"http://sfdaportal.azurewebsites.net/api/Products"
    #define SERVER_IP @"http://sfdaportal.azurewebsites.net/api/"
#else
    #define FOOD_SEARCH_URL @"http://sfdaportalbeta.azurewebsites.net/api/Products"
    #define SERVER_IP @"http://sfdaportalbeta.azurewebsites.net/api/"
#endif

// iOS 7 Support
#define IS_DEVICE_RUNNING_IOS_7_AND_ABOVE ([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] != NSOrderedAscending)

#define SFDA_TAG  0
#define NEWS_TAG 1
#define FOODS_SECTOR_TAG 2
#define DRUGS_SECTOR_TAG 3
#define MEDICAL_SECTOR_TAG 4
#define E_SERVICES_TAG 5
#define SETTINGS_TAG 6
#define ABOUT_APP_TAG 7
#define MENU_BUTTON_TAG 100
#define BACK_BUTTON_TAG 200
#define REFRESH_BUTTON_TAG 300

#define  NEWS_ALL_MAIN_TAG  @"NEWS_ALL_MAIN_SFDA_TAG"
#define  NEWS_SCROLL_MAIN_TAG   @"NEWS_SCROLL_MAIN_SFDA_TAG"
#define  NEWS_SCROLL_SFDA_TAG   @"NEWS_SCROLL_SFDA_SFDA_TAG"
#define  NEWS_SCROLL_DRUG_TAG   @"NEWS_SCROLL_DRUG_SFDA_TAG"
#define  NEWS_SCROLL_FOOD_TAG   @"NEWS_SCROLL_FOOD_SFDA_TAG"
#define  NEWS_SCROLL_MEDI_TAG   @"NEWS_SCROLL_MEDI_SFDA_TAG"

#define  ABOUT_SECTION_SFDA_TAG   @"ABOUT_SECTION_SFDA_TAG"
#define  ABOUT_SECTION_FOOD_TAG   @"ABOUT_SECTION_FOOD_TAG"
#define  ABOUT_SECTION_DRUG_TAG   @"ABOUT_SECTION_DRUG_TAG"
#define  ABOUT_SECTION_MEDI_TAG   @"ABOUT_SECTION_MEDI_TAG"

#define  QUESTION_SECTION_FOOD_TAG   @"QUESTION_SECTION_FOOD_TAG"
#define  QUESTION_SECTION_DRUG_TAG   @"QUESTION_SECTION_DRUG_TAG"
#define  QUESTION_SECTION_MEDI_TAG   @"QUESTION_SECTION_MEDI_TAG"

#define  AWARENESS_SECTION_SFDA_TAG   @"AWARENESS_SECTION_SFDA_TAG"
#define  AWARENESS_SECTION_FOOD_TAG   @"AWARENESS_SECTION_FOOD_TAG"
#define  AWARENESS_SECTION_DRUG_TAG   @"AWARENESS_SECTION_DRUG_TAG"
#define  AWARENESS_SECTION_MEDI_TAG   @"AWARENESS_SECTION_MEDI_TAG"

#define  CONTACT_US_SECTION_TAG  @"CONTACT_US_SECTION_TAG"

#define  IMPORTANT_LINKS_SECTION_TAG   @"IMPORTANT_LINKS_SECTION_TAG"

#define  E_SERVICE_SECTION_TAG   @"E_SERVICE_SECTION_TAG"

#define SHOW_LOADING  [((AppDelegate*)[[UIApplication sharedApplication]delegate]) ShowLoading:YES];
#define HIDE_LOADING  [((AppDelegate*)[[UIApplication sharedApplication]delegate]) ShowLoading:NO];
#define SET_BACGROUND_COLOR      [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"silver BG.png"]]];  \

@interface Global : NSObject
{
    
    UIImage *currentSectorImage;
    UIImage *currentSectorMenuBtn;
    UIColor *currentSectorColor;
    UIImage *currentSectorSubImage;
    
  

    
}

@property(nonatomic,retain) UIImage *currentSectorImage;
@property(nonatomic,retain) UIImage *currentSectorSubImage;
@property(nonatomic,retain) UIImage *currentSectorMenuBtn;
@property(nonatomic,retain) UIColor *currentSectorColor;


+(Global*)getInstance;
-(UIColor *)colorFromHexString:(NSString *)hexString;
@end
