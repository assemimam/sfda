//
//  AppDelegate.h
//  SFDA
//
//  Created by Assem Imam on 9/3/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WindowsAzureMessaging/WindowsAzureMessaging.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    __weak IBOutlet UIView *vLoading;
     UIAlertView *alert;
    __weak IBOutlet UIView *vModal;
    __weak IBOutlet UILabel *lblLoading;
    __weak IBOutlet UIActivityIndicatorView *aiLoading;
}

@property (strong, nonatomic) IBOutlet UIWindow *window;
@property (strong, nonatomic) SBNotificationHub *hub;
-(void)ShowLoading:(BOOL)show;
@end
