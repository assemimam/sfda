//
//  AppDelegate.m
//  SFDA
//
//  Created by Assem Imam on 9/3/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "AppDelegate.h"
#import "Language.h"
#import "Global.h"
#import <QuartzCore/QuartzCore.h>
#import "Reachability.h"
#import "SFDA_MainViewController.h"
#import <AVFoundation/AVFoundation.h>

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@interface AppDelegate ()
{
    UIAlertView *reachiblityAlertView;
    NSString *reachabilityObserver;
    
}

@end

@implementation AppDelegate
@synthesize window;
@synthesize hub;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
   
    
#pragma -mark authorize for notification
    if(IS_OS_8_OR_LATER)
    {
        UIUserNotificationSettings* notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge |
         UIUserNotificationTypeSound | UIUserNotificationTypeAlert categories:nil];
        
        [application registerUserNotificationSettings:notificationSettings];
        [application registerForRemoteNotifications];
        
    } else {
        //register to receive notifications
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:myTypes];
    }
    
#pragma -mark authorize for camera access
    
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized) {
        
    } else if(authStatus == AVAuthorizationStatusDenied){
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(!granted){
                
            }
        }];
        
    } else if(authStatus == AVAuthorizationStatusRestricted){
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(!granted){
                
            }
        }];
    } else if(authStatus == AVAuthorizationStatusNotDetermined){
        // not determined?!
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(!granted){
                
            }
        }];
    } else {
        // impossible, unknown authorization status
    }
    
    
    [[Language getObject]setLanguage:languageArabic];
    
#pragma -marknetwork change notifications
    ////////////////////////////////////////////////
    
    reachabilityObserver = @"reachabilityObserver";
    
    Reachability *internetReach = [Reachability reachabilityForInternetConnection];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(internetReachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    [internetReach startNotifier];

    
    if (internetReach.currentReachabilityStatus == NotReachable)
    {
        internetConnection = NO;
        reachiblityAlertView =[[UIAlertView alloc] initWithTitle:@""
                                                         message:[[Language getObject]getStringWithKey:@"NoInternetReachable"]
                                                        delegate:nil
                                               cancelButtonTitle:[[Language getObject]getStringWithKey:@"CancelTitle"]
                                               otherButtonTitles: nil];
        [reachiblityAlertView show];
        
        
    }
    else
    {
        internetConnection = YES;
        
    }
#pragma -mark ios window design compatipility
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        // copy database
        // Function called to create a copy of the database if needed.
        [self createCopyOfDatabaseIfNeeded];
        [self.window makeKeyAndVisible];
    }
    else{
        UINavigationController *nvConroller = [[UINavigationController alloc]initWithRootViewController:[[SFDA_MainViewController alloc]init]];
        if ([[[UIDevice currentDevice] systemVersion]intValue]>7) {
            self.window = [UIWindow new];
        }
        
        self.window.rootViewController= nvConroller;
        [self.window makeKeyAndVisible];
        self.window.frame = [[UIScreen mainScreen] bounds];
    }

    return YES;
}
-(void)AddDeviceWithToken:(NSData*)deviceToken
{
    @try {
        [hub registerNativeWithDeviceToken:deviceToken tags:nil completion:^(NSError* error) {
            if (error != nil) {
                NSLog(@"Error registering for notifications: %@", error);
            }
        }];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)RegisterDevice:(NSData*)deviceToken
{
    @try {
        id result = [Webservice GetNotificationURL];
        if ( [[NSUserDefaults standardUserDefaults]objectForKey:@"URL"]==nil) {
            NSString *connectionString =[result objectForKey:@"URL"];
            NSString *hubPath =[result objectForKey:@"HubName"];
            self.hub = [[SBNotificationHub alloc]initWithConnectionString:connectionString notificationHubPath:hubPath];
            [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"URL"] forKey:@"URL"];
            [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"HubName"] forKey:@"HubName"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        else{
            if (![[[NSUserDefaults standardUserDefaults]objectForKey:@"URL"] isEqualToString:[result objectForKey:@"URL"]]) {
                NSString *connectionString =[result objectForKey:@"URL"];
                NSString *hubPath =[result objectForKey:@"HubName"];
                self.hub = [[SBNotificationHub alloc]initWithConnectionString:connectionString notificationHubPath:hubPath];
                [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"URL"] forKey:@"URL"];
                [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"HubName"] forKey:@"HubName"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            else{
                NSString *connectionString =[[NSUserDefaults standardUserDefaults]objectForKey:@"URL"];
                NSString *hubPath =[[NSUserDefaults standardUserDefaults]objectForKey:@"HubName"];
                self.hub = [[SBNotificationHub alloc]initWithConnectionString:connectionString notificationHubPath:hubPath];
            }
            
        }
        [self performSelectorOnMainThread:@selector(AddDeviceWithToken:) withObject:deviceToken waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
    
}
#pragma -mark Ios Remote Push Notification
#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:   (UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString   *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}
#endif
-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    
    //    UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@"التنبيهات" message:
    //                           [deviceToken description] delegate:nil cancelButtonTitle:@"اخفاء" otherButtonTitles:nil, nil];
    //    [alert2 show];
    //[NSThread detachNewThreadSelector:@selector(RegisterDevice:) toTarget:self withObject:deviceToken];
    

}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@"التنبيهات" message:
                           [[userInfo objectForKey:@"aps"] valueForKey:@"alert"] delegate:nil cancelButtonTitle:@"اخفاء" otherButtonTitles:nil, nil];
    [alert2 show];
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"%@", [error localizedDescription]);
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}
- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark - Defined Functions

// Function to Create a writable copy of the bundled default database in the application Documents directory.
- (void)createCopyOfDatabaseIfNeeded {
    // First, test for existence.
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // Database filename can have extension db/sqlite.
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appDBPath = [documentsDirectory stringByAppendingPathComponent:@"SDFA.db"];
    
    success = [fileManager fileExistsAtPath:appDBPath];
    if (success){
        return;
        //[fileManager removeItemAtPath:appDBPath error:&error];
    }
    // The writable database does not exist, so copy the default to the appropriate location.
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"SDFA.db"];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:appDBPath error:&error];
    if (!success) {
        NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
}
-(void)ShowLoading:(BOOL)show
{
    lblLoading.text = [[Language getObject]getStringWithKey:@"LoadingTitle"];
    [self FormatView:vLoading];
    
    if (show) {
        [vLoading setHidden:NO];
        [aiLoading startAnimating ];
        [self.window bringSubviewToFront:vModal];
    }
    else
    {
        [aiLoading stopAnimating ];
        [vLoading setHidden:YES];
        
        [self.window sendSubviewToBack:vModal];
    }
}
-(void)FormatView:(UIView*)view
{
    view.layer.cornerRadius = 5;
    view.layer.borderColor = [UIColor blackColor].CGColor;
    view.layer.borderWidth = 0;
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOpacity = 0.3;
    view.layer.shadowRadius = 1.0;
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shouldRasterize = NO;
}

#pragma mark - Reachability

- (void)internetReachabilityChanged:(NSNotification *)note
{
    
    NetworkStatus ns = [(Reachability *)[note object] currentReachabilityStatus];
    
    if (ns == NotReachable)
    {
        
        if (internetConnection)
        {
            internetConnection = NO;
            [[NSNotificationCenter defaultCenter] postNotificationName:reachabilityObserver object:nil];
            
            if (!reachiblityAlertView)
            {
                reachiblityAlertView =[[UIAlertView alloc] initWithTitle:@""
                                                                 message:[[Language getObject]getStringWithKey:@"NoInternetReachable"]
                                                                delegate:nil
                                                       cancelButtonTitle:[[Language getObject]getStringWithKey:@"CancelTitle"]
                                                       otherButtonTitles: nil];
            }
            [reachiblityAlertView show];
        }
    }
    else
    {
        //check for both with/out internet connection
        [reachiblityAlertView dismissWithClickedButtonIndex:0 animated:YES];
        
        if (!internetConnection)
        {
            internetConnection = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:reachabilityObserver object:nil];
        }
    }
    
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    reachiblityAlertView = nil;
}


@end
