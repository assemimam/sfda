//
//  NewsViewCustomCell.h
//  SFDA
//
//  Created by yehia on 9/14/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsViewCustomCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *aiLoading;

@property (weak, nonatomic) IBOutlet UILabel *lblNewsDate;
@property (weak, nonatomic) IBOutlet UILabel *lblNewsTitle;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail;
@property (weak, nonatomic) IBOutlet UIImageView *iconType;




@end
