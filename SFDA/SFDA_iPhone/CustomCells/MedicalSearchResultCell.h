//
//  DrugSearchResultCell.h
//  SFDA
//
//  Created by Assem Imam on 10/26/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedicalSearchResultCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *mdmaListingName;
@property (weak, nonatomic) IBOutlet UILabel *medicalCommercialName;
@property (weak, nonatomic) IBOutlet UILabel *medicalManufacturerNumber;
@property (weak, nonatomic) IBOutlet UILabel *medicalSerialNumber;

@end
