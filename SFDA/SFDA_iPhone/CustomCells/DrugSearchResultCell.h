//
//  DrugSearchResultCell.h
//  SFDA
//
//  Created by Assem Imam on 10/26/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DrugSearchResultCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *drugScientificName;
@property (weak, nonatomic) IBOutlet UILabel *drugCommercialName;
@property (weak, nonatomic) IBOutlet UILabel *drugSerialNumber;

@end
