//
//  ImportanLinksCustomCell.m
//  SFDA
//
//  Created by yehia on 9/29/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "ImportanLinksCustomCell.h"

@implementation ImportanLinksCustomCell
@synthesize lblTitle;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
