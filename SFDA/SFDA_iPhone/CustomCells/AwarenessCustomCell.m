//
//  AwarenessCustomCell.m
//  SFDA
//
//  Created by yehia on 9/27/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "AwarenessCustomCell.h"

@implementation AwarenessCustomCell
@synthesize lblTitle;
@synthesize image_View;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
