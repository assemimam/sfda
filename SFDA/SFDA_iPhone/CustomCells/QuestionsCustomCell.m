//
//  QuestionsCustomCell.m
//  SFDA
//
//  Created by yehia on 9/14/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "QuestionsCustomCell.h"

@implementation QuestionsCustomCell
@synthesize answerTextView;
@synthesize questionTextView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
    
//    self.questionTextView.preferredMaxLayoutWidth = CGRectGetWidth(self.questionTextView.frame);
//    self.answerTextView.preferredMaxLayoutWidth = CGRectGetWidth(self.answerTextView.frame);

    
}
@end
