//
//  QuestionCell.h
//  SFDA
//
//  Created by Assem Imam on 5/15/14.
//  Copyright (c) 2014 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *questionTextView;
@property (weak, nonatomic) IBOutlet UIImageView *QuestionImageView;

@end
