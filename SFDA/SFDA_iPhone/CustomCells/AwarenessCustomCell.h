//
//  AwarenessCustomCell.h
//  SFDA
//
//  Created by yehia on 9/27/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AwarenessCustomCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *image_View;

@end
