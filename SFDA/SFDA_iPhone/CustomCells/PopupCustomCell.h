//
//  PopupCustomCell.h
//  SFDA
//
//  Created by yehia on 9/27/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopupCustomCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *OptionTitle;

@end
