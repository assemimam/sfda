//
//  contactUsCell.h
//  SFDA
//
//  Created by Assem Imam on 10/29/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface contactUsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *contactName;
@property (weak, nonatomic) IBOutlet UIImageView *contactImage;

@end
