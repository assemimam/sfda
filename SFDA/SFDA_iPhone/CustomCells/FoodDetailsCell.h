//
//  FoodDetailsCell.h
//  SFDA
//
//  Created by Assem Imam on 5/7/14.
//  Copyright (c) 2014 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodDetailsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *TitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *ValueLabel;

@end
