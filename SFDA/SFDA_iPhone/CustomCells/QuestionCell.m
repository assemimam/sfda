//
//  QuestionCell.m
//  SFDA
//
//  Created by Assem Imam on 5/15/14.
//  Copyright (c) 2014 NUITEX. All rights reserved.
//

#import "QuestionCell.h"

@implementation QuestionCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
