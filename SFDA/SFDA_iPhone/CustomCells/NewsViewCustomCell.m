//
//  NewsViewCustomCell.m
//  SFDA
//
//  Created by yehia on 9/14/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "NewsViewCustomCell.h"

@implementation NewsViewCustomCell
@synthesize  aiLoading;
@synthesize iconType;
@synthesize lblNewsTitle;
@synthesize lblNewsDate;
@synthesize thumbnail;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
