//
//  NewsSclidingView.m
//  SFDA
//
//  Created by Assem Imam on 10/27/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "NewsSclidingView.h"

@implementation NewsSclidingView
@synthesize  newsDate;
@synthesize newsTitle;
@synthesize newsImage;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
