//
//  NewsSclidingView.h
//  SFDA
//
//  Created by Assem Imam on 10/27/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsSclidingView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *newsImage;
@property (weak, nonatomic) IBOutlet UITextView *newsTitle;
@property (weak, nonatomic) IBOutlet UILabel *newsDate;

@end
