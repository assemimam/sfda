//
//  PopUpView.m
//  SFDA
//
//  Created by Assem Imam on 9/3/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import "PopUpView.h"
#import "PopupCustomCell.h"
#import "Language.h"

@implementation PopUpView
@synthesize delegate;
@synthesize searchBar;
@synthesize tvOptions;


#pragma  mark tableview datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PopupCustomCell *cell =(PopupCustomCell*) [tableView dequeueReusableCellWithIdentifier:@"cleOption"];
    if (cell==nil) {
        cell =(PopupCustomCell*) [[[NSBundle mainBundle]loadNibNamed:[[Language getObject]getStringWithKey:@"PopupCustomCell"] owner:nil options:nil]objectAtIndex:0];
        
    }
    
    cell.imgIcon.image = [UIImage imageNamed:[[optionsList objectAtIndex:indexPath.row] objectForKey:@"image"]];
    cell.OptionTitle.text = [[optionsList objectAtIndex:indexPath.row] objectForKey:@"option"];
    cell.OptionTitle.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
    [cell setBackgroundColor:[UIColor clearColor]];
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == self.tvOptions){
        return  [optionsList count];
    }else{
        return [searchResult count];
    }
    
}
#pragma  mark tableview delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.delegate) {
        [self.delegate didSelectItemAtIndex:indexPath.row];
    }
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    
    return self;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}
-(void)updateData:(NSMutableArray*) options{
    optionsList = [NSArray arrayWithArray:options];
}
-(void)reloadData{
    NSLog(@"%d",[optionsList count]);
    [self.tvOptions reloadData];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    if(optionsList == nil || [optionsList count]<=0){
        optionsList = [[NSArray alloc]init];
        optionsList = [[Language getObject]getArrayWithKey:@"PopUpMenuOptions"];
        [self.tvOptions reloadData];
    }
}

@end
