//
//  PopUpView.h
//  SFDA
//
//  Created by Assem Imam on 9/3/13.
//  Copyright (c) 2013 NUITEX. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol popUpProtocol
-(void)didSelectItemAtIndex:(int)index;
@end
@interface PopUpView : UIView<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>

{
  
    NSArray *optionsList;
    NSArray *searchResult;
}
@property (assign) id<popUpProtocol> delegate;
@property (nonatomic, strong) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tvOptions;

-(void)reloadData;
-(void)updateData:(NSMutableArray*) options;
@end
